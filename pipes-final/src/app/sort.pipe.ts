import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: [], propName: string): any {
    return value.sort((n1: any, n2: any) => {return n1['name'] > n2['name'] ? 1 : n1['name'] < n2['name'] ? -1 : 0; } );
  }

}
