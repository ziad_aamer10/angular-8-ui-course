import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-choose-person-ssn',
  templateUrl: './choose-person-ssn.component.html',
  styleUrls: ['./choose-person-ssn.component.css']
})
export class ChoosePersonSSNComponent implements OnInit {
persons:any[]=[]
chosenPerson=null
  constructor(public dialogRef: MatDialogRef<ChoosePersonSSNComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      
     this.persons= data.persons;
      
     }

  ngOnInit() {
  }
save(){
  if(this.chosenPerson)
  this.dialogRef.close(this.chosenPerson)
  else{
    alert("choose any party Number before close")
  }
}
close(){

  this.dialogRef.close()
}
}
