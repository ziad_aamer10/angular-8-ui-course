import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonsService {
  persons: any = [];
  constructor(private http: HttpClient) { }
  addPerson(person) {
    this.persons.push(person);
  }
  getSPersonBySSN(SSN) {
    const url = environment.projectUrl + `/sPersonsSSN/${SSN}`;
    return this.http.get(url);
  }
  saveSPerson(person) {
    
    const url = `${environment.projectUrl}/sPersons`;
    return this.http.put(url, person);
  }
  updateSPerson(person) {
    const url = `${environment.projectUrl}/sPersons`;

    return this.http.put(url, person);
  }
  getSPerson(partyNumber) {
    const url = `${environment.projectUrl}/tPersonsMap/${partyNumber}`;
    return this.http.get(url);
  }
  getDirectorByPartyNumber(partyNumber, entityNumber) {
    if(entityNumber){
      const url = `${environment.projectUrl}/sDirectorIds/partyNumber?partyNumber=${partyNumber}&entityNumber=${entityNumber}`;
      return this.http.get(url);
    }
    else{
      const url = `${environment.projectUrl}/sDirectorIds/partyNumber?partyNumber=${partyNumber}`;
      return this.http.get(url);
    }
  
  }
  SaveDirectorToSource(director) {
    const url = `${environment.projectUrl}/sDirectorIds`;
    return this.http.put(url, director);
  }
}
