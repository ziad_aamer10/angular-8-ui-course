import { Component, OnInit, Inject } from '@angular/core';
import { DialogData } from '../component/dialog-overview-example/dialog-overview-example.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-logout-alert',
  templateUrl: './logout-alert.component.html',
  styleUrls: ['./logout-alert.component.css']
})
export class LogoutAlertComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<LogoutAlertComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ngOnInit() {
  }
logout(){
  this.dialogRef.close({logout:true})
}
cancel(){
  this.dialogRef.close()

}
}
