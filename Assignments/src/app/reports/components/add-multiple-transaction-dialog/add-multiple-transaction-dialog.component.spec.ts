import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMultipleTransactionDialogComponent } from './add-multiple-transaction-dialog.component';

describe('AddMultipleTransactionDialogComponent', () => {
  let component: AddMultipleTransactionDialogComponent;
  let fixture: ComponentFixture<AddMultipleTransactionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMultipleTransactionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMultipleTransactionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
