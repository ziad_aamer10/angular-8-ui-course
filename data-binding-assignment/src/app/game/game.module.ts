import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameControlComponent } from './components/game-control/game-control.component';
import { OddComponent } from './components/odd/odd.component';
import { EvenComponent } from './components/even/even.component';

@NgModule({
  declarations: [GameControlComponent, OddComponent, EvenComponent],
  imports: [
    CommonModule
  ],
  exports: [GameControlComponent, OddComponent, EvenComponent]
})
export class GameModule { }
