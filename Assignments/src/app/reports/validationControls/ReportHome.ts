import { FormBuilder, FormControl, Validators, FormGroup, FormArray } from '@angular/forms';

const fb = new FormBuilder();
export class ReportHome {


    ReportHomeForm(): FormGroup {

        return fb.group({
            'reportCreatedDate': new FormControl(''),
            'reportUserLockId': new FormControl(''),
            'reportStatusCode': new FormControl(''),
            'submissionDate': new FormControl(''),
            'id': new FormControl(''),
            'reportCode': new FormControl(''),
            'reportCreatedBy': new FormControl('')



        });
    }
}
