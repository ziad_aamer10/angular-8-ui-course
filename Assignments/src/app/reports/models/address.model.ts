export interface address {
    Address_type?:string;
    Address?:string;
    Town?:string;
    City?:string;
    Zip?:string;
    country_code?:string;
    State?:string;
    comments?:string;
}