import { UprojectModule } from './uproject/uproject.module';
import { NgModule} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { JwtModule, JwtInterceptor } from '@auth0/angular-jwt';
import { ToastrModule } from 'ngx-toastr';
import { AuthService } from './loginModule/services/auth-service.service';
import { AuthGuardService } from './loginModule/services/auth-guard.service';
import { LoaderComponent } from './component/loader/loader.component';
import { LoaderService } from './services/loader.service';
import { LoaderInterceptor } from './services/loader.interceptor';
import { SharedModule } from './shared/shared.module';
import { environment } from 'src/environments/environment';
import { ReportingPersonService } from './services/reporting-person.service';
import {  HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WelcomeComponent } from './loginModule/components/welcome/welcome.component';
import { DialogOverviewExampleDialog, DialogOverviewExampleComponent } from './component/dialog-overview-example/dialog-overview-example.component';
import { LogoutAlertComponent } from './logout-alert/logout-alert.component';
import { AlertModule } from './alert-module/alert-module.module';


export function tokenGetter() {
  return localStorage.getItem('token');
}
@NgModule({
  declarations: [
    AppComponent,
    DialogOverviewExampleComponent,
    DialogOverviewExampleDialog,
    WelcomeComponent ,LogoutAlertComponent ],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    BrowserModule,
    JwtModule.forRoot({
      config: {
        whitelistedDomains: [environment.projectDomain],
        blacklistedRoutes: [`${environment.projectDomain}/aml/auth`],
        tokenGetter: tokenGetter
      }
    }),
    ToastrModule.forRoot({ closeButton: true, timeOut: 5000 }),
    SharedModule,
    AlertModule,
    UprojectModule
  ],

  entryComponents: [DialogOverviewExampleDialog,LogoutAlertComponent],

  providers: [AuthGuardService, AuthService, LoaderService,
     { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }, ReportingPersonService],

  bootstrap: [AppComponent],
  exports: [ LoaderComponent, WelcomeComponent, DialogOverviewExampleComponent, DialogOverviewExampleDialog,LogoutAlertComponent]
})
export class AppModule { }
