import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SarReportActivityDetailsComponent } from './sar-report-activity-details.component';

describe('SarReportActivityDetailsComponent', () => {
  let component: SarReportActivityDetailsComponent;
  let fixture: ComponentFixture<SarReportActivityDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SarReportActivityDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SarReportActivityDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
