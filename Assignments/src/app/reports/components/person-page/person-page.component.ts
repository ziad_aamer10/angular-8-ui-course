import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import { PersonsService } from '../../services/persons.service';
import { UtilService } from 'src/app/util.service';
import { LookupClass } from '../../lockups/Lookups';
import { ToastrService } from 'ngx-toastr';
import { t_person } from '../../validationControls/t_person';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { MatDialog, MatCheckbox } from '@angular/material';
import { ChoosePersonSSNComponent } from '../choose-person-ssn/choose-person-ssn.component';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';
import { Actions } from '../../Actions/action';


@Component({
  selector: 'app-person-page',
  templateUrl: './person-page.component.html',
  styleUrls: ['./person-page.component.css']
})
export class PersonPageComponent implements OnInit, OnChanges {
  @Input() conductor: boolean ;
  @Input() disablePerson: boolean
  @Input() isSignatory?: boolean
  @Input() isDirector?: boolean
  @Input() closeAccordion?: boolean
  @Input() isReportingPerson?: boolean
  @Output() changePerson = new EventEmitter()
  @Output() saveEvent = new EventEmitter();
  myDate = new Date();
  SearchBy: string[] = ['PartyNumber', 'CustomerSSN']
  filterBy: string = 'PartyNumber'
  t_person: t_person = new t_person()

  // added by abdelrahman to assign values from lookups
  gender_values = LookupClass.gender_type;
  country_values = LookupClass.country;
  party_role_type = LookupClass.party_role_type;
  entity_person_role_type = LookupClass.entity_person_role_type;
  phones: FormArray;
  addressesControl: FormArray
  @Input() personType: string;
  @Input() personForm: FormGroup
  CustomerNumberPlaceHolder: string = 'Party Number'
  cascadePersonAction: boolean = false;
  constructor(private fb: FormBuilder, public personservice: PersonsService, private utilsevice: UtilService
    , private toatser: ToastrService, private dataService: SharingDataServieService,
    public dialog: MatDialog, private auth: AuthService) { }


  ngOnInit() {
    console.log("conductor ====" ,this.conductor);
    
    // if(this.isReportingPerson){
    //   console.log("cccccccccccc");
      
    //   this.personForm.get('email').setValidators(Validators.required);
    // }

    if (this.isDirector) {
      this.CustomerNumberPlaceHolder = 'Director Number'
    }
    else if (this.isSignatory) {
      this.CustomerNumberPlaceHolder = 'Signatory Number'
    }

    this.setControls()
    this.setActions()



  }
  setControls() {
    this.changePassportNumber()
    this.phones = this.personForm.get("phones").get("phone") as FormArray
    this.addressesControl = this.personForm.get("addresses").get("address") as FormArray
    this.deceasedChanges()

  }

  get f() {
    return this.personForm.controls;
  }
  f1(value) {

    if (value == 'No') {
      this.personForm.controls['passportNumber'].clearValidators()
      this.personForm.controls['passportCountry'].clearValidators()
    }
    this.myDate.getDate()


  }
  changecloseAccordion() {


    if (this.closeAccordion) {
      this.closeAccordion = true
    }
  }
  getPersonInfo() {
    let partyNumber = this.personForm.get('partyNumber').value



    if (partyNumber) {
      this.personservice.getSPerson(partyNumber).subscribe((data: any) => {
        if (data) {
          console.log("person coming from source == ", data);
          
          this.personForm = this.t_person.setPersonControl(data)
          this.changePerson.emit(this.personForm)
          this.setControls()
          this.toatser.success("Person with partyNumber-->" + partyNumber + " is found")
        }
        else {
          this.toatser.error("No Person with Id " + partyNumber)
        }

      }
        , error => {

          this.toatser.error(error.message)

        })
    }
  }
  getCurrentUser() {
    let userName = localStorage.getItem('name')
    return userName
  }
  savePerson() {

    this.personForm.get('updatedBy').setValue(this.getCurrentUser())
    let personValueToSave = Object.assign({}, this.personForm.value)
    this.utilsevice.changeDateFormat(personValueToSave)
    console.log("personValueToSave=== ", personValueToSave);
    
    this.saveEvent.emit(personValueToSave);
    if (this.personForm.valid)
      this.personservice.saveSPerson(personValueToSave).subscribe(data => {
        if (data) {
          this.toatser.success("Person Saved successfully")
        }

      }, error => {
        this.toatser.error(error.error.message || error.message)

      })
    else {
      this.utilsevice.validateFormGroup2(this.personForm, [], [])
      this.toatser.error("Person not valid!!")
    }
  }
  updatePerson() {

    this.personservice.updateSPerson(this.personForm.value).subscribe(data => {


      if (data) {
        this.toatser.success("Person Saved successfully")
      }

    }, error => {
      this.toatser.error(error)

    })
  }
  validate() {

    this.utilsevice.validateFormGroup(this.personForm, [])
  }
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let chng = changes[propName];
      let cur = chng.currentValue;
      let prev = chng.previousValue;
      if (propName == 'personForm') {
        this.personForm = cur
        this.setControls()
      }
    }
  }
  getPersonInfoBySSN(SSN: HTMLInputElement) {
    let ssn = SSN.value
    if (ssn) {
      this.personservice.getSPersonBySSN(ssn).subscribe((data: any[]) => {
        if (data.length > 0) {
          const dialogRef = this.dialog.open(ChoosePersonSSNComponent, { data: { 'persons': data } })
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              
              this.personForm = this.t_person.setPersonControl(result)
            }
            else {
              this.personForm = this.t_person.setPersonControl(data[0])
            }
            this.changePerson.emit(this.personForm)
            this.setControls()
          }

          )

          this.toatser.success("Person with ssn-->" + ssn + " is found")
        }
        else {
          this.toatser.error("No Person with Id " + ssn)
        }
      }
        , error => {

          this.toatser.error(error.message)

        })
    }

  }
  setActions() {
    this.cascadePersonAction = this.auth.has_Capabilities(Actions.cascadePerson)
  }
  changePassportNumber() {
    let passportNumber = this.personForm.get('passportNumber')
    let passportCountry = this.personForm.get('passportCountry')
    if (passportNumber.value && passportNumber.value != '') {

      passportNumber.setValidators([Validators.required])
      passportCountry.setValidators([Validators.required])

    }

    else {
      passportCountry.setValue(null)
      passportNumber.clearValidators()
      passportCountry.clearValidators()

    }

    passportNumber.updateValueAndValidity()
    passportCountry.updateValueAndValidity()
  }
  deceasedChanges() {
    this.personForm.get('deceased').valueChanges.subscribe(data => {

      if (data) {
        this.personForm.get('dateDeceased').setValidators(Validators.required)
      } else {
        this.personForm.get('dateDeceased').clearValidators()
      }
      this.personForm.get('dateDeceased').updateValueAndValidity()

    })
  }
  falseValue = null
  trueValue = true;
  
  checkboxChange(checkbox: MatCheckbox, checked: boolean) {
    console.log('checkboxChange');
    if( !checked ){
      this.personForm.controls.deceased.setValue(null)
    }
  }
}