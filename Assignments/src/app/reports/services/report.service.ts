import { LastTop10Basic, ReportTypeStatistics, ReportStatusStatistics } from './../../dashboard/dashboard/dashboard.component';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
// import { objectEach, syncTimeout } from 'highcharts';
import {  Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FormGroup } from '@angular/forms';
import { ReportPage, Report } from '../components/report-home/report-home.component';
import { UtilService } from 'src/app/util.service';
import { resolve } from 'path';


@Injectable({
  providedIn: 'root'
})
export class ReportService {
  makeAIFRequest(StrReport) {
  this.processIndicators(StrReport)
   let url=`${environment.projectUrl}/AIF-request`
   return this.http.post(url,StrReport)
  }

  globalUrl = environment.projectUrl ; 


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',Authorization:'Bearer '+localStorage.getItem('token')
    })
  };
  

  constructor(private http: HttpClient, private util: UtilService) { }
  getReportList() {
    const url = environment.projectUrl + '/reports/basic';
    this.http.get(url).subscribe((dat: Report[]) => {
      

    });
    return this.http.get(url);

  }

  getReportList1(searchFrom: FormGroup, sortColumn: string, order: string,
    pageIndex: number, pageSize: number): Observable<ReportPage> {
    

    return this.http.get<ReportPage>(environment.projectUrl + '/reports/basic1?pageIndex=' + pageIndex
      + '&pageSize=' + pageSize
      + '&sortColumn=' + sortColumn
      + '&order=' + order
      , this.httpOptions)
  }

  getReportListBySearch(searchForm: FormGroup,sortColumn: string, order: string,
    pageIndex: number, pageSize: number): Observable<ReportPage>  {
    const url = environment.projectUrl + '/reports/basic/filtersPagination?pageIndex=' + pageIndex
    + '&pageSize=' + pageSize
    + '&sortColumn=' + sortColumn
    + '&order=' + order
    return this.http.post<ReportPage>(url, searchForm)
  }

  getReportListBySearch1(searchForm) {
    const url = environment.projectUrl + '/reports/basic/filtersPagination';
    return this.http.post(url, searchForm)
  }

  getReportById(id) {

    const url = environment.projectUrl + `/reports/${id}`;
    return this.http.get(url);

  }
  getReportsByCustomerId(CustomerNumber) {

    const url = environment.projectUrl + `/reports/${CustomerNumber}`;
    return this.http.get(url);

  }
  saveSarReport(report) {
    

    const newReport = JSON.parse(JSON.stringify(report));

    

    this.processIndicators(newReport);
    this.util.changeDateFormat(newReport)

    const url = environment.projectUrl + '/reports';
    return this.http.post(url, newReport);
  }
  processIndicators(newReport) {
    

    let ind = null
    let indicators: string[] = [];
    if (newReport && newReport.reportIndicators && newReport.reportIndicators.indicator) {
      ind = newReport.reportIndicators.indicator

      indicators = [ind.CIU, ind.CT, ...ind.DM, ...ind.PC, ...ind.SP, ind.ST, ind.CC];
      indicators = indicators.filter(a => a.length > 0);
      newReport.reportIndicators.indicator = indicators
    }

  }
  updateSarReport(report) {
    const newReport = JSON.parse(JSON.stringify(report));

    

    this.processIndicators(newReport);
    this.util.changeDateFormat(newReport)
    const url = environment.projectUrl + '/reports';
    return this.http.put(url, newReport);
  }
  GenerateXML(id) {

    const url = environment.projectUrl + `/generateReportXML/${id}`;
    return this.http.get(url);

  }

  PreviewReport(id) {
    
    const url = environment.projectUrl + `/PreviewReport/${id}`
    const httpOptions = {
      'responseType'  : 'arraybuffer' as 'json'
       //'responseType'  : 'blob' as 'json'        //This also worked
    };
    return this.http.get(url,httpOptions)
    // .pipe(map((res:any) => {
    // return new Blob([res], { type: 'application/pdf', });
// })
// );
   // return  this.http.get(url)
    // .map(
    //   (res) => {
    //       return new Blob([res.blob()], { type: 'application/pdf' })
    //   }
    //  this.http.get(url,  { responseType: ResponseContentType.Blob });

  }
  GenerateXMLAndSaveReport(report) {

    const newReport = JSON.parse(JSON.stringify(report));

    

    this.processIndicators(newReport);
    const url = environment.projectUrl + `/generateANDSaveReportXML`;
    this.util.changeDateFormat(newReport)

    return this.http.post(url, newReport);

  }

  getDateFields() {
    return ["submissionDate", "reportCreatedDate", "reportClosedDate", "dateDeceased",
      "birthdate", "dateBusinessClosed", "dateBalance", "changeBeginDate", "dateTransaction",
      "datePosting",
      "valueDate", "registration_date", "opened", "closed", "dateBalance",
      "incorporationDate", "dateBusinessClosed",
      "issueDate",
      "expiryDate"
    ]



  }
  SendToChecker(report) {
    let url = `${environment.projectUrl}/sendToChecker`
    const newReport = JSON.parse(JSON.stringify(report));
    this.processIndicators(newReport);
    return this.http.put(url, newReport)
  }
  ReturnToMaker(report) {
    let url = `${environment.projectUrl}/returnToMaker`
    const newReport = JSON.parse(JSON.stringify(report));
    this.processIndicators(newReport);
    return this.http.put(url, newReport)
  }
  reOpenReport(reportId) {
    let url = `${environment.projectUrl}/reOpenReport/${reportId}`
    return this.http.get(url)
  }
  MLCUAcknowledge(reportId) {
    let url = `${environment.projectUrl}/MLCUModification/${reportId}`
    return this.http.get(url)
  }
  ReturnToChecker(report: any) {
    let url = `${environment.projectUrl}/returnToChecker`
    const newReport = JSON.parse(JSON.stringify(report));
    this.processIndicators(newReport);
    return this.http.put(url, newReport)
  }
  sendToAMLHead(report) {
    let url = `${environment.projectUrl}/sendToAMLHead`
    const newReport = JSON.parse(JSON.stringify(report));
    this.processIndicators(newReport);
    return this.http.put(url, newReport)
  }
  submitReport(reportId) {
    let url = `${environment.projectUrl}/submitReport/${reportId}`
    return this.http.get(url)
  }
  closeReport(report) {
    let url = `${environment.projectUrl}/closeReport`
    const newReport = JSON.parse(JSON.stringify(report));
    this.processIndicators(newReport);
    return this.http.put(url, newReport);
  }
  routeReport(reportId, currentUser, Reciever) {
    
    let url = `${environment.projectUrl}/route?reportId=${reportId}&sender=${currentUser}&receiver=${Reciever}`
    let headers = new HttpHeaders()

    return this.http.get(url, { responseType: 'text' });
  }


  getReportHistory(reportId) {
    const url = environment.projectUrl+'/reports/'+reportId+'/history';
    return this.http.get(url)

  }
  lock(id,userName){
    
    const url=environment.projectUrl + `/lock/${id}/${userName}`;
    return this.http.put(url, null);
  }

  checkLocked(id,userName){
    const url=environment.projectUrl + `/lock/${id}/${userName}`;
    return this.http.get(url);}

    unLock(id,userName){
    
      const url=environment.projectUrl + `/unlock/${id}/${userName}`;
      return this.http.put(url, null);
    }

    autoLock(id,userName){
    
      const url=environment.projectUrl + `/autolock/${id}/${userName}`;
      return this.http.put(url, null);
    }

    sourceAudit(fromDate,toDate){
      const url=environment.projectUrl +
       '/sourceAudits/generate-excel-date-filter?fromDate='+fromDate+'&toDate='+toDate
      return this.http.get(url,{ responseType: 'text' });  
    }

    

    // getusergroup(username):Promise<Object>{
    //   const url=environment.projectUrl + `/getusergroup/${username}`;
    //   return this.http.get(url)
    //   .toPromise();
    // }

    getReportTypeStatistics():Observable<ReportTypeStatistics>{
      const url = this.globalUrl+'/reports/statistics/types';
      return this.http.get<ReportTypeStatistics>(url);
    }

    getReportStatusStatistics():Observable<ReportStatusStatistics>{
      const url = this.globalUrl+'/reports/statistics/status-types';
      return this.http.get<ReportStatusStatistics>(url);
    }

    getLastTop10():Observable<LastTop10Basic[]>{
      const url = this.globalUrl+'/reports/last-inserted/top10';
      return this.http.get<LastTop10Basic[]>(url);
    }


    getLastTop10ByLockedUser(userLockedId):Observable<LastTop10Basic[]>{
      const url = this.globalUrl+'/reports/last-inserted/top10/'+userLockedId;
      return this.http.get<LastTop10Basic[]>(url);
    }

    getGroupMember(){
      const url = `${environment.projectUrl}/getgroupMembers`;
      return this.http.get(url);
    }
    getusergroup(username){
      const url=environment.projectUrl + `/getusergroup/${username}`;
      return this.http.get(url); 
    }
}


 

  


