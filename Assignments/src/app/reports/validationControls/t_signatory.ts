import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { t_person } from "./t_person";
import { t_person_my_client } from "./t_person_my_client";
import { UtilService } from "src/app/util.service";

var fb: FormBuilder = new FormBuilder();
export class t_signatory {
    tperson: t_person = new t_person()
    tpersonMyClient: t_person_my_client = new t_person_my_client()
    util=new UtilService()
    constructor() {

    }
    create_signatoryControl(): FormGroup {
        return fb.group({
            id: new FormControl(),
            isPrimary: new FormControl(),
            role: new FormControl(null, Validators.required),
            tPerson: this.tperson.createPersonControl(),
            roleDesc: new FormControl()


        })
    }
    set_signatoryControl(signatory): FormGroup {



        return fb.group({
            id: new FormControl(signatory.id),
            isPrimary: signatory.isPrimary==true?[signatory.isPrimary]:new FormControl(),
            role:this.util.checkLockUpValue('SignatoryRole',signatory.role)? [signatory.role, Validators.required]:new FormControl(null,Validators.required),
            tPerson: this.tperson.setPersonControl(signatory.tPerson),
            roleDesc: [signatory.roleDesc]

        })
    }
    create_MysignatoryControl(): FormGroup {
        let Control = this.create_signatoryControl()
        this.addSignatoryValidator(Control)

        return Control
    }
    set_MysignatoryControl(signatory): FormGroup {
        let Control = this.set_signatoryControl(signatory)
        this.addSignatoryValidator(Control)
        return Control
    }
    addSignatoryValidator(signatory: FormGroup) {
        let tPerson = signatory.get('tPerson') as FormGroup
        this.tpersonMyClient.addMyPersonValidators(tPerson)
    }
}