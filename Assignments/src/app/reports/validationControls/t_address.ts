import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { UtilService } from "src/app/util.service";

export class t_address {
  util=new UtilService()
    constructor(public fb: FormBuilder) {

    }
    createAddress(): FormGroup {
        return this.fb.group({
          id:new FormControl(),
          addressType:[null,Validators.required],
          address:[null,[Validators.required,Validators.maxLength(100)]],
          city:[null,[Validators.required,Validators.maxLength(255)]],
          zip:[null,Validators.maxLength(10)],
          countryCode:[null,Validators.required],
          state:[null,[Validators.required,Validators.maxLength(255)]],
          addressComments:[null,Validators.maxLength(4000)]
        })
      }
      setAddress(address){
        if(address)
        return this.fb.group({
        id:new FormControl(address.id),
        addressType:this.util.checkLockUpValue('addressType',address.addressType)?new FormControl(address.addressType,Validators.required):new FormControl(null,Validators.required),
        address:new FormControl(address.address,[Validators.required,Validators.maxLength(100)]),
        city:new FormControl(address.city,[Validators.required,Validators.maxLength(255)]),
        zip:new FormControl(address.zip,Validators.maxLength(10)),
        countryCode:this.util.checkLockUpValue('nationality',address.countryCode)?new FormControl(address.countryCode,Validators.required):new FormControl(null,Validators.required),
        state:new FormControl(address.state,[Validators.required,Validators.maxLength(255)]),
        addressComments:    new FormControl(address.addressComments,[Validators.maxLength(4000)])
        }) 
        else
        return this.createAddress()
      }
}