import { FormGroup, Validators, FormBuilder, AbstractControl, FormControl, FormArray } from '@angular/forms';
import { t_phone } from './t_phone';
import { t_address } from './t_address';
import { t_id } from './t_id';
import { t_email } from './t_email';
import { from } from 'rxjs';
import { UtilService } from 'src/app/util.service';
const fb = new FormBuilder();
export class t_person {
    t_phone: t_phone = new t_phone(fb);
    t_address: t_address = new t_address(fb);
    t_id: t_id = new t_id(fb);
    t_email: t_email = new t_email(fb);
    util:UtilService=new UtilService()
    constructor() {

    }
    createPhones(): FormGroup {
        return fb.group({
            phone: fb.array([])
        });
    }
    setPhones(phones): FormGroup {
        const x: FormArray = fb.array([]);

        const y = fb.group({
            name: new FormControl(),
            location: fb.array([]),
            phone: fb.group({

            })

        });

        if (phones) {
            for (const i of phones) {
                const u = this.t_phone.setPhoneGroup(i);

                x.push(u);

            }
        }
        return fb.group({
            phone: x
        });
    }
    createAddresses(): FormGroup {

        return fb.group({
            address: fb.array([])
        });
    }
    setAddresses(addresses): FormGroup {
        const x: FormArray = fb.array([]);
        if (addresses) {
            for (const i of addresses) {
                const u = this.t_address.setAddress(i);
                x.push(u);

            }
        }
        return fb.group({
            address: x
        });
    }
    setEmails(emails) {
        const emailsarray = fb.array([]);
        if (emails) {
            for (const i of emails) {
                const email = this.t_email.setEmail(i);
                emailsarray.push(email);

            }
        }
        return emailsarray;
    }
    setIdentifications(ids) {
        const x: FormArray = fb.array([]);
        if (ids) {


            for (const i of ids) {
                const u = this.t_id.setId(i);
                x.push(u);

            }
        } return x;
    }
    createPersonControl(): FormGroup {
        return fb.group({
            id: new FormControl(),
            partyNumber: new FormControl(),
            gender: new FormControl(),
            title: new FormControl(null, [Validators.maxLength(30)]),
            firstName: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
            middleName: new FormControl(null, [Validators.maxLength(100)]),
            prefix: new FormControl(null, [Validators.maxLength(100)]),
            lastName: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
            birthdate: new FormControl(),
            birthPlace: new FormControl(null, [Validators.maxLength(255)]),
            mothersName: new FormControl(null, [Validators.maxLength(100)]),
            alias: new FormControl(null, [Validators.maxLength(100)]),
            ssn: new FormControl(null, [Validators.maxLength(25)]),
            passportNumber: new FormControl(null, [ Validators.maxLength(25)]),
            passportCountry: new FormControl(null, [ Validators.maxLength(25)]),
            idNumber: new FormControl(null, [Validators.maxLength(25)]),
            nationality2: new FormControl(),
            nationality3: new FormControl(),
            nationality1: new FormControl(),
            residence: new FormControl(),
            phones: this.createPhones(),
            addresses: this.createAddresses(),
            email: fb.array([]),
            occupation: new FormControl(null, [Validators.maxLength(255)]),
            employerName: new FormControl(null, [Validators.maxLength(255)]),
            employerAddressId: new FormControl(),
            employerPhoneId: new FormControl(),
            identification: fb.array([]),
            deceased: new FormControl(),
            dateDeceased: new FormControl(),
            taxNumber: new FormControl(null, [Validators.maxLength(100)]),
            taxRegNumber: new FormControl(null, [Validators.maxLength(100)]),
            sourceOfWealth: new FormControl(null, [Validators.maxLength(255)]),
            comments: new FormControl(null, [Validators.maxLength(4000)]),
            changeBeginDate: new FormControl(null, [Validators.maxLength(4000)]),
            role: new FormControl(),
            isReviewed: new FormControl(false),
            updatedBy:[null, [Validators.maxLength(4000)]]
            
        });

    }
    setPersonControl(person): FormGroup {
        
let personGroup:FormGroup


        if(person){
            personGroup= fb.group({
                id: new FormControl(person.id),
                type: ['person', []],
                partyNumber: new FormControl(person.partyNumber),
                // gender:new FormControl(person.gender),
                gender: this.util.checkLockUpValue('gender',person.gender)?new FormControl(person.gender):new FormControl(),
                title: new FormControl(person.title, [Validators.maxLength(30)]),
                firstName: new FormControl(person.firstName, [Validators.required, Validators.maxLength(100)]),
                middleName: new FormControl(person.middleName, [Validators.maxLength(100)]),
                prefix: new FormControl(person.prefix, [Validators.maxLength(100)]),
                lastName: new FormControl(person.lastName, [Validators.required, Validators.maxLength(100)]),
                birthdate: new FormControl(this.util.convertDateFromServer(person.birthdate)),
                birthPlace: new FormControl(person.birthPlace, [Validators.maxLength(255)]),
                mothersName: new FormControl(person.mothersName, [Validators.maxLength(100)]),
                alias: new FormControl(person.alias, [Validators.maxLength(100)]),
                ssn: new FormControl(person.ssn, [Validators.maxLength(25)]),
                passportNumber: new FormControl(person.passportNumber, [ Validators.maxLength(25)]),
                passportCountry: new FormControl(person.passportCountry, [ Validators.maxLength(25)]),
                idNumber: new FormControl(person.idNumber, [Validators.maxLength(25)]),
                nationality2: this.util.checkLockUpValue('nationality',person.nationality2)?new FormControl(person.nationality2):new FormControl(),
                nationality3: this.util.checkLockUpValue('nationality',person.nationality3)?new FormControl(person.nationality3):new FormControl(),
                nationality1: this.util.checkLockUpValue('nationality',person.nationality1)?new FormControl(person.nationality1):new FormControl(),
                residence: this.util.checkLockUpValue('residence',person.residence)?new FormControl(person.residence):new FormControl(),
                occupation: new FormControl(person.occupation, [Validators.maxLength(255)]),
                employerName: new FormControl(person.employerName, [Validators.maxLength(255)]),
                employerAddressId: new FormControl(person.employerAddressId),
                employerPhoneId: new FormControl(person.employerAddressId),
                deceased: new FormControl(person.deceased),
                dateDeceased: new FormControl(this.util.convertDateFromServer(person.dateDeceased)),
                taxNumber: new FormControl(person.taxNumber, [Validators.maxLength(100)]),
                taxRegNumber: new FormControl(person.taxRegNumber, [Validators.maxLength(100)]),
                sourceOfWealth: new FormControl(person.sourceOfWealth, [Validators.maxLength(255)]),
                comments: new FormControl(person.comments, [Validators.maxLength(4000)]),
                phones: person.phones.phone?this.setPhones(person.phones.phone):this.createPhones(),
                addresses: person.addresses.address?this.setAddresses(person.addresses.address):this.createAddresses(),
                email: person.email?this.setEmails(person.email):fb.array([]),
                identification: this.setIdentifications(person.identification),
                role: new FormControl(person.role),
                isReviewed: new FormControl(person.isReviewed),
                changeBeginDate: new FormControl(person.changeBeginDate),
                updatedBy: new FormControl(person.updatedBy),
            });
        } else {
          personGroup=  this.createPersonControl();
        }

this.changePassportNumber(personGroup)
this.changeDeceased(personGroup)
return personGroup
    }
    changeDeceased(personForm:FormGroup){
        let deceased=personForm.get('deceased')
        let dateDeceased=personForm.get('dateDeceased') 
        if(deceased.value){
            dateDeceased.setValidators(Validators.required)
        }else{
            dateDeceased.setValue(null)
            dateDeceased.clearValidators()
        }
        dateDeceased.updateValueAndValidity()
    }
changePassportNumber(personForm){
  let passportNumber=personForm.get('passportNumber')
  let passportCountry=personForm.get('passportCountry')
  if(passportNumber.value&&passportNumber.value!='')
  {
    
    passportNumber.setValidators([Validators.required])
    passportCountry.setValidators([Validators.required])

  }
  else{
    passportCountry.setValue(null)
    passportNumber.clearValidators()
    passportCountry.clearValidators()   
}
passportCountry.updateValueAndValidity()
passportNumber.updateValueAndValidity()
}
createDirector(): FormGroup{
    let DirectGroup:FormGroup=this.createPersonControl()
    DirectGroup.get('role').setValidators(Validators.required)
    return DirectGroup

}
setDirector(director): FormGroup{
    let DirectGroup:FormGroup=this.setPersonControl(director)
    DirectGroup.get('role').setValidators(Validators.required)
    return DirectGroup
}
}


