import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Stomp } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';

// const SockJs = require('sockjs-client');
// const Stomp = require('@stomp/stompjs');

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  stompClient = null;

  constructor() {
  }

  url = environment.projectUrl + '/socket';
  // Open connection with the back-end socket
  public connect() {
    const socket = new SockJS(this.url);
    this.stompClient = Stomp.over(socket);
    return this.stompClient;
  }

  public disconnect() {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }
    
  }

}
