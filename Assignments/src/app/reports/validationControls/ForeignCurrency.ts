import { FormGroup, FormControl, Validators } from "@angular/forms";
import { UtilService } from "src/app/util.service";

export class ForeignCurrency{
    util=new UtilService()
    constructor(){

    }
createForeignCurrency(){
    return new FormGroup({
        foreignCurrencyCode: new FormControl(),
        foreignAmount: new FormControl(),
        foreignExchangeRate: new FormControl()
    })
}
setForeignCurrency(foreignCurrency):FormGroup{
    if(foreignCurrency){
    let ForeignCurrency=  new FormGroup({
        foreignCurrencyCode:this.util.checkLockUpValue('natinality',foreignCurrency.foreignCurrencyCode) ?new FormControl(foreignCurrency.foreignCurrencyCode):new FormControl(),
        foreignAmount: new FormControl(foreignCurrency.foreignAmount),
        foreignExchangeRate: new FormControl(foreignCurrency.foreignExchangeRate)
    })
this.addValidatorToForeignCurrency(ForeignCurrency)
    return ForeignCurrency
}
else return this.createForeignCurrency()
}
addValidatorToForeignCurrency(fromForeignCurrency:FormGroup) {
    let foreignCurrencyCode = fromForeignCurrency.get('foreignCurrencyCode')
    let foreignAmount = fromForeignCurrency.get('foreignAmount')
    let foreignExchangeRate = fromForeignCurrency.get('foreignExchangeRate')
    let isEgyptianPound = foreignCurrencyCode.value == 'EGP' ||foreignCurrencyCode.value==null
    if (isEgyptianPound) {

      fromForeignCurrency.reset()
      foreignAmount.reset()
      foreignExchangeRate.reset()
      foreignCurrencyCode.reset()
      fromForeignCurrency.clearValidators()
      foreignCurrencyCode.clearValidators()
      foreignAmount.clearValidators()
      foreignExchangeRate.clearValidators()
    }
    else {
      foreignCurrencyCode.setValidators(Validators.required)
      foreignAmount.setValidators(Validators.required)
      foreignExchangeRate.setValidators(Validators.required)
    }

}}