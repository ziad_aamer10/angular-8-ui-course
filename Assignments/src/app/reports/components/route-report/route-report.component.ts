import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { ReportService } from '../../services/report.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-route-report',
  templateUrl: './route-report.component.html',
  styleUrls: ['./route-report.component.css']
})
export class RouteReportComponent implements OnInit {
  reciever=new FormControl()
  options: string[] = [];
  filteredOptions: Observable<string[]>;
  constructor(public dialogRef: MatDialogRef<RouteReportComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private reportservice:ReportService,private toaster:ToastrService) { }
  ngOnInit() {
    this.reportservice.getGroupMember().subscribe((data:string[])=>{

  this.options=data

    },error=>{
this.toaster.error(error.message||'An Error Occured')
console.log(error);

    })
    this.filteredOptions = this.reciever.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().startsWith(filterValue));
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  routeReport(){
   
    this.dialogRef.close({reciever:this.reciever.value})
  
  }
  cancel(){
    this.dialogRef.close()
  }
}
