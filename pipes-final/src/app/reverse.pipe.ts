import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {

  transform(value: string): any {
    const characters = value.split('');
    characters.reverse();

    value = '';
    for (const c of characters) {
      value += c;
    }

    return value;
  }

}


// const sz = characters.length;
// for (let i = 0; i < sz /  2 ; ++i) {
//   let temp = characters[i];
//   characters[i] = characters[sz - 1 - i];
//   characters[sz - 1 - i] = temp;
// }
