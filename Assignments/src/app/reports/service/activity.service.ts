import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  constructor(private http:HttpClient) { }
  getActivityPartyByNumber(entityNumber){
let url=`${environment.projectUrl}/sarReportPartyTypeInfo/${entityNumber}`
return this.http.get(url)
  }
}
