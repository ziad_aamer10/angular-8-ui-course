import { TestBed } from '@angular/core/testing';

import { ReportingPersonService } from './reporting-person.service';

describe('ReportingPersonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportingPersonService = TestBed.get(ReportingPersonService);
    expect(service).toBeTruthy();
  });
});
