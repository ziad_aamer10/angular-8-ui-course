import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentComponent } from './comment/comment.component';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatIconModule, MatInputModule, MatTooltipModule, MatButtonModule, MatChipsModule } from '@angular/material';

@NgModule({
  declarations: [CommentComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTooltipModule,
    MatButtonModule,

  ],
  exports: [
    CommentComponent
  ]
})
export class CommentModule { }
