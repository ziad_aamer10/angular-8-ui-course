import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatCheckboxModule, MatExpansionModule, MatMenuModule, MatSortModule, MatStepperModule,
  MatTableModule, MatTabsModule, MatTooltipModule, MatProgressSpinnerModule,
  MatIconModule, MatToolbarModule, MatCardModule, MatInputModule, MatButtonModule,
  MatSidenavModule, MatListModule, MatFormFieldModule,
   MatButtonToggleModule,
  MatChipsModule,
  MatDialogModule, MatDividerModule
  , MatPaginatorModule, MatProgressBarModule, MatRadioModule, MatSelectModule,
  MatSliderModule, MatSlideToggleModule
} from '@angular/material';
import { LoaderComponent } from '../component/loader/loader.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ValidationErrorsComponent } from '../validation-errors/validation-errors.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

@NgModule({
  declarations: [LoaderComponent,ValidationErrorsComponent
  ],
  imports: [
    CommonModule
    // ,
    // FormsModule,
    // ReactiveFormsModule,
    //  MatCheckboxModule, MatExpansionModule, MatMenuModule, MatSortModule, MatStepperModule,
    // MatTableModule, MatTabsModule, MatTooltipModule, MatProgressSpinnerModule,
    // MatIconModule, MatToolbarModule, MatCardModule, MatInputModule, MatButtonModule,
    // MatSidenavModule, MatListModule, MatFormFieldModule, 
    // MatButtonToggleModule,
    // MatChipsModule,
    // MatDialogModule, MatDividerModule
    // ,MatPaginatorModule, MatProgressBarModule, MatRadioModule, MatSelectModule,
    // MatSliderModule, MatSlideToggleModule, 
  
    // HttpClientModule,
    ,
    MatFormFieldModule,
    MatInputModule
  ]
  , exports: [LoaderComponent, 
    MatCheckboxModule, MatExpansionModule, MatMenuModule, MatSortModule, MatStepperModule,
    MatTableModule, MatTabsModule, MatTooltipModule, MatProgressSpinnerModule,
    MatIconModule, MatToolbarModule, MatCardModule, MatInputModule, MatButtonModule,
    MatSidenavModule, MatListModule, MatFormFieldModule, 
    MatButtonToggleModule,
    MatChipsModule,
    MatDialogModule, MatDividerModule
    , MatPaginatorModule, MatProgressBarModule, MatRadioModule, MatSelectModule,
    MatSliderModule, MatSlideToggleModule,
    MatFormFieldModule,
        
  
    HttpClientModule,FormsModule,ReactiveFormsModule,ValidationErrorsComponent,MatAutocompleteModule]
})
export class SharedModule { }
