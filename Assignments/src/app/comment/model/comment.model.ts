export interface Comment {
    id: Number;
    alarmed_Obj_Key: string;
    alarmed_Obj_level_Cd: string;
    uplodedById: string;
    uplodedByName: string;
    description: string;
    report_id: Number;
}
