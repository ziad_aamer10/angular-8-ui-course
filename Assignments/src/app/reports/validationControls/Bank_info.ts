import { FormBuilder, FormControl, Validators, FormGroup, FormArray } from '@angular/forms';
import { BankInfoService } from '../services/bank-info.service';

// providers: [ BankInfoService];
const fb = new FormBuilder();
export class BankInfo {

    BankInfoForm(): FormGroup {

        return fb.group({
            'rentityId': new FormControl('', Validators.required),
            'rentityBranch': new FormControl('', Validators.maxLength(200)),
            'bankName': new FormControl('', Validators.required),
            'bankSwift': new FormControl('', Validators.required),
            location: fb.group({

                'addressType': new FormControl('', Validators.required),
                'address': new FormControl('', Validators.required),
                'town': new FormControl('', Validators.maxLength(200)),
                'city': new FormControl('', Validators.required),
                'zip': new FormControl('', Validators.maxLength(10)),
                'countryCode': new FormControl('', Validators.required),
                'state': new FormControl('', Validators.required),
                'comments': new FormControl(),
            })

        });
    }
}
