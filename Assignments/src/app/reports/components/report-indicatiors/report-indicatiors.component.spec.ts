import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportIndicatiorsComponent } from './report-indicatiors.component';

describe('ReportIndicatiorsComponent', () => {
  let component: ReportIndicatiorsComponent;
  let fixture: ComponentFixture<ReportIndicatiorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportIndicatiorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportIndicatiorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
