import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrNewReportComponent } from './cr-new-report.component';

describe('CrNewReportComponent', () => {
  let component: CrNewReportComponent;
  let fixture: ComponentFixture<CrNewReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrNewReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrNewReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
