import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { t_person } from '../../validationControls/t_person';
import { UtilService } from 'src/app/util.service';
import { ToastrService } from 'ngx-toastr';
import { PersonsService } from '../../services/persons.service';
import { notValidElementWithMessageOnly } from '../../models/notValidElement';

@Component({
  selector: 'app-person-dialog',
  templateUrl: './person-dialog.component.html',
  styleUrls: ['./person-dialog.component.css']
})
export class PersonDialogComponent implements OnInit {
  entityNumber=null
  notValidElement: any[] = []
  personForm: FormGroup
  t_person: t_person = new t_person();
  constructor(public dialogRef: MatDialogRef<PersonDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public fb: FormBuilder, 
    public utilservice: UtilService,public toaster:ToastrService,private personservice:PersonsService) { 
      this.dialogRef.disableClose=true
    }

  ngOnInit() {
    
    
    
 this.personForm=this.data.director
    
    
    if(this.data.entityNumber){
this.entityNumber=this.data.entityNumber
    }
  
    

    

  }
  SaveDirector() {
    // if (this.personForm.valid) {
      this.dialogRef.close(this.personForm)

    // }
    // else{
    //   this.toaster.error("Person Inforamation not complete!!","Validation Error",{closeButton:true
    //   ,progressBar:true,disableTimeOut:true,enableHtml:true})
    // }

  }




  closePerson() {
    this.dialogRef.close()
  }
  validate() {
  
this.clearValidationErrors()
    this.utilservice.validateFormGroup(this.personForm, this.notValidElement)
  }

  clearValidationErrors() {
    this.notValidElement = []
  }
  scrollToAnchor(x) {
this.utilservice.scrollToAnchor(x)
  }
  SaveDirectorToSource(){
    let notValidSection:notValidElementWithMessageOnly[]=[]
    let entityToBeSaved=Object.assign({},this.personForm.value)
    entityToBeSaved.entityNumber=this.entityNumber
    if(!this.personForm.valid)
    {
      this.utilservice.validateFormGroup2(this.personForm, [],notValidSection);
      this.toaster.error("The Director is not valid","Director fields",{timeOut:3000})
      for (const iterator of notValidSection) {
        this.toaster.error(iterator.name,iterator.message,{timeOut:3000})

      }
  return}
    
    this.personservice.SaveDirectorToSource(entityToBeSaved).subscribe(data=>{
      
      if(data){
        this.toaster.success("Director is saved successfully")
      }
      else{
this.toaster.warning("Error in save director")

      }

    }
      ,error=>{
      
        
        this.toaster.error(error.message)
      })
  }
  getDirectorInfo(partyNumber:HTMLInputElement){
    
    let partyNum=partyNumber.value
    this.personservice.getDirectorByPartyNumber(partyNum,this.entityNumber).subscribe(data=>{
      
    if(data){
    this.toaster.success(`Director with party number${partyNum} found`)
    this.personForm=this.t_person.setPersonControl(data)
    
    }
    else{
      this.toaster.warning("No director with PartyNumber:"+partyNum)
    }
    },error=>{
      
      
      this.toaster.error(error.message)
    })
    
    
      }
}
