import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SarReportActivitiesListComponent } from './sar-report-activities-list.component';

describe('SarReportActivitiesListComponent', () => {
  let component: SarReportActivitiesListComponent;
  let fixture: ComponentFixture<SarReportActivitiesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SarReportActivitiesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SarReportActivitiesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
