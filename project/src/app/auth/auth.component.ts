import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Store} from "@ngrx/store";
import {Subscription} from "rxjs";
import {AlertComponent} from "../shared/alert/alert.component";
import {PlaceholderDirective} from "../shared/placeholder/placeholder.directive";
import * as fromApp from './../store/app.reducer'
import * as AuthActions from './store/auth.actions';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {

  isLogin = true;
  isLoading = false;
  error: string = null;
  @ViewChild(PlaceholderDirective, {static: false}) placeHolderDir;
  private alertSubscription: Subscription;
  private storeSub: Subscription;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {

    this.storeSub = this.store.select('auth').subscribe(authState => {
      this.isLoading = authState.isLoading;
      this.error = authState.authError;
      if(this.error){
        this.showErrorAlert(this.error);
      }
    });
  }

  onSwitchToLogin() {
    this.isLogin = !this.isLogin;
  }

  onSubmit(authForm: NgForm) {
    if (authForm.invalid) {
      return;
    }

    this.isLoading = true;
    if (this.isLogin) {
      this.store.dispatch(new AuthActions.LoginStart({
        email: authForm.value.email,
        password: authForm.value.password
      }));
    } else {
      this.store.dispatch(new AuthActions.Signup({
        email: authForm.value.email,
        password: authForm.value.password
    }))
    }

    authForm.reset();
  }

  private showErrorAlert(err: string) {
    const alertCmpFact = this.componentFactoryResolver.resolveComponentFactory(AlertComponent);

    const hostViewContainerRef = this.placeHolderDir.viewContainerRef;
    hostViewContainerRef.clear();

    const alertCompRef = hostViewContainerRef.createComponent(alertCmpFact);

    alertCompRef.instance.message = err;

    this.alertSubscription = alertCompRef.instance.close.subscribe(() => {
      this.alertSubscription.unsubscribe();
      hostViewContainerRef.clear();
      this.onHandleError();
    });

  }

  onHandleError(){
    this.store.dispatch(new AuthActions.ClearError());
  }

  ngOnDestroy(): void {
    if(this.alertSubscription){
      this.alertSubscription.unsubscribe();
    }
    this.storeSub.unsubscribe();
  }
}
