import { FormBuilder, Validators, FormControl, FormGroup } from "@angular/forms";
import { t_person } from "./t_person";
import { t_account } from "./t_account";
import { tEntity } from "./t_entity";
import { t_person_my_client } from "./t_person_my_client";
import { t_account_my_client } from "./t_account_my_client";
import { t_entity_my_client } from "./t_entity_my_client";
import { formControlBinding } from "@angular/forms/src/directives/ng_model";
import { ForeignCurrency } from "./ForeignCurrency";
import { UtilService } from "src/app/util.service";

export class t_to {
    tPerson: t_person = new t_person();
    t_account: t_account = new t_account()
    t_entity: tEntity = new tEntity()
    foreignCurrency:ForeignCurrency=new ForeignCurrency()
    
    tMyPerson:t_person_my_client = new t_person_my_client();
    tMyAccount: t_account_my_client= new t_account_my_client();
    tMyEntity: t_entity_my_client = new t_entity_my_client();
    util=new UtilService();
    // fb:FormBuilder
    constructor(private fb: FormBuilder) {
        this.fb = new FormBuilder()
    }
    create_t_to_my_controller() {
        return this.fb.group({
            id: [''],
            toFundsCode: ['', Validators.required],
            toFundsComment: [''],
            toForeignCurrency:this.foreignCurrency.createForeignCurrency(),
            toAccount: new FormControl(),
            toPerson: new FormControl(),
            toEntity: new FormControl(),
            toCountry: ['',Validators.required],

        })
    }

    create_t_to_other_controller() {
        return this.fb.group({
            id: [''],
            toFundsCode: ['', Validators.required],
            toFundsComment: [''],
            toForeignCurrency: this.foreignCurrency.createForeignCurrency(),
            toAccount: new FormControl(),
            toPerson: new FormControl(),
            toEntity: new FormControl(),
            toCountry: ['',Validators.required],

        })
    }

    set_t_to_my_controller(tto):FormGroup{
        if (tto == null) {
            return null
        }
        return this.fb.group({
            id: [tto.id],
            toFundsCode: this.util.checkLockUpValue('FundsCode',tto.toFundsCode)?[tto.toFundsCode, Validators.required]:new FormControl(null,Validators.required),

            toFundsComment: [tto.toFundsComment],
            toForeignCurrency: this.foreignCurrency.setForeignCurrency(tto.toForeignCurrency),
            toAccount: tto.toAccount?this.tMyAccount.setAccountControl(tto.toAccount):new FormControl(),
            toPerson: tto.toPerson?this.tMyPerson.setPersonControl(tto.toPerson):new FormControl(),
            toEntity: tto.toEntity?this.tMyEntity.setEntityControl(tto.toEntity):new FormControl(),
            toCountry: this.util.checkLockUpValue('nationality',tto.toCountry)?[tto.toCountry,Validators.required]:new FormControl(null,Validators.required),
        })
    }

    set_t_to_other_controller(tto){
        if (tto == null) {
            return null
        }
        return this.fb.group({
            id: [tto.id],
            toFundsCode: this.util.checkLockUpValue('FundsCode',tto.toFundsCode)?[tto.toFundsCode, Validators.required]:new FormControl(null,Validators.required),
            toFundsComment: [tto.toFundsComment],
            toForeignCurrency:  this.foreignCurrency.setForeignCurrency(tto.toForeignCurrency),
            toAccount: tto.toAccount?this.t_account.setAccountControl(tto.toAccount):new FormControl(),
            toPerson: tto.toPerson?this.tPerson.setPersonControl(tto.toPerson):new FormControl(),
            toEntity: tto.toEntity?this.t_entity.setEntityControl(tto.toEntity):new FormControl(),
            toCountry: this.util.checkLockUpValue('nationality',tto.toCountry)?[tto.toCountry,Validators.required]:new FormControl(null,Validators.required),

            // foreign_currency_code:['',Validators.required],
            // foreign_amount:['',Validators.required],
            // foreign_exchange_rate:['',Validators.required]
        }) 
    }

    settoForeignCurrency(fromForeignCurrency) {
        if (fromForeignCurrency == null) {
            return this.fb.group({
                foreignCurrencyCode: [''],
                foreignAmount: [''],
                foreignExchangeRate: ['']
            })
        }
        var t: FormGroup = this.fb.group({
            foreignCurrencyCode: [fromForeignCurrency.foreignCurrencyCode],
            foreignAmount: [fromForeignCurrency.foreignAmount],
            foreignExchangeRate: [fromForeignCurrency.foreignExchangeRate]
        })
        return t;

    }
}