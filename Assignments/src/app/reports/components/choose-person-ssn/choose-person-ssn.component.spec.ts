import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosePersonSSNComponent } from './choose-person-ssn.component';

describe('ChoosePersonSSNComponent', () => {
  let component: ChoosePersonSSNComponent;
  let fixture: ComponentFixture<ChoosePersonSSNComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoosePersonSSNComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoosePersonSSNComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
