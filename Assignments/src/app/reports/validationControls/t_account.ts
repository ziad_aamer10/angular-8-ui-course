import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { tEntity } from './t_entity';
import { t_signatory } from './t_signatory';
import { UtilService } from 'src/app/util.service';
import { LookupClass } from '../lockups/Lookups';
import { Data } from '../lockups/report-indiators-lockup';
const fb = new FormBuilder();
export class t_account {
  t_entity: tEntity = new tEntity();
  t_signatory: t_signatory = new t_signatory();
  private util: UtilService = new UtilService()
  constructor() {

  }
  createAccountControl(): FormGroup {
    const f: FormGroup = fb.group({
      id: new FormControl(),
      type: ['account'],
      institutionName: ['', [Validators.maxLength(255)]],
      // institutionCode: ['', [Validators.maxLength(50)]],
      swift: ['', [Validators.maxLength(11), Validators.required]],
      nonBankInstitution: [''],
      branch: ['', [Validators.maxLength(255)]],
      account: ['', [Validators.required, Validators.maxLength(50)]],
      currencyCode: new FormControl(),
      accountName: ['', [Validators.maxLength(255)]],
      iban: new FormControl(),
      personalAccountType: new FormControl(),
      clientNumber: new FormControl(),
      tEntity: new FormControl(),
      signatory: fb.array([]),
      opened: new FormControl(),
      closed: new FormControl(),
      balance: new FormControl(),
      dateBalance: new FormControl(),
      statusCode: new FormControl(),
      beneficiary: ['', [Validators.maxLength(50)]],
      beneficiaryComment: ['', [Validators.maxLength(225)]],
      comments: ['', [Validators.maxLength(4000)]],
      isEntity: [false],
      isReviewed: new FormControl(false),
      changeBeginDate: [null, [Validators.maxLength(4000)]],
      updatedBy: [null, [Validators.maxLength(4000)]],
    });
    return f;
  }
  setAccountControl(account): FormGroup {
    const f: FormGroup = fb.group({
      id: new FormControl(account.id),
      type: ['account'],
      institutionName: [account.institutionName, [Validators.maxLength(255)]],
      // institutionCode: [account.institutionCode, [Validators.maxLength(50)]],
      swift: [account.swift, [Validators.maxLength(11), Validators.required]],
      nonBankInstitution: [account.nonBankInstitution],
      branch: [account.branch, [Validators.maxLength(255)]],
      account: [account.account, [Validators.required, Validators.maxLength(50)]],
      currencyCode: this.util.checkLockUpValue('currencyCode',account.currencyCode)?account.currencyCode:new FormControl(),
      accountName: [account.accountName, [Validators.maxLength(255)]],
      iban: [account.iban],
      personalAccountType:this.util.checkLockUpValue('personalAccountType',account.personalAccountType)?account.personalAccountType:new FormControl(),
      clientNumber: [account.clientNumber],
      tEntity: this.setEntity(account.tEntity),
      signatory: this.setSignatory(account.signatory),
      opened: [this.util.convertDateFromServer(account.opened)],
      closed: [this.util.convertDateFromServer(account.closed)],
      balance: [account.balance],
      dateBalance: [this.util.convertDateFromServer(account.dateBalance)],
      statusCode: [account.statusCode],
      beneficiary: [account.beneficiary, [Validators.maxLength(50)]],
      beneficiaryComment: [account.beneficiaryComment, [Validators.maxLength(225)]],
      comments: [account.comments, [Validators.maxLength(4000)]],
      isEntity: [account.isEntity],
      changeBeginDate: [account.changeBeginDate, [Validators.maxLength(255)]],
      isReviewed: new FormControl(account.isReviewed),
      updatedBy: new FormControl(account.updatedBy),
    });


    return f;
  }
  setEntity(entity) {



    if (entity) {
      return this.t_entity.setEntityControl(entity);
    } else {
      return new FormControl();
    }
  }
  setSignatory(signatories) {
    const signatoryControl = fb.array([]);
    for (const i of signatories) {
      const control = this.t_signatory.set_signatoryControl(i);
      signatoryControl.push(control);
    }
    return signatoryControl;
  }
//   checkLockUpValue(propertyName, key) {
//     let items:lockUpChecker[]=[{lockUpValue:this.lockup.Currency,propertyName:'currencyCode'},
//     {lockUpValue:this.lockup.account_type,propertyName:'personalAccountType'},{lockUpValue:this.lockup.account_status_type,propertyName:'statusCode'}]
//    for(let item of items){
//      if(item.propertyName==propertyName){
//       return this.filterLockUp(this.lockup.Currency, key)
// break;
//      }
//    }
  
//     return false
//   }
  filterLockUp(lockupValues: Data[], key) {
    let x: Data[] = lockupValues.filter(elment => elment.key == key)
    return x.length > 0

  }
}