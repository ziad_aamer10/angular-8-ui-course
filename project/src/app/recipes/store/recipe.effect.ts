import {Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {HttpClient} from "@angular/common/http";
import {map, switchMap, withLatestFrom} from "rxjs/operators";
import {Recipe} from "../recipe.model";
import * as RecipesActions from './recipe.actions';
import * as fromApp from '../../store/app.reducer';

const url = 'https://ng-complete-guide-a4627.firebaseio.com/recipes.json';

@Injectable()
export class RecipeEffect {


  @Effect()
  fetchRecipes = this.actions$.pipe(
    ofType(RecipesActions.FETCH_RECIPES),
    switchMap(() => {
      return this.http.get<Recipe[]>(url).pipe(map(recipes => {
        return recipes.map(recipe => {
          return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
        });
      }));
    }),
    map((recipes) => {
      return new RecipesActions.SetRecipes(recipes);
    })
  )

  @Effect({dispatch: false})
  storeRecipes = this.actions$.pipe(
    ofType(RecipesActions.STORE_RECIPES),
    // get both values from observables the source/master (store_recipes action) & the other/slave (store select)
    // only at the time when the source emits (when the store_recipes action get dispatched)
    // we don't get data unless the source emits & there exists a value in the other observable.
    withLatestFrom(this.store.select('recipes')),
    switchMap(([actionData, recipesState]) => {

      return this.http.put(url, recipesState.recipes);
    })
  )

  constructor(private actions$: Actions, private http: HttpClient, private store: Store<fromApp.AppState>) {
  }
}
