import {Injectable} from "@angular/core";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {catchError, map, switchMap, tap} from "rxjs/operators";
import {of, throwError} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {AuthService} from "../auth.service";
import * as AuthActions from './auth.actions';
import {Router} from "@angular/router";
import {User} from "../user.model";


export interface AuthResponse {
  idToken: string,
  email: string,
  refreshToken: string,
  expiresIn: string,
  localId: string,
  registered: boolean
}

const handleAuthentication = (expiresIn: number, email: string, localId: string, idToken: string) => {
  const expDate = new Date(new Date().getTime() + expiresIn * 1000);

  const newUser: User = new User(email, localId, idToken, expDate);

  localStorage.setItem('userData', JSON.stringify(newUser));

  return new AuthActions.AuthenticateSuccess({
    email: email,
    userId: localId,
    _token: idToken,
    _tokenExpirationDate: expDate,
    redirect: true
  });
}

const handleError = (error) => {
  let errorMessage = 'An unknown error occurred!';
  if (!error.error || !error.error.error) {
    return of(new AuthActions.AuthenticateFail(errorMessage));
  }
  switch (error.error.error.message) {
    case 'EMAIL_EXISTS':
      errorMessage = 'This email already exists!'
      break;
    case 'EMAIL_NOT_FOUND':
      errorMessage = 'The email or the password is wrong';
      break;
    case 'INVALID_PASSWORD':
      errorMessage = 'The email or the password is wrong';
  }
  return of(new AuthActions.AuthenticateFail(errorMessage));
}

@Injectable()
export class AuthEffects {

  @Effect()
  authSignup = this.actions$.pipe(
    ofType(AuthActions.SIGNUP_START),
    switchMap((signupAction: AuthActions.Signup) => {
      return this.httpClient.post<AuthResponse>(
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' + environment.firebaseAPIKey,
        {
          'email': signupAction.payload.email,
          'password': signupAction.payload.password,
          'returnSecureToken': true
        }).pipe(map((resData) => {
        this.authService.setLogoutTimer(+resData.expiresIn * 1000);
        return handleAuthentication(
          +resData.expiresIn,
          resData.email,
          resData.localId,
          resData.idToken,
        );
      }), catchError(error => {
        return handleError(error);
      }));

    })
  );

  @Effect()
  authLogin = this.actions$.pipe(
    ofType(AuthActions.LOGIN_START),
    switchMap((authDate: AuthActions.LoginStart) => {
      return this.httpClient.post<AuthResponse>(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' + environment.firebaseAPIKey,
        {
          'email': authDate.payload.email,
          'password': authDate.payload.password,
          'returnSecureToken': true
        }).pipe(map((resData) => {
        this.authService.setLogoutTimer(+resData.expiresIn * 1000);
        return handleAuthentication(
          +resData.expiresIn,
          resData.email,
          resData.localId,
          resData.idToken,
        );
      }), catchError(error => {
        return handleError(error);
      }));
    })
  );

  @Effect({dispatch: false})
  authSuccess = this.actions$.pipe(ofType(AuthActions.AUTHENTICATE_SUCCESS),
    tap((authSuccessState: AuthActions.AuthenticateSuccess) => {
      if (authSuccessState.payload.redirect) {
        this.router.navigate(['/']);
      }
    }));


  @Effect()
  autoLogin = this.actions$.pipe(ofType(AuthActions.AUTO_LOGIN), map(() => {
    const userData: {
      email: string,
      userId: string,
      _token: string,
      _tokenExpirationDate
    } = JSON.parse(localStorage.getItem('userData'));
    if (!userData) {
      return {type: 'DUMMY'};
    }

    if (userData._token) {
      this.authService.setLogoutTimer(new Date(userData._tokenExpirationDate).getTime() - new Date().getTime());
      return new AuthActions.AuthenticateSuccess({
        email: userData.email, userId: userData.userId, _token: userData._token
        , _tokenExpirationDate: new Date(userData._tokenExpirationDate), redirect: false
      });
    }
    return {type: 'DUMMY'};
  }));

  @Effect({dispatch: false})
  autoLogout = this.actions$.pipe(ofType(AuthActions.LOGOUT), tap(() => {
    localStorage.removeItem('userData');
    this.authService.clearLogoutTimer();
    this.router.navigate(['auth']);
  }));


  constructor(private actions$: Actions, private httpClient: HttpClient,
              private router: Router, private authService: AuthService) {
  }
}
