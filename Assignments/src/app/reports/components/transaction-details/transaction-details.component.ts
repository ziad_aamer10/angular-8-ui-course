import { entity } from './../../models/entity.model';

import { t_transaction } from './../../validationControls/t_transaction';
import { Component, OnInit, ViewChild, EventEmitter, Output, Input, AfterViewInit, OnChanges, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { AddMultipleTransactionDialogComponent } from '../add-multiple-transaction-dialog/add-multiple-transaction-dialog.component';
import { FormControl, FormArray, FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { ToastrService } from 'ngx-toastr';
import { UtilService } from 'src/app/util.service';
import { notValidElement, notValidElementWithMessageOnly } from '../../models/notValidElement';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';
import { Subscription } from 'rxjs';
import { Actions } from '../../Actions/action';
import { tEntity } from '../../validationControls/t_entity';
import { t_person } from '../../validationControls/t_person';
import { t_account } from '../../validationControls/t_account';
import { R3ResolvedDependencyType } from '@angular/core/src/render3/jit/compiler_facade_interface';
import { BreakpointObserver } from '@angular/cdk/layout';





export interface Balance {
  balance: string;
  dateBalance: Date;
}

@Component({
  selector: 'app-transaction-details',
  templateUrl: './transaction-details.component.html',
  styleUrls: ['./transaction-details.component.css'],
  // encapsulation: ViewEncapsulation.None
})
export class TransactionDetailsComponent implements OnInit, OnChanges {

  status = 0;
  chagnedObjectSubscription: Subscription;
  subscription: Subscription;
  transactionRefNumber: any;
  cascadeTransactionsIndexes = [];
  indexes = [];
  numberP: Number;
  t_transaction: t_transaction = new t_transaction(this.fb);
  partyNumber: number;
  indexOFTransaction = -1;
  tr: AbstractControl;
  dataSource: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  selected = new FormControl(0);
  myClonedArray = [];
  editTransactionMode: boolean = false;
  createMode: boolean = false;
  multiMode: boolean = false;
  cascadeMode: boolean = false;
  addTransactionAction: boolean = false;
  addMultipleTransactionAction: boolean = false;
  deleteTransactionAction: boolean = false;
  viewTransactionAction: boolean = false;
  cascadeTransactionAction: boolean = false;
  tentity: tEntity = new tEntity();
  tperson: t_person = new t_person();
  tAccount: t_account = new t_account();
  showProgress: boolean;


  // @Input('transStatus')   
  // set transStatus(value){
  //   this.status = value ;
  //   
  // };
  get transStatus() {


    return this.status;
  }
  @Input() arrayOfTransactions: FormArray;
  @Output() editTransaction = new EventEmitter();
  @Output() emitNewArrayAfterUpdate = new EventEmitter();
  @Output() addNewTab = new EventEmitter();
  @Output() addNewTab1 = new EventEmitter();
  @Output() addNewTabForCascadingTransactions = new EventEmitter()
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  disableTransaction: boolean = false;

  ngOnChanges(ch: SimpleChanges) {


    if (ch['transStatus']) {

      this.status = ch['transStatus'].currentValue

    }
  }
  constructor(public dialog: MatDialog,
    private dataService: SharingDataServieService, private toaster: ToastrService,
    private fb: FormBuilder, public util: UtilService, public auth: AuthService) {
    this.dataSource = new MatTableDataSource([]);
  }

  displayedColumns: string[] = ['select',
    'Trn', 'TDate', 'TAmount', 'TTypeFrom', 'FPNumber', 'FPName', 'FromACCNumber', 'TTypeTo', 'ToACCNumber', 'ToPNumber', 'ToPName'
    , 'FReviewd', 'TReviewd', 'Validation', 'Validation_Messages',
    'Action'
  ];

  ngOnInit() {
    if (this.arrayOfTransactions.disabled) {
      this.disableTransaction = true;
    }
    // this.fillTransactionTable();
    if (localStorage.getItem("ReportST") != null)
      this.addNewTab.emit(this.t_transaction.setTransaction(JSON.parse(localStorage.getItem("ReportST"))));
    // this.setActions()
    this.fillTransactionTable();
    this.setActions()

    let selectedTran = this.selection.selected;
    for (let trans of selectedTran) {
      let index = this.arrayOfTransactions.value.indexOf(trans);
      console.log("index ===", index);
      if (index != -1) {
        // index = 
        this.arrayOfTransactions.value.splice(index, 1);
      }
    }
  }

  ngOnDestroy(): void {


    if (this.subscription != undefined && this.chagnedObjectSubscription != undefined) {

      this.subscription.unsubscribe();
      this.chagnedObjectSubscription.unsubscribe();
    }
  }

  // fill table with transactions 
  fillTransactionTable() {

    this.subscription = this.dataService.currentMessage.subscribe(message => {


      // edit
      if (this.status == 3) {
        if (message != undefined) {
          this.arrayOfTransactions.removeAt(this.indexOFTransaction);
          this.arrayOfTransactions.insert(this.indexOFTransaction, message.trnControl);
        }
        if (message.vChgObject != undefined) {
          console.log("message.vChgObject", message.vChgObject);
          let number = this.getChangedPartyNumber(message.vChgObject);
          console.log("message.vChgObject.number", number);
          this.CalculateTableInEditModeCase2(message.vChgObject, number);
        }
      }
      //add multi
      // else if (this.status == 2) {
      //   this.checkForExistingTransactionsOnMultipleCase(this.arrayOfTransactions, message);
      // }
      else if (this.status == 1) {
        let list = this.checkForRepeatedTransactionNumber(this.arrayOfTransactions.value);

        if (message.value != undefined) {
          let num = message.value.transactionnumber;
          let numFound: boolean = false;
          if (list.length > 0) {
            for (let i = 0; i < list.length; i++) {
              if (list[i] == num) {
                this.toaster.info("There is a transaction with this number" + num + " in the Table , Please don't repeat the number", '', { timeOut: 4000 });
                numFound = true
                break;
              }
            }
            if (!numFound) {
              this.arrayOfTransactions.push(message);
              list.push(num);
            }
          }
          else {
            this.arrayOfTransactions.push(message);
            list.push(num);
          }
        }

        // this.createMode = false;
      }
      else if (this.status == 4) {
        let ind = this.cascadeTransactionsIndexes;
        ind.forEach((element, index) => {
          this.arrayOfTransactions.removeAt(element);
          let tr = this.t_transaction.setTransaction(message.value[index])
          this.arrayOfTransactions.insert(element, tr);
        })

        this.status = 0;


      }
      this.calculateTableValues();
      this.status = 0;
      // this.emitNewArrayAfterUpdate.emit(this.arrayOfTransactions);
    })

    this.dataService.signatoryObject.subscribe(signatoryPersonObject => {
      this.CalculateTableInEditModeCase2(signatoryPersonObject, signatoryPersonObject.partyNumber);
    })

    this.dataService.saveValidationInTransactionTable.subscribe(refesh => {
      this.calculateTableValues();
    })

  }

  getChangedPartyNumber(returnedObject) {
    let partynumber;
    if (returnedObject) {
      if (returnedObject.entityNumber) {

        partynumber = returnedObject.entityNumber;

      }
      else if (returnedObject.partyNumber) {

        partynumber = returnedObject.partyNumber;

      }
      else if (returnedObject.account) {

        partynumber = returnedObject.account;

      }
    }

    return partynumber;

  }

  calculateTableValues() {


    this.dataSource.data = this.arrayOfTransactions.value;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    if (this.dataSource.data != undefined) {

      this.dataSource.data.forEach(i => {

        if (i.tFrom != null) {
          // 
          if (i.tFrom.fromEntity != null) {
            i.TTypeFrom = "from other client entity"
            i.FPNumber = i.tFrom.fromEntity.entityNumber
            i.FPName = i.tFrom.fromEntity.name;
            i.FromACCNumber = '';
            if (i.tFrom.fromEntity.isReviewed) {
              i.FReviewd = 'Y';
            } else {
              i.FReviewd = 'N';
            }
          }
          else if (i.tFrom.fromPerson != null) {
            i.TTypeFrom = "from other client person"
            i.FPNumber = i.tFrom.fromPerson.partyNumber
            i.FromACCNumber = '';
            if (i.tFrom.fromPerson.firstName == null) {
              i.FPName = i.tFrom.fromPerson.middleName + " " + i.tFrom.fromPerson.lastName;
            } else if (i.tFrom.fromPerson.middleName == null) {
              i.FPName = i.tFrom.fromPerson.firstName + " " + i.tFrom.fromPerson.lastName;
            } else if (i.tFrom.fromPerson.lastName == null) {
              i.FPName = i.tFrom.fromPerson.firstName + " " + i.tFrom.fromPerson.middleName;
            } else {
              i.FPName = i.tFrom.fromPerson.firstName + " " + i.tFrom.fromPerson.middleName + " " + i.tFrom.fromPerson.lastName;
            }
            if (i.tFrom.fromPerson.isReviewed) {
              i.FReviewd = 'Y';
            } else {
              i.FReviewd = 'N';
            }
          }
          else if (i.tFrom.fromAccount != null) {


            i.TTypeFrom = "from other client account"
            i.FromACCNumber = i.tFrom.fromAccount.account;
            if (i.tFrom.fromAccount.isReviewed) {
              i.FReviewd = 'Y';
            } else {
              i.FReviewd = 'N'
            }
            if (i.tFrom.fromAccount.signatory != null) {
              let signatoryIndicator: boolean = false;
              let xc: any[] = i.tFrom.fromAccount.signatory;
              for (let j = 0; j < xc.length; j++) {
                if (i.tFrom.fromAccount.signatory[j].isPrimary === true) {
                  i.FPNumber = i.tFrom.fromAccount.signatory[j].tPerson.partyNumber
                  if (i.tFrom.fromAccount.signatory[j].tPerson.firstName == null)
                    i.FPName = i.tFrom.fromAccount.signatory[j].tPerson.middleName + " " + i.tFrom.fromAccount.signatory[j].tPerson.lastName;
                  else if (i.tFrom.fromAccount.signatory[j].tPerson.middleName == null)
                    i.FPName = i.tFrom.fromAccount.signatory[j].tPerson.firstName + " " + i.tFrom.fromAccount.signatory[j].tPerson.lastName;
                  else if (i.tFrom.fromAccount.signatory[j].tPerson.lastName == null)
                    i.FPName = i.tFrom.fromAccount.signatory[j].tPerson.firstName + " " + i.tFrom.fromAccount.signatory[j].tPerson.middleName
                  else
                    i.FPName = i.tFrom.fromAccount.signatory[j].tPerson.firstName + " " + i.tFrom.fromAccount.signatory[j].tPerson.middleName + " " + i.tFrom.fromAccount.signatory[j].tPerson.lastName
                  signatoryIndicator = true;
                  break;
                }
              }
              if (!signatoryIndicator) {
                i.FPName = "No primary signatory"
                i.FPNumber = "No primary signatory"
              }
            }

          }
        }
        if (i.tFromMyClient != null) {
          // 
          if (i.tFromMyClient.fromEntity != null) {
            i.TTypeFrom = "from my client entity"
            i.FPNumber = i.tFromMyClient.fromEntity.entityNumber
            i.FPName = i.tFromMyClient.fromEntity.name;
            i.FromACCNumber = '';
            if (i.tFromMyClient.fromEntity.isReviewed) {
              i.FReviewd = 'Y';
            } else {
              i.FReviewd = 'N';
            }
          }

          else if (i.tFromMyClient.fromPerson != null) {

            i.TTypeFrom = "from my client person"
            i.FPNumber = i.tFromMyClient.fromPerson.partyNumber
            if (i.tFromMyClient.fromPerson.firstName == null) {
              i.FPName = i.tFromMyClient.fromPerson.middleName + " " + i.tFromMyClient.fromPerson.lastName;
            } else if (i.tFromMyClient.fromPerson.middleName == null) {
              i.FPName = i.tFromMyClient.fromPerson.firstName + " " + i.tFromMyClient.fromPerson.lastName;
            } else if (i.tFromMyClient.fromPerson.lastName == null) {
              i.FPName = i.tFromMyClient.fromPerson.firstName + " " + i.tFromMyClient.fromPerson.middleName;
            } else {
              i.FPName = i.tFromMyClient.fromPerson.firstName + " " + i.tFromMyClient.fromPerson.middleName + " " + i.tFromMyClient.fromPerson.lastName;
            }
            if (i.tFromMyClient.fromPerson.isReviewed) {
              i.FReviewd = 'Y';
            } else {
              i.FReviewd = 'N'
            }
          }

          else if (i.tFromMyClient.fromAccount != null) {
            i.TTypeFrom = "from my client account"
            i.FromACCNumber = i.tFromMyClient.fromAccount.account
            if (i.tFromMyClient.fromAccount.isReviewed) {
              i.FReviewd = 'Y'
            } else {
              i.FReviewd = 'N';
            }
            if (i.tFromMyClient.fromAccount.signatory != null) {
              let signatoryIndicator: boolean = false
              let xc: any[] = i.tFromMyClient.fromAccount.signatory
              for (let j = 0; j < xc.length; j++) {
                if (i.tFromMyClient.fromAccount.signatory[j].isPrimary == true) {
                  i.FPNumber = i.tFromMyClient.fromAccount.signatory[j].tPerson.partyNumber
                  if (i.tFromMyClient.fromAccount.signatory[j].tPerson.firstName == null)
                    i.FPName = i.tFromMyClient.fromAccount.signatory[j].tPerson.middleName + " " + i.tFromMyClient.fromAccount.signatory[j].tPerson.lastName;
                  else if (i.tFromMyClient.fromAccount.signatory[j].tPerson.middleName == null)
                    i.FPName = i.tFromMyClient.fromAccount.signatory[j].tPerson.firstName + " " + i.tFromMyClient.fromAccount.signatory[j].tPerson.lastName;
                  else if (i.tFromMyClient.fromAccount.signatory[j].tPerson.lastName == null)
                    i.FPName = i.tFromMyClient.fromAccount.signatory[j].tPerson.firstName + " " + i.tFromMyClient.fromAccount.signatory[j].tPerson.middleName
                  else
                    i.FPName = i.tFromMyClient.fromAccount.signatory[j].tPerson.firstName + " " + i.tFromMyClient.fromAccount.signatory[j].tPerson.middleName + " " + i.tFromMyClient.fromAccount.signatory[j].tPerson.lastName
                  signatoryIndicator = true
                  break;
                }
              }
              if (!signatoryIndicator) {

                i.FPName = "No primary signatory"
                i.FPNumber = "No primary signatory"
              }
            }
          }
        }
        if (i.tFrom == null && i.tFromMyClient == null) {
          // 
          i.FPNumber = '';
          i.FPName = '';
          i.FromACCNumber = '';
          i.TTypeFrom = '';
          i.FReviewd = '';
        }

        if (i.tTo != null) {
          // 
          if (i.tTo.toEntity != null) {
            i.TTypeTo = "to other client entity"
            i.ToPNumber = i.tTo.toEntity.entityNumber
            i.ToPName = i.tTo.toEntity.name;
            i.ToACCNumber = '';
            if (i.tTo.toEntity.isReviewed) {
              i.TReviewd = 'Y';
            } else {
              i.TReviewd = 'N';
            }
          }
          else if (i.tTo.toPerson != null) {
            i.TTypeTo = "to other client person"
            i.ToPNumber = i.tTo.toPerson.partyNumber
            if (i.tTo.toPerson.firstName == null) {
              i.ToPName = i.tTo.toPerson.middleName + " " + i.tTo.toPerson.lastName;
            } else if (i.tTo.toPerson.middleName == null) {
              i.ToPName = i.tTo.toPerson.firstName + " " + i.tTo.toPerson.lastName;
            } else if (i.tTo.toPerson.lastName == null) {
              i.ToPName = i.tTo.toPerson.firstName + " " + i.tTo.toPerson.middleName;
            } else {
              i.ToPName = i.tTo.toPerson.firstName + " " + i.tTo.toPerson.middleName + " " + i.tTo.toPerson.lastName;
            }
            // i.ToPName = i.tTo.toPerson.firstName + " " + i.tTo.toPerson.middleName + " " + i.tTo.toPerson.lastName;
            i.ToACCNumber = '';
            if (i.tTo.toPerson.isReviewed) {
              i.TReviewd = 'Y';
            } else {
              i.TReviewd = 'N'
            }
          }
          else if (i.tTo.toAccount != null) {
            i.TTypeTo = "to other client account";
            i.ToACCNumber = i.tTo.toAccount.account;
            if (i.tTo.toAccount.isReviewed) {
              i.TReviewd = 'Y';
            } else {
              i.TReviewd = 'N';
            }
            if (i.tTo.toAccount.signatory != null) {
              let signatoryIndicator: boolean = false;
              let xc: any[] = i.tTo.toAccount.signatory;
              for (let j = 0; j < xc.length; j++) {
                if (i.tTo.toAccount.signatory[j].isPrimary === true) {
                  i.ToPNumber = i.tTo.toAccount.signatory[j].tPerson.partyNumber
                  if (i.tTo.toAccount.signatory[j].tPerson.firstName == null) {
                    i.ToPName = i.tTo.toAccount.signatory[j].tPerson.middleName + " " + i.tTo.toAccount.signatory[j].tPerson.lastName;
                  } else if (i.tTo.toAccount.signatory[j].tPerson.middleName == null) {
                    i.ToPName = i.tTo.toAccount.signatory[j].tPerson.firstName + " " + i.tTo.toAccount.signatory[j].tPerson.lastName;
                  } else if (i.tTo.toAccount.signatory[j].tPerson.lastName == null) {
                    i.ToPName = i.tTo.toAccount.signatory[j].tPerson.firstName + " " + i.tTo.toAccount.signatory[j].tPerson.middleName;
                  } else {
                    i.ToPName = i.tTo.toAccount.signatory[j].tPerson.firstName + " " + i.tTo.toAccount.signatory[j].tPerson.middleName + " " + i.tTo.toAccount.signatory[j].tPerson.lastName;
                  }

                  signatoryIndicator = true;
                  break;
                }
              }
              if (!signatoryIndicator) {
                i.ToPName = " No primary signatory"
                i.ToPNumber = " No primary signatory"
              }
            }
          }
        }
        if (i.tToMyClient != null) {
          // 
          if (i.tToMyClient.toEntity != null) {
            i.TTypeTo = "to My client entity"
            i.ToPNumber = i.tToMyClient.toEntity.entityNumber
            i.ToPName = i.tToMyClient.toEntity.name;
            i.ToACCNumber = '';
            if (i.tToMyClient.toEntity.isReviewed) {
              i.TReviewd = 'Y';
            } else {
              i.TReviewd = 'N'
            }
          }
          else if (i.tToMyClient.toPerson != null) {
            i.TTypeTo = "to My client person"
            i.ToPNumber = i.tToMyClient.toPerson.partyNumber;
            if (i.tToMyClient.toPerson.firstName == null) {
              i.ToPName = i.tToMyClient.toPerson.middleName + " " + i.tToMyClient.toPerson.lastName;
            } else if (i.tToMyClient.toPerson.middleName == null) {
              i.ToPName = i.tToMyClient.toPerson.firstName + " " + i.tToMyClient.toPerson.lastName;
            } else if (i.tToMyClient.toPerson.lastName == null) {
              i.ToPName = i.tToMyClient.toPerson.firstName + " " + i.tToMyClient.toPerson.middleName;
            } else {
              i.ToPName = i.tToMyClient.toPerson.firstName + " " + i.tToMyClient.toPerson.middleName + " " + i.tToMyClient.toPerson.lastName;
            }
            // i.ToPName = i.tToMyClient.toPerson.firstName + " " + i.tToMyClient.toPerson.middleName + " " + i.tToMyClient.toPerson.lastName;
            i.ToACCNumber = '';
            if (i.tToMyClient.toPerson.isReviewed) {
              i.TReviewd = 'Y';
            } else {
              i.TReviewd = 'N';
            }
          }

          else if (i.tToMyClient.toAccount != null) {
            // 
            i.TTypeTo = "to My client account"
            i.ToACCNumber = i.tToMyClient.toAccount.account;
            if (i.tToMyClient.toAccount.isReviewed) {
              i.TReviewd = 'Y';
            } else {
              i.TReviewd = 'N'
            }
            let signatoryIndicator: boolean = false
            let xc: any[] = i.tToMyClient.toAccount.signatory
            if (xc != null) {
              // 

              for (let j = 0; j < xc.length; j++) {
                if (i.tToMyClient.toAccount.signatory[j].isPrimary === true) {
                  i.ToPNumber = i.tToMyClient.toAccount.signatory[j].tPerson.partyNumber
                  if (i.tToMyClient.toAccount.signatory[j].tPerson.firstName == null) {
                    i.ToPName = i.tToMyClient.toAccount.signatory[j].tPerson.middleName + " " + i.tToMyClient.toAccount.signatory[j].tPerson.lastName;
                  } else if (i.tToMyClient.toAccount.signatory[j].tPerson.middleName == null) {
                    i.ToPName = i.tToMyClient.toAccount.signatory[j].tPerson.firstName + " " + i.tToMyClient.toAccount.signatory[j].tPerson.lastName;
                  } else if (i.tToMyClient.toAccount.signatory[j].tPerson.lastName == null) {
                    i.ToPName = i.tToMyClient.toAccount.signatory[j].tPerson.firstName + " " + i.tToMyClient.toAccount.signatory[j].tPerson.middleName;
                  } else {
                    i.ToPName = i.tToMyClient.toAccount.signatory[j].tPerson.firstName + " " + i.tToMyClient.toAccount.signatory[j].tPerson.middleName + " " + i.tToMyClient.toAccount.signatory[j].tPerson.lastName;
                  } signatoryIndicator = true
                  break;
                }
              }
              if (!signatoryIndicator) {
                i.ToPName = " no primary signatory "
                i.ToPNumber = " no primary signatory "
              }
            }
          }
        }
        if (i.tTo == null && i.tToMyClient == null) {
          // 
          i.ToPNumber = '';
          i.TPName = '';
          i.ToACCNumber = '';
          i.TTypeTo = '';
          i.TReviewd = '';
        }
      })

      this.selection.clear();
    }
  }

  getBalanceAndDatOfBalance(account) {
    let balance = account.balance;
    let dateBalance = account.dateBalance;
    let oldBalanceObject: Balance = { balance: balance, dateBalance: dateBalance };
    let copy = Object.assign({}, oldBalanceObject);
    // console.log(" getBalanceAndDatOfBalance(account) -- old object", copy);
    return copy;
  }

  getTypeOfObject(object) {
    let type;
    if (object.partyNumber)
      type = 'person'
    else if (object.entityNumber)
      type = 'entity'
    else if (object.account)
      type = 'account'

    return type
  }

  // make all ids in transaction = 0 , usefull to backend to generate unique id .
  makeAllIdsZero(changedObjet) {
    console.log("========makeAllIdsZero======");

    if (changedObjet.entityNumber) {
      console.log("entity");
      changedObjet.id = 0;
      if (changedObjet.phones.phone.length > 0) {
        let phoneArray = changedObjet.phones.phone;
        for (let phone of phoneArray) {
          phone.id = 0
          console.log("entoty phone id == ", phone.id);

        }
      }
      if (changedObjet.addresses.address.length > 0) {
        let addressArray = changedObjet.addresses.address;
        console.log("entity adress array == ", addressArray);
        addressArray.forEach(address => {
          address.id = 0;
          console.log("address.id  ===", address.id);

        });
      }
      if (changedObjet.directorId.length > 0) {
        let directorArray = changedObjet.directorId;
        console.log("entity director array == ", directorArray);
        directorArray.forEach(director => {
          director.id = 0;
          console.log("director.id  ===", director.id);
          if (director.addresses.address.length > 0) {
            let directorAddressArray = director.addresses.address;
            console.log("director address array ===", directorAddressArray);
            for (let directorAddress of directorAddressArray) {
              console.log("director.address.id  ===", directorAddress.id);
              directorAddress.id = 0;
            }
          }
          if (director.identification.length > 0) {
            let identificationArray = director.identification;
            for (let identification of identificationArray) {
              identification.id = 0;
              console.log("director identification id ===", identification.id);
            }
          }
          if (director.phones.phone.length > 0) {
            let phoneArray = director.phones.phone;
            for (let phone of phoneArray) {
              phone.id = 0;
              console.log("director phone id ===", phone.id);
            }
          }
        });
      }

    }
    else if (changedObjet.partyNumber) {
      console.log("person");
      changedObjet.id = 0;
      if (changedObjet.addresses.address.length > 0) {
        let addressArray = changedObjet.addresses.address;
        console.log("person adress array == ", addressArray);
        addressArray.forEach(address => {
          address.id = 0;
          console.log("address.id  ===", address.id);
        });
      }
      if (changedObjet.phones.phone.length > 0) {
        let phoneArray = changedObjet.phones.phone;
        phoneArray.forEach(phone => {
          phone.id = 0;
          console.log("phone.id  ===", phone.id);
        });
      }
      if (changedObjet.identification.length > 0) {
        let identificationArray = changedObjet.identification;
        identificationArray.forEach(identification => {
          identification.id = 0;
          console.log("identification.id  ===", identification.id);
        });
      }

    }
    else if (changedObjet.account) {
      console.log("account");
      changedObjet.id = 0;
      if (changedObjet.signatory.length > 0) {
        let signatoryArray = changedObjet.signatory;
        console.log("signatoryArray == ", signatoryArray);
        signatoryArray.forEach(signatory => {
          signatory.id = 0;
          console.log("signatory.id  ===", signatory.id);
          if (signatory.tPerson) {
            console.log("signatory.tPerson.id == ", signatory.tPerson.id);

            signatory.tPerson.id = 0;

            if (signatory.tPerson.identification.length > 0) {
              let signatoryIdentificationArray = signatory.tPerson.identification;
              signatoryIdentificationArray.forEach(signatoryIdentification => {
                signatoryIdentification.id = 0;
              });
            }
            if (signatory.tPerson.addresses.address.length > 0) {
              let signatoryAddressArray = signatory.tPerson.addresses.address;
              signatoryAddressArray.forEach(signatoryAddress => {
                signatoryAddress.id = 0;
              });
            }
            if (signatory.tPerson.phones.phone.length > 0) {
              let signatoryPhoneArray = signatory.tPerson.phones.phone;
              signatoryPhoneArray.forEach(signatoryPhone => {
                signatoryPhone.id = 0;
              });
            }
          }
        });
      }
    }

    console.log("changed object ====", changedObjet);

    return changedObjet;
  }
  // this is the working one 
  CalculateTableInEditModeCase2(changdObject, changedObjectPartyNumber) {
    let changedObject = Object.assign({}, changdObject);
    let changedObj = this.makeAllIdsZero(changedObject);
    console.log("changed ob ject after zeroooo === ", changedObj);

    let numberP = changedObjectPartyNumber;

    if (this.arrayOfTransactions != undefined) {
      this.arrayOfTransactions.controls.forEach(trans => {

        if (trans.get('tFromMyClient').value) {
          let e = trans.get('tFromMyClient') as FormGroup;

          if (trans.get('tFromMyClient.fromEntity').value) {
            let p = trans.get('tFromMyClient.fromEntity.entityNumber').value
            if (numberP == p) {
              if (this.transactionRefNumber == trans.get('transactionnumber').value) {
                this.toaster.success("Transaction with reference number " + this.transactionRefNumber + " is now editd sucessfully", '', { timeOut: 3000 })
              }
              else {
                let entit = this.tentity.setEntityControl(changedObj);
                entit.get('id').setValue(0);
                e.removeControl('fromEntity');
                e.addControl('fromEntity', entit);

                // trans.get('tFromMyClient.fromEntity').setValue(changedObj);
              }
            }
          } else if (trans.get('tFromMyClient.fromPerson').value) {
            let p = trans.get('tFromMyClient.fromPerson.partyNumber').value
            if (numberP === p) {
              if (this.transactionRefNumber == trans.get('transactionnumber')) {
                this.toaster.success("Transaction with reference number " + this.transactionRefNumber + " is now editd sucessfully", '', { timeOut: 3000 })
              }
              else {
                let person = this.tperson.setPersonControl(changedObj);
                e.removeControl('fromPerson');
                e.addControl('fromPerson', person);
                // trans.get('tFromMyClient.fromPerson').setValue(changedObj);
              }
            }
          } else if (trans.get('tFromMyClient.fromAccount').value) {
            let p = trans.get('tFromMyClient.fromAccount.account').value;
            if (numberP == p) {
              if (this.transactionRefNumber == trans.get('transactionnumber').value) {
                this.toaster.success("Transaction with reference number " + this.transactionRefNumber + " is now editd sucessfully", '', { timeOut: 3000 })
              }
              else {
                let oldObj = this.getBalanceAndDatOfBalance(trans.get('tFromMyClient.fromAccount').value);

                let account = this.tAccount.setAccountControl(changedObj);
                // let ac = trans.get('tFromMyClient') as FormGroup
                e.removeControl('fromAccount');
                e.addControl('fromAccount', account);

                // trans.get('tFromMyClient.fromAccount').setValue(Object.assign({}, changedObj));
                if (oldObj) {
                  trans.get('tFromMyClient.fromAccount.balance').setValue(oldObj.balance);
                  trans.get('tFromMyClient.fromAccount.dateBalance').setValue(oldObj.dateBalance);
                }
              }
            }
            // // now if cascading is done in signatory person , lets cascade this person in all transactions
            // let signatoryFormArray = trans.get('tFromMyClient.fromAccount.signatory') as FormArray
            // console.log("signatoryFormArray==== " ,signatoryFormArray);

            // if (signatoryFormArray.controls.length > 0) {


            //   signatoryFormArray.controls.forEach(signatory => {
            //     if (signatory.get('tPerson').value) {
            //       console.log("if(signatory.get('tPerson').value");
            //       let partyNum = signatory.get('tPerson.partyNumber').value;

            //       if (numberP == partyNum) {
            //         console.log("if(numberP == partyNum){");
            //         signatory.get('tPerson').setValue(changedObj);
            //       }
            //     }
            //   });
            // }

          }
        }

        if (trans.get('tFrom').value) {
          let e = trans.get('tFrom') as FormGroup;

          if (trans.get('tFrom.fromEntity').value) {
            let p = trans.get('tFrom.fromEntity.entityNumber').value
            if (numberP == p) {
              if (this.transactionRefNumber == trans.get('transactionnumber').value) {
                this.toaster.success("Transaction with reference number " + this.transactionRefNumber + " is now editd sucessfully", '', { timeOut: 3000 })
              }
              else {
                let entit = this.tentity.setEntityControl(changedObj);
                entit.get('id').setValue(0);
                // let e = trans.get('tFrom') as FormGroup;
                e.removeControl('fromEntity');
                e.addControl('fromEntity', entit);
                // delete changedObj.accountNumber ;
                // trans.get('tFrom.fromEntity').setValue(changedObj);
              }
            }
          } else if (trans.get('tFrom.fromPerson').value) {
            let p = trans.get('tFrom.fromPerson.partyNumber').value
            if (numberP === p) {
              if (this.transactionRefNumber == trans.get('transactionnumber')) {
                this.toaster.success("Transaction with reference number " + this.transactionRefNumber + " is now editd sucessfully", '', { timeOut: 3000 })
              }
              else {
                let person = this.tperson.setPersonControl(changedObj);
                e.removeControl('fromPerson');
                e.addControl('fromPerson', person);
                // trans.get('tFrom.fromPerson').setValue(changedObj);
              }
            }
          } else if (trans.get('tFrom.fromAccount').value) {
            let p = trans.get('tFrom.fromAccount.account').value;

            if (numberP == p) {
              if (this.transactionRefNumber == trans.get('transactionnumber').value) {
                this.toaster.success("Transaction with reference number " + this.transactionRefNumber + " is now editd sucessfully", '', { timeOut: 3000 })
              }
              else {
                let oldObj = this.getBalanceAndDatOfBalance(trans.get('tFrom.fromAccount').value);
                // trans.get('tFrom.fromAccount').setValue(Object.assign({}, changedObj));
                let account = this.tAccount.setAccountControl(changedObj);
                e.removeControl('fromAccount');
                e.addControl('fromAccount', account);
                if (oldObj) {
                  trans.get('tFrom.fromAccount.balance').setValue(oldObj.balance);
                  trans.get('tFrom.fromAccount.dateBalance').setValue(oldObj.dateBalance);
                }
              }
            }
            // // now if cascading is done in signatory person , lets cascade this person
            // let signatoryFormArray = trans.get('tFrom.fromAccount.signatory') as FormArray

            // if (signatoryFormArray.controls.length > 0) {

            //   signatoryFormArray.controls.forEach(signatory => {
            //     if (signatory.get('tPerson').value) {
            //       console.log("if(signatory.get('tPerson').value");
            //       let partyNum = signatory.get('tPerson.partyNumber').value;

            //       if (numberP == partyNum) {
            //         console.log("if(numberP == partyNum){");
            //         signatory.get('tPerson').setValue(changedObj);
            //       }
            //     }
            //   });
            // }

          }
        }

        if (trans.get('tToMyClient').value) {
          let e = trans.get('tToMyClient') as FormGroup;

          if (trans.get('tToMyClient.toEntity').value) {
            let p = trans.get('tToMyClient.toEntity.entityNumber').value

            if (numberP == p) {
              if (this.transactionRefNumber == trans.get('transactionnumber').value) {
              }
              else {
                let entit = this.tentity.setEntityControl(changedObj);
                entit.get('id').setValue(0);
                e.removeControl('toEntity');
                e.addControl('toEntity', entit);
                // trans.get('tToMyClient.toEntity').setValue(changedObj);
              }
            }
          } else if (trans.get('tToMyClient.toPerson').value) {
            let p = trans.get('tToMyClient.toPerson.partyNumber').value
            if (numberP == p) {
              if (this.transactionRefNumber == trans.get('transactionnumber').value) {
                this.toaster.success("Transaction with reference number " + this.transactionRefNumber + " is now editd sucessfully", '', { timeOut: 3000 })
              }
              else {
                let person = this.tperson.setPersonControl(changedObj);
                e.removeControl('toPerson');
                e.addControl('toPerson', person);
                // trans.get('tToMyClient.toPerson').setValue(changedObj);
              }
            }
          } else if (trans.get('tToMyClient.toAccount').value) {

            let p = trans.get('tToMyClient.toAccount.account').value
            if (numberP == p) {
              if (this.transactionRefNumber == trans.get('transactionnumber').value) {
                this.toaster.success("Transaction with reference number " + this.transactionRefNumber + " is now editd sucessfully", '', { timeOut: 3000 })
              }
              else {
                let oldObj = this.getBalanceAndDatOfBalance(trans.get('tToMyClient.toAccount').value);
                // trans.get('tToMyClient.toAccount').setValue(Object.assign({}, changedObj));

                let account = this.tAccount.setAccountControl(changedObj);
                e.removeControl('toAccount');
                e.addControl('toAccount', account);

                if (oldObj) {

                  trans.get('tToMyClient.toAccount.balance').setValue(oldObj.balance);
                  trans.get('tToMyClient.toAccount.dateBalance').setValue(oldObj.dateBalance);
                }
              }
            }
            // // now if cascading is done in signatory person , lets cascade this person
            // let signatoryFormArray = trans.get('tToMyClient.toAccount.signatory') as FormArray

            // if (signatoryFormArray.controls.length > 0) {

            //   signatoryFormArray.controls.forEach(signatory => {
            //     if (signatory.get('tPerson').value) {
            //       console.log("if(signatory.get('tPerson').value");
            //       let partyNum = signatory.get('tPerson.partyNumber').value;

            //       if (numberP == partyNum) {
            //         console.log("if(numberP == partyNum){");
            //         signatory.get('tPerson').setValue(changedObj);
            //       }
            //     }
            //   });
            // }
          }
        }


        if (trans.get('tTo').value) {
          let e = trans.get('tTo') as FormGroup;

          if (trans.get('tTo.toEntity').value) {
            let p = trans.get('tTo.toEntity.entityNumber').value
            if (numberP == p) {
              if (this.transactionRefNumber == trans.get('transactionnumber').value) {
              }
              else {
                let entit = this.tentity.setEntityControl(changedObj);
                entit.get('id').setValue(0);
                e.removeControl('toEntity');
                e.addControl('toEntity', entit);
                // trans.get('tTo.toEntity').setValue(changedObj);
              }
            }
          } else if (trans.get('tTo.toPerson').value) {
            let p = trans.get('tTo.toPerson.partyNumber').value
            if (numberP == p) {
              if (this.transactionRefNumber == trans.get('transactionnumber').value) {
                this.toaster.success("Transaction with reference number " + this.transactionRefNumber + " is now editd sucessfully", '', { timeOut: 3000 })
              }
              else {
                let person = this.tperson.setPersonControl(changedObj);
                e.removeControl('toPerson');
                e.addControl('toPerson', person);
                // trans.get('tTo.toPerson').setValue(changedObj);
              }
            }
          } else if (trans.get('tTo.toAccount').value) {
            let p = trans.get('tTo.toAccount.account').value


            if (numberP == p) {
              // 
              // 
              if (this.transactionRefNumber == trans.get('transactionnumber').value) {
                // 
                this.toaster.success("Transaction with reference number " + this.transactionRefNumber + " is now editd sucessfully", '', { timeOut: 3000 })
              }
              else {
                let oldObj = this.getBalanceAndDatOfBalance(trans.get('tTo.toAccount').value);
                // trans.get('tTo.toAccount').setValue(Object.assign({}, changedObj));

                let account = this.tAccount.setAccountControl(changedObj);
                e.removeControl('toAccount');
                e.addControl('toAccount', account);

                if (oldObj) {

                  trans.get('tTo.toAccount.balance').setValue(oldObj.balance);
                  trans.get('tTo.toAccount.dateBalance').setValue(oldObj.dateBalance);
                  // 

                }
              }
            }
            // now if cascading is done in signatory person , lets cascade this person
            // let signatoryFormArray = trans.get('tTo.toAccount.signatory') as FormArray

            // if (signatoryFormArray.controls.length > 0) {

            //   signatoryFormArray.controls.forEach(signatory => {
            //     if (signatory.get('tPerson').value) {
            //       console.log("if(signatory.get('tPerson').value");
            //       let partyNum = signatory.get('tPerson.partyNumber').value;

            //       if (numberP == partyNum) {
            //         console.log("if(numberP == partyNum){");
            //         signatory.get('tPerson').setValue(changedObj);
            //       }
            //     }
            //   });
            // }

          }
        }

      }
      )
    }
  }

  // check for repeated transaction in add single case 
  checkForRepeatedTransactionNumber(transactions) {
    let numberList = [];
    if (transactions != undefined) {
      transactions.forEach(
        trans => {
          let number = trans.transactionnumber;
          numberList.push(number);
        }
      )
    }
    return numberList;
  }

  // get transaction number list of the incoming multiple array of transactions
  getTransactionNumbersListOfIncomingMultipleTransactions(array) {
    return array.map(function (x) {
      return x.transactionnumber;
    })
  }

  // get transaction number list of the existing array of transactions in the table
  getTransactionNumbersListOfExistingTransactionsIntheTable(formArray) {
    return formArray.value.map(function (x) {
      return x.transactionnumber;
    })
  }
  // check for repeated transactions in add multiple case 
  checkForExistingTransactionsOnMultipleCase(a, b) {
    var idsA = this.getTransactionNumbersListOfExistingTransactionsIntheTable(a);


    var idsB = this.getTransactionNumbersListOfIncomingMultipleTransactions(b);


    if (idsA.length > 0) {

      if (idsB.length > 0) {

        let count = 0;
        idsB.forEach(element => {
          if (idsA.includes(element)) {

            this.toaster.info("transaction" + element + "existed", '', { timeOut: 5000 })
          }
          else {

            let trans = this.getByRefNumber(b, element);

            a.push(trans);
          }
          count++

        });
      }
      else {

        this.toaster.info('No Transactions');
      }
    }
    else {



      b.forEach(t => {
        let tr = this.t_transaction.setTransaction(t);
        a.push(tr);


      })
    }
    this.calculateTableValues()

  }

  getByRefNumber(array, ref): FormGroup {
    let trans: FormGroup;
    array.forEach(element => {
      if (ref == element.transactionnumber) {
        trans = this.t_transaction.setTransaction(element);
      }
    });
    return trans;
  }

  // when click on add single transation
  onAddNewSingleTransaction() {
    if (this.status != 0) {
      this.toaster.warning("please close any opened tab");
      return;
    }
    this.status = 1;


    // this.createMode = true;
    // this.multiMode = false;
    // this.editTransactionMode = false;
    // this.cascadeMode = false;

    this.dataService.changeMessage4("addTrasnaction");
    this.addNewTab.emit(this.t_transaction.createTransactionControl());
  }
  // when click on add multiple transation
  onAddMultipleTransaction(): void {
    if (this.status != 0) {
      this.toaster.warning("please close any opened tab");
      return;
    }
    this.status = 2;
    const dialogRef = this.dialog.open(AddMultipleTransactionDialogComponent, {
      width: '900px',
      height: '600px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.status = 0;
      if (result != undefined && result.data) {
        this.checkForExistingTransactionsOnMultipleCase(this.arrayOfTransactions, result.data);
      }
    });
  }

  // the working one
  onEditSingleTransaction(transaction) {
    // this.showProgress = true ; 
    if (this.status != 0) {
      this.toaster.warning("please close any opened tab");
      return;
    }
    this.status = 3;
    let trasFormGr = this.t_transaction.setTransaction(transaction);
    console.log("trasFormGr =========", trasFormGr);

    this.indexOFTransaction = this.dataSource.data.indexOf(transaction);
    this.transactionRefNumber = transaction.transactionnumber;
    this.dataService.changeMessage4("editTrasnaction");
    this.editTransaction.emit({ transactionFormGroup: trasFormGr, transactionIndexInTable: this.indexOFTransaction });
    // this.showProgress = false ;
  }

  // when click on cascade transations
  onCascadeTransactions() {

    if (this.status != 0) {
      this.toaster.warning("please close any opened tab");
      return;
    }
    this.status = 4;
    let selectedTran = this.selection.selected;
    if (selectedTran.length > 0) {
      let indexes = [];
      this.dataSource.data.forEach((a, index) => {
        if (this.selection.selected.indexOf(a) >= 0) {
          indexes.push(index)
        }
      })
      this.cascadeTransactionsIndexes = indexes;
      let TransactionsToBeCascade = this.fb.array([])
      for (let i of indexes) {
        let transactionSelected = this.arrayOfTransactions.at(i) as FormGroup
        TransactionsToBeCascade.push(transactionSelected)
      }
      this.addNewTabForCascadingTransactions.emit(TransactionsToBeCascade);
    }
    else {
      this.toaster.warning("select at least one transaction to be cascaded")
      this.status = 0;
    }

  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  // checkboxLabel(row?: PeriodicElement1): string {
  //   if (!row) {
  //     return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
  //   }
  //   return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  // }

  deleteFieldValue(index) {
    // if (confirm("Want to delete?")) {
    let trs = this.dataSource.data[index];

    if (confirm('Confirm delete transaction ' + trs.transactionnumber)) {
      this.arrayOfTransactions.removeAt(index);
      let data = this.dataSource.data;
      data.splice(index, 1)
      this.dataSource.data = data;
      this.selection.clear();
    }
    // }
  }

  getRefss(array) {
    let refs = [];
    array.forEach(element => {
      let ref = element.transactionnumber;
      refs.push(ref);
    });
    return refs;
  }

  removeSelectedRows() {
    let selectedTran = this.selection.selected;
    if (selectedTran.length <= 0) {
      this.toaster.warning("select at least one transaction to be deleted");
    }
    else if (selectedTran.length > 0) {
      if (confirm("Want To delete?")) {

        if (selectedTran.length == this.arrayOfTransactions.value.length){
          this.arrayOfTransactions = this.fb.array([]);
        }

        else {

            for(let item of this.selection.selected) {
              let index: number = this.arrayOfTransactions.value.findIndex(d => 
                d === item
                );
              console.log("index ==== ", this.arrayOfTransactions.value.findIndex(d => d === item));
              if(index == -1){
                this.toaster.warning('please , try deleting again ...','',{timeOut:3000});
                break ; 
              }
              else 
                this.arrayOfTransactions.removeAt(index);
            };
        //   let indecies = [];
        //   for (let trans1 of selectedTran) {
        //     let index = this.dataSource.data.indexOf(trans1)
        //     // let index = this.arrayOfTransactions.value.indexOf(trans);
        //     indecies.push(index);
        //   }
        //   console.log("indecies ===",indecies);
  
        //   indecies.sort().reverse();
        //   indecies.forEach(
        //     i =>{
        //       console.log("i" , i);
              
        //       this.arrayOfTransactions.removeAt(i);
        //       indecies.sort();
        //     }
        //   )
        }
        console.log("arra of transaction after deletion ==", this.arrayOfTransactions);

        this.calculateTableValues();
        this.selection.clear();
        // }
      }
    }
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();



    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  ValidateTransactions() {
    this.util.changeReportAsRead(true)
    let indeces = this.getIndecesOfSelected()
    if (indeces.length > 0) {

      let selectedTranasctions = this.arrayOfTransactions.controls.filter((i, index) => indeces.includes(index))
      let validationErrors = []

      for (let x of selectedTranasctions) {
        let f: notValidElement[] = []
        let NestedSectionsErrors: notValidElementWithMessageOnly[] = []

        this.util.validateFormGroup2(x as FormGroup, f, NestedSectionsErrors)
        validationErrors.push(f)

        f = []

      }
      indeces.forEach((a, index) => {
        let paths: any[] = validationErrors[index].map((item: notValidElement) => (item.path.replace(`transaction.${a}.`, "")))
        this.selection.selected[index].Validation_Messages = paths
        this.selection.selected[index].Validation = this.arrayOfTransactions.at(a).valid ? 'Y' : 'N'
      })
      //  


    }
  }
  validateAllTransactions() {
    let validationErrors = []
    let validationComplexErrors = []
    for (let transaction of this.arrayOfTransactions.controls) {
      // 
      let sectionsValidationErrors: notValidElementWithMessageOnly[] = []
      let fieldValidationErrors: notValidElement[] = []
      let NestedSectionsErrors: notValidElementWithMessageOnly[] = []
      let fromNotFound = this.util.checkTransactionFrom(transaction as FormGroup)
      let toNotFound = this.util.checkTransactionTo(transaction as FormGroup)
      if (fromNotFound) {
        sectionsValidationErrors.push(fromNotFound)
      }
      if (toNotFound) {

        sectionsValidationErrors.push(toNotFound)
      }
      this.util.validateFormGroup2(transaction as FormGroup, fieldValidationErrors, NestedSectionsErrors)

      validationErrors.push(fieldValidationErrors)
      // this.validateList(transaction, sectionsValidationErrors)
      sectionsValidationErrors.push(...NestedSectionsErrors)
      validationComplexErrors.push(sectionsValidationErrors)
      fieldValidationErrors = []
      sectionsValidationErrors = []
    }

    let previousDataSource = this.dataSource.data
    previousDataSource.forEach((a, index) => {
      a.Validation_Messages = []
      let paths: any[] = validationErrors[index].map((item: notValidElement) => (item.path.replace(`transaction.${index}.`, "")))
      let errorMessages = validationComplexErrors[index].map((item: notValidElementWithMessageOnly) => this.formatNestedSectionMessage(item))
      a.Validation_Messages = [...errorMessages, ...paths]
      a.Validation = a.Validation_Messages.length == 0 ? 'Y' : 'N'
    })
    this.dataSource.data = previousDataSource
  }
  getIndecesOfSelected(): number[] {
    let selectedTran = this.selection.selected;
    let indexes: number[] = [];

    if (selectedTran.length > 0) {


      this.dataSource.data.forEach((a, index) => {
        if (selectedTran.indexOf(a) >= 0) {
          indexes.push(index)


        }
      })
    }
    return indexes
  }

  validateList(tranControl: AbstractControl, errorMessages: notValidElementWithMessageOnly[]) {

    UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromPerson.addresses.address`, 'Add At least one Address to tFromMyClient.fromPerson', `tFromMyClient.fromPerson.addresses.address`, errorMessages)
    UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromPerson.phones.phone`, 'Add At least one Phone to tFromMyClient.fromPerson', `tFromMyClient.fromPerson.phones.phone`, errorMessages)
    UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromPerson.identification`, 'Add At least one Identification  to tFromMyClient.fromPerson', `tFromMyClient.fromPerson.identification`, errorMessages)

    UtilService.FindAndFormMessage(tranControl, `tToMyClient.toPerson.addresses.address`, 'Add At least one Address to tToMyClient.toPerson', `tToMyClient.toPerson.addresses.address`, errorMessages)
    UtilService.FindAndFormMessage(tranControl, `tToMyClient.toPerson.phones.phone`, 'Add At least one Phone to tToMyClient.toPerson', `tToMyClient.toPerson.phones.phone`, errorMessages)
    UtilService.FindAndFormMessage(tranControl, `tToMyClient.toPerson.identification`, 'Add At least one Identification to tToMyClient.toPerson', `tMyClient.toPerson.identification`, errorMessages)
    UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromEntity.addresses.address`, 'Add At least one Address to tFromMyClient.fromEntity', `tFromMyClient.fromEntity.addresses.address`, errorMessages)
    UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromEntity.phones.phone`, 'Add At least one Phone to tFromMyClient.fromEntity', `tFromMyClient.fromEntity.phones.phone`, errorMessages)
    UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromEntity.directorId`, 'Add At least one Director to tFromMyClient.fromEntity', `tFromMyClient.fromEntity.directorId`, errorMessages)
    UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromAccount.signatory`, 'Add At least one Signatory to tFromMyClient.fromAccount', `tFromMyClient.fromAccount.signatory`, errorMessages)


  }
  setActions() {
    this.addTransactionAction = this.auth.has_Capabilities(Actions.addTransaction)
    this.addMultipleTransactionAction = this.auth.has_Capabilities(Actions.addMultipleTransaction)
    this.deleteTransactionAction = this.auth.has_Capabilities(Actions.deleteTransaction)
    this.viewTransactionAction = this.auth.has_Capabilities(Actions.viewTransaction)
    this.cascadeTransactionAction = this.auth.has_Capabilities(Actions.cascadeTransaction)


  }


  checkForExistingTransactionsOnMultipleCase1(a, b) {
    var idsA = this.getTransactionNumbersListOfExistingTransactionsIntheTable(a);
    // 

    var idsB = this.getTransactionNumbersListOfIncomingMultipleTransactions(b);
    // 



    if (idsA.length > 0) {

      if (idsB.length > 0) {
        console.log("(idsB.length > 0 )");
        let count = 0;
        let list = [];
        b.forEach(element => {


          if (idsA.includes(element.transactionnumber)) {
            console.log("if (idsA.includes(element)) element : ", element.transactionnumber);
            list.push(element);

          }
          else {

            let trans = this.getByRefNumber(b, element);

            a.push(trans);
          }
          count++

        });
        this.toaster.info('Transactions with numbers' + list.toString().split('') + ' already existed in the table', '', { timeOut: 3000 });
      }
      else {

        this.toaster.info('No Transactions');
      }
    }
    else {

      b.forEach(t => {
        let tr = this.t_transaction.setTransaction(t);
        a.push(tr);
      })
    }

  }
  formatNestedSectionMessage(error: notValidElementWithMessageOnly) {
    let formatteName = error.name.replace(/transaction\.\d\./, '')
    let message = `${formatteName}->${error.message}`
    return message
  }

}

// removeSelectedRows() {
  //   let selectedTran = this.selection.selected;
  //   if (selectedTran.length <= 0) {
  //     this.toaster.warning("select at least one transaction to be deleted");
  //   }
  //   else if (selectedTran.length > 0) {
  //     if (confirm("Want To delete?")) {
  //       let indexes = [];
  //       this.dataSource.data.forEach((a, index) => {
  //         if (selectedTran.indexOf(a) >= 0) {
  //           indexes.push(index)
  //           
  //         }
  //       })
  //       indexes = indexes.sort()
  //       indexes = indexes.reverse()
  //       for (let i of indexes) {
  //         
  //         this.arrayOfTransactions.removeAt(i);
  //       }
  //       
  //       this.calculateTableValues();
  //       this.selection.clear();
  //     }
  //   }
  // }

  //  when click on edit transation
  // onEditSingleTransaction1(index) {
  //   if (this.status != 0) {
  //     this.toaster.info("please close any opened tab");
  //     return;
  //   }
  //   this.status = 3;


  //   // this.editTransactionMode = true;
  //   // this.cascadeMode = false;
  //   // this.createMode = false;
  //   // this.multiMode = false;

  //   this.indexOFTransaction = index;
  //   console.log("transaction to be edited in transaction details : ", this.arrayOfTransactions.at(index).value);
  //   this.transactionRefNumber = this.arrayOfTransactions.at(index).value.transactionnumber;

  //   this.dataService.changeMessage4("editTrasnaction");

  //   this.editTransaction.emit({ transactionFormGroup: this.arrayOfTransactions.at(index), transactionIndexInTable: index });
  // }