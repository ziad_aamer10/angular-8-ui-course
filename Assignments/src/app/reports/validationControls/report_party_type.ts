import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { tEntity } from "./t_entity";
import { t_person } from "./t_person";
import { t_account } from "./t_account";

export class t_report_party_type {
tentity:tEntity=new tEntity()
t_person:t_person=new t_person()
t_account:t_account=new t_account()
    constructor(public fb: FormBuilder) {

    }
    create_report_party_type(): FormGroup {
        return this.fb.group({
            significance: new FormControl(null, [Validators.max(10)]),
            reason: [null, [Validators.maxLength(4000)]],
            comments: [null, [Validators.maxLength(4000)]],
            entity: new FormControl(),
            account: new FormControl(),
            person: new FormControl(),
            id: new FormControl()
        })
    }
    set_report_party_type(reportPartyType): FormGroup {
        return this.fb.group({
            significance: new FormControl(reportPartyType.significance),
            reason: [reportPartyType.reason, [Validators.maxLength(4000)]],
            comments: [reportPartyType.comments, [Validators.maxLength(4000)]],
            entity: reportPartyType.entity? this.tentity.setEntityControl(reportPartyType.entity):new FormControl(),
            account: reportPartyType.account?this.t_account.setAccountControl(reportPartyType.account):new FormControl(),
            person: reportPartyType.person?this.t_person.setPersonControl( reportPartyType.person):new FormControl(),
            id: new FormControl(reportPartyType.id)
        })
    }
}