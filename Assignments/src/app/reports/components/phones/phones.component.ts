import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { FormArray, FormGroup, FormBuilder } from '@angular/forms';
import { t_phone } from '../../validationControls/t_phone';
import { UtilService } from 'src/app/util.service';
import { LookupClass } from '../../lockups/Lookups';

@Component({
  selector: 'app-phones',
  templateUrl: './phones.component.html',
  styleUrls: ['./phones.component.css'],

})
export class PhonesComponent implements OnInit, OnChanges {
  // added by abdelrahman to assign values from lookups
 
  contact_values = LookupClass.contact_type;
  communication_values = LookupClass.communication_type;

inputPhone: FormGroup;
@Input()  PhonesArray: FormArray;
 public t_phone: t_phone = new t_phone(this.fb);
  constructor(public fb: FormBuilder) {
    this.inputPhone = this.createPhone();

   }
addFieldValue() {



if (this.inputPhone.invalid) {
  Object.keys(this.inputPhone.controls).forEach(key => {
    this.inputPhone.get(key).markAsDirty();
    this.inputPhone.get(key).markAsTouched();
  });
} else {
    this.addPhone();

    this.PhonesArray.at(this.PhonesArray.length - 1).setValue(this.inputPhone.value);
    this.inputPhone.reset();
}
}
deleteFieldValue(index) {
  if (confirm('You are going to delete phone')) {
 this.PhonesArray.removeAt(index);
}
}
ngOnChanges(changes: SimpleChanges) {
  for (const propName in changes) {

    const chng = changes[propName];

    const cur  = chng.currentValue;
    if (propName == 'PhonesArray') {

      this.PhonesArray = cur;


    }

  }
}
  ngOnInit() {


  }

createPhone(): FormGroup {
  return this.t_phone.createPhone();
}
addPhone() {

  const x = this.createPhone();
  this.PhonesArray.push(x);
  }

}

