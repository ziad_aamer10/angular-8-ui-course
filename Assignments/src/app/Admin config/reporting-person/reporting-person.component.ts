import { Subscription } from 'rxjs';
import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, ValidationErrors } from '@angular/forms';
import { t_person } from 'src/app/reports/validationControls/t_person';

import { UtilService } from 'src/app/util.service';
import { t_person_my_client } from 'src/app/reports/validationControls/t_person_my_client';
import { ReportingPersonService } from 'src/app/services/reporting-person.service';
import { ToastrService } from 'ngx-toastr';
import { notValidElementWithMessageOnly, notValidElement } from 'src/app/reports/models/notValidElement';


@Component({
  selector: 'app-reporting-person',
  templateUrl: './reporting-person.component.html',
  styleUrls: ['./reporting-person.component.css']
})
export class ReportingPersonComponent implements OnInit, OnDestroy {

  subscriptionGetReportingPerson: Subscription;
  subscriptionUpdateReportingPerson: Subscription;
  filterBy = 'AML Head';
  BankInfoForm = new FormGroup({
    // 'firstName':new FormControl('',Validators.required),

  });
  personFormControl: FormGroup;
  // t_person: t_person = new t_person();
  t_person_my_client: t_person_my_client = new t_person_my_client();
  notValidElement: string[] = [];
  notValidSection: notValidElementWithMessageOnly[] = [];

  @Output() saveEvent = new EventEmitter();
  constructor(private utilservice: UtilService, private reporingService: ReportingPersonService, private toaster: ToastrService) { }

  ngOnInit() {
    this.personFormControl = this.t_person_my_client.createPersonGroup();
    this.setEmailValidation();
  }
  save(element: FormGroup) {

    if (element.invalid) {
      Object.keys(element.controls).forEach(key => {
        element.get(key).markAsDirty();
        element.get(key).markAsTouched();
      });

    } else {


      // this.validateFormGroup()
      this.saveEvent.emit(element);
    }

  }
  close() {


  }
  validate() {
    this.notValidElement = [];
    this.notValidSection = [];
    this.utilservice.validateFormGroup(this.personFormControl, this.notValidElement)
    this.utilservice.validateFormGroup2(this.personFormControl, [], this.notValidSection);
    console.log("this.personFormControl",this.personFormControl);
    
    if (this.personFormControl.valid) {
      this.toaster.success('No Validation Error');
      return;

    }
    else {
      // for (const key of this.notValidSection) {
      //   this.toaster.info(key.message,key.name,{timeOut:4000})

      // }
    }
  }
  private scrollToAnchor(anchor: string): boolean {
    const element = document.querySelector(`[formControlName=${anchor}]`);
    this.utilservice.closeExpansionPanel = true;
    if (element) {
      element.scrollIntoView();

      return true;
    }
    return false;
  }

  setEmailValidation(){
    // to make email required as reporting person email must me valid , and its from type myclient person which has no validation on email
    this.personFormControl.get('email').setValidators(Validators.required);
  }

  getReportingPersonInfo() {
    this.personFormControl = this.t_person_my_client.createPersonGroup();
    this.setEmailValidation()

    this.subscriptionGetReportingPerson = this.reporingService.getReportingPerson(this.filterBy).subscribe(data => {
      if (data) {


        this.toaster.success('Reporting Person Info returned successfully');
        this.personFormControl = this.t_person_my_client.setPersonControl(data);
        this.setEmailValidation();

      } else {
        this.toaster.warning('No Data Returned');
      }
    }, error => {
      this.toaster.error(error);
    });
  }
  updateReportingPerson() {
    if (!this.personFormControl.valid) {
      this.toaster.warning('Please fill all mandatory fields before saving');
      return;
    }
    const personValue = Object.assign({}, this.personFormControl.value);
    personValue.reportingPersonType = this.filterBy;
    this.subscriptionUpdateReportingPerson = this.reporingService.updateReportingPerson(personValue).subscribe(data => {

      this.toaster.success('Reporting Person is saved successfully');


    }, error => {
      this.toaster.error(error);
    });
  }
  resetReportingPerson() {
    this.personFormControl = this.t_person_my_client.createPersonGroup();
    this.setEmailValidation();
  }

  ngOnDestroy() {
    if (this.subscriptionGetReportingPerson != undefined)
      this.subscriptionGetReportingPerson.unsubscribe();
    if (this.subscriptionUpdateReportingPerson != undefined)
      this.subscriptionUpdateReportingPerson.unsubscribe();
  }
}
