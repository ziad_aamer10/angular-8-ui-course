import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatTableDataSource, MatDialog, MatCheckbox } from '@angular/material';
import { SelectionModel, DataSource } from '@angular/cdk/collections';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { PersonDialogComponent } from '../person-dialog/person-dialog.component';
import { UtilService } from 'src/app/util.service';
import { LookupClass } from '../../lockups/Lookups';
import { EntityService } from '../../services/entity.service';
import { t_entity_my_client } from '../../validationControls/t_entity_my_client';
import { tEntity } from '../../validationControls/t_entity';
import { ToastrService } from 'ngx-toastr';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';
import { Actions } from '../../Actions/action';
import { t_person } from '../../validationControls/t_person';
import { t_person_my_client } from '../../validationControls/t_person_my_client';


export interface Entity_Directors {

  partyNumber,
  name
}



@Component({
  selector: 'app-entity-page',
  templateUrl: './entity-page.component.html',
  styleUrls: ['./entity-page.component.css']
})
export class EntityPageComponent implements OnInit, OnChanges {
  @Input() entityType?: string

  entityIds: FormArray
  phonesControl: FormArray
  addressesControl: FormArray
  @Output() changeEntity = new EventEmitter()
  @Output() saveEvent = new EventEmitter();
  displayedDColumns: string[] = ['select', 'partyNumber', 'name', 'edit', 'delete'];
  dataSource = new MatTableDataSource([]);
  tentity: tEntity = new tEntity()
  t_EntityMyClient: t_entity_my_client = new t_entity_my_client()
  tPerson: t_person = new t_person()
  tMyClientPerson: t_person_my_client = new t_person_my_client()
  selection = new SelectionModel<Entity_Directors>(true, []);
  Incorporation_values = LookupClass.legal_form_type;
  country_value = LookupClass.country;
  @Input() accountNumber?: string
  @Input() entityForm: FormGroup
  @Input() disableEntity: boolean;
  cascadeEntityAction: boolean = false;
  deleteDirectorAction: boolean = false;
  viewDirectorAction: boolean = false;

  constructor(public dialog: MatDialog, public fb: FormBuilder, private utilsevice: UtilService,
    private entityService: EntityService, private toaster: ToastrService,
    private dataService: SharingDataServieService, private auth: AuthService) { }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let chng = changes[propName];
      let cur = chng.currentValue;
      let prev = chng.previousValue;
      if (propName == 'entityForm') {


        this.entityForm = cur
        this.setControl()
      }
    }
  }
  setControl() {
    this.entityIds = this.entityForm.get('directorId') as FormArray
    this.dataSource.data = this.entityIds.value
    this.phonesControl = this.entityForm.get('phones').get('phone') as FormArray
    this.addressesControl = this.entityForm.get("addresses").get("address") as FormArray
    this.changeEntity.emit(this.entityForm)
    this.businessClosedChanges()
  }
  ngOnInit() {
    this.setControl()
    this.setActions()
    if (this.disableEntity) {
      this.disabledform(this.entityForm)
    }
  }
  disabledform(group: FormGroup): void {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.get(key);
      if (control instanceof FormGroup) {
        this.disabledform(control);
        control.disable()
        if (control.get('activity')) {
        }
      } else {
        control.disable()
      }
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  getDirectorControl() {
    if (!this.IsMyEntity()) {
      return this.tPerson.createDirector()
    }
    else {
      return this.tMyClientPerson.createDirector()
    }
  }
  addDirector() {
    let entityNumber = this.entityForm.get('entityNumber').value
    const dialogRef = this.dialog.open(PersonDialogComponent, {
      height: '800px',
      data: { entityNumber: entityNumber, 'director': this.getDirectorControl() }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.entityIds.push(result)
        this.dataSource.data = this.entityIds.value
      }
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  saveEntity() {
    let entityToBeSaved = Object.assign({}, this.entityForm.value)
    let entityToBeSavedWithoutAccountNumber = Object.assign({}, this.entityForm.value);
    this.utilsevice.changeDateFormat(entityToBeSaved)
    if (this.accountNumber) {
      entityToBeSaved.accountNumber = this.accountNumber
    }
    else {
      entityToBeSaved.accountNumber = null
    }
    console.log("this.entityForm.value======= ", this.entityForm.value);
    console.log("entityToBeSaved ==========",entityToBeSavedWithoutAccountNumber);
    this.saveEvent.emit(entityToBeSavedWithoutAccountNumber);
    if(this.entityForm.valid)
    this.entityService.saveSEntityToSource(entityToBeSaved).subscribe(data => {
      if (data) {
        this.toaster.success("Entity saved successfully")
      }
      else {
        this.toaster.success("Error in Entity save ")
      }

      }, error => {


        this.toaster.error(error.error.message)
      })
    else {
      this.utilsevice.validateFormGroup2(this.entityForm, [], [])
      this.toaster.error("Entity not valid!!")
    }
  }

  get entityCtrls() {

    return this.entityForm.controls
  }
  getEntityById() {
    let id = this.entityForm.get('entityNumber').value
    this.entityService.getEntityById(id).subscribe(data => {
      if (data) {
        this.toaster.success(`Entity with id ${id} is found `)
        this.entityForm = this.tentity.setEntityControl(data)
        this.changeEntity.emit(this.entityForm)
        this.setControl()
      }
      else {
        this.toaster.error(`No Entity with id ${id} is found`)
      }

    }, error => {
      this.toaster.error(error.message)
    })

  }
  editDirector(index, row) {
    let director = this.entityIds.at(index);
    let entityNumber = this.entityForm.get('entityNumber').value
    const dialogRef = this.dialog.open(PersonDialogComponent, {
      height: '800px',
      data: {
        'director': director,
        entityNumber: entityNumber
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined && result instanceof FormGroup) {

        this.entityIds.removeAt(index)
        this.entityIds.push(result)
        this.dataSource.data = this.entityIds.value
      }
    });
    // 

  }
  getSEntityByEntityNumber() {
    let entityNumber = this.entityForm.get('entityNumber').value
    this.entityService.getSEntityByEntityNumber(entityNumber).subscribe(data => {
      if (data) {
        console.log("data ====" , data);
        

        this.entityForm = this.tentity.setEntityControl(data)
        this.toaster.success(`Entity with entity number is ${entityNumber} found`)
        this.setControl()
      }
      else {
        this.toaster.warning(`No Entity with Number ${entityNumber} is found`)
      }


      this.entityForm = this.tentity.setEntityControl(data)
      this.setControl()
    },
      error => {
        this.toaster.error(error.message)
      })
  }
  deleteDirector(index, row) {
    if (confirm("You are gonna delete director")) {
      this.entityIds.removeAt(index);
      this.dataSource.data = this.entityIds.value
    }



  }
  setActions() {
    this.cascadeEntityAction = this.auth.has_Capabilities(Actions.cascadeEntity)
    this.deleteDirectorAction = this.auth.has_Capabilities(Actions.deleteDirector)
    this.viewDirectorAction = this.auth.has_Capabilities(Actions.viewDirector)
  }
  IsMyEntity() {
    return this.entityType ? true : false
  }
  businessClosedChanges(){
    let dateBusinessClosed=this.entityForm.get('dateBusinessClosed')
    this.entityForm.get('businessClosed').valueChanges.subscribe(data=>{
      
      if(data){      
        dateBusinessClosed.setValidators(Validators.required)
      } else{
        dateBusinessClosed.clearValidators()
      }
      dateBusinessClosed.updateValueAndValidity()
      
    })
  }
  falseValue = null
  trueValue = true;
  
  checkboxChange(checkbox: MatCheckbox, checked: boolean) {
    console.log('checkboxChange');
    if( !checked ){
      this.entityForm.controls.businessClosed.setValue(null)
    }
  }
}
