import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {RecipesComponent} from "./recipes.component";
import {RecipesStartComponent} from "./recipes-start/recipes-start.component";
import {RecipeEditComponent} from "./recipe-edit/recipe-edit.component";
import {RecipeDetailComponent} from "./recipe-detail/recipe-detail.component";
import {RecipeResolverService} from "../shared/recipe-resolver.service";
import {AuthGuard} from "../auth/auth.guard";

const routes: Routes = [
  {path: '', canActivate: [AuthGuard], component: RecipesComponent, children:[
      {path:'', component: RecipesStartComponent },
      {path:'new', component: RecipeEditComponent },
      {path:':id', component: RecipeDetailComponent, resolve: [RecipeResolverService] },
      {path:':id/edit', component: RecipeEditComponent, resolve: [RecipeResolverService] }
    ]
  },

]

@NgModule({
  imports:[RouterModule.forChild(routes)],
  exports:[
    RouterModule
  ]
})
export class RecipesRoutingModule { }
