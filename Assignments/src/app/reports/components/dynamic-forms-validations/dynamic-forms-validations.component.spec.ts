import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicFormsValidationsComponent } from './dynamic-forms-validations.component';

describe('DynamicFormsValidationsComponent', () => {
  let component: DynamicFormsValidationsComponent;
  let fixture: ComponentFixture<DynamicFormsValidationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicFormsValidationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormsValidationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
