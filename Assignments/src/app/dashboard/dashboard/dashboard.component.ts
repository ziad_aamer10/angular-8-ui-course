import { Subscription } from 'rxjs';
import { ReportService } from 'src/app/reports/services/report.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { HttpClient } from '@angular/common/http';
import { ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';



export interface ReportTypeStatistics {
  SAR: number;
  STR: number;
  AIF: number;
  totalReports: number;
}

export interface Top10 {
  reportNumber: any;
  reportCategory: any;
  reportCreatedDate: any;
  reportUserLockId: any;
}

export interface ReportStatusStatistics {
  MLCUModification: number;
  WaitingHeadDecision: number;
  REOPEN: number;
  ReturnToMaker: number;
  CLOSED: number;
  ReturnToChecker: number;
  MAKING: number;
  CHECKING: number;
  SUBMITTED: number;
  XMLGenerated: number;
  AIFRequested: number;
  totalReports: number;
}


export interface LastTop10Basic {
  id: any;
  reportCode:any;
  reportUserLockId:any;
  reportCreatedDate: any;
  reportStatusCode:any;
}


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  visibleSeries: boolean = true;
  subscriptionReportTypeStat: Subscription;
  subscriptionReportStatusStat: Subscription;
  subscriptionTop10: Subscription;
  subscriptionUserTop10: Subscription;

  constructor(private http: HttpClient, private _router: Router, private reportService: ReportService) {
  }
  // exampleDatabase: ExampleHttpDatabase | null;
  displayedColumns1: string[] = ['id', 'reportCode', 'reportCreatedDate', 'reportUserLockId', 'Action'];

  displayedColumns2: string[] = ['id', 'reportCode', 'reportCreatedDate', 'reportStatusCode', 'Action'];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  reportChart: Chart;
  statusChart: Chart;

  dataSource1: any = [];
  selection1 = new SelectionModel<LastTop10Basic>(true, []);
  @ViewChild(MatPaginator) paginator1: MatPaginator;
  @ViewChild(MatSort) sort1: MatSort;

  dataSource2: any = []
  selection2 = new SelectionModel<LastTop10Basic>(true, []);
  @ViewChild(MatPaginator) paginator2: MatPaginator;
  @ViewChild(MatSort) sort2: MatSort;

  setVisible(statusObject: ReportStatusStatistics) {
    if (statusObject.WaitingHeadDecision) {
      this.visibleSeries = false;
    }
    if (statusObject.XMLGenerated) {
      this.visibleSeries = false;
    }
    if (statusObject.WaitingHeadDecision) {
      this.visibleSeries = false;
    }
    if (statusObject.WaitingHeadDecision) {
      this.visibleSeries = false;
    }
    if (statusObject.WaitingHeadDecision) {
      this.visibleSeries = false;
    }
    if (statusObject.WaitingHeadDecision) {
      this.visibleSeries = false;
    }
    if (statusObject.WaitingHeadDecision) {
      this.visibleSeries = false;
    }
  }


  ngOnInit() {

    // report type statistics
    this.subscriptionReportTypeStat = this.reportService.getReportTypeStatistics().subscribe(typeStatistics => {
      this.reportChart = new Chart({
        chart: {
          events: {
            // load: function() {
            //   this.series[0].points.forEach(function(p) {
            //     if(!p.y) {
            //       p.graphic.destroy();
            //     }
            //   });
            // }
          },
          type: "pie",
          options3d: {
            enabled: true,
            alpha: 45,
            beta: 26
          }
        },

        title: {
          text: "Reports Types",
          style: {
            // color: '#000',
            // font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
            fontWeight: '700'
          }
        },
        tooltip: {
          headerFormat: '',
          pointFormat: '<span style="color:{point.color}">●</span> <b> {point.name}</b><br/>' +
            ' Percentage : <b>{point.percentage:.1f} %</b><br/>' +
            ' Count : <b>{point.z}</b><br/>'
        },
        legend: {
          itemStyle: {
            font: '9pt Trebuchet MS, Verdana, sans-serif',
            color: 'black'
          },
          itemHoverStyle: {
            color: 'gray'
          },
          bubbleLegend: {
            enabled: true
          }
        },
        plotOptions: {
          pie: {
            shadow: false,
            center: ['50%', '50%'],
            size: '100%',
            innerSize: '2%',
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              // alignTo: 'connectors',
              connectorShape: 'fixedOffset',
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style:
              {
              }
            },
            showInLegend: true
          }
        },
        credits: {
          enabled: false
        },
        series: [{
          type: "pie",
          name: 'Reports',
          colorByPoint: true,
          data: [
            {
              name: 'STR',
              y: Number(((typeStatistics.STR / typeStatistics.totalReports) * 100).toFixed(2)),
              z: typeStatistics.STR,
              // color: '#1400B9'
            },
            {
              name: 'SAR',
              y: Number(((typeStatistics.SAR / typeStatistics.totalReports) * 100).toFixed(2)),
              z: typeStatistics.SAR,
              color: '#C72FE3'
            },
            {
              name: 'AIF',
              y: Number(((typeStatistics.AIF / typeStatistics.totalReports) * 100).toFixed(2)),
              z: typeStatistics.AIF,
              // color: '#96040F'
            }
          ],
        }],
      });

    })
    // Report Status Statistics
    this.subscriptionReportStatusStat = this.reportService.getReportStatusStatistics().subscribe(statusStatistics => {

      console.log("statusStatistics", statusStatistics);


      if (statusStatistics.CLOSED == undefined)
        statusStatistics.CLOSED = 0;
      if (statusStatistics.MAKING == undefined)
        statusStatistics.MAKING = 0;
      if (statusStatistics.ReturnToChecker == undefined)
        statusStatistics.ReturnToChecker = 0;
      if (statusStatistics.ReturnToMaker == undefined)
        statusStatistics.ReturnToMaker = 0;
      if (statusStatistics.CHECKING == undefined)
        statusStatistics.CHECKING = 0;
      if (statusStatistics.SUBMITTED == undefined)
        statusStatistics.SUBMITTED = 0;
      if (statusStatistics.REOPEN == undefined)
        statusStatistics.REOPEN = 0;
      if (statusStatistics.XMLGenerated == undefined)
        statusStatistics.REOPEN = 0;
      if (statusStatistics.AIFRequested == undefined)
        statusStatistics.AIFRequested = 0;
      if (statusStatistics.MLCUModification == undefined || statusStatistics.MLCUModification == 0)
        statusStatistics.MLCUModification = 0;


      // if(statusStatistics.WaitingHeadDecision){
      //   this.visibleSeries = false ; 
      // }
      this.statusChart = new Chart({
        chart: {
          events: {
            // load: function() {
            //   this.series[0].points.forEach(function(p) {
            //     if(!p.y) {
            //       console.log("gggggggggggg" , p.y);

            //       p.graphic.destroy();
            //     } 
            //   });
            // }
            // load: function () {
            //   console.log("this.series[0].data", this.series[0].data);
            //   this.series[0].data.forEach((point, index) => {
            //     if (isNaN(point.y)) {
            //       // point.chart.
            //       // point.options.visible=false;
            //       // v=false;
            //       // point.visible= true;
            //       // this.series[0].points[index].visible = false;
            //       console.log("point.y", point.y);
            //       console.log("point", point);
            //     }
            //   })
            // }
          },
          type: "pie",
          options3d: {
            enabled: true,
            alpha: 45,
            beta: 26
          }
        },
        title: {
          text: "Reports Status",
          style: {
            fontWeight: '700'
          }
        },
        tooltip: {
          headerFormat: '',
          pointFormat: '<span style="color:{point.color}">●</span> <b> {point.name}</b><br/>' +
            'Percentage : <b>{point.percentage:.1f} %</b><br/>' +
            'Count : <b>{point.z}</b><br/>'
        },
        legend: {
          itemStyle: {
            font: '9pt Trebuchet MS, Verdana, sans-serif',
            color: 'black'
          },
          itemHoverStyle: {
            color: 'gray'
          }
        },
        plotOptions: {
          pie: {
            shadow: false,
            center: ['50%', '50%'],
            size: '100%',
            innerSize: '2%',
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              alignTo: 'connectors',
              // connectorShape: 'fixedOffset',
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style: {
              }
            },
            showInLegend: true,
            borderWidth: 0
          }
        },
        credits: {
          enabled: true
        },
        series: [{
          type: "pie",
          name: 'Reports',
          colorByPoint: true,
          data: [
            //
            {
              name: 'Waiting Head Descision',
              y: (Number)(((statusStatistics.WaitingHeadDecision / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.WaitingHeadDecision,
              // visible: true
              // drilldown: 'ChecWaiting Head Descisionking',
              // color:'#F445FF'

            },
            {
              name: 'Closed',
              // y: 0 ,
              y: (Number)(((statusStatistics.CLOSED / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.CLOSED,
              // showInLegend:false,
              // visible: v
              // drilldown: 'Closed',
              // color:'#010390'
            },
            {
              name: 'Xml Generated',
              y: (Number)(((statusStatistics.XMLGenerated / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.XMLGenerated,
              // visible: true
              // drilldown: 'Xml Generated',
              // color:'#'
            },
            {
              name: 'Submitted',
              y: (Number)(((statusStatistics.SUBMITTED / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.SUBMITTED,
              // visible: true
              // drilldown: 'Submitted',
              // color:'#A4428E'
            },
            {
              name: 'Making',
              y: (Number)(((statusStatistics.MAKING / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.MAKING,
              // visible: true,
              // drilldown: 'Making',
              color: '#0091FC'
            },
            {
              name: 'Return To Maker',
              y: (Number)(((statusStatistics.ReturnToMaker / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.ReturnToMaker,
              // visible: true,
              // drilldown: 'Return To Maker',
              color: '#01630D'
            },
            {
              name: 'MLCU Modification',
              y: (Number)(((statusStatistics.MLCUModification / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.MLCUModification,
              // visible: true
              // drilldown: 'MLCU Modification',
              // color:'#00F0FC'
            },
            {
              name: 'AIF Request',
              y: (Number)(((statusStatistics.AIFRequested / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.AIFRequested,
              // visible: true
              // drilldown: 'AIF Request',
              // color:'#A9FFF3'
            },
            {
              name: 'Checking',
              y: (Number)(((statusStatistics.CHECKING / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.CHECKING,
              // drilldown: 'Checking',
              // color:'#75D93A'
            },
            {
              name: 'Return To Checker',
              y: (Number)(((statusStatistics.ReturnToChecker / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.ReturnToChecker,
              // visible: true
              // drilldown: 'Return To Checker',
              // color:'#44C4E3'
            },
            {
              name: 'Reopen',
              y: (Number)(((statusStatistics.REOPEN / statusStatistics.totalReports) * 100).toFixed(2)),
              z: statusStatistics.REOPEN,
              // visible: true,
              // drilldown: 'Reopen',
              color: '#FCF800'
            }
          ],
        }],


      });
    })

    this.subscriptionTop10 = this.reportService.getLastTop10().subscribe(
      (top10) => {
        let arr: LastTop10Basic[] = top10;
        var result = arr.map(report => ({ id: report.id, reportCode: report.reportCode , reportCreatedDate: report.reportCreatedDate , reportUserLockId: report.reportUserLockId }));
        this.dataSource1 = new MatTableDataSource();
        this.dataSource1.data = result;
      })
    let userLockedId = localStorage.getItem('userName');

    this.subscriptionUserTop10 = this.reportService.getLastTop10ByLockedUser(userLockedId).subscribe(top10ByUser => {
      let arr: LastTop10Basic[] = top10ByUser;
      var result = arr.map(report => ({ id: report.id, reportCode: report.reportCode , reportCreatedDate: report.reportCreatedDate , reportStatusCode: report.reportStatusCode }));
      this.dataSource2 = new MatTableDataSource();
      this.dataSource2.data = result;
    })

  }


  // isAllSelected1() {
  //   const numSelected = this.selection1.selected.length;
  //   const numRows = this.dataSource1.data.length;
  //   return numSelected === numRows;
  // }

  // isAllSelected2() {
  //   const numSelected = this.selection2.selected.length;
  //   const numRows = this.dataSource2.data.length;
  //   return numSelected === numRows;
  // }


  // masterToggle1() {
  //   this.isAllSelected1() ?
  //     this.selection1.clear() :
  //     this.dataSource1.data.forEach(row => this.selection1.select(row));
  // }

  // masterToggle2() {
  //   this.isAllSelected2() ?
  //     this.selection2.clear() :
  //     this.dataSource2.data.forEach(row => this.selection2.select(row));
  // }


  onReportClickTable1(id, type): void {
    if (type == 'SAR')
      this._router.navigate([environment.sarEditRoute, id])
    else if (type == 'STR') {
      this._router.navigate([environment.strEditRoute, id]);
    }
    else if (type == 'AIF') {
      this._router.navigate([environment.aifEditRoute, id]);
    }
  }

  onReportClickTable2(id, type): void {
    if (type == 'SAR')
      this._router.navigate([environment.sarEditRoute, id])
    else if (type == 'STR') {
      this._router.navigate([environment.strEditRoute, id]);
    }
    else if (type == 'AIF') {
      this._router.navigate([environment.aifEditRoute, id]);
    }
  }

  deleteReport1(index) {
    let ds = this.dataSource1.data
    ds.splice(index, 1)
    this.dataSource1.data = ds
  }

  deleteReport2(index) {
    let ds = this.dataSource2.data
    ds.splice(index)
    this.dataSource2.data = ds
  }

  applyFilter1(filterValue: string) {
    this.dataSource1.filter = filterValue.trim().toLowerCase();

  }

  applyFilter2(filterValue: string) {
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }

  ngOnDestroy() {
    this.subscriptionReportStatusStat.unsubscribe();
    this.subscriptionReportTypeStat.unsubscribe();
    this.subscriptionTop10.unsubscribe();
    this.subscriptionUserTop10.unsubscribe();
  }
}
