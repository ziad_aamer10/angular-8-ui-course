
// Category Data
export class CategoryList{
    categoryName:string
}
export interface Data{
    key:string
    value:string
}
export class CategoryClass{


public categoryList:CategoryList[] =[
    {categoryName:'Customer Type'},//ct
    {categoryName:'Suspicious Pattern'},//sp
    {categoryName:'Customer Information Update'},//ciu
    {categoryName:'Customer Classification'},//cc
    {categoryName:'Suspicious Type'},//st
    {categoryName:'Detiction Mechanism'},//dm
    {categoryName:'Predicate Crime'}//pc
]


public customerTypeData:Data[]=[
//customer type
    {key:'CTC',value:'Company'},
    {key:'CTE',value:'Employee'},
    {key:'CTFI',value:'Financial Institution'},
    {key:'CTGO',value:'Government Entity'},
    {key:'CTIP',value:'International PEP'},
    {key:'CTHC',value:'Is connected to a high risk country'},
    {key:'CTHLA',value:'Is connected to a high-risk local area'},
    {key:'CTLP',value:'Local PEP'},
    {key:'CTM',value:'Minor'},
    {key:'CTNR',value:'Nonresident'},
    {key:'CTNE',value:'Not Employed'},
    {key:'CTNP',value:'NPO'},
    {key:'CTSE',value:'Self Employed'},
    {key:'CTST',value:'Student'},
    {key:'CTTL',value:'Trust or Other Form of Legal Arrangements'},
]
public customerClassificationData:Data[]=[
//Customer Classification
    {key:'CCH',value:'High'},
    {key:'CCL',value:'Low'},
    {key:'CCM',value:'Medium'},
    {key:'CCN',value:'Not Classified Yet'},
]

public customerInformatonUpdateData:Data[]=[
//Customer Information Update  
    {key:'CIUS',value:'Never Updated (Relationship has not started yet)'},
    {key:'CIUN',value:'Never updated during the last 5 years (Relationship started before a five years period)'},
    {key:'CIUR',value:'Never updated during the last 5 years (Relationship started within a five years period)'},
    {key:'CIUM',value:'Updated during the last 12 months'},
    {key:'CIUT',value:'Updated during the last 3 years'},
    {key:'CIUF',value:'Updated during the last 5 years'},
]

public detectionMethodData:Data[]=[
// Detiction Method
    {key:'DMAR',value:'Automated reports'},
    {key:'DMLA',value:'Legal Action Initiated in relation to suspect'},
    {key:'DMCM',value:'Manual monitoring via compliance officer'},
    {key:'DMER',value:'Manual reporting via other employee'},
    {key:'DMNM',value:'News on media'},
]

public predictCrimeData:Data[]=[
//Predicate Crime    
    {key:'PCC',value:'Counterfeiting and piracy of products'},
    {key:'PCAN',value:'Crimes committed against antiquities.'},
    {key:'PCBR',value:'Crimes of bribery'},
    {key:'PCP',value:'Crimes of debauchery and prostitution.'},
    {key:'PCD',value:'Crimes of deception and breach of faith'},
    {key:'PCF',value:'Crimes of falsification'},
    {key:'PCFR',value:'Crimes of forgery of banknotes and coins'},
    {key:'PCFD',value:'Crimes of fraud and deceit.'},
    {key:'PCFT',value:'Crimes of Funds theft and usurpation'},
    {key:'PCHI',value:'Crimes of hijacking means of transport and detaining individuals.'},
    {key:'PCNS',value:'Crimes of narcotics and psychotropic substances (planting, manufacturing,smuggling, exporting and trafficking)'},
    {key:'PCE',value:'Crimes of public Funds embezzlement, transgression and peculation'},
    {key:'PCT',value:'Crimes of terrorism'},
    {key:'PCTF',value:'Crimes of terrorism financing.'},
    {key:'PCW',value:'Crimes of weaponry, ammunition and explosives (unlicensed importation, tradingand manufacturing)'},
    {key:'PCRC',value:'Customs related crimes'},
    {key:'PCCC',value:'Cyber Crimes'},
    {key:'PCEN',value:'Environmental crimes related to dangerous wastes and materials.'},
    {key:'PCIG',value:'Illicit gains'},
    {key:'PCIN',value:'Insider trading'},
    {key:'PCMM',value:'Market Manipulation'},
    {key:'PCDC',value:'Not declaring or False declaration of Funds and bearer assets'},
    {key:'PCSA',value:'Offences & misdemeanors to the security of the government (abroad)'},
    {key:'PCSD',value:'Offences & misdemeanors to the security of the government (domestic)'},
    {key:'PCTX',value:'Tax Crimes'},
    {key:'PCTS',value:'Trafficking of human beings and migrant smuggling'},
    {key:'PCTC',value:'Transnational organized crimes'},
    {key:'PCU',value:'Unidentified'},
]

public suspicionPatternData:Data[]=[
//Suspicious Pattern
    {key:'SPCCW',value:'Cash deposits are directly followed by withdrawals without an apparent reason'},
    {key:'SPCCO',value:'Customer uses cash frequently or in large amounts while his/her field of business usually uses other payment methods (e.g. checks, ..)'},
    {key:'SPCEL',value:'Depositing amounts in credit card account highly exceeding its credit limit'},
    {key:'SPCLD',value:'Exchange of currency into large denomination notes'},
    {key:'SPCLM',value:'Exchanging large amounts of currency in a manner inconsistent with customer business'},
    {key:'SPCCT',value:'Large cash deposits followed by wire transfers not consistent with customer business'},
    {key:'SPCLC',value:'Large cash deposits without an apparent reason'},
    {key:'SPCAT',value:'Large cash deposits/withdrawals using ATMs in a way inconsistent with customer information'},
    {key:'SPCSW',value:'Large sudden withdrawal (often in cash) without an apparent reason'},
    {key:'SPCMC',value:'Multiple cash deposits without an apparent reason'},
    {key:'SPCMW',value:'Multiple cash withdrawals without an apparent reason'},
    {key:'SPCRD',value:'Repeated deposits or transfers to an account without an apparent reason'},
    {key:'SPCCF',value:'Requesting credit against collaterals issued by foreign financial institutions without any apparent reason'},
    {key:'SPCCA',value:'Requesting credit against collaterals owned by others'},
    {key:'SPCSD',value:'Use of small denominations SPF/Credit card purchases involve'},
    {key:'SPFEX',value:'Credit card purchases involve stores selling chemicals known to be used in making explosives.'},
    {key:'SPFVT',value:'Customer s behavior exhibiting violent tendencies'},
    {key:'SPFEB',value:'Customer s behavior indicating adherence to extremist beliefs or ideas'},
    {key:'SPFSC',value:'Purchase of expensive or sophisticated communication devices and information technology using credit card'},
    {key:'SPFCE',value:'Purchase of heavy camping equipment using credit card'},
    {key:'SPFFT',value:'Suspected to involve foreign terrorist fighters'},
    {key:'SPFKT',value:'The customer is known to be related to terrorist group or organization'},
    {key:'SPFSM',value:'Unusual financial use of social media (e.g. crowdfunding)'},
    {key:'SPGLP',value:'Beneficiary of an LG requests paying its value after a short period of its issuance or without an apparent reason'},
    {key:'SPGRI',value:'Customer repeatedly issued LGs in a way inconsistent with his/her business'},
    {key:'SPGCI',value:"Opening LGs using collaterals inconsistent with customer's business"},
    {key:'SPISP',value:'Single premium life insurance policy'},
    {key:'SPLGI',value:"Imported/ exported goods are inconsistent with customer's business"},
    {key:'SPLUT',value:'LC includes unusual terms without any apparent reason'},
    {key:'SPLCU',value:"Opening LCs using collaterals inconsistent with customer's business"},
    {key:'SPLAL',value:'The transaction involves the use of repeatedly amended or extended letters of credit without an apparent reason'},
    {key:'SPLLI',value:"The use of LCs or other trade finance instruments in a way inconsistent with customer's business"},
    {key:'SPNDN',value:'Repeated large deposits or transfers to the accounts of a newly founded or an unknown NPO'},
    {key:'SPNNP',value:'Transactions conducted on the account of an NPO which are inconsistent with the pattern and size of the organization’s purpose or business'},
    {key:'SPNHR',value:'Transfers from/to an NPO connected with a terrorism or terrorism financing high risk country'},
    {key:'SPOAA',value:'Accounts abandoned by the customer after being used to conduct several transactions or transactions in large sums of money.'},
    {key:'SPOAS',value:'Activity conducted over a short time frame'},
    {key:'SPTAR',value:'Alternative money remittance or underground banking'},
    {key:'SPOCC',value:'Customer contact details changes frequently without an apparent reason, or are incorrect or continuously non-operational.'},
    {key:'SPORH',value:'Customer is a resident or connected to a high-risk jurisdiction.'},
    {key:'SPOCS',value:'Customer presenting obviously counterfeit documents'},
    {key:'SPORU',value:'Customer repeatedly refused to update his/her CDD information and documents'},
    {key:'SPOCO',value:'Customer requests not to receive correspondents from the financial institution'},
    {key:'SPCTL',value:'Customer requests the transfer value of a loan/ credit facilities to other financial institutions without any apparent reason.'},
    {key:'SPOCH',value:'Customer submitted cheques for collection inconsistent with his/her business or without apparent relation with the issuer.'},
    {key:'SPOIT',value:'Customer’s appearance does not match with his/her inflated transactions.'},
    {key:'SPOIN',value:'Customer’s income does not match with his/her inflated transactions.'},
    {key:'SPTSA',value:'Customers (natural person) acquire several accounts for the purpose of receiving and/or sending transfers in relatively small values'},
    {key:'SPOCZ',value:'Customers accessing their e-banking facilities or sending orders from an IP address within a conflict zone or address not associated with CDD records.'},
    {key:'SPTIA',value:'Customer s account was used as an intermediary account to transfer money between two parties'},
    {key:'SPOAW',value:'Customers opening an account in regions away from their work address or place of residence without an apparent reason'},
    {key:'SPOUI',value:'Customers shows unusual interest in the reporting requirements, transactions thresholds or record-keeping requirements'},
    {key:'SPOAO',value:'Customers suspected to conduct transactions or act on behalf of another individual'},
    {key:'SPOAC',value:'Customers who deliberately avoid the direct contact with the financial institution'},
    {key:'SPODL',value:'Daily using the maximum permitted limit of a payment card'},
    {key:'SPOEQ',value:'Debit and credit transactions are almost equal in a short period of time'},
    {key:'SPOER',value:'Early redemption of loan/ credit facilities by the customer or other parties'},
    {key:'SPOFS',value:'Frequent purchase / sale of specific securities with no economic reason'},
    {key:'SPOHJ',value:'Involved high risk jurisdiction'},
    {key:'SPOSC',value:'Issuance of several payment cards without an apparent reason'},
    {key:'SPOTE',value:'Manipulations for evading taxes'},
    {key:'SPORI',value:'New customers who are reluctant to provide needed information'},
    {key:'SPFBT',value:'Purchase of precious metals or stones'},
    {key:'SPOPS',value:'Purchase of precious metals or stones'},
    {key:'SPTLC',value:'Receiving large transfers accompanied by instructions to be paid in cash'},
    {key:'SPOPR',value:'Related to a person that was previously reported to the FIU'},
    {key:'SPTTC',value:'Repeated or large transfers following cash deposits'},
    {key:'SPTRH',value:'Repeated or large transfers from high risk countries'},
    {key:'SPOTC',value:'Repeated requests to issue traveller cheques or bearer negotiable instruments in a manner inconsistent with customer business'},
    {key:'SPOPC',value:'Repeated transactions using payment cards'},
    {key:'SPOS',value:'Structuring'},
    {key:'SPODO',value:'Sudden use of dormant accounts'},
    {key:'SPOSI',value:'Suspicion is related to a person listed in sanction lists (international)'},
    {key:'SPOSL',value:'Suspicion is related to a person listed in sanction lists (local)'},
    {key:'SPOPM',value:'The customer is subject to provisional measures'},
    {key:'SPOC',value:'The customer shows no interest in the costs of the transaction'},
    {key:'SPOR',value:'The involved customer has a bad reputation all over the media'},
    {key:'SPORE',value:'The use of real estate finance / purchases'},
    {key:'SPOBT',value:'Transactions below reporting threshold'},
    {key:'SPOHA',value:'Transactions conducted in a high-risk local area'},
    {key:'SPOEC',value:'Transactions lacking an economic purpose'},
    {key:'SPOUD',value:'Ultimate disposition of funds is unknown'},
    {key:'SPOUT',value:'Unexplained termination of the business relationship or contract'},
    {key:'SPOUS',value:'Unknown source of funds'},
    {key:'SPOUB',value:"Unusual financial behaviour by one of the financial institution's employees."},
    {key:'SPOGA',value:'Unusual gambling behavior'},
    {key:'SPOSD',value:'Unusual use of a safe deposit box'},
    {key:'SPOMM',value:'Unusual use of derivatives or money market instruments'},
    {key:'SPOFM',value:'Use of a family member account without an apparent reason'},
    {key:'SPODN',value:'Use of accountants/ lawyers'},
    {key:'SPODC',value:'Use of different currencies without an apparent reason'},
    {key:'SPOOF',value:'Use of offshore entities/ arrangements/ accounts'},
    {key:'SPOPB',value:'Use of personal account for business'},
    {key:'SPOSH',value:'Use of shell company'},
    {key:'SPOVC',value:'Use of virtual currencies'},
    {key:'SPTTR',value:'Transfers from/to customer without an apparent relation with the other party'},
]

public suspicionTypeData:Data[]=[
//Suspicious Type    
    {key:'STFP',value:'Financing the Polfiration of Weapons of Mass Destruction'},
    {key:'STML',value:'Money Laundering'},
    {key:'STPC',value:'Proceeds of crimes'},
    {key:'STTF',value:'Terrorist Financing'},
]

constructor(){

}

}