import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { DialogOverviewExampleDialog } from 'src/app/component/dialog-overview-example/dialog-overview-example.component';
import { MatDialog } from '@angular/material';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  }),
};
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userName: string = localStorage.getItem('name');
  constructor(private http: HttpClient, private router: Router) { }

  login(credentials) {

    return this.http.post<UserResponse>(environment.projectUrl + '/aml/auth',
      // return this.http.poprost<UserResponse>('http://localhost:8080/aml/auth',
      JSON.stringify(credentials), httpOptions);
  }

  logout() {

    localStorage.removeItem('token');
    localStorage.removeItem('name');
    localStorage.removeItem('id');
    localStorage.removeItem('Report');
    localStorage.removeItem('ReportAct');
    localStorage.removeItem('ReportStr');
    localStorage.removeItem('ReportST');
    localStorage.removeItem('ReportAif');
    localStorage.removeItem('ReportAi');

    // localStorage.removeItem('authorities');

    this.userName = null;
    this.router.navigate(['/login']);
  }

  isLoggedIn() {

    const token = localStorage.getItem('token');

    if (!token) {
      return null;
    }
    // if (localStorage.getItem('Report') != null || localStorage.getItem('ReportStr') != null|| localStorage.getItem('ReportAif') != null) {
    //   const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
    //     width: '250px',
    //   });
    //   dialogRef.afterClosed().subscribe(result => {
    //
    //   });
    // } else {
    //   localStorage.removeItem('Report');
    //   localStorage.removeItem('ReportStr');
    //   localStorage.removeItem('ReportST');
    //   localStorage.removeItem('ReportAct');
    //   localStorage.removeItem('ReportAif')
    //   localStorage.removeItem('ReportAi')
    //   // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
    //   // this.router.navigate([returnUrl || '/']);
    // }
    return true;
    // const helper = new JwtHelperService();

    // return !helper.isTokenExpired(token);
  }

  get currentUser() {
    const token = localStorage.getItem('token');
    if (!token) {
      return null;
    }
    const decodedToken = new JwtHelperService().decodeToken(token);
    return decodedToken.userName || '';
  }
  has_Capabilities(capability): boolean {
    const token = localStorage.getItem('token');
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    const authorities: any[] = decodedToken.authorities;
if(authorities)
    return authorities.includes(capability);
    else
    return false
  }

  getGroup(user) {

    const token = localStorage.getItem('token');
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    const authorities = decodedToken.groups;
    user = authorities;

    if (user === 'GOAML_MANAGER') {
      return 3;
    } else if (user === 'GOAML_CHECKER') {
      return 2;
    } else if (user === 'GOAML_MAKER') {
      return 1;
    } else {
      return 0;
    }


  }

  getCapabilities(): any[] {
    // if (environment.mockLogin) {
    //   return true
    // }
    const token = localStorage.getItem('token');

    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    const authorities: any[] = decodedToken.authorities
    return authorities;
    // for (var index in authorities) {
    //   if (authorities[index] =='GOAML_unlockCheckerReport'){
    //     return 'manager';
    //   }
    //   else if (authorities[index] =='GOAML_unlockMakerReport'){
    //     return 'checker';
    //   }
    // }
    // return false
  }
}
interface UserResponse {
  login: string;
  token: string;
}
