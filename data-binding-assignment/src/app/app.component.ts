import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  numbersList= [0];

  createNextNumberComponent(event: number){
    this.numbersList.push(event);
  }

  isEven(x: number){
    return (x%2)==0;
  }

}
