import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Data, CategoryClass } from '../../lockups/report-indiators-lockup';
import { FormGroup, FormControl, Validators, FormArray, ValidationErrors } from '@angular/forms';
import { UtilService } from 'src/app/util.service';




@Component({
  selector: 'app-report-indicatiors',
  templateUrl: './report-indicatiors.component.html',
  styleUrls: ['./report-indicatiors.component.css'],
  // encapsulation: ViewEncapsulation.None

})
export class ReportIndicatiorsComponent implements OnInit {
  indicatorsExpansion:boolean[]=[false,false,false,false,false,false,false]
  @Input() reportController: FormGroup

  // added by abdelrahman to assign values from lookups
  CategoryObject = new CategoryClass();
  customerTypeData_values = this.CategoryObject.customerTypeData;
  customerClassificationData_values = this.CategoryObject.customerClassificationData;
  customerInformatonUpdateData_values = this.CategoryObject.customerInformatonUpdateData;
  detectionMethodData_values = this.CategoryObject.detectionMethodData;
  predictCrimeData_values = this.CategoryObject.predictCrimeData;
  suspicionPatternData_values = this.CategoryObject.suspicionPatternData;
  suspicionTypeData_values = this.CategoryObject.suspicionTypeData;
  notValidElement: string[] = []
  reportIndicators: FormGroup

  validateFormControl(name, x: FormControl) {


    const controlErrors: ValidationErrors = x.errors;
    x.markAsDirty()
    x.markAsTouched()

    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(keyError => {
     
        this.notValidElement.push(name)
      });
    }
  }

  validateFormGroup(x: FormGroup) {
    Object.keys(x.controls).forEach(key => {
      if (x.get(key) instanceof FormGroup) {
        this.validateFormGroup(x.get(key) as FormGroup)
      }
      else if (x.get(key) instanceof FormArray) {
        this.validateFormArray(x.get(key) as FormArray)
      }

      else if (x.get(key) instanceof FormControl) {
        this.validateFormControl(key, x.get(key) as FormControl)
      }

    });
  }
  validateFormArray(formArray: FormArray) {
    Object.keys(formArray.controls).forEach(key => {
      if (formArray.get(key) instanceof FormGroup) {
        this.validateFormGroup(formArray.get(key) as FormGroup)
      }
      else if (formArray.get(key) instanceof FormControl) {
        this.validateFormControl(key, formArray.get(key) as FormControl)

      }
      else if (formArray.get(key) instanceof FormArray) {
        this.validateFormArray(formArray.get(key) as FormArray)
      }
    })


  }
  validate(x: FormGroup) {
    
    this.notValidElement = []
for(let x in this.indicatorsExpansion){
  
  this.indicatorsExpansion[x]=true
}

    
    this.validateFormGroup(x)

  }
  private scrollToAnchor(anchor: string): boolean {
    const element = document.querySelector(`[formControlName=${anchor}]`);
    this.utilservice.closeExpansionPanel = true
    if (element) {
      element.scrollIntoView()

      return true;
    }
    return false;
  }
  clearValidationErrors() {
    this.notValidElement = []
  }


  constructor(private _router: Router, public utilservice: UtilService) { }

  ngOnInit() {

    this.reportIndicators = this.reportController.get('reportIndicators').get("indicator") as FormGroup
    this.reportIndicators.valueChanges.subscribe(data=>{
  
    })
   
    //  this.reportIndicators=this.reportController.get('reportIndicators').get("indicator") as FormGroup

  }
  f(type) {
    return type == 'Customer Classification'
  }
  getValueByKey(list: Data[], key) {
    for (let x of list) {

      if (x.key == key)
        return x.value

    }
    return null;
  }
  test() {
    let x = ["STFP", "CTC", "CTE", "DMER", "PCBR", "SPCLM"]
    this.reportIndicators.setValue({ "CT": [], "CIU": [], "CC": "", "DM": [], "PC": [], "SP": [], "ST": [] })
 

    for (let a of x) {

      if (this.customerTypeData_values.map(b => b.key).includes(a)) {
        let valueByKey = this.getValueByKey(this.customerTypeData_values, a)
        if (valueByKey) {
         

          this.reportIndicators.get("CT").setValue(a)

        }

      }
      else if (this.customerClassificationData_values.map(b => b.key).includes(a)) {
        let valueByKey = this.getValueByKey(this.customerClassificationData_values, a)
        if (valueByKey) {

          this.reportIndicators.get("CC").setValue(a)

        }

      }
      else if (this.customerInformatonUpdateData_values.map(b => b.key).includes(a)) {

        let valueByKey = this.getValueByKey(this.customerInformatonUpdateData_values, a)
        if (valueByKey) {
              this.reportIndicators.get("CIU").setValue(a)
        }

      }
      // 
      else if (this.detectionMethodData_values.map(b => b.key).includes(a)) {

        let valueByKey = this.getValueByKey(this.detectionMethodData_values, a)
        if (valueByKey) {
          let value: string[] = this.reportIndicators.get("DM").value
          value.push(a)

          this.reportIndicators.get("DM").setValue(value)
        }

      }
      // 

      else if (this.predictCrimeData_values.map(b => b.key).includes(a)) {
        //PC
        let valueByKey = this.getValueByKey(this.predictCrimeData_values, a)

        if (valueByKey) {
          let value: string[] = this.reportIndicators.get("PC").value
          value.push(a)

          this.reportIndicators.get("PC").setValue(value)
        }
      }
      else if (this.suspicionPatternData_values.map(b => b.key).includes(a)) {
        //SP
        let valueByKey = this.getValueByKey(this.suspicionPatternData_values, a)

        if (valueByKey) {
          let value: string[] = this.reportIndicators.get("SP").value
          value.push(a)

          this.reportIndicators.get("SP").setValue(value)
        }
      }
      else if (this.suspicionTypeData_values.map(b => b.key).includes(a)) {
        //ST
        let valueByKey = this.getValueByKey(this.suspicionTypeData_values, a)

     
        if (valueByKey) {
          this.reportIndicators.get("ST").setValue(a)
    }
      }








    }
  }

}
