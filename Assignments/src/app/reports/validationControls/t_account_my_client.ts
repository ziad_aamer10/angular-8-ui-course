import { FormGroup, Validators, FormBuilder, AbstractControl, FormArray } from "@angular/forms";
import { t_account } from "./t_account";
import { utils } from "protractor";
import { UtilService } from "src/app/util.service";
import { t_signatory } from "./t_signatory";
import { t_entity_my_client } from "./t_entity_my_client";
let fb = new FormBuilder();
export class t_account_my_client {
    UtilService=new UtilService();
    constructor() { }
    account: t_account = new t_account()
    t_signatory: t_signatory = new t_signatory();
    t_entityMyClient: t_entity_my_client = new t_entity_my_client()

    createAccountControl(): FormGroup {
        let f: FormGroup = this.account.createAccountControl()
        this.addAccountValidators(f);
        return f
    }

    setAccountControl(account) {
        let accountv: FormGroup = this.account.setAccountControl(account);
        this.addAccountValidators(accountv)
        return accountv
    }
    addAccountValidators(account: FormGroup) {
        account.controls["institutionName"].setValidators(Validators.required)
        account.controls["nonBankInstitution"].setValidators(Validators.required)
        account.controls["branch"].setValidators(Validators.required)
        account.controls["account"].setValidators(Validators.required)
        account.controls["currencyCode"].setValidators(Validators.required)
        account.controls["personalAccountType"].setValidators(Validators.required)
        account.controls["signatory"].setValidators([Validators.required, UtilService.minLengthArray(1)])
        account.controls["opened"].setValidators(Validators.required)
        account.controls["balance"].setValidators(Validators.required)
        account.controls["dateBalance"].setValidators(Validators.required)
        account.controls["statusCode"].setValidators(Validators.required)
        let signatories = account.get('signatory') as FormArray
        
        if (signatories && signatories.length > 0){
        
            for (let signatory of signatories.controls) {
                this.t_signatory.addSignatoryValidator(signatory as FormGroup)

            }
            signatories.updateValueAndValidity()
        } 
        let tEntity = account.get('tEntity') as FormGroup
        if (tEntity&&tEntity.value) {
            this.t_entityMyClient.addEntityValidators(tEntity )
            tEntity.updateValueAndValidity()
        }
account.updateValueAndValidity()
let Fields=['institutionName','nonBankInstitution','branch','account','currencyCode','personalAccountType',
        'signatory','opened','balance','dateBalance','statusCode','signatory','tEntity']
        this.UtilService.updateFormGroupValidity(account,Fields)
    }


}