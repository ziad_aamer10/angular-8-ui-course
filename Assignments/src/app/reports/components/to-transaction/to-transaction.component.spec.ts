import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToTransactionComponent } from './to-transaction.component';

describe('ToTransactionComponent', () => {
  let component: ToTransactionComponent;
  let fixture: ComponentFixture<ToTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
