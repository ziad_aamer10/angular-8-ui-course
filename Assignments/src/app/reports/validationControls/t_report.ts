import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { activity } from './activity';
import { Indicator } from './indicators';
import { t_transaction } from './t_transaction';
import { ReportService } from "../services/report.service";
import { UtilService } from 'src/app/util.service';

const fb = new FormBuilder();

export class t_report {
  [x: string]: any;
    activity: activity = new activity(fb);
    transaction: t_transaction = new t_transaction(fb);
    indicators: Indicator = new Indicator()
    util: UtilService = new UtilService()

    constructor(private Fb: FormBuilder,
        private reportservice: ReportService) { }
    createReport(): FormGroup {
        let t_report: FormGroup = fb.group({
            id: new FormControl(),
            // rentityId:new FormControl(null,[Validators.required]),
            // new FormControl(null,[Validators.required]),
            rentityBranch: new FormControl(null),
            submissionCode: new FormControl(null, [Validators.required]),
            reportCode: new FormControl(null, [Validators.required]),
            entityReference: new FormControl(),
            fiuRefNumber: new FormControl(),
            submissionDate: [],
            currencyCodeLocal: new FormControl(null, [Validators.required]),
            reportingPersonType: new FormControl(null, [Validators.required]),
            location: new FormControl(),
            reason: new FormControl(null, [Validators.required]),
            action: new FormControl(null, [Validators.required]),
            transaction: fb.array([]),
            activity: this.activity.createActivity(),
            reportIndicators: this.indicators.createindicatorsController(),
            reportStatusCode: new FormControl(),
            reportCreatedDate: new FormControl(),
            reportClosedDate: new FormControl(),
            reportUserLockId: new FormControl(),
            reportRiskSignificance: new FormControl(),
            version: new FormControl(),
            reportXml: new FormControl(),
            isValid: new FormControl(),
            priority: new FormControl(),
            reportCreatedBy: new FormControl(),

        });
        return t_report;
    }
    setReport(report): FormGroup {
        let t_report: FormGroup = fb.group({
            id: new FormControl(report.id),
            //  rentityId:[report.rentityId,[Validators.required]],
            //  rentityBranch:report.rentityBranch,
            rentityBranch: [report.rentityBranch, [Validators.required]],
            submissionCode: this.util.checkLockUpValue('submissionCode',report.submissionCode)?[report.submissionCode, [Validators.required]]:new FormControl(null,Validators.required),
            reportCode: this.util.checkLockUpValue('reportCode',report.reportCode)?[report.reportCode, [Validators.required]]:new FormControl(null,Validators.required),
            entityReference: [report.entityReference],
            fiuRefNumber: new FormControl(report.fiuRefNumber),
            submissionDate: [this.util.convertDateFromServer(report.submissionDate)],
            currencyCodeLocal: [report.currencyCodeLocal, [Validators.required]],
            reportingPersonType: [report.reportingPersonType, [Validators.required]],
            location: new FormControl(report.location),
            reason: [report.reason, [Validators.required]],
            action: [report.action, [Validators.required]],
            transaction: fb.array([]),
            activity: report.activity ? this.activity.setActivity(report.activity) : this.activity.createActivity(),
            reportIndicators: this.indicators.setIndicatorsController(report.reportIndicators.indicator),
            reportStatusCode: new FormControl(report.reportStatusCode),
            reportCreatedDate: new FormControl(this.util.convertDateFromServer(report.reportCreatedDate)),
            reportClosedDate: new FormControl(this.util.convertDateFromServer(report.reportClosedDate)),
            reportUserLockId: new FormControl(report.reportUserLockId),
            reportRiskSignificance: new FormControl(report.reportRiskSignificance),
            version: new FormControl(report.version),
            reportXml: new FormControl(report.reportXml),
            isValid: new FormControl(report.isValid),
            priority: new FormControl(report.priority),
            reportCreatedBy: new FormControl(report.reportCreatedBy),

        });
        return t_report;
    }

    /**
     * display the auto save report
     * @param report 
     */
    setReportAu(report): FormGroup {
        this.reportservice.processIndicators(report)
        
       
        let t_report: FormGroup = fb.group({
            id: new FormControl(report.id),
            //  rentityId:{value:report.rentityId,disabled:true},
            //  rentityBranch:report.rentityBranch,
            rentityBranch:{value:report.rentityBranch,disabled:true},
            submissionCode: this.util.checkLockUpValue('submissionCode',report.submissionCode)?[report.submissionCode, [Validators.required]]:new FormControl(null,Validators.required),
            reportCode: this.util.checkLockUpValue('reportCode',report.reportCode)?[report.reportCode, [Validators.required]]:new FormControl(null,Validators.required),
             entityReference:{value:report.entityReference,disabled:true},
             fiuRefNumber:new FormControl(report.fiuRefNumber),
             submissionDate:{value:this.util.convertDateFromServer((report.submissionDate)),disabled:true},
             currencyCodeLocal:{value:report.currencyCodeLocal,disabled:true},
             reportingPersonType:[report.reportingPersonType],
             location:new FormControl(report.location),
             reason:[report.reason],
             action:[report.action],
             transaction:fb.array([]),
             activity:report.activity?this.activity.setActivity(report.activity):this.activity.createActivity(),
          //   reportIndicators: this.indicators.setIndicatorsController(JSON.stringify(report.reportIndicators.indicator)),
           reportIndicators: this.indicators.setIndicatorsController(report.reportIndicators.indicator),
                        reportStatusCode: new FormControl(report.reportStatusCode),
             reportCreatedDate: new FormControl(this.util.convertDateFromServer(report.reportCreatedDate)),
             reportClosedDate: new FormControl(this.util.convertDateFromServer(report.reportClosedDate)),
             reportUserLockId: new FormControl(report.reportUserLockId),
             reportRiskSignificance: new FormControl(report.reportRiskSignificance),
             version: new FormControl(report.version),
             reportXml: new FormControl(report.reportXml),
             isValid: new FormControl(report.isValid)
         
         })
         return t_report
     }
// create report str nested form group
     createStrReport(): FormGroup {
        const t_report: FormGroup = fb.group({
            id: new FormControl(),
            //  rentityId: ['', [Validators.required]],
            rentityBranch: new FormControl(),
            submissionCode: ['', [Validators.required]],
            reportCode: new FormControl(null, [Validators.required]),
            entityReference: [],
            fiuRefNumber: new FormControl(),
            submissionDate: [''],
            currencyCodeLocal: ['', [Validators.required]],
            reportingPersonType: ['', [Validators.required]],
            location: new FormControl(),
            reason: new FormControl(null, [Validators.required]),
            action: new FormControl(null, [Validators.required]),
            transaction: this.Fb.array([],UtilService.minLengthArray(1)),
            activity: new FormControl(),
            reportIndicators: this.indicators.createindicatorsController(),
            reportStatusCode: new FormControl(),
            reportCreatedDate: new FormControl(),
            reportClosedDate: new FormControl(),
            reportUserLockId: new FormControl(),
            reportRiskSignificance: new FormControl(),
            version: new FormControl(),
            reportXml: new FormControl(),
            isValid: new FormControl(),
            priority: new FormControl(),
            reportCreatedBy: new FormControl(),

        });
        return t_report;
    }

    // set form group for str nested form groups
    setReport1(report): FormGroup {
        let t_report: FormGroup = fb.group({
            id: new FormControl(report.id),
            //  rentityId:[report.rentityId,[Validators.required]],
            rentityBranch: report.rentityBranch,
            submissionCode: this.util.checkLockUpValue('submissionCode',report.submissionCode)?[report.submissionCode, [Validators.required]]:new FormControl(null,Validators.required),
            reportCode: this.util.checkLockUpValue('reportCode',report.reportCode)?[report.reportCode, [Validators.required]]:new FormControl(null,Validators.required),
            entityReference: [report.entityReference],
            fiuRefNumber: new FormControl(report.fiuRefNumber),
            submissionDate: [this.util.convertDateFromServer(report.submissionDate)],
            currencyCodeLocal: [report.currencyCodeLocal, [Validators.required]],
            reportingPersonType: [report.reportingPersonType, [Validators.required]],
            location: new FormControl(report.location),
            reason: [report.reason, [Validators.required]],
            action: [report.action, [Validators.required]],
            transaction: this.transaction.setTransactions(report.transaction),
            activity: new FormControl(),
            reportIndicators: this.indicators.setIndicatorsController(report.reportIndicators.indicator),
            reportStatusCode: new FormControl(report.reportStatusCode),
            reportCreatedDate: new FormControl(this.util.convertDateFromServer(report.reportCreatedDate)),
            reportClosedDate: new FormControl(this.util.convertDateFromServer(report.reportClosedDate)),
            reportUserLockId: new FormControl(report.reportUserLockId),
            reportRiskSignificance: new FormControl(report.reportRiskSignificance),
            version: new FormControl(report.version),
            reportXml: new FormControl(report.reportXml),
            isValid: new FormControl(report.isValid),
            priority: new FormControl(report.priority),
            reportCreatedBy: new FormControl(report.reportCreatedBy),

        });
        return t_report;
    }
    /*AutoSave Str */
    setReportAuStr(report): FormGroup {
        
        let t_report: FormGroup = fb.group({
            id: new FormControl(report.id),
            //  rentityId:[report.rentityId],
            rentityBranch: report.rentityBranch,
            submissionCode: this.util.checkLockUpValue('submissionCode',report.submissionCode)?[report.submissionCode, [Validators.required]]:new FormControl(null,Validators.required),
            reportCode: this.util.checkLockUpValue('reportCode',report.reportCode)?[report.reportCode, [Validators.required]]:new FormControl(null,Validators.required),
            entityReference: [report.entityReference],
            fiuRefNumber: new FormControl(report.fiuRefNumber),
            submissionDate: [this.util.convertDateFromServer(report.submissionDate)],
            currencyCodeLocal: [report.currencyCodeLocal],
            reportingPerson: [report.reportingPerson],
            location: new FormControl(report.location),
            reason: [report.reason],
            action: [report.action],
            reportingPersonType: [report.reportingPersonType],
            transaction: this.transaction.setTransactions(report.transaction),
            activity: new FormControl(),
            reportIndicators: this.indicators.setIndicatorsController(report.reportIndicators.indicator),
            reportStatusCode: new FormControl(report.reportStatusCode),
            reportCreatedDate: new FormControl(this.util.convertDateFromServer(report.reportCreatedDate)),
            reportClosedDate: new FormControl(this.util.convertDateFromServer(report.reportClosedDate)),
            reportUserLockId: new FormControl(report.reportUserLockId),
            reportRiskSignificance: new FormControl(report.reportRiskSignificance),
            version: new FormControl(report.version),
            reportXml: new FormControl(report.reportXml),
            isValid: new FormControl(report.isValid),
            priority: new FormControl(report.priority),
            reportCreatedBy: new FormControl(report.reportCreatedBy),
        })
        return t_report
    }
    /*AutoSave Aif */
    setReportAuAif(report): FormGroup {
        let t_report: FormGroup = fb.group({
            id: new FormControl(report.id),
            //  rentityId:[report.rentityId],
            rentityBranch: report.rentityBranch,
            submissionCode: this.util.checkLockUpValue('submissionCode',report.submissionCode)?[report.submissionCode, [Validators.required]]:new FormControl(null,Validators.required),
            reportCode: this.util.checkLockUpValue('reportCode',report.reportCode)?[report.reportCode, [Validators.required]]:new FormControl(null,Validators.required),
            entityReference: [report.entityReference],
            fiuRefNumber: new FormControl(report.fiuRefNumber),
            submissionDate: [this.util.convertDateFromServer(report.submissionDate)],
            currencyCodeLocal: [report.currencyCodeLocal],
            reportingPerson: [report.reportingPerson],
            location: new FormControl(report.location),
            reason: [report.reason],
            action: [report.action],
            reportingPersonType: [report.reportingPersonType],
            transaction: this.transaction.setTransactions(report.transaction),
            activity: new FormControl(),
            reportStatusCode: new FormControl(report.reportStatusCode),
            reportCreatedDate: new FormControl(this.util.convertDateFromServer(report.reportCreatedDate)),
            reportClosedDate: new FormControl(this.util.convertDateFromServer(report.reportClosedDate)),
            reportUserLockId: new FormControl(report.reportUserLockId),
            reportRiskSignificance: new FormControl(report.reportRiskSignificance),
            version: new FormControl(report.version),
            reportXml: new FormControl(report.reportXml),
            isValid: new FormControl(report.isValid),
            priority: new FormControl(report.priority),
            reportCreatedBy: new FormControl(report.reportCreatedBy),
        })
        return t_report
    }
}
