import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from "@angular/forms";
import { t_address } from "src/app/reports/validationControls/t_address";
import { UtilService } from "src/app/util.service";

export class goodAndServices {
    address: t_address = new t_address(this.fb)
    util = new UtilService()
    constructor(private fb: FormBuilder) {

    }
    createControl() {
        

        return this.fb.group({
            item: this.fb.array([])
        })
    }
    setControl(items) {
        return this.fb.group({
            item: this.setItems(items.item)
        })
    }
    createItem() {
        let item = this.fb.group({
            address: this.address.createAddress(),
            comments: new FormControl(),
            currencyCode: new FormControl(),
            description: new FormControl(),
            disposedValue: new FormControl(),
            estimatedValue: new FormControl(),
            id: new FormControl(),
            identificationNumber: new FormControl(),
            itemMake: new FormControl(),
            itemType: new FormControl(),
            presentlyRegisteredTo: new FormControl(),
            previouslyRegisteredTo: new FormControl(),
            registrationDate: new FormControl(),
            registrationNumber: new FormControl(),
            size: new FormControl(),
            sizeUom: new FormControl(),
            statusCode: new FormControl(),
            statusComments: new FormControl()

        })
        this.addItemValidators(item)
        return item
    }
    setItem(item): FormGroup {
        let itemForm = this.fb.group({
            address: this.address.setAddress(item.address),
            comments: new FormControl(item.comments),
            currencyCode: new FormControl(item.currencyCode),
            description: new FormControl(item.description),
            disposedValue: new FormControl(item.disposedValue),
            estimatedValue: new FormControl(item.estimatedValue),
            id: new FormControl(item.id),
            identificationNumber: new FormControl(item.identificationNumber),
            itemMake: new FormControl(item.itemMake),
            itemType: new FormControl(item.itemType),
            presentlyRegisteredTo: new FormControl(item.presentlyRegisteredTo),
            previouslyRegisteredTo: new FormControl(item.previouslyRegisteredTo),
            registrationDate: new FormControl(item.registrationDate),
            registrationNumber: new FormControl(item.registrationNumber),
            size: new FormControl(item.size),
            sizeUom: new FormControl(item.sizeUom),
            statusCode: new FormControl(item.statusCode),
            statusComments: new FormControl(item.statusComments)

        })
        this.addItemValidators(itemForm)
        return itemForm
    }
    setItems(items: any[]): FormArray {
        // 
        // 

        let itemsArrays = this.fb.array([])
        if (items) {
            // 

            for (let item of items) {
                let itemGroup = this.setItem(item)
                itemsArrays.push(itemGroup)
            }
            // 

        }

        return itemsArrays
    }


    addItemValidators(item: FormGroup) {
        let itemType = item.get("itemType")
        itemType.setValidators(Validators.required)
        if (!this.util.checkLockUpValue('TransactionItemType', itemType.value)) {
            itemType.reset()
        }
        let statusCode = item.get("statusCode")
        if (!this.util.checkLockUpValue('PropertyStatusMaster', statusCode.value)) {
            statusCode.reset()
        }
        let currencyCode = item.get("currencyCode")
        if (!this.util.checkLockUpValue('currencyCode', currencyCode.value)) {
            currencyCode.reset()
        }

    }
}