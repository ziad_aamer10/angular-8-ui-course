import { FormGroup, Validators, FormBuilder, FormControl, FormArray } from '@angular/forms';
import { t_address } from './t_address';
import { t_phone } from './t_phone';
import { t_person } from './t_person';
import { formControlBinding } from '@angular/forms/src/directives/ng_model';
import { UtilService } from 'src/app/util.service';
const fb = new FormBuilder();
export class tEntity {
    t_phone: t_phone = new t_phone(fb);
    t_address: t_address = new t_address(fb);
    t_person: t_person = new t_person();
    util = new UtilService()
    constructor() { }
    createEntityControl(): FormGroup {
        return fb.group({
            id: new FormControl(),
            entityNumber: [null, [Validators.required]],
            name: [null, [Validators.required, Validators.maxLength(255)]],
            commercialName: [null, [Validators.maxLength(255)]],
            incorporationLegalForm: [null],
            incorporationNumber: [null, [Validators.maxLength(50)]],
            business: [null, [Validators.maxLength(255)]],
            email: [null, [Validators.maxLength(255), Validators.email]],
            url: [null, [Validators.maxLength(255)]],
            directorId: fb.array([]),
            incorporationState: [null, [Validators.maxLength(255)]],
            incorporationCountryCode: [null],
            incorporationDate: [null],
            businessClosed: new FormControl(),
            dateBusinessClosed: new FormControl(),
            taxNumber: [null, [Validators.maxLength(100)]],
            taxRegNumber: [null, [Validators.maxLength(100)]],
            comments: [null, [Validators.maxLength(4000)]],
            changeBeginDate: [null, [Validators.maxLength(4000)]],
            isReviewed: new FormControl(false),
            updatedBy: [null, [Validators.maxLength(4000)]],
            addresses: fb.group({
                address: fb.array([])
            }),
            phones: fb.group({
                phone: fb.array([])
            }),
        });
    }
    //
    setEntityControl(entity): FormGroup {
        return fb.group({
            id: new FormControl(entity.id),
            entityNumber: [entity.entityNumber, [Validators.required]],
            name: [entity.name, [Validators.required, Validators.maxLength(255)]],
            commercialName: [entity.commercialName, [Validators.maxLength(255)]],
            incorporationLegalForm: [entity.incorporationLegalForm],
            incorporationNumber: [entity.incorporationNumber, [Validators.maxLength(50)]],
            business: [entity.business, [Validators.maxLength(255)]],
            email: [entity.email, [Validators.maxLength(255), Validators.email]],
            url: [entity.url, [Validators.maxLength(255)]],
            directorId: this.setDirecors(entity.directorId),
            incorporationState: [entity.incorporationState, [Validators.maxLength(255)]],
            incorporationCountryCode: this.util.checkLockUpValue('nationality', entity.incorporationCountryCode) ? [entity.incorporationCountryCode] : new FormControl(),
            incorporationDate: [this.util.convertDateFromServer(entity.incorporationDate)],
            businessClosed: [entity.businessClosed],
            dateBusinessClosed: [this.util.convertDateFromServer(entity.dateBusinessClosed)],
            taxNumber: [entity.taxNumber, [Validators.maxLength(100)]],
            taxRegNumber: [entity.taxRegNumber, [Validators.maxLength(100)]],
            comments: [entity.comments, [Validators.maxLength(4000)]],
            addresses: entity.addresses ? this.setAddresses(entity.addresses.address) : this.setAddresses(null),
            phones: this.setPhones(entity.phones.phone),
            changeBeginDate: [entity.changeBeginDate, [Validators.maxLength(255)]],
            isReviewed: [entity.isReviewed, [Validators.maxLength(255)]],
            updatedBy: new FormControl(entity.updatedBy),
        });
    }
    setAddresses(addresses): FormGroup {
        const x: FormArray = fb.array([]);
        if (addresses) {
            for (const i of addresses) {
                const u = this.t_address.setAddress(i);
                x.push(u);

            }
        }
        return fb.group({
            address: x
        });

    }
    setDirecors(directors) {
        const directorsArray = fb.array([]);
        if (directors) {
            for (const i of directors) {
                const director: FormGroup = this.t_person.setPersonControl(i);
                director.addControl('role', new FormControl(i.role, Validators.required));
                directorsArray.push(director);

            }
        }
        return directorsArray;
    }
    setPhones(phones): FormGroup {
        const x: FormArray = fb.array([]);
        if (phones) {


            for (const i of phones) {
                const u = this.t_phone.setPhoneGroup(i);

                x.push(u);

            }
        }
        return fb.group({
            phone: x
        });
    }

}
