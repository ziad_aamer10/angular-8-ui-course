import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../model/comment.model';
import { Report } from 'src/app/reports/components/report-home/report-home.component';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  commentAlarmUrl = environment.projectUrl + '/data-gear/comment/api/v1/comment/comment-alarm';
  commentReportUrl = environment.projectUrl + '/data-gear/comment/api/v1/comment/comment-report';
  deleteCommentAlarmUrl = environment.projectUrl + '/data-gear/comment/api/v1/comment/comment-alarm';
  deleteCommentReportUrl = environment.projectUrl + '/data-gear/comment/api/v1/comment/comment-report';
  removeAttachmentAlarmUrl = environment.projectUrl + '/data-gear/comment/api/v1/Attachment/attachment-alarm';
  removeAttachmentReportUrl = environment.projectUrl + '/data-gear/comment/api/v1/Attachment/attachment-report';
  uploadMultipleFilesAlarmUrl = environment.projectUrl + '/data-gear/comment/api/v1/Attachment/upload-multiple-files-alarm';
  uploadMultipleFilesReportUrl = environment.projectUrl + '/data-gear/comment/api/v1/Attachment/upload-multiple-files-report';
  deleteAttachmentUrl = environment.projectUrl + '/data-gear/comment/api/v1/Attachment/delete';
  addNewFilesToCommentAlarmUrl = environment.projectUrl + '/data-gear/comment/api/v1/Attachment/files-Comment-alarm';
  addNewFilesToCommentReportUrl = environment.projectUrl + '/data-gear/comment/api/v1/Attachment/files-Comment-alarm';
  updateCommentAlarmUrl = environment.projectUrl + '/data-gear/comment/api/v1/comment/comment-alarm';
  updateCommentReportUrl = environment.projectUrl + '/data-gear/comment/api/v1/comment/comment-report';
  downloadFileUrl = environment.projectUrl + '/data-gear/comment/api/v1/Attachment/downloadFile';

  constructor(private http: HttpClient) {
  }

  getCommentsAlarm(alarmed_Obj_Key, alarmed_Obj_level_Cd) {

    const url = this.commentAlarmUrl + '?alarmed_Obj_level_Cd='
      + alarmed_Obj_level_Cd + '&alarmed_Obj_Key=' + alarmed_Obj_Key;
    return this.http.get<Comment[]>(url);
  }

  getCommentsReport(report_id) {
    const url = this.commentReportUrl + '?report_id='
      + report_id;
    return this.http.get<Comment[]>(url);
  }

  deleteCommentAlarm(id_, userId, alarmed_Obj_Key, alarmed_Obj_level_Cd) {
    const url = this.deleteCommentAlarmUrl + '?commentId=' + id_
      + '&updaterId=' + userId + '&alarmed_Obj_Key=' + alarmed_Obj_Key
      + '&alarmed_Obj_level_Cd=' + alarmed_Obj_level_Cd;
    return this.http.delete(url);
  }

  deleteCommentReport(id_, userName, report_id) {
    const url = this.deleteCommentReportUrl + '?commentId=' + id_
      + '&updaterName=' + userName + '&report_id=' + report_id;
    return this.http.delete(url);
  }

  deleteSpecificFileAlarm(file_id_, user_id, alarmed_Obj_Key, alarmed_Obj_level_Cd) {

    const url = this.removeAttachmentAlarmUrl
      + '?attachmentid=' + file_id_ + '&userId=' + user_id
      + '&alarmed_Obj_level_Cd=' + alarmed_Obj_level_Cd + '&alarmed_Obj_Key=' + alarmed_Obj_Key;
    return this.http.delete(url).subscribe(data => { });
  }

  deleteSpecificFileReport(file_id_, user_id, report_id) {

    const url = this.removeAttachmentReportUrl
      + '?attachmentid=' + file_id_ + '&userName=' + user_id
      + '&report_id=' + report_id;
    return this.http.delete(url).subscribe(data => { });
  }
  /**********************************************************/
  getAttachmentsAlarm(key, code) {
    const url = this.commentAlarmUrl + `?code=${code}&key=${key}`;

    return this.http.get(url);
  }

  getAttachmentsReport(report_id) {
    const url = this.commentReportUrl + `?report_id=${report_id}`;
    return this.http.get(url);
  }

  uploadFilesAlarm(f: FileList, alarmed_Obj_Key, alarmed_Obj_level_Cd, description, uplodedById) {
    const files: FileList = f;
    const formData = new FormData();
    for (let i = 0; i < files.length; i++) {
      formData.append('files', files[i]);
    }
    formData.append('alarmed_Obj_Key', alarmed_Obj_Key);
    formData.append('alarmed_Obj_level_Cd', alarmed_Obj_level_Cd);
    formData.append('description', description);
    formData.append('uplodedById', uplodedById);

    const url = this.uploadMultipleFilesAlarmUrl;
    return this.http.post(url, formData);
  }

  uploadFilesReport(f: FileList, report_id, description, uplodedByName) {
    const files: FileList = f;
    const formData = new FormData();
    for (let i = 0; i < files.length; i++) {
      formData.append('files', files[i]);
    }
    formData.append('report_id', report_id);
    formData.append('description', description);
    

    formData.append('uplodedByName', uplodedByName);
    const url = this.uploadMultipleFilesReportUrl;
    return this.http.post(url, formData);
  }

  deleteAttachment(id) {
    const url = this.deleteAttachmentUrl + `/${id}`;
    return this.http.delete(url, { responseType: 'text' });
  }

  addNewFilesToCommentAlarm(f: FileList, commentid, userid, alarmed_Obj_level_Cd, alarmed_Obj_Key) {
    const files: FileList = f;
    const formData = new FormData();
    for (let i = 0; i < files.length; i++) {
      formData.append('files', files[i]);
    }
    formData.append('commentId', commentid);
    formData.append('userId', userid);

    formData.append('alarmed_Obj_level_Cd', alarmed_Obj_level_Cd);
    formData.append('alarmed_Obj_Key', alarmed_Obj_Key);

    const url = this.addNewFilesToCommentAlarmUrl;
    return this.http.post(url, formData).subscribe(data => { });
  }

  addNewFilesToCommentReport(f: FileList, commentid, userName, report_id) {
    const files: FileList = f;
    const formData = new FormData();
    for (let i = 0; i < files.length; i++) {
      formData.append('files', files[i]);
    }
    formData.append('commentId', commentid);
    formData.append('userName', userName);
    formData.append('report_id', report_id);

    const url = this.addNewFilesToCommentReportUrl;
    return this.http.post(url, formData).subscribe(data => { });
  }

  updateCommentAlarm(f: FileList, comment_: Comment) {
    const files: FileList = f;
    const formData = new FormData();
    for (let i = 0; i < files.length; i++) {
      formData.append('files', files[i]);
    }

    formData.append('commentid', String(comment_.id));
    formData.append('description', comment_.description);
    formData.append('uplodedById', comment_.uplodedById);
    formData.append('alarmed_Obj_level_Cd', comment_.alarmed_Obj_level_Cd);
    formData.append('alarmed_Obj_Key', comment_.alarmed_Obj_Key);

    const url = this.updateCommentAlarmUrl;

    return this.http.put(url, formData).subscribe(data => { });
  }

  updateCommentReport(f: FileList, comment_: Comment) {
    const files: FileList = f;
    const formData = new FormData();
    for (let i = 0; i < files.length; i++) {
      formData.append('files', files[i]);
    }

    formData.append('commentid', String(comment_.id));
    formData.append('description', comment_.description);
    formData.append('uplodedByName', comment_.uplodedByName);
    formData.append('report_id', String(comment_.report_id));

    const url = this.updateCommentReportUrl;

    return this.http.put(url, formData).subscribe(data => { });
  }

  downloadFile(filename) {
    const url = this.downloadFileUrl + `/${filename}`;

    return this.http.get(url, { responseType: 'blob' });
  }

}
