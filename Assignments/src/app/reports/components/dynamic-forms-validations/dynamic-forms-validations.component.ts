import { SharingDataServieService } from './../../services/sharing-data-servie.service';
import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatCheckbox } from '@angular/material';
import { ValidationErrors, FormGroup, FormControl } from '@angular/forms';
import { notValidElement, notValidElementWithMessageOnly } from '../../models/notValidElement';

@Component({
  selector: 'app-dynamic-forms-validations',
  templateUrl: './dynamic-forms-validations.component.html',
  styleUrls: ['./dynamic-forms-validations.component.css']
})
export class DynamicFormsValidationsComponent implements OnInit {
  @Input() reportControl: FormGroup
  @Input() validationErrors: notValidElement[]
  @Output() closeValidationTab = new EventEmitter()
  @Input() validationErrorsLabels?: notValidElementWithMessageOnly[]
  constructor(private dataService: SharingDataServieService) {

  }


  ngOnInit() {
    console.log(this.validationErrors);
    
 this.setTypeInNumberFields()
  }

  close() {
    for (let x of this.validationErrors) {
      this.reportControl.get(x.path).setValue(x.formcontrol.value)
    }
    this.closeValidationTab.emit()
    // this emitter to refresh table in transaction details component so that the datasource has the new data after saving validation .
    this.dataService.saveValidationInTransactionTable.emit('refreshDataSource')
  }
  get numberFields(): string[] {
    return ['amountLocal', 'estimatedValue', 'disposedValue', 'size', 'balance', 'significance','foreignExchangeRate','foreignAmount','.number']
  }
  setTypeInNumberFields(){
    let numberFields: string[] = this.numberFields
    this.validationErrors.forEach(a => {
      for (let field of numberFields) {
        if (a.path.endsWith(field))
         {a.inputType = 'number'
        break;}
      }
    }
    )
  }
  falseValue = null
  trueValue = true;
  
  checkboxChange(checkbox: MatCheckbox, checked: boolean,control) {
    console.log('checkboxChange');
    if( !checked ){
      control.setValue(null)
      this.reportControl.get(control).setValue(null)
    }
  }
}
