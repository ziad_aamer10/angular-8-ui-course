import { Actions } from 'src/app/reports/Actions/action';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth-service.service';
import { LogoutAlertComponent } from 'src/app/logout-alert/logout-alert.component';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit  {
  loadingRouteConfig: boolean;
  viewAdminPage:boolean;
  ngOnInit(){
    this.userName=localStorage.getItem('name')
 
    this.viewAdminPage = this.auth.has_Capabilities(Actions.viewAdminPage);
  }
  userName:string=''
  constructor(public auth:AuthService,private toastr: ToastrService,
    private router: Router,private dialog: MatDialog,private http: HttpClient){
    
    }
  title = 'DG-SMC-UI';
  isOn = false;
  userRole:string;
  over = 'over';
  displayName = localStorage.getItem('username');
  index = 0

  logout(){
   
   
    const dialogRef = this.dialog.open(LogoutAlertComponent, {
      width: '100vw',panelClass:'custom-dialog-container'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if(result&&result.logout)
      this.auth.logout()
      localStorage.removeItem('Report')
      localStorage.removeItem('ReportStr')
      localStorage.removeItem('ReportST')
      localStorage.removeItem('ReportAct')
      localStorage.removeItem('ReportAif')
      localStorage.removeItem('ReportAi')
    }
    );
  }
   

  isLoggedIn() {
    
    return this.auth.isLoggedIn()
    
  }
  GoToDashboard(){
    localStorage.removeItem('Report')
    localStorage.removeItem('ReportStr')
    localStorage.removeItem('ReportST')
    localStorage.removeItem('ReportAct')
    localStorage.removeItem('ReportAif')
    localStorage.removeItem('ReportAi')
    this.router.navigate(['dashboard'])
  }

  testToastr() {
    this.toastr.success('test', 'Test');
  }

  goToReports() {
  
    localStorage.removeItem('Report')
    localStorage.removeItem('ReportStr')
    localStorage.removeItem('ReportST')
    localStorage.removeItem('ReportAct')
    localStorage.removeItem('ReportAif')
    localStorage.removeItem('ReportAi')
    this.loadingRouteConfig=true
      this.router.navigate(['/reports']).then(value=>{
        this.loadingRouteConfig=false
      });
  
  }
  
  goToGroupsdashboard() {
    localStorage.removeItem('Report')
    localStorage.removeItem('ReportStr')
    localStorage.removeItem('ReportST')
    localStorage.removeItem('ReportAct')
    localStorage.removeItem('ReportAif')
    localStorage.removeItem('ReportAi')
    this.router.navigate(['/groups']); 
    
  }

  goToRolesdashboard() {
    localStorage.removeItem('Report')
    localStorage.removeItem('ReportStr')
    localStorage.removeItem('ReportST')
    localStorage.removeItem('ReportAct')
    localStorage.removeItem('ReportAif')
    localStorage.removeItem('ReportAi')
    this.router.navigate(['/roles']);
  }

  goToAdmin() {
    localStorage.removeItem('Report')
    localStorage.removeItem('ReportStr')
    localStorage.removeItem('ReportST')
    localStorage.removeItem('ReportAct')
    localStorage.removeItem('ReportAif')
    localStorage.removeItem('ReportAi')
    this.router.navigate(['/reports/Admin']);
  }

  changeIsOn() {
    this.isOn = !this.isOn
  }
 
}
