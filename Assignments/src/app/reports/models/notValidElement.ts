export interface notValidElement {
    path: string,
    formcontrol,
    isMultiSelect?,
    options?
    inputType?
  
  }
  export interface notValidElementWithMessageOnly {
    name:string,
    message:string
  }