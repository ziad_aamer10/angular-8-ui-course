import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { UtilService } from "src/app/util.service";

export class t_phone{
  unamePattern = "^\\+?[0-9]*$";
  util=new UtilService()
    constructor(public fb:FormBuilder)
    {

    }
    createPhone(): FormGroup {
        return this.fb.group({
          id:new FormControl(),
          tphContactType:[null,[Validators.required]],
          tphCommunicationType:[null,[Validators.required]],
          tphCountryPrefix:[null,[Validators.maxLength(4)]],
          tphNumber:[null,[Validators.required,Validators.maxLength(100),Validators.pattern(this.unamePattern)]],
          tphExtension:[null,[Validators.maxLength(10)]],
          phoneComments:[null,[Validators.maxLength(4000)]]
        })
      }
      setPhoneGroup(phone)
      {
        return this.fb.group({
          id:new FormControl(phone.id),
          tphContactType:this.util.checkLockUpValue('tphContactType',phone.tphContactType)?new FormControl(phone.tphContactType,[Validators.required]):new FormControl(null,Validators.required),
          tphCommunicationType:this.util.checkLockUpValue('tphCommunicationType',phone.tphCommunicationType)?new FormControl(phone.tphCommunicationType,[Validators.required]):new FormControl(null,Validators.required),
          tphCountryPrefix:new FormControl(phone.tphCountryPrefix,[Validators.maxLength(4)]),
          tphNumber:new FormControl(phone.tphNumber,[Validators.required,Validators.maxLength(100),Validators.pattern(this.unamePattern)]),
          tphExtension:new FormControl(phone.tphExtension,[Validators.maxLength(10)]),
          phoneComments:new FormControl(phone.phoneComments,[Validators.maxLength(4000)])
        })
      }
}