import { Subscription } from 'rxjs';
import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { CreateNewReport } from '../../validationControls/CreateNewReport';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { t_report } from '../../validationControls/t_report';
import { ReportService } from '../../services/report.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';


export interface Category {
  value: string;
  viewValue: string;
}

export class Owner {
  ownerType: string;
}


@Component({
  selector: 'app-cr-new-report',
  templateUrl: './cr-new-report.component.html',
  styleUrls: ['./cr-new-report.component.css']
})
export class CrNewReportComponent implements OnInit, OnDestroy {
  subscriptionSaveSarReport: Subscription;
  subscriptionSaveStrReport: Subscription;
  subscriptionSaveAifReport: Subscription;

  CrNewReportForm: FormGroup;
  CreateNewReport: CreateNewReport = new CreateNewReport();
  report_Category = '';
  categories: Category[] = [
    { value: 'STR', viewValue: 'STR' },
    { value: 'SAR', viewValue: 'SAR' },
    { value: 'AIF', viewValue: 'AIF' }
  ];
  priorities: Array<string> = ['High', 'Medium', 'Low'];
  owners: Owner[] = [
    { ownerType: 'user1' },
    { ownerType: 'user2' }
  ];
  SarReportControl: FormGroup;
  t_report = new t_report(this.fb, this.reportService);
  StrReportControl: FormGroup;
  AIFReportControl: FormGroup;

  constructor(private _router: Router, public dialogRef: MatDialogRef<CrNewReportComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private dataService: SharingDataServieService,
    private reportService: ReportService, private fb: FormBuilder, private toaster: ToastrService) { }


  onCreateClick(): void {

    if (this.report_Category === 'SAR') {
      this.saveSarReport()
      // this._router.navigate(['/sar'],{queryParams:{Priority:this.CrNewReportForm.get('Priority').value}});
    }
    else if (this.report_Category === 'STR') {
      this.saveSTRReport()
      // this._router.navigate(['/strgeneral'],{queryParams:{Priority:this.CrNewReportForm.get('Priority').value}});
      // this.dialogRef.close(this.CrNewReportForm);
    } else if (this.report_Category === 'AIF') {
      // this._router.navigate(['/aif'],{queryParams:{Priority:this.CrNewReportForm.get('Priority').value}});
      // this.dialogRef.close(this.CrNewReportForm);
      this.saveAIFReport()
    } else {
      alert('Choose Report Type');
    }
  }

  autoLock() {

  }
  cancel() {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.CrNewReportForm = this.CreateNewReport.CrNewReportForm();

  }
  saveSarReport() {
    this.SarReportControl = this.t_report.createReport()
    this.SarReportControl.controls.reportCode.setValue('SAR')
    this.SarReportControl.controls.reportCreatedBy.setValue(localStorage.getItem('name'));
    this.SarReportControl.controls.priority.setValue(this.CrNewReportForm.get('Priority').value);
    this.SarReportControl.controls.reportUserLockId.setValue(localStorage.getItem('name'));  // Lock Report

    this.subscriptionSaveSarReport = this.reportService.saveSarReport(this.SarReportControl.getRawValue()).subscribe((data: any) => {
      if (data) {
        let reportId = data.id || 0
        this.SarReportControl.controls.id.setValue(reportId)
        this._router.navigate([environment.sarEditRoute, reportId])

      }
      else {

      }
      this.dialogRef.close(this.CrNewReportForm);


    },error=>{
      this.toaster.error(error.message)
      
      this.toaster.error(error.message)

    })

  }
  saveSTRReport() {
    this.StrReportControl = this.t_report.createStrReport()
    this.StrReportControl.controls.reportCreatedBy.setValue(localStorage.getItem('name'));
    this.StrReportControl.controls.priority.setValue(this.CrNewReportForm.get('Priority').value);
    this.StrReportControl.controls.reportUserLockId.setValue(localStorage.getItem('name'));  // Lock Report

    this.StrReportControl.controls.reportCode.setValue('STR')
    this.subscriptionSaveStrReport = this.reportService.saveSarReport(this.StrReportControl.getRawValue()).subscribe((data: any) => {
      if (data) {
        let reportId = data.id || 0
        this.StrReportControl.controls.id.setValue(reportId)
        this._router.navigate([environment.strEditRoute, reportId])

      }
      else {

      }
      this.dialogRef.close(this.CrNewReportForm);


    }, error => {
      
      this.toaster.error(error.message)

    })

  }
  saveAIFReport() {
    this.AIFReportControl = this.t_report.createStrReport()
    this.AIFReportControl.controls.reportCode.setValue('AIF')
    this.AIFReportControl.controls.reportCreatedBy.setValue(localStorage.getItem('name'));
    this.AIFReportControl.controls.priority.setValue(this.CrNewReportForm.get('Priority').value);
    this.AIFReportControl.controls.reportUserLockId.setValue(localStorage.getItem('name'));  // Lock Report

    this.subscriptionSaveAifReport = this.reportService.saveSarReport(this.AIFReportControl.getRawValue()).subscribe((data: any) => {
      if (data) {
        let reportId = data.id || 0
        this.AIFReportControl.controls.id.setValue(reportId)
        this._router.navigate([environment.aifEditRoute, reportId])

      }
      else {

      }
      this.dialogRef.close(this.CrNewReportForm);


    }, error => {
      
      this.toaster.error(error.message)

    })

  }

  ngOnDestroy() {
    if (this.subscriptionSaveSarReport != undefined)
      this.subscriptionSaveSarReport.unsubscribe()
    if (this.subscriptionSaveStrReport != undefined)
      this.subscriptionSaveStrReport.unsubscribe()
    if (this.subscriptionSaveAifReport != undefined)
      this.subscriptionSaveAifReport.unsubscribe()
  }
}

