import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { UsersComponent } from "./users/users.component";
import { UserComponent } from "./users/user/user.component";
import { ServersComponent } from "./servers/servers.component";
import { ServerComponent } from "./servers/server/server.component";
import { EditServerComponent } from "./servers/edit-server/edit-server.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { AuthGuardService } from "./auth-guard.service";
import { canDeactivateGuard } from "./servers/edit-server/can-deactivate-guard.service";
import { ServerResolver } from "./servers/server/server-resolver.service";

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'users', component: UsersComponent, children:[
      {path: ':id/:name', component: UserComponent}
    ]},
    {path: 'servers', canActivateChild: [ AuthGuardService], component: ServersComponent, children:[
      {path: ':id', component: ServerComponent, resolve:{ server: ServerResolver }},
      {path: ':id/edit', canDeactivate:[canDeactivateGuard] , component: EditServerComponent}
    ]},
    {path:'not-found', component:NotFoundComponent},
    {path:'**', redirectTo:'not-found', pathMatch: 'full'}
    
  ]

@NgModule({
    imports:[
        RouterModule.forRoot(appRoutes)
    ],
    exports:[RouterModule],
    providers:[canDeactivateGuard]
})
export class RoutingModule {

}