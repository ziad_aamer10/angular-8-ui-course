import { Injectable } from '@angular/core';
import { AifReportComponent } from '../components/aif-report/aif-report.component';
import { CanDeactivate } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class ConfirmDeactivateGuardServiceAifService implements CanDeactivate<AifReportComponent> {

  canDeactivate(target: AifReportComponent) {
    if (!target.confirmCloseReport) {
        return window.confirm('Do you want to close report?');
    }
    return true;
}


  constructor() { }
}
