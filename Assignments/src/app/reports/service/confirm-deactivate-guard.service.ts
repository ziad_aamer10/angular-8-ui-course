import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { SarReportComponent } from '../components/sar-report/sar-report.component';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDeactivateGuardService  implements CanDeactivate<SarReportComponent> {

    canDeactivate(target: SarReportComponent) {
    localStorage.removeItem('Report');
    localStorage.removeItem('ReportAct');
    localStorage.removeItem('ReportStr');
      if (!target.confirmCloseReport) {
          return window.confirm('Do you want to close report?');
      }
      return true;
  }

}
