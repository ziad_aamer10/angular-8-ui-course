import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReportHomeComponent } from './components/report-home/report-home.component';
import { StrComponent } from './components/str/str.component';
import { ConfirmDeactivateGuardServiceStrService } from './service/confirm-deactivate-guard-service-str.service';
import { SarReportComponent } from './components/sar-report/sar-report.component';
import { ConfirmDeactivateGuardService } from './service/confirm-deactivate-guard.service';
import { AifReportComponent } from './components/aif-report/aif-report.component';
import { ConfirmDeactivateGuardServiceAifService } from './service/confirm-deactivate-guard-service-aif.service';
import { AdminPageComponent } from '../Admin config/admin-page/admin-page.component';


const routes: Routes = [
  
      { path: '', component: ReportHomeComponent },
      { path: 'str/edit/:id', component: StrComponent ,canDeactivate:[ConfirmDeactivateGuardServiceStrService]},
      { path: 'sar/edit/:id', component: SarReportComponent ,canDeactivate:[ConfirmDeactivateGuardService]},
      { path: 'aif/edit/:id', component: AifReportComponent ,canDeactivate:[ConfirmDeactivateGuardServiceAifService]},
      { path: 'Admin', component: AdminPageComponent }
  
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)]
    ,exports:[RouterModule]
  
})
export class ReportsRoutingModule { }
