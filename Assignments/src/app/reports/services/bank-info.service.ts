import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BankInfoService {
  uri = environment.projectUrl + '/bankData';

  constructor(private http: HttpClient) { }
  SaveBankInfo(BankInfoForm) {
    return this.http.post(this.uri, BankInfoForm);

  }
  GetBankInfo() {
    return this.http.get(this.uri);
  }
}
