import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth-service.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  subscrioptionLogin: Subscription;
  constructor(
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    public toastr: ToastrService) {

  }
  invalidLogin: boolean;

  // userName: string = localStorage.getItem('name');
  ngOnInit() {
  }

  signIn(credentials) {
    if (environment.mockLogin) {
      this.signInMock(credentials);
    } else {
      this.signInReal(credentials);
    }
  }

  signInReal(credentials) {
    if (credentials.username.length === 0 || credentials.password.length === 0) {
      this.toastr.warning('Username or Password shouldn\'t be empty!', 'Error!');
    } else {

      this.subscrioptionLogin = this.authService.login(credentials).subscribe(data => {

        this.toastr.success('Logged in Successfully');
        if (data && data.hasOwnProperty('token')) {

          localStorage.setItem('token', data.token);
          const myRawToken = data.token;
          const helper = new JwtHelperService();
          const decodedToken = helper.decodeToken(myRawToken);


          localStorage.setItem('name', decodedToken.userName);

          localStorage.setItem('id', decodedToken.id);
          localStorage.setItem('userName', decodedToken.sub);

          // if (localStorage.getItem('Report') != null || localStorage.getItem('ReportStr') != null|| localStorage.getItem('ReportAif') != null) {
          //   const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
          //     width: '250px',
          //   });
          //   dialogRef.afterClosed().subscribe(result => {
          //
          //   });
          // } else {
          //   localStorage.removeItem('Report');
          //   localStorage.removeItem('ReportStr');
          //   localStorage.removeItem('ReportST');
          //   localStorage.removeItem('ReportAct');
          //   localStorage.removeItem('ReportAif')
          //   localStorage.removeItem('ReportAi')
          const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
          this.router.navigate([returnUrl || '']);
          // }
        } else {
          this.invalidLogin = true;
        }
      }
        , error => {
          this.toastr.error('Invalid Username or Password', 'Error!');
        }
      );
    }
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  signInMock(credentials) {
    console.log('signInMock', environment.mockLogin);

    if (credentials.username === 'admin' && credentials.password === 'admin') {
      localStorage.setItem('token', 'testtoken');
      localStorage.setItem('name', credentials.username);
      // if (localStorage.getItem('Report') != null || localStorage.getItem('ReportStr') != null|| localStorage.getItem('ReportAif') != null) {
      //   const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      //     width: '250px',
      //   });
      //   dialogRef.afterClosed().subscribe(result => {
      //
      //   });
      // } else {
      //   localStorage.removeItem('Report');
      //   localStorage.removeItem('ReportStr');
      //   localStorage.removeItem('ReportST');
      //   localStorage.removeItem('ReportAct');
      //   localStorage.removeItem('ReportAif')
      //   localStorage.removeItem('ReportAi')
      this.rootUrl();
      // }
    } else if ((credentials.username === 'goaml_maker@saspw' || credentials.username === 'GOAML_MAKER@saspw')
      && credentials.password === 'maker1') {
      console.log('maker mock');
      localStorage.setItem('token', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJHT0FNTF9NQUtFUiIsImdyb3VwcyI6WyJHT0FNTF9NQUtFUiIsIkdPQU1MIl0sInVzZXJOYW1lIjoiR09BTUxfTUFLRVIiLCJleHAiOjE1NjA4NDkwNzMsImlhdCI6MTU2MDI0NDI3MywiYXV0aG9yaXRpZXMiOlsiR09BTUxfdmlld1JlcG9ydCIsIkdPQU1MX3ZpZXdEaXJlY3RvciIsIkdPQU1MX2Nhc2NhZGVUcmFuc2FjdGlvbiIsIkdPQU1MX2VkaXRSZXBvcnQiLCJHT0FNTF9WSUVXUkVQT1JUU19SZXR1cm5Ub01ha2VyIiwiR09BTUxfZGVsZXRlVHJhbnNhY3Rpb24iLCJHT0FNTF9hZGRTaWduYXRvcnkiLCJHT0FNTF9zYXZlQWN0aXZpdHkiLCJHT0FNTF9zYXZlUmVwb3J0IiwiR09BTUxfVklFV1JFUE9SVFNfTUFLSU5HIiwiR09BTUxfZGVsZXRlRGlyZWN0b3IiLCJHT0FNTF9jYXNjYWRlUGVyc29uIiwiR09BTUxfZWRpdFRyYW5zYWN0aW9uIiwiR09BTUxfdmlld1RyYW5zYWN0aW9uIiwiR09BTUxfZGVsZXRlU2lnbmF0b3J5IiwiR09BTUxfZWRpdEFjdGl2aXR5IiwiR09BTUxfY2FzY2FkZVNpZ25hdG9yeSIsIkdPQU1MX3VwZGF0ZUNvbW1lbnQiLCJHT0FNTF9lZGl0U2lnbmF0b3J5IiwiR09BTUxfZWRpdERpcmVjdG9yIiwiR09BTUxfZGVsZXRlQ29tbWVudCIsIkdPQU1MX2Nhc2NhZGVBY2NvdW50IiwiR09BTUxfZGVsZXRlQWN0aXZpdHkiLCJHT0FNTF92aWV3U2lnbmF0b3J5IiwiR09BTUxfYWRkVHJhbnNhY3Rpb24iLCJHT0FNTF9hZGRNdWx0aXBsZVRyYW5zYWN0aW9uIiwiR09BTUxfYWRkQWN0aXZpdHkiLCJHT0FNTF9zZW5kVG9jaGVja2VyIiwiR09BTUxfYWRkVHJhbnNhY3Rpb25BY3Rpb24iLCJHT0FNTF9hZGREaXJlY3RvciIsIkdPQU1MX2NyZWF0ZVJlcG9ydCIsIkdPQU1MX2Nhc2NhZGVFbnRpdHkiXSwianRpIjoiQTVUN0dJVjIuQVAwMDA3MU0ifQ.YJYE9BhMMk-UUxY5tuOXCcZZu1m5PPyAFncm0ld0xaHeO38Q4brKmLHhB-5HgShPXkXfyMcl_-RNsf24qcKwVw');
      localStorage.setItem('name', credentials.username);
      this.rootUrl();
    } else if ((credentials.username === 'goaml_checker@saspw' || credentials.username === 'GOAML_CHECKER@saspw')
      && credentials.password === 'checker1') {
      console.log('checker mock');
      localStorage.setItem('token', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJHT0FNTF9DSEVDS0VSIiwiZ3JvdXBzIjpbIkdPQU1MX0NIRUNLRVIiLCJHT0FNTCJdLCJ1c2VyTmFtZSI6IkdPQU1MX0NIRUNLRVIiLCJleHAiOjE1NjA4NDkxNTMsImlhdCI6MTU2MDI0NDM1MywiYXV0aG9yaXRpZXMiOlsiR09BTUxfYWRkQWN0aXZpdHkiLCJHT0FNTF9lZGl0RGlyZWN0b3IiLCJHT0FNTF9WSUVXUkVQT1JUU19DSEVDS0lORyIsIkdPQU1MX2VkaXRUcmFuc2FjdGlvbiIsIkdPQU1MX2Nhc2NhZGVQZXJzb24iLCJHT0FNTF9lZGl0QWN0aXZpdHkiLCJHT0FNTF9jYXNjYWRlVHJhbnNhY3Rpb24iLCJHT0FNTF9kZWxldGVTaWduYXRvcnkiLCJHT0FNTF9hZGRTaWduYXRvcnkiLCJHT0FNTF9kZWxldGVEaXJlY3RvciIsIkdPQU1MX2FkZERpcmVjdG9yIiwiR09BTUxfZGVsZXRlQ29tbWVudCIsIkdPQU1MX2FkZFRyYW5zYWN0aW9uQWN0aW9uIiwiR09BTUxfYWRkVHJhbnNhY3Rpb24iLCJHT0FNTF9yZXR1cm5Ub01ha2VyIiwiR09BTUxfdmlld1NpZ25hdG9yeSIsIkdPQU1MX1ZJRVdSRVBPUlRTX01BS0lORyIsIkdPQU1MX3JvdXRlUmVwb3J0IiwiR09BTUxfdXBkYXRlQ29tbWVudCIsIkdPQU1MX3NhdmVBY3Rpdml0eSIsIkdPQU1MX3N1Ym1pdHRlZCIsIkdPQU1MX3NhdmVSZXBvcnQiLCJHT0FNTF9BSUZSZXF1ZXN0IiwiR09BTUxfY3JlYXRlUmVwb3J0IiwiR09BTUxfdmlld1RyYW5zYWN0aW9uIiwiR09BTUxfZGVsZXRlQWN0aXZpdHkiLCJHT0FNTF92aWV3UmVwb3J0IiwiR09BTUxfY2FzY2FkZUFjY291bnQiLCJHT0FNTF9zZW5kVG9BTUxIZWFkIiwiR09BTUxfY2FzY2FkZUVudGl0eSIsIkdPQU1MX2FkZE11bHRpcGxlVHJhbnNhY3Rpb24iLCJHT0FNTF9kZWxldGVUcmFuc2FjdGlvbiIsIkdPQU1MX2Nhc2NhZGVTaWduYXRvcnkiLCJHT0FNTF92aWV3RGlyZWN0b3IiLCJHT0FNTF9lZGl0U2lnbmF0b3J5IiwiR09BTUxfZWRpdFJlcG9ydCIsIkdPQU1MX1ZJRVdSRVBPUlRTX0FJRlJlcXVlc3RlZCIsIkdPQU1MX3VubG9ja01ha2VyUmVwb3J0IiwiR09BTUxfVklFV1JFUE9SVFNfUmV0dXJuVG9DaGVja2VyIiwiR09BTUxfVklFV1JFUE9SVFNfUmV0dXJuVG9NYWtlciJdLCJqdGkiOiJBNVQ3R0lWMi5BUDAwMDcxTiJ9.2JGZjH-hEzbtESqPHucwux7crzJ7-ctAJs6SBlgoq7HLlfjwazwd-UHppYTLvBojImtqVmWmUjc4NJB06L5bJw');
      localStorage.setItem('name', credentials.username);
      this.rootUrl();
    } else if ((credentials.username === 'goaml_manager@saspw' || credentials.username === 'GOAML_MANAGER@saspw')
      && credentials.password === 'manager1') {
      console.log('manager mock');
      localStorage.setItem('token', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJHT0FNTF9NQU5BR0VSIiwiZ3JvdXBzIjpbIkdPQU1MX01BTkFHRVIiLCJHT0FNTCJdLCJ1c2VyTmFtZSI6IkdPQU1MX01BTkFHRVIiLCJleHAiOjE1NjEyNzk5NTcsImlhdCI6MTU2MDY3NTE1NywiYXV0aG9yaXRpZXMiOlsiR09BTUxfZGVsZXRlRGlyZWN0b3IiLCJHT0FNTF9jYXNjYWRlU2lnbmF0b3J5IiwiR09BTUxfVklFV1JFUE9SVFNfQ0hFQ0tJTkciLCJHT0FNTF92aWV3RGlyZWN0b3IiLCJHT0FNTF9jcmVhdGVSZXBvcnQiLCJHT0FNTF9lZGl0UmVwb3J0IiwiR09BTUxfVklFV1JFUE9SVFNfTUxDVU1vZGlmaWNhdGlvbiIsIkdPQU1MX3NhdmVSZXBvcnQiLCJHT0FNTF9hZGRUcmFuc2FjdGlvbiIsIkdPQU1MX2Nhc2NhZGVBY2NvdW50IiwiR09BTUxfZWRpdEFjdGl2aXR5IiwiR09BTUxfcmV0dXJuVG9DaGVja2VyIiwiR09BTUxfY2FzY2FkZVBlcnNvbiIsIkdPQU1MX2FkZFRyYW5zYWN0aW9uQWN0aW9uIiwiR09BTUxfQUlGUmVxdWVzdCIsIkdPQU1MX3ZpZXdUcmFuc2FjdGlvbiIsIkdPQU1MX1ZJRVdSRVBPUlRTX1JldHVyblRvQ2hlY2tlciIsIkdPQU1MX2FkZEFjdGl2aXR5IiwiR09BTUxfVklFV1JFUE9SVFNfWE1MR2VuZXJhdGVkIiwiR09BTUxfVklFV1JFUE9SVFNfcmVPcGVuIiwiR09BTUxfY2FzY2FkZUVudGl0eSIsIkdPQU1MX2FkZFNpZ25hdG9yeSIsIkdPQU1MX3VwZGF0ZUNvbW1lbnQiLCJHT0FNTF9lZGl0VHJhbnNhY3Rpb24iLCJHT0FNTF9WSUVXUkVQT1JUU19DTE9TRUQiLCJHT0FNTF9nZW5lcmF0ZVhNTCIsIkdPQU1MX1ZJRVdSRVBPUlRTX1NVQk1JVFRFRCIsIkdPQU1MX1ZJRVdSRVBPUlRTX1JldHVyblRvTWFrZXIiLCJHT0FNTF92aWV3UmVwb3J0IiwiR09BTUxfYWRkRGlyZWN0b3IiLCJHT0FNTF9WSUVXUkVQT1JUU19NQUtJTkciLCJHT0FNTF9jbG9zZVJlcG9ydCIsIkdPQU1MX2RlbGV0ZUFjdGl2aXR5IiwiR09BTUxfZGVsZXRlU2lnbmF0b3J5IiwiR09BTUxfY2FzY2FkZVRyYW5zYWN0aW9uIiwiR09BTUxfZWRpdERpcmVjdG9yIiwiR09BTUxfYWRkTXVsdGlwbGVUcmFuc2FjdGlvbiIsIkdPQU1MX2RlbGV0ZUNvbW1lbnQiLCJHT0FNTF9zYXZlQWN0aXZpdHkiLCJHT0FNTF9yZU9wZW5SZXBvcnQiLCJHT0FNTF9WSUVXUkVQT1JUU19XYWl0aW5nSGVhZERlY2lzaW9uIiwiR09BTUxfdmlld1NpZ25hdG9yeSIsIkdPQU1MX3JvdXRlUmVwb3J0IiwiR09BTUxfZWRpdFNpZ25hdG9yeSIsIkdPQU1MX3JldHVyblRvTWFrZXJGcm9tTWFuYWdlciIsIkdPQU1MX2RlbGV0ZVRyYW5zYWN0aW9uIiwiR09BTUxfc3VibWl0dGVkIiwiR09BTUxfdmlld0FkbWluUGFnZSIsIkdPQU1MX1ZJRVdSRVBPUlRTX0FJRlJlcXVlc3RlZCIsIkdPQU1MX3VubG9ja01ha2VyUmVwb3J0IiwiR09BTUxfdW5sb2NrQ2hlY2tlclJlcG9ydCJdLCJqdGkiOiJBNVQ3R0lWMi5BUDAwMDcxTyJ9.8XCPxEwl4PBtPN77QKOjDC0yx5uJokCD346dZXPfsMI-5v7WYJBi9LflPfWcsvRaC2Bir9McknDZ8CtTpkJq2w');
      localStorage.setItem('name', credentials.username);
      this.rootUrl();
    } else {
      this.toastr.error('Enter Correct credintial');
    }
  }

  rootUrl() {
    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
    this.router.navigate([returnUrl || '/']);
  }

  ngOnDestroy() {
    if (this.subscrioptionLogin != undefined) {
      this.subscrioptionLogin.unsubscribe();
    }
  }

}
