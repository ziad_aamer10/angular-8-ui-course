export class User {
  constructor(
    public email: string,
    public userId: string,
    private _token: string,
    private _tokenExpirationDate
  ) {}

  get token(){
    if(this._tokenExpirationDate==null || new Date() > this._tokenExpirationDate){
      return null;
    }
    return this._token;
  }
}
