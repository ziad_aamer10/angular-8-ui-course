import { Component, OnInit, ViewChild } from '@angular/core';
import {
  MatTableDataSource, MatDialog, MatPaginator, MatSortModule, Sort, MatSort,
  SELECT_PANEL_PADDING_X, PageEvent
} from '@angular/material';
// import { MatTableDataSource, MatDialog, MatPaginator, SELECT_PANEL_PADDING_X } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { RouteReportComponent } from '../route-report/route-report.component';
import { CrNewReportComponent } from '../cr-new-report/cr-new-report.component';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { merge, Observable, of as observableOf, Subscription } from 'rxjs';
import { startWith, switchMap, map, catchError } from 'rxjs/operators';
import { ReportService } from '../../services/report.service';
import { Toast, ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';
import { Actions } from '../../Actions/action';
import { Action } from 'rxjs/internal/scheduler/Action';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { LookupClass } from '../../lockups/Lookups';
import { environment } from 'src/environments/environment';
import { HttpErrorResponse } from '@angular/common/http';
import { JsonPipe } from '@angular/common';
// import { time } from 'highcharts';


export interface Report {
  id: number; reportCode: string;
  reportStatusCode: string; submissionDate: string;
  reportCreatedBy: string; reportCreatedDate: string;
  entityReference: string; fiuRefNumber: string;
  priority: string; reportUserLockId: string;
}

export interface ReportPage {
  content: Report[];
  totalElements: number;
  numberOfElements: number;
  size: number;
  totalPages: number;
}

@Component({
  selector: 'app-report-home',
  templateUrl: './report-home.component.html',
  styleUrls: ['./report-home.component.css']
})
export class ReportHomeComponent implements OnInit {
  sub: Subscription
  lockedReport: boolean = false;
  createReportAction: boolean = false
  viewReportAction: boolean = false
  routeReportAction: boolean = false

  code_values = LookupClass.report_type;
  report_priority = LookupClass.report_priority;
  report_status = LookupClass.report_status;

  constructor(public dialog: MatDialog, private _router: Router,
    private reportService: ReportService, private toastr: ToastrService, private auth: AuthService, private dataService: SharingDataServieService) {

  }
  ;
  displayedColumns: string[] = ['select', 'id', 'reportCode',
    'reportStatusCode', 'submissionDate', 'reportUserLockId', 'reportCreatedDate', 'reportCreatedBy',
    'entityReference', 'fiuRefNumber', 'priority', 'Action'
  ];
  resultsLength = 0;
  dataSource: any = [];
  pageEvent: PageEvent;
  data: ReportPage;
  isLoadingResults = true;
  // isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ELEMENT_DATA: Report[] = [
  ];
  // dataSource : MatTableDataSource<Report>;
  // = new MatTableDataSource(this.ELEMENT_DATA);
  selection = new SelectionModel<Report>(true, []);
  reportTypes = ['SAR', 'STR', 'AIF']
  sortedData: Report[];

  searchForm = new FormGroup({
    'partyNumber': new FormControl(),
    'partySSN': new FormControl(),
    'reportNumber': new FormControl(),
    'reportCode': new FormControl(),
    'reportStatusCode': new FormControl(),
    'reportUserLockId': new FormControl(),
    'reportCreatedDateFrom': new FormControl(),
    'reportCreatedDateTo': new FormControl(),
    'submissionDateFrom': new FormControl(),
    'submissionDateTo': new FormControl(),
    'reportCreatedBy': new FormControl(),
    'priority': new FormControl(),
    'entityReference': new FormControl(),
    'fiuRefNumber': new FormControl(),
  });

  getServerData(event?: PageEvent) {
    this.getReports();
    return event;
  }

  getServerData1(event) {

    this.sort.direction = event.direction;
    this.getReports();
    return event;
  }


  getReports() {

    merge()
      .pipe(
        startWith({}),
        switchMap(() => {
          // let or =  "asc";
          if (this.sort.direction == '' || this.sort.direction == null) {
            this.sort.direction = "asc";
          }
          if (this.sort.active == undefined) {
            this.sort.active == 'id';
          }
          this.isLoadingResults = true;

          if (this.searchForm.get('partyNumber').value || this.searchForm.get('partySSN').value || this.searchForm.get('reportNumber').value ||
            this.searchForm.get('reportCode').value || this.searchForm.get('reportStatusCode').value || this.searchForm.get('reportUserLockId').value ||
            this.searchForm.get('reportCreatedDateFrom').value || this.searchForm.get('reportCreatedDateTo').value || this.searchForm.get('submissionDateFrom').value ||
            this.searchForm.get('submissionDateTo').value || this.searchForm.get('reportCreatedBy').value || this.searchForm.get('priority').value ||
            this.searchForm.get('entityReference').value || this.searchForm.get('fiuRefNumber').value) {

            return this.reportService.getReportListBySearch(this.searchForm.value, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
          } else {

            return this.reportService.getReportList1(this.searchForm, this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
          }
        }),
        map(data => {
          this.isLoadingResults = false;

          this.resultsLength = data.totalElements;
          return data.content;
        }),
        catchError((error) => {
          this.isLoadingResults = false;
          this.toastr.error(error.error.message||error.message, '', { timeOut: 3000 })
          return observableOf([]);
        })
      ).subscribe(data => {

        this.dataSource = new MatTableDataSource();
        this.dataSource.data = data;
        this.selection.clear()

        // this.dataSource.paginator = this.paginator;
        // this.dataSource.sort = this.sort;
      });

  }

  ngOnChanges(): void {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;

  }

  GetReportsByFilters() {


    this.getReports();
    this.paginator.firstPage();
  }
  ClearSearchFields() {
    this.searchForm.reset();
  }
  onKeydown(event) {
    if (event.key === "Enter") {

      this.GetReportsByFilters();
    }
  }

  ngAfterViewInit() {


    this.getReports();
    this.paginator.firstPage();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }



  ngOnInit() {



    this.dataSource.filterPredicate =
      (data: Report, filtersJson: string) => {
        // 
        const matchFilter = [];
        const filters = JSON.parse(filtersJson);
        // 

        filters.forEach(filter => {
          const val = data[filter.id] === null ? '' : data[filter.id];
          if (typeof (val) == 'number')
            //  matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
            matchFilter.push(val == filter.value || filter.value == '');
          else
            matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
        });
        return matchFilter.every(Boolean);
      };

    this.setActions()



  }

  ngOnDestroy(): void {


  }



  FilterByColumn(filterValue: any, columnName: string) {
    // const tableFilters = [];
    // tableFilters.push({
    //   id: columnName,
    //   value: filterValue
    // });
    // // 



    // this.dataSource.filter = JSON.stringify(tableFilters);
    // // 

    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }
  applyFilter(filterValue) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

  }
  applyFilter2(filterValue) {
    this.dataSource.data.filter(x => {
      x.id == filterValue

    })
  }

  FilterByCustomer(Customeid: any) {
    // this.reportService.getReportsByCustomerId(Customeid).subscribe((data: Report[]) => {
    //   

    //   if (data) {
    //     this.dataSource.data = data
    //   }
    // },
    //   error => {

    //   })


  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  opencreateNewReportDialog() {
    const dialogRef = this.dialog.open(CrNewReportComponent, {
      width: '800px',

      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      // 

    });
  }



  // checkReportLocked(id: number, userName: string):any {
  //   return  this.reportService.checkLocked(id, userName).subscribe(data => {

  //     if(data)
  //     this.lockedReport=true
  //     else
  //     this.lockedReport=false

  //      }, error => {});

  // }

  onReportClick(id: number, type: string, userName: string, index: number): void {

    this.autoLock(id, userName, index, type);

    // if (type == 'SAR') {
    //   this._router.navigate([environment.sarEditRoute, 
    //   this.autoLock(id, userName, index)
    // } else if (type == 'STR') {
    //   this._router.navigate([environment.strEditRoute, id]);
    //   this.autoLock(id, userName, index)
    // }
    // else if (type == 'AIF') {
    //   this._router.navigate([environment.aifEditRoute, id]);
    //   this.autoLock(id, userName, index)
    // }

  }


  //Auto_Lock
  autoLock(id: number, userName: string, index: number, type) {
    userName = localStorage.getItem('name');

    if (this.dataSource.data[index].reportUserLockId) {
      if (type == 'SAR') {
        this._router.navigate([environment.sarEditRoute, id])
      } else if (type == 'STR') {
        this._router.navigate([environment.strEditRoute, id]);
      }
      else if (type == 'AIF') {
        this._router.navigate([environment.aifEditRoute, id]);
      }
    }

    else {

      this.reportService.autoLock(id, userName).subscribe(data => {
        if (data) {
          // console.log("data===", data);

          this.dataSource.data[index].reportUserLockId = userName;
          if (type == 'SAR') {
            this._router.navigate([environment.sarEditRoute, id])
          } else if (type == 'STR') {
            this._router.navigate([environment.strEditRoute, id]);
          }
          else if (type == 'AIF') {
            this._router.navigate([environment.aifEditRoute, id]);
          }
        }

      }, error => {
        this.toastr.error('Error_auto_Lock', error);
      });

    }

  }


  //getGroupName for unlocking scenarios 
  getGroupName(id, userName, index1) {
    userName = localStorage.getItem('name');
    let roles = this.getRoles();

    let name = this.dataSource.data[index1].reportUserLockId
    let groupName;
    this.reportService.getusergroup(name).subscribe(
      groups => {
        console.log("groups====>", groups);
        for (var index in groups) {
          if (groups[index] == 'GOAML_MANAGER') {
            groupName = groups[index];
            console.log('groupName:====>', groupName);
          }
          else if (groups[index] == 'GOAML_CHECKER') {
            groupName = groups[index];
            console.log('groupName:====>', groupName);
          }
          else if (groups[index] == 'GOAML_MAKER') {
            groupName = groups[index];
            console.log('groupName:====>', groupName);
          }

        }

        //unlock maker 
        if (groupName == 'GOAML_MAKER' && roles.includes('GOAML_unlockMakerReport')) {

          console.log("unlock maker ");

          this.reportService.unLock(id, userName).subscribe(
            data => {
              if (data) {
                this.dataSource.data[index1].reportUserLockId = null;
                this.toastr.success('Report unLock!')
              }
            },
            error => {
              this.toastr.error(error.error.message)
            }
          )
        }

        //unlock checker 
        else if (groupName == 'GOAML_CHECKER' && roles.includes('GOAML_unlockCheckerReport')) {

          console.log("unlock checker ");

          this.reportService.unLock(id, userName).subscribe(
            data => {
              if (data) {
                this.dataSource.data[index1].reportUserLockId = null;
                this.toastr.success('Report unLock!')
              }
            },
            error => {
              this.toastr.error(error.error.message)
            }
          )
        }
        // manager unlock both maker and checker and it self . 
        else if (roles.includes('GOAML_unlockCheckerReport') && roles.includes('GOAML_unlockMakerReport')) {
          console.log("manager unlock both maker and checker and it self");

          this.reportService.unLock(id, userName).subscribe(
            data => {
              if (data) {
                this.dataSource.data[index1].reportUserLockId = null;
                this.toastr.success('Report unLock!')
              }
            },
            error => {
              this.toastr.error(error.error.message)
            }
          )
        }
        // any user unlock itself
        else if (this.dataSource.data[index1].reportUserLockId == userName) {
          console.log("any user unlock itself");

          this.reportService.unLock(id, userName).subscribe(
            data => {
              if (data) {
                this.dataSource.data[index1].reportUserLockId = null;
                this.toastr.success('Report unLock!')
              }
            },
            error => {
              this.toastr.error(error.error.message)
            }
          )
        }
        else {
          this.toastr.info(" unLock not Allowed",'',{timeOut:2000})
        }
      }
    );

  }

  getRoles(): any[] {

    let roles = [];
    roles = this.auth.getCapabilities();
    return roles;
  }


  Locked(id, userName, index) {
    userName = localStorage.getItem('name');
    if (this.dataSource.data[index].reportUserLockId) {
      // when unlocking 
      this.getGroupName(id, userName, index);
    }
    else {
      // when locking 
      this.reportService.autoLock(id, userName).subscribe(
        locked => {
          if (locked) {
            this.dataSource.data[index].reportUserLockId = userName;
            this.toastr.success(" Report Locked !");
          }
        }
      )
    }
  }



  // GetReportsByFilters() { }
  has_capability(action) {
    return this.auth.has_Capabilities(action)
  }
  setActions() {
    this.createReportAction = this.auth.has_Capabilities(Actions.createReport)
    this.viewReportAction = this.auth.has_Capabilities(Actions.viewReport)
    this.routeReportAction = this.auth.has_Capabilities(Actions.routeReport)
  }
  RouteReport() {
    let selectedReports = this.selection.selected
    if (selectedReports.length == 0) {
      alert("Select one Report to route")
      return
    }
    if (selectedReports.length > 1) {
      alert("Select only one Report to route")
      return
    }

    let user = this.auth.currentUser
    let reportId = selectedReports[0].id
    let currentUser = this.auth.currentUser
    const dialogRef = this.dialog.open(RouteReportComponent, {
      width: '500px',

    });

    dialogRef.afterClosed().pipe().subscribe(result => {
      console.log('result', result);

      if (result && result.reciever) {
        this.reportService.routeReport(reportId, currentUser, result.reciever).subscribe((data: any) => {
          this.ClearSearchFields();
          this.GetReportsByFilters();
          this.toastr.success('Report is routed successfully')
          this.getReports()
        }, (error) => {
        // console.log(error.error);
        //  console.log('-----------');
         let message=error.message
         if(error.error)
         message= JSON.parse(error.error).message

      
          
          this.toastr.error(message,"Routing Error",{timeOut:2000})
        })
      }
      // 

    });
  }
  get _routeReportAction() {
    return this.routeReportAction
  }
}

// function compare(a: number | string, b: number | string, isAsc: boolean) {
//   return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
// }



 // getReports() {

  //   this.reportService.getReportList().subscribe((data: Report[]) => {
  //     // 
  // constructor(public dialog: MatDialog, private _router: Router, private reportService: ReportService) 
  // {
  //   // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  //   // this.sortedData = this.ELEMENT_DATA.slice();
  //  }

  // applyFilter(filterValue: string) {
  //   this.dataSource.filter = filterValue.trim().toLowerCase();

  //   if (this.dataSource.paginator) {
  //     this.dataSource.paginator.firstPage();
  //   }
  // }

   // this.reportService.getReportList().subscribe((data: Report[]) => {
    //   // 

    //   if (data) {
    //     this.dataSource.data = data
    //   }
    // },
    //   error => {

    //   })

     // "reportCreatedDate" : null,
  // "reportClosedDate" : null,
  // "reportUserLockId" : "admin",
  // "reportStatusCode" : null,
  // "submissionDate" : null,
  // "entityReference" : null,
  // "id" : 1,
  // "reportCode" : null,
  // "reportCreatedBy" : null,
  // "fiuRefNumber" : null,
  // "priority" : null


  // deleteReport(index) {
  //   let ds = this.dataSource.data
  //   ds.splice(index)
  //   this.dataSource.data = ds
  // }
  // sortData(event) {
  //   this.(dataSource) = this.(your-list).sort((a, b) => {
  //     return a.date > b.date ? 1 : -1;
  //   }
  // }
  // sortData(sort: Sort) {
  //   const data = this.ELEMENT_DATA.slice();
  //   if (!sort.active || sort.direction === '') {
  //     this.sortedData = data;
  //     
  //     

  //     return;
  //   }

  //   this.sortedData = data.sort((a, b) => {
  //     const isAsc = sort.direction === 'asc';
  //     switch (sort.active) {
  //       case 'id': return compare(a.id, b.id, isAsc);
  //       case 'reportCode': return compare(a.reportCode, b.reportCode, isAsc);
  //       case 'reportStatusCode': return compare(a.reportStatusCode, b.reportStatusCode, isAsc);
  //       case 'submissionDate': return compare(a.submissionDate, b.submissionDate, isAsc);
  //       case 'reportUserLockId': return compare(a.reportUserLockId, b.reportUserLockId, isAsc);
  //       case 'reportCreatedBy': return compare(a.reportCreatedBy, b.reportCreatedBy, isAsc);
  //       case 'reportCreatedDate': return compare(a.reportCreatedDate, b.reportCreatedDate, isAsc);
  //       case 'priority': return compare(a.priority, b.priority, isAsc);
  //       case 'entityReference': return compare(a.entityReference, b.entityReference, isAsc);
  //       case 'fiuRefNumber': return compare(a.fiuRefNumber, b.fiuRefNumber, isAsc);
  //       default: return 0;
  //       
  //     }
  //   });
  // }
