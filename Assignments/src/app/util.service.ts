import { Injectable } from '@angular/core';
import { FormControl, FormGroup, FormArray, ValidationErrors, AbstractControl } from '@angular/forms';
import { LookupClass } from './reports/lockups/Lookups';
import { CategoryClass, Data } from './reports/lockups/report-indiators-lockup';
import { notValidElement, notValidElementWithMessageOnly } from './reports/models/notValidElement';
import { BehaviorSubject } from 'rxjs';
import { DatePipe } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class UtilService {
  markReportAsRead: boolean = false
  public closeExpansionPanel: boolean = false;
  private messageSource = new BehaviorSubject(false);
  currentMessage = this.messageSource.asObservable();
  private datepipe: DatePipe = new DatePipe("en-US")
  changeReportAsRead(message: boolean) {
    this.messageSource.next(message)
  }
  CategoryObject = new CategoryClass();
  constructor() { }
  validateFormControl(name, x: FormControl, notValidElement: any[]) {
    if (!x.valid) {
      let path = ''
    }
    const controlErrors: ValidationErrors = x.errors;
    x.markAsDirty()
    x.markAsTouched()
    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(keyError => {
        notValidElement.push(name)
      });
    }
  }
  validateFormGroup(x: FormGroup, notValidElement: any[]) {
    Object.keys(x.controls).forEach(key => {
      if (x.get(key) instanceof FormGroup) {
        this.validateFormGroup(x.get(key) as FormGroup, notValidElement)
      }
      else if (x.get(key) instanceof FormArray) {
        this.validateFormArray(x.get(key) as FormArray, notValidElement)
      }
      else if (x.get(key) instanceof FormControl) {
        this.validateFormControl(key, x.get(key) as FormControl, notValidElement)
      }
    });
  }
  validateFormGroup2(vFormGroup, notValidElements: any[], notValidElementsLabels: notValidElementWithMessageOnly[]) {
    if (vFormGroup instanceof FormGroup) {

      Object.keys(vFormGroup.controls).forEach(key => {
        this.validateFormGroup2(vFormGroup.get(key) as FormGroup, notValidElements, notValidElementsLabels)
      })
    }
    else if (vFormGroup instanceof FormArray) {
      let err = this.handleFormArrayError(vFormGroup)
      if (err) { notValidElementsLabels.push(err) }
      for (let aa of vFormGroup.controls) {
        this.validateFormGroup2(aa, notValidElements, notValidElementsLabels)

      }
    }
    else if (vFormGroup instanceof FormControl) {

      this.validateFormControl2(this.getName(vFormGroup), vFormGroup, notValidElements)
    }
  }
  handleFormArrayError(formArray: FormArray) {
    const controlErrors: ValidationErrors = formArray.errors;
    if (controlErrors != null) {
      let path = ''
      path = this.getControlPath(formArray, path)
      let element: notValidElementWithMessageOnly = { message: 'Add at least one Item', name: path }
      return element
    }
    return null
  }
  validateFormArray(formArray: FormArray, notValidElement: any[]) {
    Object.keys(formArray.controls).forEach(key => {
      if (formArray.get(key) instanceof FormGroup) {
        this.validateFormGroup(formArray.get(key) as FormGroup, notValidElement)
      }
      else if (formArray.get(key) instanceof FormControl) {
        this.validateFormControl(key, formArray.get(key) as FormControl, notValidElement)
      }
      else if (formArray.get(key) instanceof FormArray) {
        this.validateFormArray(formArray.get(key) as FormArray, notValidElement)
      }
    })
  }
  scrollToAnchor(x) {
    const element = document.querySelector(`[formControlName=${x}]`);
    this.closeExpansionPanel = true
    if (element) {
      element.scrollIntoView()
      return true;
    }
    return false;
  }
  getControlPath(c: AbstractControl, path: string): string | null {
    path = this.getName(c) + path;
    if (c.parent && this.getName(c.parent)) {
      path = "." + path;
      return this.getControlPath(c.parent, path);
    } else {
      return path;
    }
  }
  validateFormControl2(name: string, vField: FormControl, notValidElement: any[]) {
    const controlErrors: ValidationErrors = vField.errors;
    vField.markAsDirty()
    vField.markAsTouched()

    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(keyError => {
        let path = ''
        path = this.getControlPath(vField, path)
        let element: notValidElement = { formcontrol: vField, path: path }
        let dateFields = this.getDateFields()
        for (let item of dateFields) {
          if (path.includes(item)) {
            element.inputType = 'date'
            break;
          }
        }
        notValidElement.push(element)
      });
    }
  }
  validateFormArray2(formArray: FormArray, notValidElement: notValidElement[], notValidElementsLabels: any[]) {
    Object.keys(formArray.controls).forEach(key => {
      if (formArray.get(key) instanceof FormGroup) {
        this.validateFormGroup2(formArray.get(key) as FormGroup, notValidElement, notValidElementsLabels)
      }
      else if (formArray.get(key) instanceof FormControl) {
        this.validateFormControl2(key, formArray.get(key) as FormControl, notValidElement)
      }
      else if (formArray.get(key) instanceof FormArray) {
        this.validateFormArray2(formArray.get(key) as FormArray, notValidElement, notValidElementsLabels)
      }
    })
  }
  validateBooleanFields(name: string, x: FormControl, notValidElement: any[]) {

  }
  getName(control: AbstractControl): string | null {
    let group = <FormGroup>control.parent;
    if (!group) {
      return null;
    }
    let name: string;
    Object.keys(group.controls).forEach(key => {
      let childControl = group.get(key);
      if (childControl !== control) {
        return;
      }
      name = key;
    });
    return name;
  }
  getOptionsFromLockup(field: notValidElement) {
    let boolenFields: string[] = this.getBooleanFields()
    let booleanOptions = [{ value: 'Y', key: true }, { value: 'N', key: false }]
    let isPrimaryOptions=[{ value: 'Y', key: true }, { value: 'N', key: null }]
    for (let element of boolenFields) {
      if (field.path.includes(element)) {
        field.inputType = 'boolean'

        break;
      }
    }
    if (field.path.includes("tphContactType")) {
      field.options = LookupClass.contact_type
    }
    else if (field.path.includes("tphCommunicationType")) {
      field.options = LookupClass.communication_type
    }
    else if (field.path.includes("identification") && field.path.includes("type")) {
      field.options = LookupClass.identifier_type
    }
    else if (field.path.includes("issueCountry")) {
      field.options = LookupClass.country
    }
    else if (field.path.includes("addressType")) {
      field.options = LookupClass.contact_type
    }
    else if (field.path.includes("countryCode")) {
      field.options = LookupClass.country
    }
    else if (field.path.includes("passportCountry")) {
      field.options = LookupClass.country
    }
    else if (field.path.includes("incorporationCountryCode")) {
      field.options = LookupClass.country
    }
    else if (field.path.includes("currencyCode")) {
      field.options = LookupClass.Currency
    }
    else if (field.path.includes("personalAccountType")) {
      field.options = LookupClass.account_type
    }
    else if (field.path.includes("statusCode")) {
      field.options = LookupClass.account_status_type
    }
    else if (field.path.includes("reportIndicators.indicator.CT")) {
      field.options = this.CategoryObject.customerTypeData
    }
    else if (field.path.includes("reportIndicators.indicator.CIU")) {
      field.options = this.CategoryObject.customerInformatonUpdateData
    }
    else if (field.path.includes("reportIndicators.indicator.CC")) {
      field.options = this.CategoryObject.customerClassificationData
    }
    else if (field.path.includes("reportIndicators.indicator.DM")) {
      field.options = this.CategoryObject.detectionMethodData
      field.isMultiSelect = true
    }
    else if (field.path.includes("reportIndicators.indicator.PC")) {
      field.options = this.CategoryObject.predictCrimeData
      field.isMultiSelect = true
    }
    else if (field.path.includes("reportIndicators.indicator.SP")) {
      field.options = this.CategoryObject.suspicionPatternData
      field.isMultiSelect = true
    }
    else if (field.path.includes("reportIndicators.indicator.ST")) {
      field.options = this.CategoryObject.suspicionTypeData
    }
    //reportingPersonType
    else if (field.path.includes("reportingPersonType")) {
      field.options = LookupClass.reportingPersons
    }
    else if (field.path.includes("transmodeCode")) {
      field.options = LookupClass.transmode_code
    }
    else if (field.path.includes("role")) {
      field.options = LookupClass.account_person_role_type
    }

    else if (field.path.includes("nationality") ||
      field.path.includes("fromCountry") || field.path.includes("toCountry") || field.path.includes("residence")) {
      field.options = LookupClass.country
    }
    else if (field.path.includes("gender")) {
      field.options = LookupClass.gender_type
    }
    else if (field.path.includes("fromFundsCode") || field.path.includes("toFundsCode")) {
      field.options = LookupClass.funds_type
    }
    //incorporationLegalForm
    else if (field.path.includes("incorporationLegalForm")) {
      field.options = LookupClass.legal_form_type
    }
    else if (field.path.includes("itemType")) {
      field.options = LookupClass.item_type
    }
    else if (field.path.includes("isPrimary")) {
      field.options = isPrimaryOptions
    }
    // 



  }
  getFieldsWithSelect(validationErrors: notValidElement[]) {
    let result = []
    let selectFields = ["tphContactType", "tphCommunicationType",
      "identification.type", "issueCountry", "addressType", "countryCode", "passportCountry",
      "incorporationCountryCode", "currencyCode", "personalAccountType", "statusCode"]
    validationErrors.forEach(a => {
      if (a.path.includes("reportIndicators.indicator")) {
        result.push(a.path)
      }
      else {
        for (let x of selectFields) {
          if (a.path.includes(x)) {
            result.push(a.path)
            break;
          }
        }
      }
    })
    return result
  }
  getDateFields() {
    return ["submissionDate", "reportCreatedDate", "reportClosedDate", "dateDeceased",
      "birthdate", "dateBusinessClosed", "dateBalance", "changeBeginDate", "dateTransaction",
      "datePosting",
      "valueDate", "registration_date", "opened", "closed", "dateBalance",
      "incorporationDate", "dateBusinessClosed",
      "issueDate",
      "expiryDate"
    ]
  }
  getBooleanFields() {
    return ["deceased", "businessClosed", "value", "nonBankInstitution"]
  }
  static markFormControl(x: AbstractControl) {
    if (x instanceof FormControl) {
      x.markAsDirty()
      x.markAsTouched()
    }
    else if (x instanceof FormArray) {
      for (let z of x.controls) {
        this.markFormControl(z)
      }
    }
    else if (x instanceof FormGroup) {
      Object.keys(x).forEach(a => {
        this.markFormControl(x[a])
      })
    }
  }
  getFormGroupWithAllKeysEqualNull(F_Group: FormGroup, keys: any[]): boolean {
    let filteredKeys = keys.filter(a =>
      F_Group.contains(a) && (F_Group.get(a).value == null)
    )
    return filteredKeys.length == keys.length
  }
  static minLengthArray(min: number) {
    return (c: AbstractControl): { [key: string]: any } => {
      if (c.value.length >= min)
        return null;
      return { 'minLengthArray': { valid: false } };
    }
  }
  static checkFormArrayAtLeastOne(control: AbstractControl, path: string) {
    let array: FormArray = control.get(path) as FormArray
    let check = array && array.length < 1
    return check
  }
  static FindAndFormMessage(control, path, message, location, store: any[]) {
    let lessThanOne = UtilService.checkFormArrayAtLeastOne(control, path)
    if (lessThanOne) {
      let result: notValidElementWithMessageOnly = { message, name: location }
      store.push(result)
    }
  }
  convertDateFromServer(date: string) {
    // let pattern="(\\d{4}-\\d{2}-\\d{2})(T)(\\d{2}:\\d{2}:\\d{2})(Z)"
    // let strin:string="
    if (Date.parse(date) && date) {
      return this.datepipe.transform(date, "yyyy-MM-dd")
    }
    return null
  }
  convertDateToServer(date: string) {
    if (date) {
      let result: any[] = date.match("\\d{4}-\\d{2}-\\d{2}")
      if (result && result.length > 0) {
        return result[0] + "T00:00:00Z"
      }
      //  return this.datepipe.transform(date,"yyyy-MM-ddT00:00:00Z")
    }
    return null
  }
  changeDateFormat(x) {
    if (x && (typeof x == 'object')) {
      for (let item in x) {
        if (this.getDateFields().includes(item) && x[item]) {
          x[item] = this.convertDateToServer(x[item])
        }
        this.changeDateFormat(x[item])
      }
    }
    else if (x && this.getDateFields().includes(x)) {
      x = this.convertDateToServer(x)
    }
  }
  checkLockUpValue(propertyName, key) {
    let items: lockUpChecker[] = [{ lockUpValue: LookupClass.Currency, propertyName: 'currencyCode' },
    { lockUpValue: LookupClass.account_type, propertyName: 'personalAccountType' },
    { lockUpValue: LookupClass.account_status_type, propertyName: 'statusCode' },
    { propertyName: 'SignatoryRole', lockUpValue: LookupClass.account_person_role_type },
    { propertyName: 'gender', lockUpValue: LookupClass.gender_type },
    { propertyName: 'nationality', lockUpValue: LookupClass.country },
    { lockUpValue: LookupClass.country, propertyName: 'residence' },
    { propertyName: 'tphContactType', lockUpValue: LookupClass.contact_type },
    { propertyName: 'tphCommunicationType', lockUpValue: LookupClass.communication_type }, {
      lockUpValue: LookupClass.identifier_type, propertyName: 'idType'
    },
    { propertyName: 'addressType', lockUpValue: LookupClass.contact_type }
      , { propertyName: 'FundsCode', lockUpValue: LookupClass.funds_type },
      {propertyName:'transmodeCode',lockUpValue:LookupClass.transmode_code},
      {propertyName:'submissionCode',lockUpValue:LookupClass.submission_type},
      {propertyName:'reportCode',lockUpValue:LookupClass.report_type},
      {propertyName:'PropertyStatusMaster',lockUpValue:LookupClass.trans_item_status},
      {propertyName:'TransactionItemType',lockUpValue:LookupClass.item_type

      }
    ]
    for (let item of items) {
      if (item.propertyName == propertyName) {
        return this.filterLockUp(item.lockUpValue, key)
        break;
      }
    }

    return false
  }
  filterLockUp(lockupValues: Data[], key) {
    
    let x: Data[] = lockupValues.filter(elment => elment.key == key)
    return x.length > 0

  }
  updateFormGroupValidity(form:FormGroup,fields:string[]){
    for (const field of fields) {
     if( form.get(field))
        form.get(field).updateValueAndValidity()
    }
}
checkTransactionTo(transaction: FormGroup) {
  let tTo=transaction.get('tTo')
  let tToMyClient=transaction.get('tToMyClient')
  this.getFormGroupWithAllKeysEqualNull(transaction as FormGroup, ['tToMyClient', 'tTo'])
  let toNotFound: notValidElementWithMessageOnly = { message: "Select Transaction To Type", name: `transaction ` }
  if (this.getFormGroupWithAllKeysEqualNull(transaction as FormGroup, ['tToMyClient', 'tTo']))
    return toNotFound
  if(tTo.value!=null){
   return this.getFormGroupWithAllKeysEqualNull(tTo as FormGroup , ['toAccount', 'toPerson','toEntity'])?  toNotFound : null
     
  }
  
 else if(tToMyClient.value!=null ){
     return this.getFormGroupWithAllKeysEqualNull(tToMyClient as FormGroup , ['toAccount', 'toPerson','toEntity'])?toNotFound:null
  }   
  return null
}

checkTransactionFrom(transaction: FormGroup) {
  let tFrom=transaction.get('tFrom')
  let tFromMyClient=transaction.get('tFromMyClient')
  let fromNotFound: notValidElementWithMessageOnly = { message: "Select Transaction From Type", name: `transaction ` }
  if (this.getFormGroupWithAllKeysEqualNull(transaction as FormGroup, ['tFromMyClient', 'tFrom']))
    return fromNotFound
    if(tFrom.value!=null){
      return this.getFormGroupWithAllKeysEqualNull(tFrom as FormGroup , ['fromAccount', 'fromPerson','fromEntity'])?  fromNotFound : null
        
     }
     
    else if(tFromMyClient.value!=null ){
        return this.getFormGroupWithAllKeysEqualNull(tFromMyClient as FormGroup , ['fromAccount', 'fromPerson','fromEntity'])?fromNotFound:null
     }   
     return null



}
}

export interface lockUpChecker {
  propertyName: string,
  lockUpValue: Data[]
}