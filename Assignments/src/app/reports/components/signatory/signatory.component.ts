import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatCheckbox } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UtilService } from 'src/app/util.service';
import { ToastrService } from 'ngx-toastr';
import { t_signatory } from '../../validationControls/t_signatory';
import { SignatoryService } from '../../services/signatory.service';
import { LookupClass } from '../../lockups/Lookups';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { Data } from '../../lockups/report-indiators-lockup';
import { notValidElementWithMessageOnly } from '../../models/notValidElement';

@Component({
  selector: 'app-signatory',
  templateUrl: './signatory.component.html',
  styleUrls: ['./signatory.component.css']
})
export class SignatoryComponent implements OnInit {
  falseValue = null
trueValue = true;

checkboxChange(checkbox: MatCheckbox, checked: boolean) {
  console.log('checkboxChange');
  if( !checked ){
    this.signatoryFormControl.controls.isPrimary.setValue(null)
  }
}
   options=[{key:'Yes',value:true},{key:'No',value:null}]
  notValidElement: any[] = [];
  personForm: FormGroup;
  partyNumber = '';
  AccountNumber: string = null;
  t_signatory: t_signatory = new t_signatory();
  signatoryFormControl: FormGroup;
  gender_values = LookupClass.gender_type;
  country_values = LookupClass.country;
  account_person_role_type = LookupClass.account_person_role_type;

  constructor(public dialogRef: MatDialogRef<SignatoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public fb: FormBuilder, public utilservice: UtilService,
    public toaster: ToastrService, private signatoryService: SignatoryService, private dataService: SharingDataServieService) {
    this.dialogRef.disableClose = true;
  }

  ngOnInit() {

    if (!this.data.IsMyAccount)

      this.signatoryFormControl = this.t_signatory.set_signatoryControl(this.data.signatoryControl.value);
    else {
      this.signatoryFormControl = this.t_signatory.set_MysignatoryControl(this.data.signatoryControl.value);

    }
   

    if (this.data.accountNumber) {
      this.AccountNumber = this.data.accountNumber;
    }

    this.personForm = this.signatoryFormControl.get('tPerson') as FormGroup;

  }

  saveSignatory() {
    this.dialogRef.close(this.signatoryFormControl);
    if (this.personForm.valid) {
      this.dialogRef.close(this.signatoryFormControl)

    }
    else {
      this.toaster.error("Person Inforamation not complete!!", "Validation Error", {
        closeButton: true
        , progressBar: true,  enableHtml: true
      })
    }

  }




  closePerson() {

    this.dialogRef.close();
  }
  validate() {
    this.clearValidationErrors();
    this.utilservice.validateFormGroup(this.signatoryFormControl, this.notValidElement);
  }

  clearValidationErrors() {
    this.notValidElement = [];
  }
  scrollToAnchor(x) {
    this.utilservice.scrollToAnchor(x);
  }
  searchSignatoryByPartyNumber() {

    this.signatoryService.getSignatoryDetailByPartyNumber(this.partyNumber, this.AccountNumber).subscribe((data: any) => {
      

      if (data && data.tPerson) {
        this.toaster.success('Signatory with party number is found');
        this.signatoryFormControl = this.t_signatory.set_signatoryControl(data);
        // this.setIsPrimary()
        this.personForm = this.signatoryFormControl.get('tPerson') as FormGroup;
      } else {
        this.toaster.warning(`There is no signatory with party number ${this.partyNumber}`);
      }
    }, error => {

      

      this.toaster.error(error.message);
    });


  }
  saveSignatoryToSource() {
    let notValidSection:notValidElementWithMessageOnly[]=[]

    if(!this.signatoryFormControl.valid){
      this.toaster.error("Signatory is not valid!!","",{timeOut:3000})
      this.utilservice.validateFormGroup2(this.signatoryFormControl, [],notValidSection);
      for (const iterator of notValidSection) {
        this.toaster.error(iterator.name,iterator.message,{timeOut:3000})

      }
      return
    }
    this.signatoryFormControl.get('roleDesc').setValue(this.getRoleDesc());
    const personValue = this.signatoryFormControl.get('tPerson').value;

    const Signatory = Object.assign({}, personValue);
    Signatory.role = this.signatoryFormControl.get('role').value;
    Signatory.roleDesc = this.signatoryFormControl.get('roleDesc').value;
    Signatory.isPrimary = this.signatoryFormControl.get('isPrimary').value;
    Signatory.accountNumber = this.AccountNumber;
    




    this.dataService.signatoryObject.emit(personValue);

    this.signatoryService.saveSignatoryToSource(Signatory).subscribe(data => {
      if (data) {
        this.toaster.success('Signatory is saved successfully');
      }
    },
      error => {

        

        this.toaster.error(error.error.message);
      });
  }
  getRoleDesc() {
    const key = this.signatoryFormControl.get('role').value;
    

    // let z:Data[]=this.party_role_type.filter((a)=>{a.key==key})
    for (const v of this.account_person_role_type) {
      

      if (v.key == key) {
        
        return v.value;
      }
    }


    return '-'
  }
  changeRole() {

    this.signatoryFormControl.get('roleDesc').setValue(this.getRoleDesc());

  }
  setIsPrimary() {
    const signatoryRole = this.signatoryFormControl.get('role').value;

    this.signatoryFormControl.get('isPrimary').setValue(signatoryRole == 'PS');
  }
}
