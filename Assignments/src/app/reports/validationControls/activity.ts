import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from "@angular/forms";
import { ReportParties } from "./reportParties";

export class activity{
    reportParties:ReportParties=new ReportParties(this.fb);
    constructor(public fb: FormBuilder) {

    }
createActivity():FormGroup{
    return this.fb.group({
        id:new FormControl(),
        goodsServices:new FormControl(),
        reportParties:this.reportParties.createReportParties(),
        comments:new FormControl()
    })
}
setActivity(activity):FormGroup{
    return this.fb.group({
        id:new FormControl(activity.id),
        goodsServices:new FormControl(),

        reportParties:this.reportParties.setReportParties(activity.reportParties),
        comments:new FormControl(activity.comments)
    })
}
}