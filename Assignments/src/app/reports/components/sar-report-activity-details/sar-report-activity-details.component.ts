import { Component, OnInit, Inject, EventEmitter, Output, Input, ChangeDetectionStrategy, ChangeDetectorRef, DoCheck, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, ValidationErrors, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { EntityForm } from '../../models/entityFormGroup';
import { AccountForm } from '../../models/accountFormGroup';
import { t_person } from '../../validationControls/t_person';
import { t_report_party_type } from '../../validationControls/report_party_type';
import { ToastrService } from 'ngx-toastr';
import { t_phone } from '../../validationControls/t_phone';
import { UtilService } from 'src/app/util.service';
import { t_account } from '../../validationControls/t_account';
import { of } from 'rxjs';
import { tEntity } from '../../validationControls/t_entity';
import { ActivityService } from '../../service/activity.service';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';
import { Action } from 'rxjs/internal/scheduler/Action';
import { Actions } from '../../Actions/action';
@Component({
  selector: 'app-sar-report-activity-details',
  templateUrl: './sar-report-activity-details.component.html',
  styleUrls: ['./sar-report-activity-details.component.css'],
  // changeDetection:ChangeDetectionStrategy.OnPush
})

export class SarReportActivityDetailsComponent implements OnInit, OnChanges, OnDestroy {

  disableParty:boolean;
significanceList=[1,2,3,4,5,6,7,8,9,10]
  timerid = null;
  ngOnChanges(changes: SimpleChanges) {
    
    for (const propName in changes) {

      const chng = changes[propName];

      const cur  = chng.currentValue;
      const prev = chng.previousValue;
      if (propName == 'ActivityControl') {
       
        this.ActivityControl = cur;
        // this.setActivity();



      }


    }
  }
  @Input() ActivityControl: FormGroup;
  notValidElement: string[] = [];
  party_type = '';
  personFormControl: FormGroup;
  entityFormControl: FormGroup;
  accountFormControl: FormGroup;
  reportPartyForm = new t_report_party_type(this.fb);
  t_person: t_person = new t_person();
  tEntity: tEntity = new tEntity();
  t_account: t_account = new t_account();
  @Output() saveEvent = new EventEmitter();
  @Output() changeEvent = new EventEmitter();
  reportParty: FormGroup; 
  createMode= true;
  saveActivityAction:boolean=false
  editActivityAction:boolean=false

  constructor(public fb: FormBuilder,
    public toaster: ToastrService,
    private utilservice: UtilService,
    private ref: ChangeDetectorRef,
    private activityService:ActivityService,private auth:AuthService) {
      // this.timerid = setInterval(() => this.TimeInterval(), 10000);



  }
  TimeInterval(): void {

    localStorage.setItem('ReportAct', JSON.stringify(this.reportParty.value));

  }
  setActivity() {
    this.party_type = '';
    this.createMode = true;

    this.reportParty  = this.reportPartyForm.create_report_party_type();

    if (this.ActivityControl) {
      this.reportParty = this.reportPartyForm.set_report_party_type(this.ActivityControl.value);

      this.createMode = false;


      if (this.reportParty.get('entity').value) {
        this.party_type = 'Entity';
        this.entityFormControl = this.reportParty.get('entity') as FormGroup;
      } else if (this.reportParty.get('account').value) {
        this.party_type = 'Account';
        this.accountFormControl = this.reportParty.get('account') as FormGroup;

      } else if (this.reportParty.get('person').value) {
        this.party_type = 'Person';
        this.personFormControl = this.reportParty.get('person') as FormGroup;
      }
    } else {

      this.personFormControl = this.t_person.createPersonControl();
      this.entityFormControl = this.tEntity.createEntityControl();
      this.accountFormControl = this.t_account.createAccountControl();

    }


  }

  ngOnInit() {
    
this.setActivity();
this.timerid = setInterval(() => this.TimeInterval(), 10000);
this.setActions()

  }

  // disabledform(group: FormGroup): void {

  //   Object.keys(group.controls).forEach((key: string) => {
  //     const control = group.get(key);
  //     if (control instanceof FormGroup) {
  //       this.disabledform(control);

  //       control.disable()
       
  //     } else {
  //       control.disable()
  //     }
  //   });
  // }

  closeDialog() {
  clearInterval(this.timerid)
  localStorage.removeItem('ReportAct')
  }
  save(element: FormGroup) {
clearInterval(this.timerid)

  localStorage.removeItem('ReportAct')
    this.validateFormGroup(element)
    if (element.invalid) {
      Object.keys(element.controls).forEach(key => {
        element.get(key).markAsDirty();
        element.get(key).markAsTouched();
      });

    }


    this.saveEvent.emit(this.reportParty);


  }

  validateFormControl(name, x: FormControl) {


    const controlErrors: ValidationErrors = x.errors;
    x.markAsDirty();
    x.markAsTouched();

    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(keyError => {

        this.notValidElement.push(name);
      });
    }
  }
  validateFormGroup(x: FormGroup) {
    Object.keys(x.controls).forEach(key => {
      if (x.get(key) instanceof FormGroup) {
        this.validateFormGroup(x.get(key) as FormGroup);
      } else if (x.get(key) instanceof FormArray) {
        this.validateFormArray(x.get(key) as FormArray);
      } else if (x.get(key) instanceof FormControl) {
        this.validateFormControl(key, x.get(key) as FormControl);
      }

    });
  }
  validateFormArray(formArray: FormArray) {
    Object.keys(formArray.controls).forEach(key => {
      if (formArray.get(key) instanceof FormGroup) {
        this.validateFormGroup(formArray.get(key) as FormGroup);
      } else if (formArray.get(key) instanceof FormControl) {
        this.validateFormControl(key, formArray.get(key) as FormControl);

      } else if (formArray.get(key) instanceof FormArray) {
        this.validateFormArray(formArray.get(key) as FormArray);
      }
    });


  }
  validate(x: FormGroup) {
    this.notValidElement = [];
    this.validateFormGroup(x);




  }
  private scrollToAnchor(anchor: string): boolean {
    const element = document.querySelector(`[formControlName=${anchor}]`);
    this.utilservice.closeExpansionPanel = true;
    if (element) {
      element.scrollIntoView();

      return true;
    }
    return false;
  }
  clearValidationErrors() {
    this.notValidElement = [];
  }
  changeReportParty() {


    this.accountFormControl.reset();
    this.entityFormControl.reset();
    this.personFormControl.reset();
    this.clearValidationErrors();

    if (this.party_type == 'Entity') {
      this.reportParty.setControl('entity', this.entityFormControl);

      this.reportParty.setControl('account', new FormControl());
      this.reportParty.setControl('person', new FormControl());
    } else if (this.party_type == 'Person') {
      this.reportParty.setControl('person', this.personFormControl);
      this.reportParty.setControl('entity', new FormControl());
      this.reportParty.setControl('account', new FormControl());

    } else if (this.party_type == 'Account') {
      this.reportParty.setControl('account', this.accountFormControl);

      this.reportParty.setControl('entity', new FormControl());
      this.reportParty.setControl('person', new FormControl());

    }
  }
  saveActivity() {
    clearInterval(this.timerid)
  localStorage.removeItem('ReportAct')
    
   if( this.party_type==''){
  this.toaster.warning("Plase select Activity Type")
   }
   else{
    this.saveEvent.emit(this.reportParty)
   }
  }
  changePerson(event: FormGroup) {

this.reportParty.removeControl('person');
this.reportParty.addControl('person', event);
this.personFormControl = this.reportParty.get('person') as FormGroup;
// this.personFormControl.patchValue({"id":0})
  }
  changeEntity(event) {

    this.reportParty.removeControl('entity');
    this.reportParty.addControl('entity', event);
    this.entityFormControl = this.reportParty.get('entity') as FormGroup;
  }
  changeAccount(event) {



    this.reportParty.removeControl('account');
    this.reportParty.addControl('account', event);
    this.accountFormControl = this.reportParty.get('account') as FormGroup;
  }
  ngOnDestroy(){
    localStorage.removeItem('ReportAct')
    clearInterval(this.timerid)
   
    
  }
  editActivity() {

    const tyt = (this.ActivityControl.parent as FormArray);


  tyt.controls.filter(a => a != this.ActivityControl);
  let index = 0;
  for (const x of tyt.controls) {
if (x == this.ActivityControl) {

tyt.removeAt(index);
tyt.push(this.reportParty);
  break;

}
index++;
  }

this.changeEvent.emit(this.reportParty);
  }
  getActivityPartyByNumber(idNumber: HTMLInputElement) {


    this.activityService.getActivityPartyByNumber(idNumber.value).subscribe((data: any) => {
      if (data) {

        this.reportParty.setControl('person', new FormControl());
        this.reportParty.setControl('account', new FormControl());
        this.reportParty.setControl('entity', new FormControl());

      const type = data.type;
      if (type) {
        if (type == 'person') {
          this.personFormControl = this.t_person.setPersonControl(data.person);
          this.reportParty.removeControl('person');
          this.reportParty.addControl('person', this.personFormControl);

          this.party_type = 'Person';
        } else if (type == 'entity') {
          this.reportParty.removeControl('entity');
          this.entityFormControl = this.tEntity.setEntityControl(data.entity);
          this.reportParty.addControl('entity', this.entityFormControl);
          this.party_type = 'Entity';
        } else if (type == 'account') {
          this.reportParty.removeControl('account');
          this.accountFormControl = this.t_account.setAccountControl(data.account);
          this.reportParty.addControl('account', this.accountFormControl);
          this.party_type = 'Account';
        }
      }
    } else {
this.toaster.error('No data returned');
this.party_type = 'None';
}
}, error => {


  this.toaster.error(error.error);  
  this.party_type = 'None';
});
  }
  setActions(){
    this.editActivityAction=this.auth.has_Capabilities(Actions.editActivity)
    this.saveActivityAction=this.auth.has_Capabilities(Actions.saveActivity)
  }
}
