import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {

  @Output() increamentNumberEvent  = new EventEmitter<number>();
  timer: number;
  interval: any;
  
  constructor() { }

  ngOnInit() {
  
  }

  startGame(){
    this.timer = 0 ;
    this.interval = setInterval(()=>{this.increamentNumberEvent.emit(++this.timer)}, 1000);
  }

  stopGame(){
    console.log(this.interval);
    clearInterval(this.interval);
 //   this.interval = 0;
  }

}
