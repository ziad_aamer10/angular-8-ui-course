export const environment = {
  production: true,
  projectName: '/dg-goaml',
  entity_branch: 'Head Office',
  mockLogin: false,
  // DG Test
  projectUrl: 'http://192.168.1.72:9090/dg-goaml',
  projectDomain: '192.168.1.72:9090',
  // Dev
  // projectUrl: 'http://localhost:8080/dg-goaml',
  // projectDomain: 'localhost:8080',
  // ADIB PreUAT
  // projectUrl: 'http://10.240.5.229:8082/dg-goaml',
  // projectDomain: '10.240.5.229:8082',
  // ADIB UAT
  // projectUrl: 'http://10.240.10.229:8082/dg-goaml',
  // projectDomain: '10.240.10.229:8082',
  // CIB dev
  // projectUrl: 'http://10.11.91.95:8082/dg-goaml',
  // projectDomain: '10.11.91.95:8082/dg-goaml',
  sarEditRoute: 'reports/sar/edit',
  strEditRoute: 'reports/str/edit',
  aifEditRoute: 'reports/aif/edit',
  adminRoute: 'reports/Admin',
};
