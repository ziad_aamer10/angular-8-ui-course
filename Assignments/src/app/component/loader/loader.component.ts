import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Subject } from 'rxjs';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit,AfterViewInit {
  color = 'primary';
  mode = 'indeterminate';
  value = 50;
  isLoading: Subject<boolean> = this.loaderService.isLoading;
  constructor(private loaderService: LoaderService,private cdRef:ChangeDetectorRef){}
  

  ngOnInit() {
    // console.log("isLoading===" , this.isLoading);
    
  }
  ngAfterViewInit() {

    this.cdRef.detectChanges();
     }
}
