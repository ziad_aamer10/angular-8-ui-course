import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  serverElements = [{name:'TestServer', content:'just a test', type: 'server'}];

  onServerAdded(event: {serverName: string, serverContent: string}) {
    this.serverElements.push({
      name: event.serverName,
      content: event.serverContent,
      type: 'server'
    });
  }

  onBlueprintAdded(blueprintData: {serverName: string, serverContent: string}) {
     this.serverElements.push({
       type: 'blueprint',
       name: blueprintData.serverName,
       content: blueprintData.serverContent
     });
  }
  changeFirstElementName(){
    this.serverElements[0].name = 'changed';
  }

  deletedFirstElement(){
    this.serverElements.splice(0,1);
  }

}
