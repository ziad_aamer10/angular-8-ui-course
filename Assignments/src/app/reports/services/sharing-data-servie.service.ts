import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { t_transaction } from '../validationControls/t_transaction';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';


// this a sharing data between Unrelated Components , not child or parent relationships .
@Injectable({
  providedIn: 'root'
})
export class SharingDataServieService {
  
  signatoryObject = new EventEmitter<Object>() ;
  saveValidationInTransactionTable = new EventEmitter<Object>();

  constructor(private fb:FormBuilder) { }
  
  // startedEditing = new Subject<number>() ; 
  // editedObject = new Subject<Object>() ; 


  t_transaction:t_transaction = new t_transaction(this.fb);

  private messageSource = new BehaviorSubject(null);
  currentMessage = this.messageSource.asObservable();

  //  sharing edited entity or person or account 
  private messageSource1 = new BehaviorSubject(FormGroup);
  currentMessage1 = this.messageSource1.asObservable();


  private messageSource2 = new BehaviorSubject(null);
  currentMessage2 = this.messageSource2.asObservable();

  private messageSource3 = new BehaviorSubject(null);
  currentMessage3 = this.messageSource3.asObservable();


  private messageSource4 = new BehaviorSubject("");
  currentMessage4 = this.messageSource4.asObservable();

  
  
  changeMessage(t_transactions) {
    this.messageSource.next(t_transactions)
  }
  changeMessage1(changedObject) {
    this.messageSource1.next(changedObject)
  }

  changeMessage2(msg) {
    this.messageSource2.next(msg)
  }
  
  
  changeMessage3(msg) {
    this.messageSource3.next(msg)
  }
  
  changeMessage4(msg) {
    this.messageSource4.next(msg)
  }
  
  private messageSource5 = new BehaviorSubject(null);
  currentMessage5 = this.messageSource5.asObservable();
  sendTrasactionAfterEdit(msg) {
    this.messageSource5.next(msg)
  }

  private messagSourceDisableReport = new BehaviorSubject(null);
  currentMessageDisableReport = this.messagSourceDisableReport.asObservable()

  changeMessageDisableReport(msg){
    
    this.messagSourceDisableReport.next(msg)
  }
}
