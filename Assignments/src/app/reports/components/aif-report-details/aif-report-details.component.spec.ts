import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AifReportDetailsComponent } from './aif-report-details.component';

describe('AifReportDetailsComponent', () => {
  let component: AifReportDetailsComponent;
  let fixture: ComponentFixture<AifReportDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AifReportDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AifReportDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
