import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {FormArray, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {Store} from "@ngrx/store";
import {map} from "rxjs/operators";
import * as fromApp from '../../store/app.reducer';
import * as RecipesActions from '../store/recipe.actions';
import {AddRecipe} from "../store/recipe.actions";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit, OnDestroy {

  id: number = null;
  editMode = false;
  recipeForm: FormGroup;
  storeSubscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((param: Params) => {
      this.id = +param.id;
      this.editMode = param.id != null;
      this.initForm();
    });
  }

  initForm() {
    let recipeName = '';
    let imagePath = '';
    let recipeDescription = '';
    const recipeIngredients = new FormArray([]);

    if (this.editMode) {
      // const editRecipe = this.recipeService.getRecipe(this.id);
      this.storeSubscription = this.store.select('recipes').pipe(
        map( resState => resState.recipes.find((res, idx)=> this.id === idx)))
        .subscribe(editRecipe => {
          recipeName = editRecipe.name;
          imagePath = editRecipe.imagePath;
          recipeDescription = editRecipe.description;
          if (editRecipe.ingredients) {
            for (const ingred of editRecipe.ingredients) {
              recipeIngredients.push(new FormGroup({
                name: new FormControl(ingred.name, Validators.required),
                amount: new FormControl(ingred.amount,
                  [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
              }));
            }
          }
        });

    }

    this.recipeForm = new FormGroup({
      name: new FormControl(recipeName, Validators.required),
      imagePath: new FormControl(imagePath, Validators.required),
      description: new FormControl(recipeDescription, Validators.required),
      ingredients: recipeIngredients
    });

  }

  get ingredientsControls() { // a getter!
    return (this.recipeForm.get('ingredients') as FormArray).controls;
  }

  onSubmit() {

    if (this.editMode) {
      // this.recipeService.updateRecipe(this.id, this.recipeForm.value);
      this.store.dispatch(new RecipesActions.UpdateRecipe({index: this.id, recipe: this.recipeForm.value}));
    } else {
      // this.recipeService.addNewRecipe(this.recipeForm.value);
      this.store.dispatch(new AddRecipe(this.recipeForm.value));
    }
    this.router.navigate(['../'], {relativeTo: this.activatedRoute});
  }

  onAddIngredient() {
    (this.recipeForm.get('ingredients') as FormArray).push(
      new FormGroup({
        name: new FormControl(null, Validators.required),
        amount: new FormControl(null,
          [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)])
      })
    );
  }

  onDeleteIngredient(index: number) {
    (this.recipeForm.get('ingredients') as FormArray).removeAt(index);
  }

  ngOnDestroy(): void {
    if(this.storeSubscription)
      this.storeSubscription.unsubscribe();
  }

}
