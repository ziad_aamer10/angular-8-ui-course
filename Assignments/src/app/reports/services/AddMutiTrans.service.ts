import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { Observable } from 'rxjs';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
// import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class AddMultiTransService {
//  private uri = environment.projectUrl + '/multiTransactionFromSourceExt';
//   error handling 
//   https://scotch.io/bar-talk/error-handling-with-angular-6-tips-and-best-practices192
// https://stackblitz.com/edit/error-handling-httpclient?file=src%2Fapp%2Fuser.service.ts
  constructor(private http: HttpClient) { }
  SearchMultiTrans(Filter){
// // return this.http.post(this.uri,Filter)
 
// // return this.transjson

const url = environment.projectUrl + '/multiTransactionFromSourceExt';
return this.http.post(url , Filter);
  }
 

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
  
}
