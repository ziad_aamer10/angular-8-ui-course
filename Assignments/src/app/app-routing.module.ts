
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AuthGuardService } from './loginModule/services/auth-guard.service';
import { WelcomeComponent } from './loginModule/components/welcome/welcome.component';
import { CustomPreloadingStrategy } from './LoadingStrategy/CustomPreloadingStrategy';




const routes: Routes = [
  {
    path: 'login',
    loadChildren: './loginModule/login.module#LoginModule'
  },


  {
    path: '',
    component: WelcomeComponent,
    canActivate: [AuthGuardService],


    children: [
     { path: 'reports', loadChildren: './reports/reports.module#ReportsModule'},
      { path: '', loadChildren: './dashboard/dashboard.module#DashboardModule' }
    ]

  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true })],
  exports: [RouterModule],
  providers:[CustomPreloadingStrategy]
})
export class AppRoutingModule { }
