import { Component, OnInit } from '@angular/core';

import { ServersService } from '../servers.service';
import { ActivatedRoute, Params, Router, Data } from '@angular/router';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  server: {id: number, name: string, status: string};

  constructor(private serversService: ServersService, private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    //  let id: number = +this.activatedRoute.snapshot.params.id;
    //  this.server = this.serversService.getServer(id);
     
    this.activatedRoute.data.subscribe((data: Data)=>{
      console.log('data:', data);
      
      this.server = data['server'];
    });
  }

  onEditServer(){
    //use relative path instead of absolute path
    this.router.navigate(['edit'], {relativeTo: this.activatedRoute, queryParamsHandling: 'preserve'});
  }

}
