import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";

export class t_email {
   emailPattern=new RegExp("[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{1,}[.]{1}[a-zA-Z]{1,}")
    constructor(public fb: FormBuilder) {

    }
    createEmail(): FormControl {
       return this.fb.control('',Validators.compose([Validators.required,Validators.pattern(this.emailPattern)]))
      
    }
    setEmail(email){
        return this.fb.control(email,Validators.compose([Validators.required,Validators.pattern(this.emailPattern)]))

    }
}