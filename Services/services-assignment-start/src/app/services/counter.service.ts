import { EventEmitter } from "@angular/core";

export class CounterService{
    inactivesCounter: number = 0;
    activesCounter: number = 0;
    actionTaken: EventEmitter<void> = new EventEmitter<void>();


    // constructor(){
    //     this.activesCounter = 0;
    //     this.inactivesCounter = 0;
    // }

    addToActive(){
        ++this.activesCounter;
        this.actionTaken.emit();
    }

    addToInActive(){
        ++this.inactivesCounter;
        this.actionTaken.emit();
    }

}