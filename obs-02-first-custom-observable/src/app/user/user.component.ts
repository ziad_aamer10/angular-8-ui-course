import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ActivationService } from '../activation.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit , OnDestroy{
  id: number;
  activatedUser: boolean = false;
  activationSubscription: Subscription;

  constructor(private route: ActivatedRoute, private activationService: ActivationService) {
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params.id;
    });

    this.activationSubscription = this.activationService.activateEmitter.subscribe((data: boolean)=>{
      this.activatedUser = data;
    })
  }

  activate(){
    this.activationService.activateEmitter.next(true);
  }

  ngOnDestroy(){
    this.activationSubscription.unsubscribe();
  }

}
