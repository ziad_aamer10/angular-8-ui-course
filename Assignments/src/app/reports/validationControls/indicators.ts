import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { CategoryClass } from '../lockups/report-indiators-lockup';
import { Data } from '@angular/router';

const fb = new FormBuilder();
export class Indicator {
    CategoryObject = new CategoryClass();
customerTypeData_values = this.CategoryObject.customerTypeData;
customerClassificationData_values = this.CategoryObject.customerClassificationData;
customerInformatonUpdateData_values = this.CategoryObject.customerInformatonUpdateData;
detectionMethodData_values = this.CategoryObject.detectionMethodData;
predictCrimeData_values = this.CategoryObject.predictCrimeData;
suspicionPatternData_values = this.CategoryObject.suspicionPatternData;
suspicionTypeData_values = this.CategoryObject.suspicionTypeData;
getValueByKey(list: Data[], key) {
    for (const x of list) {

      if (x.key == key) {
      return x.value
      }

    }
    return null;
    }
    setIndicators(x) {
        const initial = {'CT':'', "CIU":'', "CC":'', "DM": [], "PC": [],'SP': [],'ST':''};

        for (const a of x) {

         if ( this.customerTypeData_values.map(b => b.key).includes(a)) {
           if (this.getValueByKey(this.customerTypeData_values, a)) {


            initial['CT'] = a;

           }

         } else if ( this.customerClassificationData_values.map(b => b.key).includes(a)) {
           const valueByKey = this.getValueByKey(this.customerClassificationData_values, a);
          if (valueByKey) {

            initial['CC'] = a;

          }

         } else if (  this.customerInformatonUpdateData_values.map(b => b.key).includes(a)) {

        const valueByKey = this.getValueByKey(this.customerInformatonUpdateData_values, a);
          if (valueByKey) {


            initial['CIU'] = a;
          }

         } else if (  this.detectionMethodData_values.map(b => b.key).includes(a)) {

          const valueByKey = this.getValueByKey(this.detectionMethodData_values, a);
            if (valueByKey) {
             const value: string[] = initial['DM'];
              value.push(a);

              initial['DM'] = value;
            }

           }
        //

         else if ( this.predictCrimeData_values.map(b => b.key).includes(a)) {
           // PC
           const valueByKey = this.getValueByKey(this.predictCrimeData_values, a);

           if (valueByKey) {
          const value: string[] = initial['PC'];
          value.push(a);

          initial['PC'] = value;
          }
        } else if (     this.suspicionPatternData_values.map(b => b.key).includes(a)) {
           // SP
           const valueByKey = this.getValueByKey(this.suspicionPatternData_values, a);

           if (valueByKey) {
            const value: string[] = initial['SP'];
          value.push(a);

          initial['SP'] = value;
          }
        } else if ( this.suspicionTypeData_values.map(b => b.key).includes(a)) {
          // ST
          const valueByKey = this.getValueByKey(this.suspicionTypeData_values, a);

          if (valueByKey) {


          initial['ST'] = a;

         }
        }








        }
        return initial;
      }

    createindicatorsController(): FormGroup {
      return  fb.group({

          indicator: fb.group({
            'CC': new FormControl([], Validators.required),
            'CIU': new FormControl([], Validators.required),
            'CT': new FormControl([], Validators.required),
            'DM': new FormControl([], Validators.required),
            'PC': new FormControl([], Validators.required),
            'SP': new FormControl([], Validators.required),
            'ST': new FormControl([], Validators.required),
        })
        });

    }
    setIndicatorsController(indicaors): FormGroup {
      

       const result = this.setIndicators(indicaors);
       

        return  fb.group({

            indicator: fb.group({
              'CC': new FormControl(result.CC, Validators.required),
              'CIU': new FormControl(result.CIU, Validators.required),
              'CT': new FormControl(result.CT, Validators.required),
              'DM': new FormControl(result.DM, Validators.required),
              'PC': new FormControl(result.PC, Validators.required),
              'SP': new FormControl(result.SP, Validators.required),
              'ST': new FormControl(result.ST, Validators.required),
          })
          });

      }
}
