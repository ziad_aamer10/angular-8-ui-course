import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  @ViewChild('f', {static: true}) signUpForm: NgForm;
  secretQuestion = 'teacher';

  suggestUserName() {
    const suggestedName = 'Superuser';
  }


  onSubmit(f){
    console.log('Sign up form: ',this.signUpForm);
    console.log('secretQuestion: ', this.secretQuestion);
    
  }
  // onSubmit(f){
  //   console.log('f: ',f);
  // }
}
