import {Component, OnInit} from '@angular/core';
import {Recipe} from '../recipe.model';
import {ActivatedRoute, Data, Params, Router} from '@angular/router';
import {map, switchMap} from "rxjs/operators";
import {Store} from "@ngrx/store";
import * as fromApp from '../../store/app.reducer';
import * as RecipesActions from '../store/recipe.actions';
import * as ShoppingListActions from "../../shopping-list/store/shopping-list.actions";

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  recipe: Recipe;
  id: number;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.activatedRoute.params.pipe(map(params => +params['id']),
      switchMap(id => {
        this.id = id;
        return this.store.select('recipes').pipe(
          map(state => state.recipes.find((res, index) => index === this.id )))
      }))
      .subscribe((recipe) => {
      this.recipe = recipe;
    });
  }

  sendIngredientsToShoppingList() {
    this.store.dispatch(new ShoppingListActions.AddIngredients(this.recipe.ingredients));
  }

  // setRecipeFromEvent(eventRecipe: Recipe){
  //   this.recipe = eventRecipe;
  // }

  onDelete() {
    this.store.dispatch(new RecipesActions.DeleteRecipe(this.id));
    // this.recipeService.deleteIngredient(this.id);
    this.router.navigate(['../'], {relativeTo: this.activatedRoute});
  }
}
