import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportingPersonService {

  constructor(private http:HttpClient) { }
  getReportingPerson(reportingPersonType:string){
    const url=`${environment.projectUrl}/reportingPersons/${reportingPersonType}`
    return this.http.get(url)
  }
  updateReportingPerson(reportingPerson){
    
    const url=`${environment.projectUrl}/reportingPersons`
    return this.http.put(url,reportingPerson)
  }
}
