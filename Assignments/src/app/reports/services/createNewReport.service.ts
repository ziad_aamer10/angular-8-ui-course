import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { CreateNewReport } from '../validationControls/CreateNewReport';

@Injectable()
export class CrNewReportService {


CreateNewReport: CreateNewReport = new CreateNewReport();

  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  changeMessage(message: string) {
    this.messageSource.next(message);
  }
}
