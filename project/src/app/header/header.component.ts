import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {map} from "rxjs/operators";
import * as fromApp from '../store/app.reducer'
import * as AuthActions from "../auth/store/auth.actions";
import * as RecipesActions from './../recipes/store/recipe.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  isAuthenticated: boolean = false;
  userSubscription: Subscription;
  collapsed = true;


  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.userSubscription = this.store.select('auth').pipe(map(authState => authState.user))
      .subscribe((user)=>{
      this.isAuthenticated = user!=null ? !!user: false;
    });
  }


  onSaveData() {
    this.store.dispatch(new RecipesActions.StoreRecipe());
  }

  onFetchData() {
    this.store.dispatch(new RecipesActions.FetchRecipes());
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  logout() {
    this.store.dispatch(new AuthActions.Logout());
  }
}
