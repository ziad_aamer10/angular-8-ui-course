import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { t_transaction } from '../../validationControls/t_transaction';
import { t_report } from '../../validationControls/t_report';
import { notValidElementWithMessageOnly } from '../../models/notValidElement';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ReportService } from '../../services/report.service';
import { ToastrService } from 'ngx-toastr';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { UtilService } from 'src/app/util.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { Actions } from '../../Actions/action';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';

import { TransactionDetailsComponent } from '../transaction-details/transaction-details.component';




@Component({
  selector: 'app-aif-report',
  templateUrl: './aif-report.component.html',
  styleUrls: ['./aif-report.component.css']
})
export class AifReportComponent implements OnInit, OnDestroy {
  // this view child for making reference that references transactiondetailscomponent to be able to change the status value in transactiondetailscomponent
  @ViewChild('transStat') transStat: TransactionDetailsComponent;

  closeCascadeTabSubscription: Subscription;
  subscriptionParams: Subscription;
  subscriptionReportById: Subscription;
  subscriptionPreview: Subscription;
  subscriptionSaveSAR: Subscription;

  dataSource: any = [];
  confirmCloseReport: boolean = false;
  disableButton: boolean = false
  transaction: FormGroup;
  transactionControldd: t_transaction = new t_transaction(this.fb);
  strReportTransactionControl: FormGroup;
  parentArrayOfTransactions: FormArray;
  transactionIndexInTable: number;
  transactionArray: FormArray;
  t_report: t_report = new t_report(this.fb, this.reportservice);
  reportDetailControl: FormGroup
  index = 0;
  addNewTabActive: boolean = false;
  addNewTabActiveForCascadeTab: boolean = false;
  createMode: boolean = true
  CascadeTrans: FormArray
  editMode: boolean;
  isLoadingResults = false;
  isRateLimitReached = false;

  transactionArrayComingWithReport = [];
  timerid = null;
  validationIndicators: boolean = false;
  validationErrors
  validationErrorsLabels: notValidElementWithMessageOnly[] = []
  createReportAction: any;

  editReportAction: any;
  generateXMLAction: any;
  addActivityAction: any;
  SendToMakerAction: any;
  SendToCheckerAction: any;
  REOPENReportAction: any;
  MLCUAcknowledgeAction: any;
  ReturnToCheckerAction: any;
  ReturnToMakerAction: any;
  ReturnToMakerFromManagerAction: any;
  SendToAmlHeadAction: any;
  closeReportAction: any;
  SubmitAction: any;
  ReportStatus: string;
  routeReportAction: boolean;
  AIFRequestAction: boolean;
  ActionButton: boolean;
  sendToManagerFromMakerAction: boolean;



  constructor(private _router: Router, public dialogActivity: MatDialog, private reportservice: ReportService
    , private fb: FormBuilder, private route: ActivatedRoute, private toaster: ToastrService, private dataService: SharingDataServieService,
    private util: UtilService, private auth: AuthService) { }

  ngOnInit() {
    this.isLoadingResults = true;
    this.subscriptionParams = this.route.params.subscribe(params => {
      
      let id = params.id
      if (id) {
        
        this.createMode = false
        if (localStorage.getItem("ReportAif") == null) {
          this.subscriptionReportById = this.reportservice.getReportById(id).subscribe(data => {
            
            this.reportDetailControl = this.t_report.setReport1(data);
            this.timerid = setInterval(() => this.TimeInterval(), 10000);

            
            this.getReportStatus();
            this.parentArrayOfTransactions = this.reportDetailControl.get('transaction') as FormArray
            if (this.reportDetailControl.get('transaction').value == null) {
              
              this.reportDetailControl.removeControl('transaction')
              this.reportDetailControl.addControl('transaction', this.t_report.transaction.createTransactionControl())
            }
            this.isLoadingResults = false;
            console.log('locked by', this.reportDetailControl.get('reportUserLockId').value);

            //Disabled Forms
            let reportUserLockId = this.reportDetailControl.get('reportUserLockId').value
            let userName = localStorage.getItem('name')
            if (userName == reportUserLockId || reportUserLockId == null) {
              this.setStaticValue();
            }
            else if (userName != reportUserLockId && reportUserLockId != null) {
              this.reportDetailControl.disable()
              this.disableButton = true
            }
          },
            error => {
              
              this.isLoadingResults = false;
            })
        }

        else {
          const report = JSON.parse(localStorage.getItem('ReportAif'))
          this.reportDetailControl = this.t_report.setReportAuAif(report);
          this.setStaticValue()
          this.parentArrayOfTransactions = this.reportDetailControl.get('transaction') as FormArray
          this.getReportStatus()
          if (localStorage.getItem("ReportSt") != null)
            this.index = 2;
          this.isLoadingResults = false;
        }

      } else {
        this.isLoadingResults = false;
        

        
        this.transactionIndexInTable = -1;
        // if (localStorage.getItem("ReportAif") == null) {
        this.reportDetailControl = this.t_report.createStrReport()
        this.parentArrayOfTransactions = this.reportDetailControl.get('transaction') as FormArray
        // }
        // else {
        // this.reportDetailControl = this.t_report.setReportAuStr(JSON.parse(localStorage.getItem("ReportAif")))
        // this.parentArrayOfTransactions = this.reportDetailControl.get('transaction') as FormArray
        // if (localStorage.getItem("ReportSt") != null)
        //   this.index = 2;
        // this.addNewTab.emit(this.t_transaction.setTransaction(JSON.parse(localStorage.getItem("ReportSt"))));
        //   this.parentArrayOfTransactions.value.slice().forEach(element => {
        //     var index = this.parentArrayOfTransactions.value.indexOf(element); 
        //     this.transactionArrayComingWithReport[index] = Object.assign({} , element) ; 
        //   });
        //  // this.parentArrayOfTransactions = this.reportDetailControl.
        // }


        this.setStaticValue()
      }
      // this.timerid = setInterval(() => this.TimeInterval(), 10000);
    })
    let x: string = new Date().toLocaleString()

    this.closeCascadeTabSubscription = this.dataService.currentMessage2.subscribe(
      msg => {
        if (msg != null) {
          this.addNewTabActiveForCascadeTab = false;
          this.index = 1
        }
      }
    )
    this.setActions()
  }

  onPress() {
    

  }
  getReportStatus() {
    this.ReportStatus = this.reportDetailControl.get('reportStatusCode').value
  }
  async ngOnDestroy() {
    this.unLock();
    if (this.closeCascadeTabSubscription != undefined)
      this.closeCascadeTabSubscription.unsubscribe();
    if (this.subscriptionParams != undefined)
      this.subscriptionParams.unsubscribe();
    if (this.subscriptionReportById != undefined)
      this.subscriptionReportById.unsubscribe();
    if (this.subscriptionPreview != undefined)
      this.subscriptionPreview.unsubscribe();
    if (this.subscriptionSaveSAR != undefined)
      this.subscriptionSaveSAR.unsubscribe();
    localStorage.removeItem('ReportAif')
    localStorage.removeItem('ReportSt')
    clearInterval(this.timerid)
    await this.delay(500);

  }

  unLock() {
    const userName = localStorage.getItem('name');
    let reportUserLockId = this.reportDetailControl.get('reportUserLockId').value
    if(reportUserLockId==userName){
    this.route.params.subscribe(params => {
      const id = params.id;
      this.reportservice.unLock(id, userName).subscribe(data => {
            
      }, error => { });
    });

  }else
  console.log('unLock Not Allowed! ');
  }

   delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

  accept(event) {
    
    this.addNewTabActive = true
    this.index = 4
    this.strReportTransactionControl = event
    this.transactionIndexInTable = -1;
  }
  TimeInterval(): void {
    //
    localStorage.setItem('ReportAif', JSON.stringify(this.reportDetailControl.value))

  }
  ReportPreview() {
    this.updateAIFReport();
    this.toaster.info("Pdf Tab Openning ..... ", '', { timeOut: 3000 })
    this.subscriptionPreview = this.reportservice.PreviewReport(this.reportDetailControl.get('id').value).subscribe((data: any) => {

      // var blob = new Blob([data], { type: "application/pdf" });
      // if (blob) {
      //   if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      //     window.navigator.msSaveOrOpenBlob(blob);
      //     // return;
      //     // var fileURL = window.URL.createObjectURL(blob);
      //     // window.open(fileURL);
      //   }
      // var fileURL = URL.createObjectURL(data);
      let file = new Blob([data], { type: "application/pdf;charset=utf-8" });
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL, '_blank');
      // window.open(fileURL,'_blank');
      // }
      // const fileURL = URL.createObjectURL(data);
      // window.open(fileURL, '_blank');
    });
    // window.open(this.reportservice.PreviewReport(this.reportDetailControl.get('id').subscribe((data: any) => {

    //   }));

  }
  setStaticValue() {
    // 
    // 
    this.reportDetailControl.controls.reportCode.setValue('AIF')
    this.reportDetailControl.controls.reportingPersonType.setValue('AML Head');
    this.reportDetailControl.controls.currencyCodeLocal.setValue('EGP');
    this.reportDetailControl.controls.rentityBranch.setValue(environment.entity_branch)
    this.reportDetailControl.controls.submissionCode.setValue('E')
    // this.reportDetailControl.controls.reportCode.disable();
    this.reportDetailControl.controls.submissionCode.disable()
    this.reportDetailControl.controls.submissionDate.disable()
    this.reportDetailControl.controls.rentityBranch.disable()
    this.reportDetailControl.controls.currencyCodeLocal.disable()
    this.reportDetailControl.controls.entityReference.disable();

    // this.reportDetailControl.removeControl('reason');
    // this.reportDetailControl.removeControl('action');
    this.reportDetailControl.removeControl('reportIndicators');

    // this.reportDetailControl.controls.rentityId.disable()

  }

  // disabledform(group:FormGroup):void{
  // this.reportDetailControl.controls['transaction']


  //   Object.keys(group.controls).forEach((key:string)=>{
  //   const abstractControl=group.get(key);
  //   if(abstractControl instanceof FormGroup){
  //     this.disabledform(abstractControl);
  //   }else{
  //     abstractControl.disable();
  //   }
  //   });

  //     }


  saveAIFReport() {
    console.log("saveAIFReport()");

    clearInterval(this.timerid)
    localStorage.removeItem('ReportAif')
    localStorage.removeItem('ReportSt')
    this.isLoadingResults = true
    this.createMode = true;
    this.parentArrayOfTransactions = this.reportDetailControl.get('transaction').value;
    this.subscriptionSaveSAR = this.reportservice.saveSarReport(this.reportDetailControl.value).subscribe(value => {
      this.toaster.success('Report saved successfully');
      this.confirmCloseReport = true
      this._router.navigate(['reports']);
      this.isLoadingResults = false
    }, (error) => {
      this.toaster.error(error.message);
      this.isLoadingResults = false
    }
    )
  }

  updateAIFReport() {
    console.log("updateAIFReport()");

    clearInterval(this.timerid)
    localStorage.removeItem('ReportAif')
    localStorage.removeItem('ReportSt')
    this.editMode = true;
    this.parentArrayOfTransactions = this.reportDetailControl.get('transaction').value;
    

    this.reportservice.updateSarReport(this.reportDetailControl.value).subscribe(value => {
      this.toaster.success('Report updated successfully');
      this.confirmCloseReport = true
      this.isLoadingResults = false
    }, (error) => {
      this.toaster.error(error.message);
      this.isLoadingResults = false
    }
    )
  }

  acceptTrasactionAddedFromChild(result) {
    this.addNewTabActive = false;
    this.index = 1;
    if (this.transactionIndexInTable != -1) {
      //   
      //   this.parentArrayOfTransactions.push(result);
      // } else {
      this.strReportTransactionControl = this.transactionControldd.setTransaction(result.value)
      this.parentArrayOfTransactions.removeAt(this.transactionIndexInTable);
      this.parentArrayOfTransactions.insert(this.transactionIndexInTable, result);
      // }
      
    }
  }


  acceptNewArrayAfterUpdate(event) {
    //  
    this.parentArrayOfTransactions = event;
  }

  editTransactionEvent(event) {
    this.transactionIndexInTable = event.transactionIndexInTable
    this.addNewTabActive = true;
    this.index = 4;
    this.strReportTransactionControl = this.transactionControldd.setTransaction(event.transactionFormGroup.value)
    this.strReportTransactionControl.markAsDirty()
    this.strReportTransactionControl.markAsTouched()
  }

  acceptIncomingCascadedTransactions(event: FormArray) {
    this.CascadeTrans = event
    
    this.addNewTabActiveForCascadeTab = true
    this.index = 4;
  }

  closeAddNewSingleActivity() {
    if (confirm("You will lose information")) {
      this.addNewTabActive = false;
      this.index = 1;
      this.transStat.status = 0;
    }
  }

  closeCascadeActivity() {
    if (confirm("Confirm Close Cascade")) {
      this.addNewTabActiveForCascadeTab = false;
      this.index = 1;
      this.transStat.status = 0;
    }
  }




  closeValidationTab(event?) {
    this.validationIndicators = false
    this.index = 1;
  }
  validateReport() {

    this.util.changeReportAsRead(true);
    const resultc = [];
    let NestedSectionsErrors: notValidElementWithMessageOnly[] = []

    this.util.validateFormGroup2(this.reportDetailControl, resultc, NestedSectionsErrors);
    
    let resultc2: notValidElementWithMessageOnly[] = [];
    resultc2 = [...NestedSectionsErrors]
    const transactionsControl = this.reportDetailControl.get('transaction') as FormArray;
    if (transactionsControl) {

      transactionsControl.controls.forEach((tranControl, index) => {

        if (this.util.getFormGroupWithAllKeysEqualNull(tranControl as FormGroup, ['tFromMyClient', 'tFrom'])) {
          const fromNotFound: notValidElementWithMessageOnly = { message: 'Select Transaction From Type', name: `transaction ${index}` };

          resultc2.push(fromNotFound);
        }

        if (this.util.getFormGroupWithAllKeysEqualNull(tranControl as FormGroup, ['tToMyClient', 'tTo'])) {
          const toNotFound: notValidElementWithMessageOnly = { message: 'Select Transaction To Type', name: `transaction ${index}` };

          resultc2.push(toNotFound);
        }


      });


    }
    this.validationErrorsLabels = resultc2;
    this.validationErrors = resultc;
    for (const x of resultc) {
      this.util.getOptionsFromLockup(x);
    }

    if (resultc.length > 0 || resultc2.length > 0) {

      this.validationIndicators = true;
      this.index = 7;
    } else if (this.reportDetailControl.valid) {
      this.toaster.warning('No Validation Errors Found!!');
    }

  }

  GenerateXML() {
    console.log("this.reportDetailControl.get('id').value", this.reportDetailControl.get("id").value);

    if (this.reportDetailControl.valid) {

      this.reportservice.GenerateXMLAndSaveReport(this.reportDetailControl.getRawValue()).subscribe(data => {
        this.toaster.success("Generate xml successfully AIF report")
      },
        error => {
          this.toaster.error(error.message)

        })
    }
    else {
      this.toaster.warning("Please validate report before XML Generation")
    }
  }
  setActions() {
    

    this.createReportAction = this.auth.has_Capabilities(Actions.createReport)
    this.editReportAction = this.auth.has_Capabilities(Actions.editReport)
    this.generateXMLAction = this.auth.has_Capabilities(Actions.generateXML)
    this.addActivityAction = this.auth.has_Capabilities(Actions.addActivity)
    this.SendToMakerAction = this.auth.has_Capabilities(Actions.sendToMaker)
    this.SendToCheckerAction = this.auth.has_Capabilities(Actions.sendTochecker)
    this.REOPENReportAction = this.auth.has_Capabilities(Actions.reOpenReport)
    this.MLCUAcknowledgeAction = this.auth.has_Capabilities(Actions.MLCUAcknowledgeAction)
    this.ReturnToCheckerAction = this.auth.has_Capabilities(Actions.returnToChecker)
    this.ReturnToMakerAction = this.auth.has_Capabilities(Actions.returnToMaker)
    this.ReturnToMakerFromManagerAction = this.auth.has_Capabilities(Actions.returnToMakerFromManager)
    this.SendToAmlHeadAction = this.auth.has_Capabilities(Actions.sendToAMLHead)
    this.closeReportAction = this.auth.has_Capabilities(Actions.closeReport)
    this.SubmitAction = this.auth.has_Capabilities(Actions.submitReport)
    this.routeReportAction = this.auth.has_Capabilities(Actions.routeReport)
    this.AIFRequestAction = this.auth.has_Capabilities(Actions.AIFRequest)
  this.sendToManagerFromMakerAction=this.auth.has_Capabilities(Actions.sendToManagerFromMaker) 
  }
  SendToChecker() {

    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid')
      return;
    }
    if (!confirm("Send To Checker")) return
    const report = Object.assign({}, this.reportDetailControl.getRawValue());
    this.reportservice.SendToChecker(report).subscribe((data: any) => {
      if (data) {

        this.toaster.success('SendToChecker Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }
    },
      error => {
        

        this.toaster.error(error.error.message);
      });
  }
  ReturnToMaker() {
    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid');
      return;
    }
    if (!confirm("Return To Maker")) return

    let report = Object.assign({}, this.reportDetailControl.getRawValue());
    this.reportservice.ReturnToMaker(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Send To Maker Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();
      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  ReturnToChecker() {
    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid');
      return;
    }
    if (!confirm("Return To Checker")) return

    let report = Object.assign({}, this.reportDetailControl.getRawValue());
    this.reportservice.ReturnToChecker(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Return To Checker Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);

        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();
      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  reOpenReport() {
    if (!confirm("Re Open Report")) return

    let reportid = this.reportDetailControl.get('id').value;
    this.reportservice.reOpenReport(reportid).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Report re-opened Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  MLCUAcknowledge() {
    if (!confirm("MLCUAcknowledge Report")) return

    let reportid = this.reportDetailControl.get('id').value;
    this.reportservice.MLCUAcknowledge(reportid).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Report MLCU acknowledge Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  SendToAMLHead() {
    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid');
      return;
    }
    if (!confirm("Send To AMLHead")) return

    let report = Object.assign({}, this.reportDetailControl.getRawValue());
    this.reportservice.sendToAMLHead(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Send To AML HEAD Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }




  closeReportPage() {

    clearInterval(this.timerid);
    localStorage.removeItem('Report');
    localStorage.removeItem('ReportAct');
    this._router.navigate(['reports']);
  }
  isReportValid(): boolean {
    return this.reportDetailControl.valid;
  }
  closeReport() {
    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid');
      return;
    }
    if (!confirm("Close Report")) return

    let report = Object.assign({}, this.reportDetailControl.getRawValue());
    this.reportservice.closeReport(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Report is Closed Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });

  }
  Submit() {
    let reportid = this.reportDetailControl.get('id').value

    this.reportservice.submitReport(reportid).subscribe((data: any) => {
      if (!confirm("Submit Report")) return

      if (data) {
        this.toaster.success('Report is Submitted Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });

  }
  get _ReturnToCheckerAction() {
    return this.ReturnToCheckerAction && (['WaitingHeadDecision'].includes(this.ReportStatus))
  }
  get _ReturnToMakerAction() {
    return this.ReturnToMakerAction && (['CHECKING', 'ReturnToChecker', 'MLCUModification', 'REOPEN'].includes(this.ReportStatus))

  }
  get _SendToCheckerAction() {
    return this.SendToCheckerAction && (['MAKING', 'ReturnToMaker', 'MLCUModification', 'REOPEN'].includes(this.ReportStatus))
  }
  get _SendToAMLHEADAction() {
    return this.SendToAmlHeadAction && (['CHECKING', 'ReturnToChecker'].includes(this.ReportStatus))
  }
  get _reOpenReportAction() {
    return this.REOPENReportAction && (['CLOSED', 'SUBMITTED'].includes(this.ReportStatus))
  }
  get _closeReportAction() {
    return this.closeReportAction && (['WaitingHeadDecision']).includes(this.ReportStatus)
  }
  get _ReturnToMakerActionManager() {
    return this.ReturnToMakerFromManagerAction && (['WaitingHeadDecision','REOPEN'].includes(this.ReportStatus))

  }
  get _generateXMLAction() {
    return this.generateXMLAction && (['WaitingHeadDecision'].includes(this.ReportStatus))

  }
  get _MLCUAcknowledgeAction() {
    return this.MLCUAcknowledgeAction && (['XMLGenerated']).includes(this.ReportStatus)
  }
  get _SubmitAction() {
    return this.SubmitAction && (['XMLGenerated']).includes(this.ReportStatus)

  }

  closeReportPageAndConfirmClose() {
    this.setConfirmCloseReport(true)
    this.closeReportPage()
  }
  setConfirmCloseReport(confirm: boolean) {
    this.confirmCloseReport = confirm

  }
  makeAIfRequest() {
    let StrReport = Object.assign({}, this.reportDetailControl.getRawValue())
    this.reportservice.makeAIFRequest(StrReport).subscribe((report: any) => {
      if (report) {
        let AifReportId = report.id
        this.setConfirmCloseReport(true)
        this._router.navigate([environment.aifEditRoute, AifReportId])
      }
      else {
        this.toaster.error("No Returned Data")
      }
    }, error => this.errorFunction(error))
  }
  get _AIFRequestAction() {

    return this.AIFRequestAction && (['XMLGenerated']).includes(this.ReportStatus)
  }
  errorFunction(error) {
    this.toaster.error(error.error.message)
  }
  get _ButtonAction(){
    return this._AIFRequestAction||this._MLCUAcknowledgeAction||
     this._ReturnToCheckerAction||this._ReturnToMakerAction
     ||this._ReturnToMakerActionManager||this._SendToAMLHEADAction||this._SendToCheckerAction
     ||this._SubmitAction||this._closeReportAction||this._generateXMLAction||this._reOpenReportAction
       
   }
   get _SendToAMLHEADActionFromMaker(){
    return this.sendToManagerFromMakerAction&& (['MAKING', 'ReturnToMaker'].includes(this.ReportStatus))
  }
}

       // console.log('json array of transactions',JSON.stringify(this.parentArrayOfTransactions.value));

            // this.parentArrayOfTransactions.value.slice().forEach(element => {
            //   var index = this.parentArrayOfTransactions.value.indexOf(element); 
            //   this.transactionArrayComingWithReport[index] = Object.assign({} , element) ; 
            // });

             // acceptTrasactionAddedFromChild(result) {
    //   this.addNewTabActive = false;
    //   this.index = 2;
    //   if (this.transactionIndexInTable == -1) {
    //     
    //     this.parentArrayOfTransactions.push(result);
    //   } else {
    //     this.strReportTransactionControl = this.transactionControldd.setTransaction(result.value)
    //     this.parentArrayOfTransactions.removeAt(this.transactionIndexInTable);
    //     this.parentArrayOfTransactions.insert(this.transactionIndexInTable, result);
    //   }
    //   
    // }