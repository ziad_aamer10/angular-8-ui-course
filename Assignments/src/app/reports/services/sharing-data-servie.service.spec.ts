import { TestBed } from '@angular/core/testing';

import { SharingDataServieService } from './sharing-data-servie.service';

describe('SharingDataServieService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharingDataServieService = TestBed.get(SharingDataServieService);
    expect(service).toBeTruthy();
  });
});
