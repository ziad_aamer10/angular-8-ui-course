import {Component, OnInit, ViewChild, ElementRef, ViewChildren, Output, EventEmitter, OnDestroy} from '@angular/core';
import {Ingredient} from '../../shared/ingredient.model';
import {Subscription} from "rxjs";
import {NgForm} from "@angular/forms";
import {Store} from "@ngrx/store";
import * as ShoppingListActions from '../store/shopping-list.actions';
import * as fromApp from '../../store/app.reducer';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

  editSubscription: Subscription;
  @ViewChild('f', {static:false}) ingredientForm: NgForm;
  editMode: boolean = false;
  constructor(private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.editSubscription = this.store.select('shoppingList').subscribe(stateData => {
      if(stateData.editedIngredientIndex > -1){
        this.editMode = true;
        this.ingredientForm.setValue({
          name: stateData.editedIngredient.name,
          amount: stateData.editedIngredient.amount
        });
      }else {
        this.editMode = false;
      }
    });
    // this.editSubscription = this.shoppingListService.$editIngredient.subscribe((index) => {
    //   const ingredient: Ingredient = this.shoppingListService.getIngredient(index);
    //   this.ingredientForm.setValue({
    //       name: ingredient.name,
    //       amount: ingredient.amount
    //     })
    //   this.editMode = true;
    //   this.updateIngIndex = index;
    // });
  }

  onSubmit(formValue) {
    const ingredient = new Ingredient(formValue.name, formValue.amount);
    console.log('new Ingredient: ', new Ingredient(formValue.name, formValue.amount));
    if (this.editMode){
      // this.shoppingListService.updateIngredient(this.updateIngIndex, new Ingredient(formValue.name, formValue.amount));
      this.store.dispatch(new ShoppingListActions.UpdateIngredient(new Ingredient(formValue.name, formValue.amount)));
    } else {
      this.store.dispatch(new ShoppingListActions.AddIngredient( ingredient));
      // this.shoppingListService.addIngredient(new Ingredient(formValue.name, formValue.amount));
    }
    this.onClear();
  }


  onClear() {
    this.ingredientForm.resetForm();
    this.editMode = false;
    this.store.dispatch(new ShoppingListActions.StopEdit());
  }

  onDelete() {
    //this.shoppingListService.deleteIngredient(this.updateIngIndex);
    this.store.dispatch(new ShoppingListActions.DeleteIngredient());
    this.onClear();
  }


  ngOnDestroy() {
    this.editSubscription.unsubscribe();
    this.store.dispatch(new ShoppingListActions.StopEdit());
  }

}
