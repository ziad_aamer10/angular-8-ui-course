import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { t_report_party_type } from "./report_party_type";

export class ReportParties {
    ReportPartyType: t_report_party_type = new t_report_party_type(this.fb)
    constructor(public fb: FormBuilder) {


    }
    createReportParties(): FormGroup {
        return this.fb.group({
            id: new FormControl(),
            reportParty: this.fb.array([],Validators.required),
            comments: new FormControl()
        })
    }
    setReportParties(reportParties): FormGroup {
        return this.fb.group({
            id: new FormControl(reportParties.id),
            reportParty: this.setReportParty(reportParties.reportParty),
            comments: new FormControl(reportParties.comments)
        })
    }
    setReportParty(reportParty) {
        let reportPartyControl = this.fb.array([],Validators.required)
        for (let i of reportParty) {
            let r = this.ReportPartyType.set_report_party_type(i)
            reportPartyControl.push(r)
        }

        return reportPartyControl
    }
}