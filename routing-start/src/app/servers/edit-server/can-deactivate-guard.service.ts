import { Observable } from "rxjs";
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";

export interface CanComponentDeactivate{
    canDeactivate: () => boolean | Observable<boolean> | Promise<boolean>;
}

export class canDeactivateGuard implements CanDeactivate<CanComponentDeactivate>{

    canDeactivate(component: CanComponentDeactivate, currentRoute: ActivatedRouteSnapshot,
                    currentState: RouterStateSnapshot, nextState: RouterStateSnapshot ){
        return component.canDeactivate();
    }
}