import { Component, OnInit, OnChanges } from '@angular/core';
import { CounterService } from './services/counter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  private toActiveActions: number = 0;
  private toInActiveActions: number = 0;
  
  constructor(private counterService: CounterService){}

  ngOnInit(){
    this.counterService.actionTaken.subscribe(()=>{
      this.toActiveActions = this.counterService.activesCounter;
      this.toInActiveActions = this.counterService.inactivesCounter;  
    });
  }

}
