import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FromTransactionComponent } from './from-transaction.component';

describe('FromTransactionComponent', () => {
  let component: FromTransactionComponent;
  let fixture: ComponentFixture<FromTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FromTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FromTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
