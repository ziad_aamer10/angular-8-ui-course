import { Component, OnInit, Input, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import { LookupClass } from '../../lockups/Lookups';
import { t_report } from '../../validationControls/t_report';
import { FormGroup } from '@angular/forms';
import { UtilService } from 'src/app/util.service';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { Subscription } from 'rxjs';

export interface Category {
  name: string;
}

@Component({
  selector: 'app-str-report-details',
  templateUrl: './str-report-details.component.html',
  styleUrls: ['./str-report-details.component.css']
})
export class StrReportDetailsComponent implements OnInit, OnChanges {

  showReasonAndActionSubscription: Subscription;
  showFiu: boolean = false;
  showResonAndAction: boolean = false;

  ngOnChanges(changes: SimpleChanges) {

    for (let propName in changes) {
      let change = changes[propName];
      let curVal = change.currentValue;
      let prevVal = change.previousValue;
      if (propName == 'reportDetailControl') {
        this.reportDetailControl = curVal
      }


    }
  }
  notValidElement: any[] = [];
  @Input() reportDetailControl: FormGroup;
  submission_values = LookupClass.submission_type;
  report_values = LookupClass.report_type;
  Currency_values = LookupClass.Currency;
  selectedReporting = 'AMLHead';
  categories: Category[] = [
    { name: 'mostafa' },
    { name: 'mostafa1' }
  ];
  constructor(private utilservice: UtilService, private dataService: SharingDataServieService) { }

  ngOnInit() {
    
  

    if (this.reportDetailControl.getRawValue().reportCode == 'STR' || this.reportDetailControl.getRawValue().reportCode == 'SAR') {
      
      this.showResonAndAction = true;
      // this.showFiu = false;
    } else {
      // this.showResonAndAction = false;
      this.showFiu = true;
    }

  }

  ngOnDestroy(): void {
  }
  get Report() {
    return this.reportDetailControl.controls;
  }
  validate() {
    this.clearValidationErrors();
    this.utilservice.validateFormGroup(this.reportDetailControl, this.notValidElement);
  }
  clearValidationErrors() {
    this.notValidElement = [];
  }
  SendToChecker() { }
  SendToMaker() { }
  ReturnToMaker() { }
  ReturnToChecker() { }
}
