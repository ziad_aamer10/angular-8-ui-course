import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import {Observable, Subscription} from 'rxjs';
import {Store} from "@ngrx/store";
import * as ShoppingListActions from './store/shopping-list.actions';
import * as fromApp from '../store/app.reducer';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
 // providers:[ShoppingListService]
})
export class ShoppingListComponent implements OnInit, OnDestroy {

  ingredients: Observable<{ ingredients: Ingredient[] }>;
  subscription: Subscription;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.ingredients = this.store.select('shoppingList');
    // this.ingredients = this.shoppingListService.getIngredients();
    // this.subscription = this.shoppingListService.ingredientsChanged.subscribe((ings: Ingredient[]) => {
    //   this.ingredients = ings;
    // });
  }


  ngOnDestroy() {
    //this.subscription.unsubscribe();
  }

  onEditItem(index: number) {
    this.store.dispatch( new ShoppingListActions.StartEdit(index));
    //this.shoppingListService.$editIngredient.next(index);
  }

}
