import { Directive, Input, HostListener, Renderer2, ElementRef, HostBinding } from '@angular/core';


//There are 2 types to get things form outside this class:
//1- using @Input()
//2- Event listeners
@Directive({
    selector:'[appDropdown]'
})
export class DropdownDirective{
    //if error happens that directive does not exist ==> add the class to the declarations list in the app.module
//    @Input('appDropdown') className: string;
    @HostBinding('class.open') isOpen = false;

    constructor(private elRef: ElementRef, private renderer: Renderer2){}

    @HostListener('document:click', ['$event']) toggleOpen(event: Event) {
        this.isOpen = this.elRef.nativeElement.contains(event.target) ? !this.isOpen : false;
    }

    // @HostListener('click') showOrHideDropdownList(eventData: Event){

    //     this.isOpen = !this.isOpen;
    // }



    //add/remove class open from this componenet
    /*
    @HostListener('click') showOrHideDropdownList(eventData: Event){
        //console.log('I got clicked :)');
        if(this.elRef.nativeElement.classList.contains('open')){
            this.renderer.removeClass(this.elRef.nativeElement, this.className);
        }
        else{
            this.renderer.addClass(this.elRef.nativeElement, this.className);
        }

    }
    */



}
