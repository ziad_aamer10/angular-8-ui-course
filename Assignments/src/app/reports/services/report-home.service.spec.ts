import { TestBed } from '@angular/core/testing';

import { ReportHomeService } from './report-home.service';

describe('ReportHomeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportHomeService = TestBed.get(ReportHomeService);
    expect(service).toBeTruthy();
  });
});
