import { Injectable } from '@angular/core';
import { StrComponent } from '../components/str/str.component';
import { CanDeactivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDeactivateGuardServiceStrService  implements CanDeactivate<StrComponent> {

  canDeactivate(target: StrComponent) {
    if (!target.confirmCloseReport) {
        return window.confirm('Do you want to close report?');
    }
    return true;
}


  constructor() { }
}
