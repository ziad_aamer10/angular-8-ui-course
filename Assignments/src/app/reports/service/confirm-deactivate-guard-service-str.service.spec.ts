import { TestBed } from '@angular/core/testing';

import { ConfirmDeactivateGuardServiceStrService } from './confirm-deactivate-guard-service-str.service';

describe('ConfirmDeactivateGuardServiceStrService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfirmDeactivateGuardServiceStrService = TestBed.get(ConfirmDeactivateGuardServiceStrService);
    expect(service).toBeTruthy();
  });
});
