import { Component, OnInit, Input, OnChanges, OnDestroy, ViewChild, ElementRef, AfterViewInit, ContentChild, AfterContentInit } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit, AfterContentInit {

  @Input('srvElement') element: {name: string, content: string, type: string};
  @Input() name: string;
  @ViewChild('heading', { static:true }) headingElement: ElementRef;
  @ContentChild('contentParagraph', {static:true}) contentParagraph: ElementRef;

 // Content queries are set before the ngAfterContentInit callback is called.
  constructor() { }

  ngOnInit() {
    console.log("ngOnInit is called!");
    console.log('headingElement: ', this.headingElement.nativeElement.textContent);
    console.log('contentParagraph: ', this.contentParagraph.nativeElement.textContent);
  }

  ngOnChanges(){
    console.log("ngOnChanges is called!");
  }

  ngAfterViewInit(){
    console.log('ngAfterViewInit is called!');
    console.log('headingElement: ', this.headingElement.nativeElement.textContent);
  }
  ngAfterContentInit(){
    console.log('ngAfterContentInit is called!');
    console.log('contentParagraph: ', this.contentParagraph.nativeElement.textContent);
   }

  ngOnDestroy(){
    console.log("ngOnDestroy is called!");
  }

}
