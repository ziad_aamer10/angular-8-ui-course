import { Component, OnInit ,Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { LookupClass } from '../../lockups/Lookups';
import { t_person } from '../../validationControls/t_person';
import { goodAndServices } from 'src/environments/goodsAndServices';


@Component({
  selector: 'app-goods-services',
  templateUrl: './goods-services.component.html',
  styleUrls: ['./goods-services.component.css']
})
export class GoodsServicesComponent implements OnInit,OnChanges {

  @Input() transactionControl: FormGroup
  addressControl:FormArray
  
  item_type_values=LookupClass.item_type;
  trans_item_status_values=LookupClass.trans_item_status;
  CurrencyValues=LookupClass.Currency;
  t_person:t_person=new t_person()
  itemsArray:FormArray
  
  @Output() saveGoods = new EventEmitter() 
  goodAndServices=new goodAndServices(this.fb)
  testItem:FormGroup=this.goodAndServices.createItem()
  itemControl:FormGroup
 

  
 
  constructor(private fb: FormBuilder) { }
  createAddress(): FormGroup {
    return this.fb.group({
      Address_type:[],
      Address:[],
      Town:[],
      City:[],
      Zip:[],
      country_code:[],
      State:[],
      comments:[]
    })
  }

  
  ngOnInit() {
    this.itemsArray=this.transactionControl.get('goodsServices.item') as FormArray
    
  }
  addItem(){
   let itemControl= this.goodAndServices.createItem()

   this.itemsArray.push(itemControl)

  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let chng = changes[propName];
      let cur = chng.currentValue;
      let prev = chng.previousValue;
      if (propName == 'transactionControl') {


        this.transactionControl = cur
        this.itemsArray=this.transactionControl.get('goodsServices.item') as FormArray

      }
    }
  }
  removeItem(index){
    if(confirm("You will delete an item"))
    this.itemsArray.removeAt(index)
  }
}
