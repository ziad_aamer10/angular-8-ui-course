import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { UtilService } from 'src/app/util.service';
import { LookupClass } from '../../lockups/Lookups';
import { t_id } from '../../validationControls/t_id';

@Component({
  selector: 'app-ids',
  templateUrl: './ids.component.html',
  styleUrls: ['./ids.component.css']
})
export class IdsComponent implements OnInit , OnChanges {
inputId: FormGroup;
identifications: FormArray;
myDate = new Date();
 identifier_values = LookupClass.identifier_type;
 country_values = LookupClass.country;


 public t_id = new t_id(this.fb);
 @Input() IdFormGroup: FormGroup;
  constructor(public fb: FormBuilder, private utilsevice: UtilService) {
    this.inputId = this.createId();
   }
addFieldValue() {


if (this.inputId.invalid) {
  Object.keys(this.inputId.controls).forEach(key => {
    this.inputId.get(key).markAsDirty();
    this.inputId.get(key).markAsTouched();
  });
} else {
    this.addId();

    this.identifications.at(this.identifications.length - 1).setValue(this.inputId.value);
    this.inputId.reset();
}
}
deleteFieldValue(index) {
  if (confirm('You are going to delete Identification')) {
 this.identifications.removeAt(index);
}}
  ngOnInit() {
    this.setIdentification();

  }
ngOnChanges(changes: SimpleChanges) {

    for (const propName in changes) {

      const chng = changes[propName];

      const cur  = chng.currentValue;
      const prev = chng.previousValue;
      if (propName == 'IdFormGroup') {

        this.IdFormGroup = cur;
        this.setIdentification();



    }
  }
}
createId(): FormGroup {
  return this.t_id.createId();
}
addId() {

  const x = this.createId();
  this.identifications.push(x);
  }
get ExpiryDate() {
  return this.inputId.controls.expiryDate.value;
}
get issueDate() {
  return this.inputId.controls.issueDate.value;
}
setIdentification() {this.identifications = this.IdFormGroup.get('identification') as FormArray;

}
}
