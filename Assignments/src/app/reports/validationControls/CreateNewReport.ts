import { FormBuilder, FormControl, Validators, FormGroup, FormArray } from '@angular/forms';

const fb = new FormBuilder();
export class CreateNewReport {


    CrNewReportForm(): FormGroup {

        return fb.group({
            'reportCategory': new FormControl(''),
            'Priority': new FormControl(''),
            'Owner': new FormControl(''),
           });
    }
}
