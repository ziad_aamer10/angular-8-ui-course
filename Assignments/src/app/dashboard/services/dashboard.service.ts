import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http:HttpClient) { }
  getReportsTypesStats(){
const url=`${environment.projectUrl}/reports/statistics/types`
return this.http.get(url)
  }
  getReportsStatusTypesStats(){
    const url=`${environment.projectUrl}/reports/statistics/status-types`
    return this.http.get(url)
  }
}
