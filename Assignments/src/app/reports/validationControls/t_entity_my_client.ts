import { FormGroup, Validators, FormArray } from '@angular/forms';
import { tEntity } from './t_entity';
import { UtilService } from 'src/app/util.service';
import { t_person_my_client } from './t_person_my_client';


export class t_entity_my_client {
    tEntity: tEntity = new tEntity()
    UtilService = new UtilService();
    t_person_my_client:t_person_my_client=new t_person_my_client()
    createEntityControl(): FormGroup {
        let f: FormGroup = this.tEntity.createEntityControl()
        this.addEntityValidators(f);
        return f
    }

    setEntityControl(entity) {
        let f: FormGroup = this.tEntity.setEntityControl(entity)
        this.addEntityValidators(f)
        return f

    }

    addEntityValidators(entity: FormGroup) {
        entity.controls["commercialName"].setValidators(Validators.required)
        entity.controls["incorporationLegalForm"].setValidators(Validators.required)

        entity.controls["incorporationNumber"].setValidators(Validators.required)
        entity.controls["business"].setValidators(Validators.required)
        entity.controls["incorporationCountryCode"].setValidators(Validators.required)

        entity.controls["incorporationDate"].setValidators(Validators.required)
        entity.controls["taxNumber"].setValidators(Validators.required)
        entity.controls["directorId"].setValidators([Validators.required, UtilService.minLengthArray(1)])
        let directors:FormArray=entity.controls["directorId"] as FormArray
        this.addMyEntityDirecorsValidation(directors)
        let Fields = ['commercialName', 'incorporationLegalForm', 'incorporationNumber',
        'business', 'incorporationCountryCode', 'incorporationDate',
        'taxNumber', 'directorId']
        this.UtilService.updateFormGroupValidity(entity, Fields)
        entity.updateValueAndValidity()
    }
    addMyEntityDirecorsValidation(directors:FormArray){

for (const director of directors.controls) {
    this.t_person_my_client.addMyPersonValidators(director as FormGroup)
    
}
    }
}