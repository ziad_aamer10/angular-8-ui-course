import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { EntityForm } from '../../models/entityFormGroup';
import { AccountForm } from '../../models/accountFormGroup';
import { t_transaction } from '../../validationControls/t_transaction';
import { ToastrService } from 'ngx-toastr';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { single } from 'rxjs/operators';

@Component({
  selector: 'app-cascade-transaction',
  templateUrl: './cascade-transaction.component.html',
  styleUrls: ['./cascade-transaction.component.css']
})
export class CascadeTransactionComponent implements OnInit, OnChanges {

  cascadeF:boolean=true;
  cascadeMe: string;
  trasactions = [];
  t_from: FormGroup;
  t_to: FormGroup;
  Cascad_type: string = "None"
  t_transaction: t_transaction = new t_transaction(this.fb)
  TransactionControl: FormGroup
  @Input() CascadeTrans: FormArray;
  personFormControl: FormGroup;
  entityFormControl: FormGroup;
  entityForm = new EntityForm();
  accountFormControl: FormGroup;
  accountForm = new AccountForm();
  dataSource = new MatTableDataSource([]);

  displayedColumns: string[] = [
    'Trn', 'TDate', 'TAmount', 'TTypeFrom', 'FPNumber', 'FPName', 'FromACCNumber', 'TTypeTo', 'ToACCNumber', 'ToPNumber', 'ToPName'
    , 'FReviewd', 'TReviewd', 'Validation', 'Validation_Messages', 'Action'];

  selection = new SelectionModel<any>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  deleteFieldValue(index) {
    if (confirm('Confirm delete transaction')) {
      this.CascadeTrans.removeAt(index);
      this.fillTable();
      this.selection.clear();

    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  constructor(private fb: FormBuilder
    , private toaster: ToastrService, private dataService: SharingDataServieService) {

  }

  ngOnInit() {
    this.TransactionControl = this.t_transaction.createTransactionControl()
    // this.entityFormControl = this.entityForm._FormEntity;
    // this.accountFormControl = this.accountForm._FormAccount;
    this.fillTable();
    this.setControls();
    this.cascadeMe = "cascade";
    console.log('ngoninit cascade component , transactions selected from transaction detail comp', this.CascadeTrans)
  }

  fillTable() {
    this.dataSource.data = this.CascadeTrans.value;
    

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.data.forEach(i => {
      if (i.tFrom != null) {
        // 
        if (i.tFrom.fromEntity != null) {
          i.TTypeFrom = "from other client entity"
          i.FPNumber = i.tFrom.fromEntity.entityNumber
          i.FPName = i.tFrom.fromEntity.name;
          i.FromACCNumber = '';
          if (i.tFrom.fromEntity.isReviewed) {
            i.FReviewd = 'Y';
          } else {
            i.FReviewd = 'N';
          }
        }
        else if (i.tFrom.fromPerson != null) {
          i.TTypeFrom = "from other client person"
          i.FPNumber = i.tFrom.fromPerson.partyNumber
          i.FromACCNumber = '';
          if (i.tFrom.fromPerson.firstName == null) {
            i.FPName = i.tFrom.fromPerson.middleName + " " + i.tFrom.fromPerson.lastName;
          } else if (i.tFrom.fromPerson.middleName == null) {
            i.FPName = i.tFrom.fromPerson.firstName + " " + i.tFrom.fromPerson.lastName;
          } else if (i.tFrom.fromPerson.lastName == null) {
            i.FPName = i.tFrom.fromPerson.firstName + " " + i.tFrom.fromPerson.middleName;
          } else {
            i.FPName = i.tFrom.fromPerson.firstName + " " + i.tFrom.fromPerson.middleName + " " + i.tFrom.fromPerson.lastName;
          }
          if (i.tFrom.fromPerson.isReviewed) {
            i.FReviewd = 'Y';
          } else {
            i.FReviewd = 'N';
          }
        }
        else if (i.tFrom.fromAccount != null) {
          

          i.TTypeFrom = "from other client account"
          i.FromACCNumber = i.tFrom.fromAccount.account;
          if (i.tFrom.fromAccount.isReviewed) {
            i.FReviewd = 'Y';
          } else {
            i.FReviewd = 'N'
          }
          if (i.tFrom.fromAccount.signatory != null) {
            let signatoryIndicator: boolean = false;
            let xc: any[] = i.tFrom.fromAccount.signatory;
            for (let j = 0; j < xc.length; j++) {
              if (i.tFrom.fromAccount.signatory[j].isPrimary === true) {
                i.FPNumber = i.tFrom.fromAccount.signatory[j].tPerson.partyNumber
                i.FPName = i.tFrom.fromAccount.signatory[j].tPerson.firstName + " " + i.tFrom.fromAccount.signatory[j].tPerson.middName + " " + i.tFrom.fromAccount.signatory[j].tPerson.lastName
                signatoryIndicator = true;
                break;
              }
            }
            if (!signatoryIndicator) {
              i.FPName = "No primary signatory"
              i.FPNumber = "No primary signatory"
            }
          }

        }
      }
      if (i.tFromMyClient != null) {
        // 
        if (i.tFromMyClient.fromEntity != null) {
          i.TTypeFrom = "from my client entity"
          i.FPNumber = i.tFromMyClient.fromEntity.entityNumber
          i.FPName = i.tFromMyClient.fromEntity.name;
          i.FromACCNumber = '';
          if (i.tFromMyClient.fromEntity.isReviewed) {
            i.FReviewd = 'Y';
          } else {
            i.FReviewd = 'N';
          }
        }

        else if (i.tFromMyClient.fromPerson != null) {

          i.TTypeFrom = "from my client person"
          i.FPNumber = i.tFromMyClient.fromPerson.partyNumber
          if (i.tFromMyClient.fromPerson.firstName == null) {
            i.FPName = i.tFromMyClient.fromPerson.middleName + " " + i.tFromMyClient.fromPerson.lastName;
          } else if (i.tFromMyClient.fromPerson.middleName == null) {
            i.FPName = i.tFromMyClient.fromPerson.firstName + " " + i.tFromMyClient.fromPerson.lastName;
          } else if (i.tFromMyClient.fromPerson.lastName == null) {
            i.FPName = i.tFromMyClient.fromPerson.firstName + " " + i.tFromMyClient.fromPerson.middleName;
          } else {
            i.FPName = i.tFromMyClient.fromPerson.firstName + " " + i.tFromMyClient.fromPerson.middleName + " " + i.tFromMyClient.fromPerson.lastName;
          }
          if (i.tFromMyClient.fromPerson.isReviewed) {
            i.FReviewd = 'Y';
          } else {
            i.FReviewd = 'N'
          }
        }

        else if (i.tFromMyClient.fromAccount != null) {
          i.TTypeFrom = "from my client account"
          i.FromACCNumber = i.tFromMyClient.fromAccount.account
          if (i.tFromMyClient.fromAccount.isReviewed) {
            i.FReviewd = 'Y'
          } else {
            i.FReviewd = 'N';
          }
          if (i.tFromMyClient.fromAccount.signatory != null) {
            let signatoryIndicator: boolean = false
            let xc: any[] = i.tFromMyClient.fromAccount.signatory
            for (let j = 0; j < xc.length; j++) {
              if (i.tFromMyClient.fromAccount.signatory[j].isPrimary == true) {
                i.FPNumber = i.tFromMyClient.fromAccount.signatory[j].tPerson.partyNumber
                i.FPName = i.tFromMyClient.fromAccount.signatory[j].tPerson.firstName + " " + i.tFromMyClient.fromAccount.signatory[j].tPerson.middleName + " " + i.tFromMyClient.fromAccount.signatory[j].tPerson.lastName
                signatoryIndicator = true
                break;
              }
            }
            if (!signatoryIndicator) {

              i.FPName = "No primary signatory"
              i.FPNumber = "No primary signatory"
            }
          }
        }
      }
      if (i.tFrom == null && i.tFromMyClient == null) {
        // 
        i.FPNumber = '';
        i.FPName = '';
        i.FromACCNumber = '';
        i.TTypeFrom = '';
        i.FReviewd = '';
      }

      if (i.tTo != null) {
        // 
        if (i.tTo.toEntity != null) {
          i.TTypeTo = "to other client entity"
          i.ToPNumber = i.tTo.toEntity.entityNumber
          i.ToPName = i.tTo.toEntity.name;
          i.ToACCNumber = '';
          if (i.tTo.toEntity.isReviewed) {
            i.TReviewd = 'Y';
          } else {
            i.TReviewd = 'N';
          }
        }
        else if (i.tTo.toPerson != null) {
          i.TTypeTo = "to other client person"
          i.ToPNumber = i.tTo.toPerson.partyNumber
          if (i.tTo.toPerson.firstName == null) {
            i.ToPName = i.tTo.toPerson.middleName + " " + i.tTo.toPerson.lastName;
          } else if (i.tTo.toPerson.middleName == null) {
            i.ToPName = i.tTo.toPerson.firstName + " " + i.tTo.toPerson.lastName;
          } else if (i.tTo.toPerson.lastName == null) {
            i.ToPName = i.tTo.toPerson.firstName + " " + i.tTo.toPerson.middleName;
          } else {
            i.ToPName = i.tTo.toPerson.firstName + " " + i.tTo.toPerson.middleName + " " + i.tTo.toPerson.lastName;
          }
          // i.ToPName = i.tTo.toPerson.firstName + " " + i.tTo.toPerson.middleName + " " + i.tTo.toPerson.lastName;
          i.ToACCNumber = '';
          if (i.tTo.toPerson.isReviewed) {
            i.TReviewd = 'Y';
          } else {
            i.TReviewd = 'N'
          }
        }
        else if (i.tTo.toAccount != null) {
          i.TTypeTo = "to other client account";
          i.ToACCNumber = i.tTo.toAccount.account;
          if (i.tTo.toAccount.isReviewed) {
            i.TReviewd = 'Y';
          } else {
            i.TReviewd = 'N';
          }
          if (i.tTo.toAccount.signatory != null) {
            let signatoryIndicator: boolean = false;
            let xc: any[] = i.tTo.toAccount.signatory;
            for (let j = 0; j < xc.length; j++) {
              if (i.tTo.toAccount.signatory[j].isPrimary === true) {
                i.ToPNumber = i.tTo.toAccount.signatory[j].tPerson.partyNumber
                i.ToPName = i.tTo.toAccount.signatory[j].tPerson.firstName + " " + i.tTo.toAccount.signatory[j].tPerson.middName + " " + i.tTo.toAccount.signatory[j].tPerson.lastName
                signatoryIndicator = true;
                break;
              }
            }
            if (!signatoryIndicator) {
              i.ToPName = " No primary signatory"
              i.ToPNumber = " No primary signatory"
            }
          }
        }
      }
      if (i.tToMyClient != null) {
        // 
        if (i.tToMyClient.toEntity != null) {
          i.TTypeTo = "to My client entity"
          i.ToPNumber = i.tToMyClient.toEntity.entityNumber
          i.ToPName = i.tToMyClient.toEntity.name;
          i.ToACCNumber = '';
          if (i.tToMyClient.toEntity.isReviewed) {
            i.TReviewd = 'Y';
          } else {
            i.TReviewd = 'N'
          }
        }
        else if (i.tToMyClient.toPerson != null) {
          i.TTypeTo = "to My client person"
          i.ToPNumber = i.tToMyClient.toPerson.partyNumber;
          if (i.tToMyClient.toPerson.firstName == null) {
            i.ToPName = i.tToMyClient.toPerson.middleName + " " + i.tToMyClient.toPerson.lastName;
          } else if (i.tToMyClient.toPerson.middleName == null) {
            i.ToPName = i.tToMyClient.toPerson.firstName + " " + i.tToMyClient.toPerson.lastName;
          } else if (i.tToMyClient.toPerson.lastName == null) {
            i.ToPName = i.tToMyClient.toPerson.firstName + " " + i.tToMyClient.toPerson.middleName;
          } else {
            i.ToPName = i.tToMyClient.toPerson.firstName + " " + i.tToMyClient.toPerson.middleName + " " + i.tToMyClient.toPerson.lastName;
          }
          // i.ToPName = i.tToMyClient.toPerson.firstName + " " + i.tToMyClient.toPerson.middleName + " " + i.tToMyClient.toPerson.lastName;
          i.ToACCNumber = '';
          if (i.tToMyClient.toPerson.isReviewed) {
            i.TReviewd = 'Y';
          } else {
            i.TReviewd = 'N';
          }
        }

        else if (i.tToMyClient.toAccount != null) {
          // 
          i.TTypeTo = "to My client account"
          i.ToACCNumber = i.tToMyClient.toAccount.account;
          if (i.tToMyClient.toAccount.isReviewed) {
            i.TReviewd = 'Y';
          } else {
            i.TReviewd = 'N'
          }
          let signatoryIndicator: boolean = false
          let xc: any[] = i.tToMyClient.toAccount.signatory
          if (xc != null) {
            // 

            for (let j = 0; j < xc.length; j++) {
              if (i.tToMyClient.toAccount.signatory[j].isPrimary === true) {
                i.ToPNumber = i.tToMyClient.toAccount.signatory[j].tPerson.partyNumber
                i.ToPName = i.tToMyClient.toAccount.signatory[j].tPerson.firstName + "" + i.tToMyClient.toAccount.signatory[j].tPerson.middleName + " " + i.tToMyClient.toAccount.signatory[j].tPerson.lastName
                signatoryIndicator = true
                break;
              }
            }
            if (!signatoryIndicator) {
              i.ToPName = " no primary signatory "
              i.ToPNumber = " no primary signatory "
            }
          }
        }
      }
      if (i.tTo == null && i.tToMyClient == null) {
        // 
        i.ToPNumber = '';
        i.TPName = '';
        i.ToACCNumber = '';
        i.TTypeTo = '';
        i.TReviewd = '';
      }
    })

  }

  ngOnChanges(changes: SimpleChanges) {
    // for (let propName in changes) {
    //   let chng = changes[propName];
    //   let cur = chng.currentValue;
    //   let prev = chng.previousValue;
    //   if (propName == 'CascadeTrans') {
    //     this.CascadeTrans = cur
    //     this.setControls()
    //   }
    // }
  }
  setControls() {
    this.dataSource.data = this.CascadeTrans.value
    

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    
  }

  getSelectedIndex() {
    var indexes = []
    this.dataSource.data.forEach((a, index) => {
      if (this.selection.selected.indexOf(a) >= 0) {
        indexes.push(index)
      }
    })
    return indexes
  }

  acceptFromComponent(event) {
    
    this.t_from = event;
    this.cascadeChangeFrom();

  }

  acceptToComponent(event) {
    
    this.t_to = event;
    this.cascadeChangeTo();

  }

  cascadeChangeTo() {
    console.log("====cascadeChange()===to");
    this.CascadeTrans.value.forEach(singletRa => {
      if (this.t_to) {
        this.t_from = null;
        console.log(" if (this.t_to)");;
        if (this.t_to.value.tToMyClient) {
          singletRa.tToMyClient = this.t_to.value.tToMyClient;
          singletRa.tTo = null;
        } else if (this.t_to.value.tTo) {
          singletRa.tTo = this.t_to.value.tTo;
          singletRa.tToMyClient = null;
        }
        else {
          singletRa.tTo = null;
          singletRa.tToMyClient = null;
        }
      }

    });
    console.table("CascadeTrans after cascade", this.CascadeTrans);
    // this.fillTable();
    this.TransactionControl = this.t_transaction.createTransactionControl();// not imp
    // this.Cascad_type = "None"
    this.dataService.changeMessage(this.CascadeTrans);
  }

  cascadeChangeFrom() {
    console.log("====cascadeChange()===from");
    this.CascadeTrans.value.forEach(singletRa => {
      if (this.t_from.value) {
        this.t_to = null;
        
        if (this.t_from.value.tFromMyClient) {
          // singletRa.tFromMyClient=Object.assign({},this.t_from.controls.tFromMyClient.value)
          singletRa.tFromMyClient = this.t_from.value.tFromMyClient;
          
          singletRa.tFrom = null;
        } else if (this.t_from.value.tFrom) {
          
          singletRa.tFrom = this.t_from.value.tFrom;
          singletRa.tFromMyClient = null;
        }
        else {
          
          singletRa.tFrom = null;
          singletRa.tFromMyClient = null;
        }
      }
    });
    console.table("CascadeTrans after cascade", this.CascadeTrans);
    // this.fillTable();
    this.TransactionControl = this.t_transaction.createTransactionControl();
    // this.Cascad_type = "None"
    this.dataService.changeMessage(this.CascadeTrans);
  }
}

 // masterToggle() {
  //   this.isAllSelected() ?
  //     this.selection.clear() :
  //     this.dataSource.data.forEach(row => this.selection.select(row));
  // }


  // isAllSelected() {
  //   const numSelected = this.selection.selected.length;
  //   const numRows = this.dataSource.data.length;
  //   return numSelected === numRows;
  // }