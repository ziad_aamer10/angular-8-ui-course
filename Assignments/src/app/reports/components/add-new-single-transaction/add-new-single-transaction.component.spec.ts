import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewSingleTransactionComponent } from './add-new-single-transaction.component';

describe('StrTransactionDetailsPageComponent', () => {
  let component: AddNewSingleTransactionComponent;
  let fixture: ComponentFixture<AddNewSingleTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewSingleTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewSingleTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
