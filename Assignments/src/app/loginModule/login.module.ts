import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule, MatButtonModule, MatTooltipModule} from '@angular/material';
import {MatListModule} from '@angular/material/list';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import { RouterModule } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import { LoginRoutingModule } from './login-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
   
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatMenuModule,
    RouterModule,
    LoginRoutingModule,

    MatTooltipModule,SharedModule
  ],
  exports: [ LoginComponent],
  entryComponents: [
      ],
})
export class LoginModule { }
