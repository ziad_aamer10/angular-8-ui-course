import { Component, OnInit, Inject, Input, Output, EventEmitter, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';

import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { LookupClass } from '../../lockups/Lookups';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { TransactionService } from '../../services/transaction.service';
import { t_transaction } from '../../validationControls/t_transaction';
import { AccountComponent } from '../account/account.component';
import { EntityPageComponent } from '../entity-page/entity-page.component';
import { PersonPageComponent } from '../person-page/person-page.component';
import { UtilService } from 'src/app/util.service';
import { DatePipe } from '@angular/common';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';
import { Actions } from '../../Actions/action';


export interface Type {
  value: string;
}

@Component({
  selector: 'app-add-new-single-transaction',
  templateUrl: './add-new-single-transaction.component.html',
  styleUrls: ['./add-new-single-transaction.component.css'],
  providers: [DatePipe]
})
export class AddNewSingleTransactionComponent implements OnInit , OnDestroy{

  signatoryPersonObject;
  chhh:Object;
  addTransactionState: boolean = false;
  editTransactionState: boolean = false;
  lateDeposite;
  dirtyValuesArray = {}
  subscription: Subscription;
  subscription1:Subscription;
  subscriptionSaveTransaction:Subscription;
  signatorySubscription:Subscription;
  @Output() trasnactionFormOutput = new EventEmitter();
  @Input() strReportTransactionControl: FormGroup;
  T_transaction: t_transaction = new t_transaction(this.fb)
  transactionControl: FormGroup
  createMode: boolean = true;
  Goods_type: string;
  type: string = ''
  checkedValue: boolean;
  transmode_code = LookupClass.transmode_code;
  timerid=null;
  addTransactionAction: boolean=false;
  late:boolean=false
  constructor(public dialog: MatDialog, private fb: FormBuilder,
    private transactionServive: TransactionService, private dataService: SharingDataServieService,
    private toaster: ToastrService,public util:UtilService,private datePipe: DatePipe,private auth:AuthService) { }

  ngOnInit() {
    
    this.transactionControl = this.strReportTransactionControl
    this.subscription = this.dataService.currentMessage4.subscribe(transactionState => {
      
      if (transactionState == "editTrasnaction") {
        this.editTransactionState = true;
      } else {
        this.addTransactionState = true;
      }
    })
    this.subscription1 = this.util.currentMessage.subscribe(data => {
      // 
      if (data && this.transactionControl.touched) {
        this.util.validateFormGroup(this.transactionControl, [])
      }

    })
    this.timerid = setInterval(() => this.TimeInterval(), 10000);

    // this.signatorySubscription = this.dataService.signatoryObject.subscribe(
    //   (signatoryObject:any) => {
    //     
        
    //     this.signatoryPersonObject = Object.assign({},signatoryObject);
    //     
    //   }
    // )


  }
  ngOnDestroy() {
    
    
    this.subscription.unsubscribe();
    this.subscription1.unsubscribe();
    if(this.subscriptionSaveTransaction != undefined)
      this.subscriptionSaveTransaction.unsubscribe();
    // this.signatorySubscription.unsubscribe();
    clearInterval(this.timerid)
    localStorage.removeItem('ReportST')
  }
  TimeInterval(): void {
    //
    localStorage.setItem('ReportST', JSON.stringify(this.transactionControl.value))

  }

  types: Type[] = [
    { value: 'my-client-account' },
    { value: 'my-client-entity' },
    { value: 'my-client-person' },
    { value: 'other-client-account' },
    { value: 'other-client-entity' },
    { value: 'other-client-person' }
  ];


  acceptGoddsForm(form: FormGroup) {
    
    this.transactionControl.setControl('isGoodsServices', form)
  }

  acceptBooleanPartyChange1(event){
    
    
    this.transactionControl.value.changedObjectPartyNumber = event;
    this.chhh = Object.assign({},event);
        
    
  }

  saveFullTransaction() {
    // this.subscription.unsubscribe();
    if (this.editTransactionState) {
      this.updateFullTransaction();
    } else {
      
      this.trasnactionFormOutput.emit(this.transactionControl);
      this.dataService.changeMessage(this.transactionControl);
    }
  }

  updateFullTransaction() { 
    let obj ;
    
    
    let trnControl = this.transactionControl ;
    // let vChgObject ; 
    // if(this.signatoryPersonObject){
    //   
    //   vChgObject = this.signatoryPersonObject;
    // }else{
      // vChgObject = this.chhh;
    // }
    let  vChgObject = this.chhh;
    obj = {trnControl , vChgObject};
    
    this.dataService.changeMessage(obj);
    this.trasnactionFormOutput.emit(this.transactionControl);
  }

  acceptBooleanPartyChange(event){
    
    this.chhh = Object.assign({},event);
    // this.transactionControl.value.changedObjectPartyNumber = event;
    //  
  }


  acceptTransationFrom(form: FormGroup) {
    if (form.get('tFromMyClient')) {
      this.transactionControl.setControl('tFromMyClient', form);
    } else if (form.get('tFrom')) {
      this.transactionControl.setControl('tFrom', form);
    }
  }

  onGoClick(): void {
    if (this.type == 'my-client-account')
      this.openAccountDialog();
    else if (this.type == 'my-client-person')
      this.openPersonDialog();
    else if (this.type == 'my-client-entity')
      this.openEntityDialog();
  }

  openAccountDialog(): void {
    const dialogRef = this.dialog.open(AccountComponent, {
      width: '800px',
      height: '800px',
      data: { name: 'this.name', animal: 'this.animal' }
    });

    dialogRef.afterClosed().subscribe(result => {
      

    });
  }
  openPersonDialog(): void {
    const dialogRef = this.dialog.open(PersonPageComponent, {
      width: '500px',
      height: '500px',
      data: { name: 'this.name', animal: 'this.animal' }
    });

    dialogRef.afterClosed().subscribe(result => {
      

    });
  }
  openEntityDialog(): void {
    const dialogRef = this.dialog.open(EntityPageComponent, {
      width: '500px',
      height: '500px',
      data: { name: 'this.name', animal: 'this.animal' }
    });

    dialogRef.afterClosed().subscribe(result => {
      

    });
  }
  close() {

  }
  save(element) {
    clearInterval(this.timerid)
    localStorage.removeItem("ReportST");
    

  }
  get TranCtrl() {
    return this.transactionControl.controls
  }

  getTransactionFromSource(tRef: HTMLInputElement) {

    let transNumb = tRef.value
    if (transNumb) {
      this.transactionServive.getTransactionFromSource(transNumb).subscribe(data => {
        if (data) {
          console.log("transaction === " , data);
          
          this.toaster.success("Transaction is returned successfully")
          this.setTransaction(data)
        }
        else {
          this.transactionControl.reset()

          this.toaster.error("No data returned")
        }
      }, error => {
        

        this.toaster.error(error.error.message)
      })
    }
    else {
      alert("Transaction reference Number is Empty")
    }

  }
  setTransaction(data) {
    this.transactionControl = this.T_transaction.setTransaction(data)
  }
  saveTransactionToSrc() {
    clearInterval(this.timerid)
    localStorage.removeItem("ReportST");
    let transaction = this.transactionControl.value
    
    
    this.subscriptionSaveTransaction =  this.transactionServive.saveTransaction(transaction).subscribe(data => {
      this.toaster.success("transaction saved")
    },
      error => {
        this.toaster.error(error.message)
      })
  }
  validateTrans() {
    this.util.validateFormGroup(this.transactionControl, [])
  }
  setActions(){
    this.addTransactionAction=this.auth.has_Capabilities(Actions.addTransaction)
  }
}
