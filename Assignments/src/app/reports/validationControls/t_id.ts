import { FormGroup, Validators, FormBuilder, FormControl } from "@angular/forms";
import { UtilService } from "src/app/util.service";

export class t_id{
  private util:UtilService=new UtilService()
    constructor(public fb:FormBuilder)
    {

    }
    createId(): FormGroup {
        return this.fb.group({
          id:new FormControl(),
          type:[null,[Validators.required]],
          number:[null,[Validators.required,Validators.maxLength(255)]],
          issueDate:new FormControl(),
          expiryDate:[null,[Validators.required]],
          issuedBy:[null,[Validators.maxLength(255)]],
          issueCountry:[null,[Validators.required]],
          comments:[null,[Validators.maxLength(4000)]]
        })
      }
      setId(id){
        return this.fb.group({
          id:new FormControl(id.id),
          type:this.util.checkLockUpValue('idType',id.type)?new FormControl(id.type,[Validators.required]):new FormControl(null,Validators.required), 
          number:new FormControl(id.number,[Validators.required,Validators.maxLength(255)]),   
          issueDate:new FormControl(this.util.convertDateFromServer(id.issueDate)),
          expiryDate:new FormControl(this.util.convertDateFromServer(id.expiryDate),[Validators.required]),
        issueCountry  :this.util.checkLockUpValue('nationality',id.issueCountry)?new FormControl(id.issueCountry,[Validators.required]):new FormControl(null,Validators.required),
        issuedBy:new FormControl(id.issuedBy,[Validators.maxLength(255)]),
          comments:new FormControl(id.comments,[Validators.maxLength(4000)])
        })
      }
}