import { FormGroup, Validators, FormBuilder } from "@angular/forms";
let fb = new FormBuilder();
import { t_person } from './t_person'
import { utils } from "protractor";
import { UtilService } from "src/app/util.service";


export class t_person_my_client {
    t_person: t_person = new t_person()
    UtilService:UtilService=new UtilService()
    createPersonGroup(): FormGroup {
        let f: FormGroup = this.t_person.createPersonControl()
        this.addMyPersonValidators(f);
        return f
    }


    setPersonControl(tPerson){
        let f: FormGroup = this.t_person.setPersonControl(tPerson);
        this.addMyPersonValidators(f);

        return f ; 
    }

    addMyPersonValidators(person:FormGroup){
    
        person.controls["gender"].setValidators([Validators.required])
        person.controls["birthdate"].setValidators([Validators.required])
        person.controls["ssn"].setValidators([Validators.required])
        person.controls["nationality1"].setValidators([Validators.required])

        person.controls["residence"].setValidators([Validators.required])

        person.controls["occupation"].setValidators([Validators.required])
        person.controls["identification"].setValidators([Validators.required,UtilService.minLengthArray(1)])
        person.get("phones.phone").setValidators([Validators.required])
        person.get("addresses.address").setValidators([Validators.required])
        person.updateValueAndValidity()
        let Fields=['gender','birthdate','ssn','nationality1','residence','occupation',
        'identification','phones.phone','addresses.address']
        this.UtilService.updateFormGroupValidity(person,Fields)
      
    }
    createDirector(): FormGroup{
        let DirectGroup: FormGroup = this.t_person.createDirector();
        this.addMyPersonValidators(DirectGroup);

        return DirectGroup
    
    }
    setDirector(director): FormGroup{
        let DirectGroup:FormGroup=this.t_person.setDirector(director)
        this.addMyPersonValidators(DirectGroup);

        return DirectGroup
    }
  
}

