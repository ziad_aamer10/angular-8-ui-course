import { activity } from './../../validationControls/activity';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';
import { reportActivity } from '../../models/reportActivity.model';
import { FormGroup, FormArray } from '@angular/forms';


@Component({
  selector: 'app-sar-report-activities-list',
  templateUrl: './sar-report-activities-list.component.html',
  styleUrls: ['./sar-report-activities-list.component.css']
})
export class SarReportActivitiesListComponent implements OnInit {
  @Input() reportControl: FormGroup;
  @Output() editActivityEvent = new EventEmitter();
  disableEdit: boolean=false;
  displayedColumns = ['select', 'activity_number', 'activity_type'];
 ActivitiesArray: FormArray;
 activity: FormGroup;
  dataSource = new MatTableDataSource([]);
  selection = new SelectionModel<reportActivity>(true, []);
  data: any[] = [];
    constructor() { 
      
    }

    ngOnInit() {
      
      this.activity = this.reportControl.get('activity') as FormGroup;
    this.ActivitiesArray = this.reportControl.get('activity.reportParties.reportParty') as FormArray;
  this.data = this.ActivitiesArray.value;
if(this.activity.disabled){
this.disableEdit=true;
//this.disabledform(this.reportControl)


}
    }

   
    applyFilter(filterValue: string) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }

    masterToggle() {
      this.isAllSelected() ?
          this.selection.clear() :
          this.dataSource.data.forEach(row => this.selection.select(row));
    }
    editActivity2(act: FormGroup) {
     
      
      
      this.editActivityEvent.emit(act);
    }
    editActivity(activity) {
      

      for (const i of this.ActivitiesArray.controls) {

if (i.value.account && i.value.account.account && activity.account && activity.account.account
  && (i.value.account.account == activity.account.account)) {

this.editActivityEvent.emit(i);
return;
} else if (i.value.entity && i.value.entity.entityNumber && activity.entity && activity.entity.entityNumber
  && (i.value.entity.entityNumber == activity.entity.entityNumber)) {
  
  this.editActivityEvent.emit(i);
  return;
} else if (i.value.person && i.value.person.partyNumber && activity.person && activity.person.partyNumber
  && (i.value.person.partyNumber == activity.person.partyNumber)) {
  
  this.editActivityEvent.emit(i);
  return;
}
      }
    }
    deleteActivity(i) {
      if (confirm('You are going to delete Activity')) {
        this.ActivitiesArray.removeAt(i);
      }

    }
}
export interface activityType {
  activity_number: string;
  activity_type: string;

}
