import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { SharedModule } from '../shared/shared.module';
import { UtilService } from '../util.service';
import { AccountComponent } from './components/account/account.component';
import { AddMultipleTransactionDialogComponent } from './components/add-multiple-transaction-dialog/add-multiple-transaction-dialog.component';
import { AddNewSingleTransactionComponent } from './components/add-new-single-transaction/add-new-single-transaction.component';
import { AddressesComponent } from './components/addresses/addresses.component';
import { AifReportDetailsComponent } from './components/aif-report-details/aif-report-details.component';
import { AifReportComponent } from './components/aif-report/aif-report.component';
import { CascadeTransactionComponent } from './components/cascade-transaction/cascade-transaction.component';
import { ChoosePersonSSNComponent } from './components/choose-person-ssn/choose-person-ssn.component';
import { CrNewReportComponent } from './components/cr-new-report/cr-new-report.component';
import { DynamicFormsValidationsComponent } from './components/dynamic-forms-validations/dynamic-forms-validations.component';
import { DynamicMatSelectComponent } from './components/dynamic-mat-select/dynamic-mat-select.component';
import { EmailComponent } from './components/email/email.component';
import { EntityPageComponent } from './components/entity-page/entity-page.component';
import { FromTransactionComponent } from './components/from-transaction/from-transaction.component';
import { GoodsItemComponent } from './components/goods-item/goods-item.component';
import { GoodsServicesComponent } from './components/goods-services/goods-services.component';
import { IdsComponent } from './components/ids/ids.component';
import { PersonDialogComponent } from './components/person-dialog/person-dialog.component';
import { PersonPageComponent } from './components/person-page/person-page.component';
import { PhonesComponent } from './components/phones/phones.component';
import { ReportHomeComponent } from './components/report-home/report-home.component';
import { ReportIndicatiorsComponent } from './components/report-indicatiors/report-indicatiors.component';
import { RouteReportComponent } from './components/route-report/route-report.component';
import { SarReportActivitiesListComponent } from './components/sar-report-activities-list/sar-report-activities-list.component';
import { SarReportActivityDetailsComponent } from './components/sar-report-activity-details/sar-report-activity-details.component';
import { SarReportComponent } from './components/sar-report/sar-report.component';
import { SignatoryComponent } from './components/signatory/signatory.component';
import { StrReportDetailsComponent } from './components/str-report-details/str-report-details.component';
import { StrComponent } from './components/str/str.component';
import { ToTransactionComponent } from './components/to-transaction/to-transaction.component';
import { TransactionDetailsComponent } from './components/transaction-details/transaction-details.component';
import { HistoryComponent } from './history/history.component';
import { ActivityService } from './service/activity.service';
import { ConfirmDeactivateGuardServiceStrService } from './service/confirm-deactivate-guard-service-str.service';
import { ConfirmDeactivateGuardService } from './service/confirm-deactivate-guard.service';
import { EntityService } from './services/entity.service';
import { PersonsService } from './services/persons.service';
import { ReportService } from './services/report.service';
import { SharingDataServieService } from './services/sharing-data-servie.service';
// import { TableFilterComponent } from './components/table-filter/table-filter.component';
import { SignatoryService } from './services/signatory.service';
import { TransactionService } from './services/transaction.service';
import { CommentModule } from '../comment/comment.module';
import { ReportsRoutingModule } from './reports-routing.module';
import { ReportingPersonComponent } from '../Admin config/reporting-person/reporting-person.component';
import { AdminPageComponent } from '../Admin config/admin-page/admin-page.component';
import { BankComponent } from '../Admin config/bank/bank.component';
import { AuditComponent } from '../Admin config/audit/audit.component';
import { JwtInterceptor } from '@auth0/angular-jwt';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptor } from '../services/loader.interceptor';

@NgModule({
  declarations: [ReportHomeComponent, CrNewReportComponent,
    RouteReportComponent,
    SarReportActivityDetailsComponent,
    PersonPageComponent,
    EntityPageComponent,
    AddMultipleTransactionDialogComponent,
    StrComponent,
    ReportIndicatiorsComponent,
    TransactionDetailsComponent,
    AccountComponent,
    CascadeTransactionComponent,
    PhonesComponent,
    SarReportComponent,
    AddNewSingleTransactionComponent,
    EmailComponent,
    AddressesComponent,
    IdsComponent,
    SarReportActivitiesListComponent,
    StrReportDetailsComponent,
    PersonDialogComponent,
    FromTransactionComponent,
    ToTransactionComponent,
    GoodsServicesComponent,
    AifReportComponent,
    AifReportDetailsComponent,
    SignatoryComponent,
    ChoosePersonSSNComponent,
    DynamicFormsValidationsComponent,
    DynamicMatSelectComponent,
    GoodsItemComponent,
    // TableFilterComponent,
    HistoryComponent,
    ReportingPersonComponent,
    AdminPageComponent,
    BankComponent, AuditComponent

  ],
  imports: [

    CommonModule,

    FilterPipeModule,
    SharedModule,
    ScrollingModule,

    CommentModule,
    ReportsRoutingModule
  ],
  exports: [
    ReportHomeComponent,
    SarReportActivityDetailsComponent,
    PersonPageComponent,
    EntityPageComponent,
    AddMultipleTransactionDialogComponent,
    SarReportComponent, AddressesComponent, ReportingPersonComponent,
    AdminPageComponent,
    BankComponent, AuditComponent
  ],

  entryComponents: [
    RouteReportComponent,
    CascadeTransactionComponent,
    AddMultipleTransactionDialogComponent,
    SarReportActivityDetailsComponent,
    AccountComponent,
    PersonPageComponent,
    EntityPageComponent,
    PersonDialogComponent,
    SignatoryComponent,
    ChoosePersonSSNComponent,
    CrNewReportComponent
  ],
  providers: [PersonsService, UtilService, ReportService, EntityService, SignatoryService, TransactionService,
    SharingDataServieService, ActivityService, ConfirmDeactivateGuardService, ConfirmDeactivateGuardServiceStrService,
     DatePipe,{provide: HTTP_INTERCEPTORS,
     useClass: JwtInterceptor,
     multi: true},{ provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }],
})
export class ReportsModule { }
