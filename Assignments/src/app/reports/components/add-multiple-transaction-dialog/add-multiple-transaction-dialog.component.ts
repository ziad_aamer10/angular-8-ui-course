import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, Validators, FormControl, ValidationErrors, FormArray } from '@angular/forms';
import { LookupClass } from '../../lockups/Lookups';
import { UtilService } from 'src/app/util.service';
import { AddMultiTransService } from '../../services/AddMutiTrans.service';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-multiple-transaction-dialog',
  templateUrl: './add-multiple-transaction-dialog.component.html',
  styleUrls: ['./add-multiple-transaction-dialog.component.css']
})

export class AddMultipleTransactionDialogComponent implements OnInit,OnDestroy {
  subscriptionGetMultiTrans:Subscription
  constructor(private multitrans: AddMultiTransService, private dataService: SharingDataServieService,
    private utilservice: UtilService, private toaster: ToastrService,
    public dialogRef: MatDialogRef<AddMultipleTransactionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
  // added by abdelrahman to assign values from lookups
  selected = '';
  Transmod_code = LookupClass.transmode_code;
  TransTypeData_values = LookupClass.TransTypeData;
  notValidElement: string[] = [];
  myDate = new Date();
  // '100011707361'
  MultipleTransactionForm = new FormGroup({
    'searchBy': new FormControl('', Validators.compose([Validators.required])),
    'searchValue': new FormControl('', Validators.compose([Validators.required])),
    'partiesFrom': new FormControl('', Validators.compose([Validators.required])),
    'partiesFromValue': new FormControl('', Validators.compose([Validators.required])),
    'partiesTo': new FormControl('', Validators.compose([Validators.required])),
    'partiesToValue': new FormControl('', Validators.compose([Validators.required])),
    'fromDate': new FormControl(),
    'toDate': new FormControl(),
    'fromAmount': new FormControl('', Validators.min(1)),
    'toAmount': new FormControl(),
    'transmodeCode': new FormControl(),
    'tType': new FormControl(),

  });
  validateFormControl(name, x: FormControl) {


    const controlErrors: ValidationErrors = x.errors;
    x.markAsDirty();
    x.markAsTouched();

    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(keyError => {
        this.notValidElement.push(name);
      });
    }
  }

  validateFormGroup(x: FormGroup) {
    Object.keys(x.controls).forEach(key => {
      if (x.get(key) instanceof FormGroup) {
        this.validateFormGroup(x.get(key) as FormGroup);
      } else if (x.get(key) instanceof FormArray) {
        this.validateFormArray(x.get(key) as FormArray);
      } else if (x.get(key) instanceof FormControl) {
        this.validateFormControl(key, x.get(key) as FormControl);
      }

    });
  }
  validateFormArray(formArray: FormArray) {
    Object.keys(formArray.controls).forEach(key => {
      if (formArray.get(key) instanceof FormGroup) {
        this.validateFormGroup(formArray.get(key) as FormGroup);
      } else if (formArray.get(key) instanceof FormControl) {
        this.validateFormControl(key, formArray.get(key) as FormControl);

      } else if (formArray.get(key) instanceof FormArray) {
        this.validateFormArray(formArray.get(key) as FormArray);
      }
    });


  }
  validate(x: FormGroup) {
    this.notValidElement = [];
    this.validateFormGroup(x);

  }
  private scrollToAnchor(anchor: string): boolean {
    const element = document.querySelector(`[formControlName=${anchor}]`);
    this.utilservice.closeExpansionPanel = true;
    if (element) {
      element.scrollIntoView();

      return true;
    }
    return false;
  }
  clearValidationErrors() {
    this.notValidElement = [];
  }

  ngOnInit() {
  }
  close() {
    this.dialogRef.close(0);
  }
  GetMultiTrans() {

    if (this.MultipleTransactionForm.get('searchBy').invalid ||
      this.MultipleTransactionForm.get('searchValue').invalid && (
        this.MultipleTransactionForm.get('partiesFrom').invalid ||
        this.MultipleTransactionForm.get('partiesFromValue').invalid ||
        this.MultipleTransactionForm.get('partiesTo').invalid ||
        this.MultipleTransactionForm.get('partiesToValue').invalid)) {
      window.alert('Please fill one of  ' + this.notValidElement);
      // 
      // this.MultipleTransactionForm.disable();
    } else {
      this.subscriptionGetMultiTrans = this.multitrans.SearchMultiTrans(this.MultipleTransactionForm.value).subscribe(data => {
        if (!Array.isArray(data) || !data.length) {
          this.toaster.error(`No transaction found`);
        } 
        else {
          this.dialogRef.close({data:data});
          this.toaster.success(`transactions loaded`,'',{timeOut:2000});
          
          
        }
        


      }
        ,
        error => {
          

        }
      )
      

    }
  }
  clearAll() {
    this.MultipleTransactionForm.patchValue({
      SearchValue: null,
      partiesFromValue: null,
      partiesToValue: null
    });
  }


  ngOnDestroy(){
    if(this.subscriptionGetMultiTrans != undefined)
      this.subscriptionGetMultiTrans.unsubscribe();
  }

}
