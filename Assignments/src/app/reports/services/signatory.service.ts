import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SignatoryService {

  constructor(private http:HttpClient) { }
  getSignatoryDetailByPartyNumber(partyNumber,accountnumber){
    let url;
    if(accountnumber){
    url=`${environment.projectUrl}/tSignatoryMap?partyNumber=${partyNumber}&accountNumber=${accountnumber}`
    }
    else{
      url=`${environment.projectUrl}/tSignatoryMap?partyNumber=${partyNumber}`
    }

  return this.http.get(url)
  }
  saveSignatoryToSource(signatory){

let url=`${environment.projectUrl}/sPersons`
return this.http.put(url,signatory)
  }
}
