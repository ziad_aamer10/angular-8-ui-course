import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { t_person } from './t_person';
import { t_account } from './t_account';
import { tEntity } from './t_entity';
import { t_person_my_client } from './t_person_my_client';
import { t_account_my_client } from './t_account_my_client';
import { t_entity_my_client } from './t_entity_my_client';
import { formControlBinding } from '@angular/forms/src/directives/ng_model';
import { ForeignCurrency } from './ForeignCurrency';
import { UtilService } from 'src/app/util.service';

export class t_from {
    // fb:FormBuilder
    tPerson: t_person = new t_person();
    t_account: t_account = new t_account();
    t_entity: tEntity = new tEntity();

    tMyPerson: t_person_my_client = new t_person_my_client();
    tMyAccount: t_account_my_client = new t_account_my_client();
    tMyEntity: t_entity_my_client = new t_entity_my_client();
    foreignCurrency:ForeignCurrency=new ForeignCurrency()
    util=new UtilService();
    constructor(public fb: FormBuilder) {
        // this.fb=new FormBuilder()
    }
    create_t_from_my_controller(): FormGroup {
        return this.fb.group({
            id: [''],
            fromFundsCode: ['', Validators.required],
            fromFundsComment: [''],
            fromForeignCurrency: this.foreignCurrency.createForeignCurrency(),
            tConductor: new FormControl(),
            fromAccount: new FormControl(),
            fromPerson: new FormControl(),
            fromEntity: new FormControl(),
            fromCountry: ['',Validators.required],
            isConductor: ['']
        });
    }




    set_t_from_my_controller(tfrom): FormGroup {
        if (tfrom == null) {
            return null;
        }
        return this.fb.group({
            id: [tfrom.id],
            fromFundsCode: this.util.checkLockUpValue('FundsCode',tfrom.fromFundsCode)?[tfrom.fromFundsCode, Validators.required]:new FormControl(null,Validators.required),
            fromFundsComment: [tfrom.fromFundsComment],
            fromForeignCurrency: tfrom.fromForeignCurrency?this.foreignCurrency.setForeignCurrency(tfrom.fromForeignCurrency):this.foreignCurrency.createForeignCurrency(),
            tConductor: tfrom.tConductor ? this.tMyPerson.setPersonControl(tfrom.tConductor) : new FormControl(),
            fromAccount: tfrom.fromAccount ? this.tMyAccount.setAccountControl(tfrom.fromAccount) : new FormControl(),
            fromPerson: tfrom.fromPerson ? this.tMyPerson.setPersonControl(tfrom.fromPerson) : new FormControl(),
            fromEntity: tfrom.fromEntity ? this.tMyEntity.setEntityControl(tfrom.fromEntity) : new FormControl(),
            fromCountry:this.util.checkLockUpValue('nationality',tfrom.fromCountry)? [tfrom.fromCountry,Validators.required]:new FormControl(null,Validators.required),
            isConductor: [tfrom.isConductor]


           
        });
    }
    // myclient
    create_t_from_other_controller(): FormGroup {
        return this.fb.group({
            id: [''],
            fromFundsCode: ['', Validators.required],
            fromFundsComment: [''],
            fromForeignCurrency: this.foreignCurrency.createForeignCurrency(),
            tConductor: new FormControl(),
            fromAccount: new FormControl(),
            fromPerson: new FormControl(),
            fromEntity: new FormControl(),
            fromCountry: ['', Validators.required],
            isConductor: ['']

        });
    }




    set_t_from_other_controller(tfrom): FormGroup {
        if (tfrom == null) {
            return null;
        }
        return this.fb.group({
            id: [tfrom.id],
            fromFundsCode: this.util.checkLockUpValue('FundsCode',tfrom.fromFundsCode)?[tfrom.fromFundsCode, Validators.required]:new FormControl(null,Validators.required) ,
            fromFundsComment: [tfrom.fromFundsComment],
            fromForeignCurrency: tfrom.fromForeignCurrency?this.foreignCurrency.setForeignCurrency(tfrom.fromForeignCurrency):this.foreignCurrency.createForeignCurrency(),
            tConductor: tfrom.tConductor ? this.tMyPerson.setPersonControl(tfrom.tConductor) : new FormControl(),
            fromAccount: tfrom.fromAccount ? this.t_account.setAccountControl(tfrom.fromAccount) : new FormControl(),
            fromPerson: tfrom.fromPerson ? this.tPerson.setPersonControl(tfrom.fromPerson) : new FormControl(),
            fromEntity: tfrom.fromEntity ? this.t_entity.setEntityControl(tfrom.fromEntity) : new FormControl(),
            fromCountry: this.util.checkLockUpValue('nationality',tfrom.fromCountry)?[tfrom.fromCountry, Validators.required]:new FormControl(null,Validators.required),
            isConductor: [tfrom.isConductor]


          
        });
    }

    
}
