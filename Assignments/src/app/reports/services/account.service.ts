import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  accounts:any=[];
  constructor(private http:HttpClient) { }
 addPerson(account){
   this.accounts.push(account)
 } 
 findAccountById(id){
   let url=`${environment.projectUrl}/tAccounts/${id}`
   return this.http.get(url)
   
 }
 saveAccountToSource(account){
   let url=`${environment.projectUrl}/sAccounts`
   return this.http.put(url,account)
 }
 getAccountByAccountNumber(accountNumber)
 {
   let url=`${environment.projectUrl}/tAccountsMap/${accountNumber}`
   return this.http.get(url)
 }
}
