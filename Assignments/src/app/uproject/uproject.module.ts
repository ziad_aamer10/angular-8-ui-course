import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { RecipesComponent } from './recipes/recipes.component';
import { RecipeListComponent } from './recipes/recipe-list/recipe-list.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { ShoppingEditComponent } from './shopping-list/shopping-edit/shopping-edit.component';
import { RecipeItemComponent } from './recipes/recipe-list/recipe-item/recipe-item.component';

@NgModule({
  declarations: [HeaderComponent, RecipesComponent, RecipeListComponent, RecipeDetailComponent, ShoppingListComponent, ShoppingEditComponent, RecipeItemComponent],
  imports: [
    CommonModule
  ],
  exports:[HeaderComponent, RecipesComponent, RecipeListComponent, RecipeDetailComponent, ShoppingListComponent, ShoppingEditComponent, RecipeItemComponent]
})
export class UprojectModule { }
