import { Component, OnInit, EventEmitter, Output, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class CockpitComponent implements OnInit {

  @Output() blueprintCreated  = new EventEmitter<{serverName: string, serverContent: string}>();
  @Output() serverCreated = new EventEmitter<{serverName: string, serverContent: string}>();

  @ViewChild('inputServerContent', {static: true}) newServerContent: ElementRef;


  constructor() { }

  ngOnInit() {
  }

  onAddServer(inputName: HTMLInputElement) {
    this.serverCreated.emit({serverName: inputName.value, serverContent: this.newServerContent.nativeElement.value});
  }

  onAddBlueprint(inputName: string) {
    this.blueprintCreated.emit({serverName: inputName, serverContent:  this.newServerContent.nativeElement.value});
  }

}
