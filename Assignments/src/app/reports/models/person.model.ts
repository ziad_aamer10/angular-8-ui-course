import { address } from './address.model';
import { phone } from './phone.model';

export interface person {
    gender;
    Title;
    firstName;
    middleName;
    prefix;
    lastName;
    birthdate;
    birthPlace;
    mothersName;
    alias;
    ssn;
    passportNumber;
    passportCountry;
    idNumber;
    nationality2;
    nationality3;
    nationality1;
    residence;
    Phones: phone[];
     Addresses: address[];
    Email;
    occupation;
    employerName;
    employerAddressId;
    employerPhoneId;
    identification;
    deceased;
    dateDeceased;
    tax_numeber;
    taxRegNumber;
    sourceOfWealth;
    comments;
}
