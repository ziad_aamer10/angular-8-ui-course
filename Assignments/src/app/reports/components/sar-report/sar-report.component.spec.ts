import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SarReportComponent } from './sar-report.component';

describe('SarReportComponent', () => {
  let component: SarReportComponent;
  let fixture: ComponentFixture<SarReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SarReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SarReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
