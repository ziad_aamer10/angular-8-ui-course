import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicMatSelectComponent } from './dynamic-mat-select.component';

describe('DynamicMatSelectComponent', () => {
  let component: DynamicMatSelectComponent;
  let fixture: ComponentFixture<DynamicMatSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicMatSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicMatSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
