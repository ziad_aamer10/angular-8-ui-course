import {Directive, EventEmitter, Input, Output, ViewContainerRef} from "@angular/core";

@Directive({
  selector: '[appPlaceHolder]'
})
export class PlaceholderDirective{

  constructor(public viewContainerRef: ViewContainerRef) {
  }

}
