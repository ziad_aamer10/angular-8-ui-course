import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { BankInfo } from 'src/app/reports/validationControls/Bank_info';
import { LookupClass } from 'src/app/reports/lockups/Lookups';
import { BankInfoService } from 'src/app/reports/services/bank-info.service';
import { UtilService } from 'src/app/util.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.css']
})

export class BankComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  subscriptionGetInfo: Subscription;
  notValidSection: any[]=[];
  constructor(private bankinfoservice: BankInfoService, private utilservice: UtilService, private toaster: ToastrService) {
  }
  get f() {
    return this.BankInfoForm.controls;
  }
  //   BankInfoForm=new FormGroup({
  //     'rentityId':new FormControl('',Validators.required),
  //     'rentityBranch':new FormControl('',Validators.maxLength(200)),
  //     'bankName':new FormControl('',Validators.required),
  //     'bankSwift':new FormControl('',Validators.required),
  //     'addresses':new FormGroup({
  //       address: new FormArray([])
  //     })

  // signatoryFormControl:FormGroup;
  // this.signatoryFormControl=this.t_signatory.create_signatoryControl()
  // 
  // this.personForm= this.signatoryFormControl.get('tPerson') as FormGroup
  // create_signatoryControl

  // })
  // added by abdelrahman to assign values from lookups
  country_values = LookupClass.country;
  addtype_values = LookupClass.contact_type;

  notValidElement: string[] = [];
  BankInfoForm: FormGroup;
  BankInfo: BankInfo = new BankInfo();
  location: FormGroup;
  addresses: FormArray;
  SaveBankInfo() {
    if (!this.BankInfoForm.valid) {
      this.toaster.warning('Please fill all mandatory fields before saving');
      return;
    }
    this.subscription = this.bankinfoservice.SaveBankInfo(this.BankInfoForm.value).subscribe(data => {
      this.toaster.success('Bank Info is saved successfully');

    },
      error => {
        this.toaster.error(error);

      }
    );
  }
  GetBankInfo() {
    this.subscriptionGetInfo = this.bankinfoservice.GetBankInfo().subscribe(data => {
      if (data) {
        this.BankInfoForm.setValue(data);
        this.toaster.success('Bank Info returned successfully');
      } else {
        this.toaster.warning('No Data Returned');
      }
    },
      error => {
        this.toaster.error(error);

      }
    );
  }
  validate(x: FormGroup) {
    this.notValidElement = [];
    this.notValidSection = [];
    this.utilservice.validateFormGroup(x, this.notValidElement)
    this.utilservice.validateFormGroup2(x, [], this.notValidSection);
    if (x.valid) {
      this.toaster.success('No Validation Error');
      return;



    }}
  private scrollToAnchor(anchor: string): boolean {
    const element = document.querySelector(`[formControlName=${anchor}]`);
    this.utilservice.closeExpansionPanel = true;
    if (element) {
      element.scrollIntoView();

      return true;
    }
    return false;
  }
  clearValidationErrors() {
    this.notValidElement = [];
  }
  ngOnInit() {
    this.BankInfoForm = this.BankInfo.BankInfoForm(),
      this.location = this.BankInfoForm.get('location') as FormGroup;
  }

  ngOnDestroy() {
    if (this.subscription != undefined)
      this.subscription.unsubscribe();
    if (this.subscriptionGetInfo != undefined)
      this.subscriptionGetInfo.unsubscribe();
  }
}
