import { Component, OnInit, Input } from '@angular/core';
import { UtilService } from '../util.service';

@Component({
  selector: 'app-validation-errors',
  templateUrl: './validation-errors.component.html',
  styleUrls: ['./validation-errors.component.css']
})
export class ValidationErrorsComponent implements OnInit {
@Input()notValidElement;

clearValidationErrors() {
  this.notValidElement = []
}
scrollToAnchor(x) {
this.utilservice.scrollToAnchor(x)
}
  constructor(private utilservice:UtilService) { }

  ngOnInit() {
  }

}
