import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { t_email } from '../../validationControls/t_email';
import { UtilService } from 'src/app/util.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit, OnChanges {

inputEmail;
 Emails: FormArray;
 public t_email: t_email = new t_email(this.fb);
 @Input() EmailFormGroup: FormGroup;
 @Input() disableEmail: boolean
  constructor(public fb: FormBuilder, private utilsevice: UtilService) {
    this.inputEmail = this.t_email.createEmail();
   }
addFieldValue() {

if (this.inputEmail.invalid) {
  this.inputEmail.markAsDirty();
  this.inputEmail.markAsTouched();

} else {
    this.addEmail();

    this.Emails.at(this.Emails.length - 1).setValue(this.inputEmail.value);
    

    this.inputEmail.reset();
}
}
deleteFieldValue(index) {
  if (confirm('You are going to delete Email')) {
 this.Emails.removeAt(index);
}}
  ngOnInit() {
    this.setEmails();
    if(this.EmailFormGroup.disabled){

for (let control of (<FormArray>this.EmailFormGroup.get('email')).controls) {
  if (control instanceof FormControl) {
     control.disable()
  }
  if (control instanceof FormGroup) {
    control.disable()  
  }
  if (control instanceof FormArray) {
    control.disable()
  }

}

      
    }
  
  }

  disabledform(group: FormGroup): void {
    

    Object.keys(group.controls).forEach((key: string) => {
      const control = group.get(key);
      if (control instanceof FormGroup) {
        this.disabledform(control);

        control.disable()
       
      }
      else {
        control.disable()
      }
    });
  }
  
setEmails() {
  this.Emails = this.EmailFormGroup.get('email') as FormArray;
}
createEmail(): FormControl {
  return this.t_email.createEmail();
}
addEmail() {

  const x = this.t_email.createEmail();
   this.Emails.push(x);


  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {

      const chng = changes[propName];

      const cur  = chng.currentValue;
      const prev = chng.previousValue;
      if (propName == 'EmailFormGroup') {

        this.EmailFormGroup = cur;
        this.setEmails();


      }

    }
}}
