import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntityService {

  constructor(private http:HttpClient) { }
  getEntityById(id){
    let url=`${environment.projectUrl}/tEntities/${id}`
    return this.http.get(url)

  }
getSEntityByEntityNumber(entityNumber){
  let url=`${environment.projectUrl}/tEntitiesMap/${entityNumber}`
  return this.http.get(url)
}
  saveSEntityToSource(entity){
    let url=`${environment.projectUrl}/sEntities`
   return this.http.put(url,entity)
  }
 
}
