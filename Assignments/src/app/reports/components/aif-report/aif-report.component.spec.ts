import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AifReportComponent } from './aif-report.component';

describe('AifReportComponent', () => {
  let component: AifReportComponent;
  let fixture: ComponentFixture<AifReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AifReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AifReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
