import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth-service.service';
import { DialogOverviewExampleDialog } from 'src/app/component/dialog-overview-example/dialog-overview-example.component';
import { MatDialog } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService,
    private dialog: MatDialog) { }

  canActivate(routr, state: RouterStateSnapshot) {
    
    if (this.authService.isLoggedIn()) {
      if (localStorage.getItem('Report') != null || localStorage.getItem('ReportStr') != null|| localStorage.getItem('ReportAif') != null) {
            const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
              width: '250px',
            });
            dialogRef.afterClosed().subscribe(result => {
              
            });
            return true;
          } else {
            localStorage.removeItem('Report');
            localStorage.removeItem('ReportStr');
            localStorage.removeItem('ReportST');
            localStorage.removeItem('ReportAct');
            localStorage.removeItem('ReportAif')
            localStorage.removeItem('ReportAi')
            return true;
            // const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            // this.router.navigate([returnUrl || '/']);
          }
    
    }
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}