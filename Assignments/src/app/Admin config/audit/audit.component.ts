import { Subscription } from 'rxjs';


import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ReportService } from 'src/app/reports/services/report.service';



@Component({
  selector: 'app-audit',
  templateUrl: './audit.component.html',
  styleUrls: ['./audit.component.css']
})
export class AuditComponent implements OnInit, OnDestroy {

  subsribtion: Subscription;
  // isLoading: boolean;
  formdate: boolean = false;
  range: boolean = false;
  todate: boolean = false;
  myDate = new Date();



  constructor(private reportService: ReportService, private toastr: ToastrService) {

  }

  ngOnInit() {

  }


  auditForm = new FormGroup({
    fromDate: new FormControl('', Validators.required),
    toDate: new FormControl('', Validators.required)
  });


  getSourceAudit(event) {
    const fromDate: Date = event.fromDate
    const toDate: Date = event.toDate

    if (!fromDate)
      this.formdate = true;
    else if (!toDate)
      this.todate = true;
    else if (toDate < fromDate)
      this.range = true;
    else {
      // this.isLoading = true
      this.subsribtion = this.reportService.sourceAudit(fromDate, toDate).subscribe(response => {

        this.toastr.success('Excel file generated !')
        // this.isLoading = false
        this.formdate = false;
        this.todate = false;
        this.range = false;
      }, error => {
        this.toastr.error('Not generated !')
        // this.isLoading = false
      });
    }
  }


  ngOnDestroy() {
    if (this.subsribtion != undefined)
      this.subsribtion.unsubscribe();
  }

}
