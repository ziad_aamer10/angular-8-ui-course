import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { t_report } from '../../validationControls/t_report';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatMenu } from '@angular/material';
import { ReportService } from '../../services/report.service';
import { t_transaction } from '../../validationControls/t_transaction';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { UtilService } from 'src/app/util.service';
import { notValidElementWithMessageOnly } from '../../models/notValidElement';
import { Actions } from '../../Actions/action';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';
import { Subscription } from 'rxjs';
import { TransactionDetailsComponent } from '../transaction-details/transaction-details.component';




@Component({
  selector: 'app-str',
  templateUrl: './str.component.html',
  styleUrls: ['./str.component.css']
})
export class StrComponent implements OnInit, OnDestroy {
  @ViewChild('menu')menu:MatMenu
 
 
  // this view child for making reference that references transactiondetailscomponent to be able to change the status value in transactiondetailscomponent
  @ViewChild('transStat') transStat: TransactionDetailsComponent;

  closeCascadeTabSubscription: Subscription;
  disableButton: boolean = false
  closeAddNewTransactionTabSubscription: Subscription;
  confirmCloseReport: boolean = false;
  transaction: FormGroup;
  transactionControldd: t_transaction = new t_transaction(this.fb);
  strReportTransactionControl: FormGroup;
  parentArrayOfTransactions: FormArray;
  transactionIndexInTable: number;
  transactionArray: FormArray;
  t_report: t_report = new t_report(this.fb, this.reportservice);
  reportDetailControl: FormGroup;
  index = 0;
  addNewTabActive = false;
  addNewTabActiveForCascadeTab = false;
  createMode = true;
  CascadeTrans: FormArray;
  editMode: boolean;
  isRateLimitReached = false;
  dataSource: any = [];
  transactionArrayComingWithReport = [];
  timerid = null;
  validationIndicators: boolean = false;
  validationErrors
  validationErrorsLabels: notValidElementWithMessageOnly[] = []
  createReportAction: boolean = false
  editReportAction: boolean = false
  generateXMLAction: boolean = false
  SendToMakerAction: boolean = false;
  SendToCheckerAction: boolean = false;
  reOpenReportAction: boolean = false;
  MLCUAcknowledgeAction: boolean = false;
  ReturnToCheckerAction: boolean = false;
  ReturnToMakerAction: boolean = false;
  ReportStatus: string = '';
  SendToAmlHeadAction: boolean;
  closeReportAction: boolean;
  SubmitAction: boolean;
  REOPENReportAction: boolean = false;
  ReturnToMakerFromManagerAction: boolean;
  routeReportAction: boolean;
  AIFRequestAction: boolean;
  ActionButton:boolean=false
  sendToManagerFromMakerAction: boolean;


  constructor(private _router: Router, public dialogActivity: MatDialog, private reportservice: ReportService
    , private fb: FormBuilder, private route: ActivatedRoute, private toaster: ToastrService, private dataService: SharingDataServieService,
    private util: UtilService, private auth: AuthService) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      
      const id = params.id;
      if (id) {
        
        // this.timerid = setInterval(() => this.TimeInterval(), 10000);
        this.createMode = false;
        // console.log("localStorage.getItem('ReportStr')", localStorage.getItem('ReportStr'));

        if (localStorage.getItem('ReportStr') == null) {
          this.reportservice.getReportById(id).subscribe(data => {
            
            this.reportDetailControl = this.t_report.setReport1(data);
            this.timerid = setInterval(() => this.TimeInterval(), 10000);

            this.parentArrayOfTransactions = this.reportDetailControl.get('transaction') as FormArray;
            this.ReportStatus = this.reportDetailControl.get('reportStatusCode').value

            
            // console.log('json array of transactions',JSON.stringify(this.parentArrayOfTransactions.value));
            if (this.reportDetailControl.get('transaction').value == null) {
              
              this.reportDetailControl.removeControl('transaction');
              this.reportDetailControl.addControl('transaction', this.t_report.transaction.createTransactionControl());
            }
            // 
            console.log('locked by', this.reportDetailControl.get('reportUserLockId').value);

            console.log("ffffffffffffffffffffffffff");
            
            //Disabled Forms
            let reportUserLockId = this.reportDetailControl.get('reportUserLockId').value
            console.log("reportUserLockId=========", reportUserLockId);
            
            let userName = localStorage.getItem('name') 
            console.log("usernamer=========", userName);
            

            console.log("",userName != reportUserLockId && reportUserLockId != null);

            if (userName == reportUserLockId || reportUserLockId == null) {
              console.log("iffffffffffff");
              
              this.setStaticValue();
            }

            

            else if (reportUserLockId) {
              console.log("elseeeeeeeeeee");
              
              this.reportDetailControl.disable()
              this.disableButton = true
            }
          },
            error => {
              
            });
        }
        else {
          const report = JSON.parse(localStorage.getItem('ReportStr'))
          this.reportservice.processIndicators(report)
          this.reportDetailControl = this.t_report.setReport1(report);
          this.setStaticValue();
          this.parentArrayOfTransactions = this.reportDetailControl.get('transaction') as FormArray;
          this.ReportStatus = this.reportDetailControl.get('reportStatusCode').value

          if (localStorage.getItem('ReportST') != null) {
            this.index = 2;
          }
        }
      } else {
        
        
        // this.timerid = setInterval(() => this.TimeInterval(), 10000);
        this.transactionIndexInTable = -1;
        this.reportDetailControl = this.t_report.createStrReport();
        this.setStaticValue();
        this.parentArrayOfTransactions = this.reportDetailControl.get('transaction') as FormArray;
        this.reportDetailControl.controls.reportCreatedBy.setValue(localStorage.getItem('name'));



      }
      // this.timerid = setInterval(() => this.TimeInterval(), 10000);
    });
    const x: string = new Date().toLocaleString();


    this.closeAddNewTransactionTabSubscription = this.dataService.currentMessage.subscribe(d => {
      if (d != null) {
        this.addNewTabActive = false;
        this.index = 2;
      }
    })

    this.closeCascadeTabSubscription = this.dataService.currentMessage2.subscribe(
      msg => {
        if (msg != null) {
          this.addNewTabActiveForCascadeTab = false;
          this.index = 2;
        }
      }
    )

    this.setActions()
  }
  async ngOnDestroy() {
    this.unLock();
    this.closeCascadeTabSubscription.unsubscribe();

    localStorage.removeItem('ReportStr')
    localStorage.removeItem('ReportST')
    clearInterval(this.timerid)
    await this.delay(500);

  }
  ReportPreview() {
    this.updateStrReport()
    this.reportservice.PreviewReport(this.reportDetailControl.get('id').value).subscribe((data: any) => {

      // var blob = new Blob([data], { type: "application/pdf" });
      // if (blob) {
      //   if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      //     window.navigator.msSaveOrOpenBlob(blob);
      //     // return;
      //     // var fileURL = window.URL.createObjectURL(blob);
      //     // window.open(fileURL);
      //   }
      // var fileURL = URL.createObjectURL(data);
      let file = new Blob([data], { type: "application/pdf;charset=utf-8" });
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL, '_blank');
      // window.open(fileURL, '_blank');
      // }
      // const fileURL = URL.createObjectURL(data);
      // window.open(fileURL, '_blank');
    });
    // window.open(this.reportservice.PreviewReport(this.reportDetailControl.get('id').subscribe((data: any) => {

    //   }));

  }

  unLock() {
    const userName = localStorage.getItem('name');
    let reportUserLockId = this.reportDetailControl.get('reportUserLockId').value
    if(reportUserLockId==userName){
    this.route.params.subscribe(params => {
      const id = params.id;
      this.reportservice.unLock(id, userName).subscribe(data => {
            
      }, error => { });
    });

  }else
  console.log('unLock Not Allowed! ');
  
  }
  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  accept(event) {
    
    this.addNewTabActive = true;
    this.index = 3;
    this.strReportTransactionControl = event;
    this.transactionIndexInTable = -1;
  }
  TimeInterval(): void {
    // 
    localStorage.setItem('ReportStr', JSON.stringify(this.reportDetailControl.value));

  }
  setStaticValue() {
    // 
    // 
    this.reportDetailControl.controls.reportCode.setValue('STR');
    this.reportDetailControl.controls.reportingPersonType.setValue('AML Head');
    this.reportDetailControl.controls.currencyCodeLocal.setValue('EGP');
    this.reportDetailControl.controls.rentityBranch.setValue(environment.entity_branch);
    this.reportDetailControl.controls.submissionCode.setValue('E');

    // this.reportDetailControl.controls.reportCode.disable();
    this.reportDetailControl.controls.submissionCode.disable();
    this.reportDetailControl.controls.submissionDate.disable();
    this.reportDetailControl.controls.rentityBranch.disable();
    this.reportDetailControl.controls.currencyCodeLocal.disable();
    this.reportDetailControl.controls.entityReference.disable();


  }
  // disabledform(group:FormGroup):void{
  //   Object.keys(group.controls).forEach((key:string)=>{
  //   const abstractControl=group.get(key);
  //   if(abstractControl instanceof FormGroup){
  //     this.disabledform(abstractControl);
  //   }else{
  //     abstractControl.disable();
  //   }
  //   });
  //     }


  saveStrReport() {
    

    clearInterval(this.timerid);
    localStorage.removeItem('ReportStr');
    localStorage.removeItem('ReportST');
    this.createMode = true;
    this.parentArrayOfTransactions = this.reportDetailControl.get('transaction').value;
    this.reportservice.saveSarReport(this.reportDetailControl.value).subscribe(value => {
      

      this.toaster.success('Report saved successfully');
      this.confirmCloseReport = true;
      
      

    }, (error) => {
      this.toaster.error(error.message);
    }
    );
  }

  updateStrReport() {
    
    clearInterval(this.timerid);
    localStorage.removeItem('ReportStr');
    localStorage.removeItem('ReportST');
    this.editMode = true;
    // this.parentArrayOfTransactions = this.reportDetailControl.get('transaction').value;
    this.reportservice.updateSarReport(this.reportDetailControl.value).subscribe(value => {
      this.toaster.success('Report updated successfully');
      
    }, (error) => {
      this.toaster.error(error.message);
    }
    );
  }

  acceptTrasactionAddedFromChild(result) {
    this.addNewTabActive = false;
    this.index = 2;
    if (this.transactionIndexInTable != -1) {

      this.strReportTransactionControl = this.transactionControldd.setTransaction(result.value);
      this.parentArrayOfTransactions.removeAt(this.transactionIndexInTable);
      this.parentArrayOfTransactions.insert(this.transactionIndexInTable, result);
      
    }
  }

  acceptNewArrayAfterUpdate(event) {
    this.parentArrayOfTransactions = event;
  }

  editTransactionEvent(event) {
    this.transactionIndexInTable = event.transactionIndexInTable;
    this.addNewTabActive = true;
    this.index = 3;
    this.strReportTransactionControl = this.transactionControldd.setTransaction(event.transactionFormGroup.value);
    

    this.strReportTransactionControl.markAsDirty();
    this.strReportTransactionControl.markAsTouched();
  }

  acceptIncomingCascadedTransactions(event: FormArray) {
    this.CascadeTrans = event;
    
    this.addNewTabActiveForCascadeTab = true;
    this.index = 5;
  }

  closeAddNewSingleActivity() {
    if (confirm('You will lose information')) {
      this.addNewTabActive = false;
      this.index = 2;
      this.transStat.status = 0;

    }
  }

  closeCascadeActivity() {
    if (confirm('Confirm Close Cascade')) {
      this.addNewTabActiveForCascadeTab = false;
      this.index = 2;
      this.transStat.status = 0;
    }
  }



  closeValidationTab(event?) {
    // this.dataService.saveValidationInTransactionTable.emit('refreshTable')
    this.validationIndicators = false;
    this.index = 2;
  }
  validateReport() {

    this.util.changeReportAsRead(true);
    const resultc = [];
    let NestedSectionsErrors: notValidElementWithMessageOnly[] = []

    this.util.validateFormGroup2(this.reportDetailControl, resultc, NestedSectionsErrors);
    
    let resultc2: notValidElementWithMessageOnly[] = [];
    resultc2 = [...NestedSectionsErrors]
    const transactionsControl = this.reportDetailControl.get('transaction') as FormArray;
    if (transactionsControl) {

      transactionsControl.controls.forEach((tranControl, index) => {
        if (this.util.checkTransactionFrom(tranControl as FormGroup)) {
          const fromNotFound: notValidElementWithMessageOnly = { message: 'Select Transaction From Type', name: `transaction ${index}` };

          resultc2.push(fromNotFound);
        }


        if (this.util.checkTransactionTo(tranControl as FormGroup)) {
          const toNotFound: notValidElementWithMessageOnly = { message: 'Select Transaction To Type', name: `transaction ${index}` };

          resultc2.push(toNotFound);
        }

        let signatories: FormArray = tranControl.get('tFromMyClient.fromAccount.signatory') as FormArray
        let isPrimarySignatory: boolean = false
        if (signatories) {
          for (let signatory of signatories.controls) {
            if (signatory.get('isPrimary').value) {
              isPrimarySignatory = true
              break;
            }
          }
          if (!isPrimarySignatory) {
            resultc2.push({ message: "Add at least one primary signatory ", name: `transaction.${index}.tFromMyClient.fromAccount.signatory` })
          }
        }

        // UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromPerson.addresses.address`,
        //   'Add At least one Address', `transaction.${index}.tFromMyClient.fromPerson.addresses.address`, resultc2);
        // UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromPerson.phones.phone`,
        //   'Add At least one Phone', `transaction.${index}.tFromMyClient.fromPerson.phones.phone`, resultc2);
        // UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromPerson.email`,
        //   'Add At least one Email', `transaction.${index}.tFromMyClient.fromPerson.email`, resultc2);
        // UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromPerson.identification`,
        //   'Add At least one Identification', `transaction.${index}.tFromMyClient.fromPerson.identification`, resultc2);

        // UtilService.FindAndFormMessage(tranControl, `tToMyClient.toPerson.addresses.address`,
        //   'Add At least one Address', `transaction.${index}.tToMyClient.toPerson.addresses.address`, resultc2);
        // UtilService.FindAndFormMessage(tranControl, `tToMyClient.toPerson.phones.phone`,
        //   'Add At least one Phone', `transaction.${index}.tToMyClient.toPerson.phones.phone`, resultc2);
        // UtilService.FindAndFormMessage(tranControl, `tToMyClient.toPerson.email`,
        //   'Add At least one Email', `transaction.${index}.tToMyClient.toPerson.email`, resultc2);
        // UtilService.FindAndFormMessage(tranControl, `tToMyClient.toPerson.identification`,
        //   'Add At least one Identification', `transaction.${index}.tMyClient.toPerson.identification`, resultc2);

        // UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromEntity.addresses.address`,
        //   'Add At least one Address', `transaction.${index}.tFromMyClient.fromEntity.addresses.address`, resultc2);
        // UtilService.FindAndFormMessage(tranControl, `tFromMyClient.fromEntity.phones.phone`,
        //   'Add At least one Phone', `transaction.${index}.tFromMyClient.fromEntity.phones.phone`, resultc2);


      });


    }
    this.validationErrorsLabels = resultc2;
    this.validationErrors = resultc;
    for (const x of resultc) {
      this.util.getOptionsFromLockup(x);
    }

    if (resultc.length > 0 || resultc2.length > 0) {

      this.validationIndicators = true;
      this.index = 7;
    } else if (this.reportDetailControl.valid) {
      this.toaster.warning('No Validation Errors Found!!');
    }

  }
  GenerateXML() {

    if (this.reportDetailControl.valid) {

      this.reportservice.GenerateXMLAndSaveReport(this.reportDetailControl.getRawValue()).subscribe((data: any) => {
        if (data) {
          this.toaster.success('Generate xml successfully Str report');
          this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode)
          this.ReportStatus = data.reportStatusCode
          this.closeReportPage()

        }
        else {
          this.toaster.warning("An error has occured")
        }

      },
        error => {
          this.toaster.error(error.message);

        });
    } else {
      this.toaster.warning('Please validate report before XML Generation');
    }
  }






  closeReportPage() {
    clearInterval(this.timerid);
    localStorage.removeItem('Report');
    localStorage.removeItem('ReportAct');
    this._router.navigate(["reports"])


  }
  /////

  SendToChecker() {
    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid')
      return
    }
    let report = Object.assign({}, this.reportDetailControl.getRawValue())
    this.reportservice.SendToChecker(report).subscribe((data: any) => {
      if (data) {

        this.toaster.success("SendToChecker Successfully")
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode
        this.closeReportPageAndConfirmClose()

      }
      else {
        this.toaster.warning("An error has occured")
      }
    },
      error => {
        

        this.toaster.error(error.error.message);
      });
  }
  ReturnToMaker() {
    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid')
      return
    }
    if (!confirm("Return To Maker ")) return

    let report = Object.assign({}, this.reportDetailControl.getRawValue())
    this.reportservice.ReturnToMaker(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success("Send To Maker Successfully")
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode)
        this.ReportStatus = data.reportStatusCode
        this.closeReportPageAndConfirmClose()
      }
      else {
        this.toaster.warning("An error has occured")
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  ReturnToChecker() {
    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid')
      return
    }
    if (!confirm("Return To Checker ")) return

    let report = Object.assign({}, this.reportDetailControl.getRawValue())
    this.reportservice.ReturnToChecker(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success("Return To Checker Successfully")
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode)

        this.ReportStatus = data.reportStatusCode
        this.closeReportPageAndConfirmClose()
      }
      else {
        this.toaster.warning("An error has occured")
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  reOpenReport() {
    if (!confirm("Re Open Report ")) return

    let reportid = this.reportDetailControl.get('id').value
    this.reportservice.reOpenReport(reportid).subscribe((data: any) => {

      if (data) {
        this.toaster.success("Report re-opened Successfully")
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode)
        this.ReportStatus = data.reportStatusCode
        this.closeReportPageAndConfirmClose()

      }
      else {
        this.toaster.warning("An error has occured")
      }

    },
      error => {
        

        this.toaster.error(error.error.message)


      })
  }
  MLCUAcknowledge() {
    if (!confirm("MLCUAcknowledge Report ")) return

    let reportid = this.reportDetailControl.get('id').value
    this.reportservice.MLCUAcknowledge(reportid).subscribe((data: any) => {

      if (data) {
        this.toaster.success("Report MLCU acknowledge Successfully")
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode)
        this.ReportStatus = data.reportStatusCode
        this.closeReportPageAndConfirmClose()

      }
      else {
        this.toaster.warning("An error has occured")
      }

    },
      error => {
        

        this.toaster.error(error.error.message)


      })
  }
  SendToAMLHead() {
    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid')
      return
    }
    if (!confirm("MLCUAcknowledge Report ")) return

    let report = Object.assign({}, this.reportDetailControl.getRawValue())
    this.reportservice.sendToAMLHead(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success("Send To AML HEAD Successfully")
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode)
        this.ReportStatus = data.reportStatusCode
        this.closeReportPageAndConfirmClose()

      }
      else {
        this.toaster.warning("An error has occured")
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }


  Submit() {
    if (!confirm("Submit Report ")) return

    let reportid = this.reportDetailControl.get('id').value

    this.reportservice.submitReport(reportid).subscribe((data: any) => {

      if (data) {
        this.toaster.success("Report is Submitted Successfully")
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode)
        this.ReportStatus = data.reportStatusCode
        this.closeReportPageAndConfirmClose()

      }
      else {
        this.toaster.warning("An error has occured")
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });

  }


  isReportValid(): boolean {
    return this.reportDetailControl.valid
  }
  closeReport() {
    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid')
      return
    }
    if (!confirm("Close Report ")) return

    let report = Object.assign({}, this.reportDetailControl.getRawValue())
    this.reportservice.closeReport(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success("Report is Closed Successfully")
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode)
        this.ReportStatus = data.reportStatusCode
        this.closeReportPageAndConfirmClose()

      }
      else {
        this.toaster.warning("An error has occured")
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });

  }


  //
  get _ReturnToCheckerAction() {
    return this.ReturnToCheckerAction && (['WaitingHeadDecision'].includes(this.ReportStatus))
  }
  get _ReturnToMakerAction() {
    return this.ReturnToMakerAction && (['CHECKING', 'ReturnToChecker', 'MLCUModification', 'REOPEN'].includes(this.ReportStatus))

  }
  get _SendToCheckerAction() {
    return this.SendToCheckerAction && (['MAKING', 'ReturnToMaker', 'MLCUModification', 'REOPEN'].includes(this.ReportStatus))
  }
  get _SendToAMLHEADAction() {
    return this.SendToAmlHeadAction && (['CHECKING', 'ReturnToChecker'].includes(this.ReportStatus))
  }
  get _reOpenReportAction() {
    return this.REOPENReportAction && (['CLOSED', 'SUBMITTED'].includes(this.ReportStatus))
  }
  get _closeReportAction() {
    return this.closeReportAction && (['WaitingHeadDecision']).includes(this.ReportStatus)
  }
  get _ReturnToMakerActionManager() {
    return this.ReturnToMakerFromManagerAction && (['WaitingHeadDecision','REOPEN'].includes(this.ReportStatus))

  }
  get _generateXMLAction() {
    return this.generateXMLAction && (['WaitingHeadDecision'].includes(this.ReportStatus))

  }
  get _MLCUAcknowledgeAction() {
    return this.MLCUAcknowledgeAction && (['XMLGenerated']).includes(this.ReportStatus)
  }
  get _SubmitAction() {
    return this.SubmitAction && (['XMLGenerated']).includes(this.ReportStatus)

  }
  setActions() {
    this.createReportAction = this.auth.has_Capabilities(Actions.createReport)
    this.editReportAction = this.auth.has_Capabilities(Actions.editReport)
    this.generateXMLAction = this.auth.has_Capabilities(Actions.generateXML)
    this.SendToMakerAction = this.auth.has_Capabilities(Actions.sendToMaker)
    this.SendToCheckerAction = this.auth.has_Capabilities(Actions.sendTochecker)
    this.REOPENReportAction = this.auth.has_Capabilities(Actions.reOpenReport)
    this.MLCUAcknowledgeAction = this.auth.has_Capabilities(Actions.MLCUAcknowledgeAction)
    this.ReturnToCheckerAction = this.auth.has_Capabilities(Actions.returnToChecker)
    this.ReturnToMakerAction = this.auth.has_Capabilities(Actions.returnToMaker)
    this.ReturnToMakerFromManagerAction = this.auth.has_Capabilities(Actions.returnToMakerFromManager)
    this.SendToAmlHeadAction = this.auth.has_Capabilities(Actions.sendToAMLHead)
    this.closeReportAction = this.auth.has_Capabilities(Actions.closeReport)
    this.SubmitAction = this.auth.has_Capabilities(Actions.submitReport)
    this.routeReportAction = this.auth.has_Capabilities(Actions.routeReport)
    this.AIFRequestAction = this.auth.has_Capabilities(Actions.AIFRequest)
this.sendToManagerFromMakerAction=this.auth.has_Capabilities(Actions.sendToManagerFromMaker)

  }



  closeReportPageAndConfirmClose() {
    this.setConfirmCloseReport(true)
    this.closeReportPage()
  }
  setConfirmCloseReport(confirm: boolean) {
    this.confirmCloseReport = confirm

  }
  makeAIfRequest() {
    let StrReport = Object.assign({}, this.reportDetailControl.getRawValue())
    this.reportservice.makeAIFRequest(StrReport).subscribe((report: any) => {
      if (report) {
        let AifReportId = report.id
        this.setConfirmCloseReport(true)
        this._router.navigate([environment.aifEditRoute, AifReportId])
      }
      else {
        this.toaster.error("No Returned Data")
      }
    }, error => this.errorFunction(error))
  }
  get _AIFRequestAction() {

    return this.AIFRequestAction && (['XMLGenerated']).includes(this.ReportStatus)
  }
  errorFunction(error) {
    this.toaster.error(error.error.message)
  }
get _ButtonAction(){
 return this._AIFRequestAction||this._MLCUAcknowledgeAction||
  this._ReturnToCheckerAction||this._ReturnToMakerAction
  ||this._ReturnToMakerActionManager||this._SendToAMLHEADAction||this._SendToCheckerAction
  ||this._SubmitAction||this._closeReportAction||this._generateXMLAction||this._reOpenReportAction
    
}
get _SendToAMLHEADActionFromMaker(){
  return this.sendToManagerFromMakerAction&& (['MAKING', 'ReturnToMaker'].includes(this.ReportStatus))
}
}

