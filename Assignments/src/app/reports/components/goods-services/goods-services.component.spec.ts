import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodsServicesComponent } from './goods-services.component';

describe('GoodsServicesComponent', () => {
  let component: GoodsServicesComponent;
  let fixture: ComponentFixture<GoodsServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoodsServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoodsServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
