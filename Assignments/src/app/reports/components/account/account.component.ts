import { account } from './../../models/account.model';
import { Subscription } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, OnDestroy } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatTableDataSource, MatCheckbox } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { LookupClass } from '../../lockups/Lookups';
import { EntityForm } from '../../models/entityFormGroup';
import { AccountService } from '../../services/account.service';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { t_account } from '../../validationControls/t_account';
import { tEntity } from '../../validationControls/t_entity';
import { SignatoryComponent } from '../signatory/signatory.component';
import { UtilService } from 'src/app/util.service';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';
import { Actions } from '../../Actions/action';
import { t_signatory } from '../../validationControls/t_signatory';
import { t_entity_my_client } from '../../validationControls/t_entity_my_client';




@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit, OnChanges ,OnDestroy{
  falseValue = null
  trueValue = true;
  
  checkboxChange(checkbox: MatCheckbox, checked: boolean) {
    console.log('checkboxChange');
    if( !checked ){
      this.accountForm.controls.nonBankInstitution.setValue(null)
    }
  }
  subscriptionAccountById:Subscription;
  subscriptionSaveAccountToSource:Subscription;
  subscriptionAccountByNumber:Subscription;
  @Input() accountType?: string

  // added by abdelrahman to assign values from lookups
  Currency_values = LookupClass.Currency;
  acctype_values = LookupClass.account_type;
  accstatus_values = LookupClass.account_status_type;

  party_type = '';
  addEntityDetail: boolean;
  signatoriesControl: FormArray;
  @Input() disableAccount: boolean;
  @Input() accountForm: FormGroup;
  @Output() saveEvent = new EventEmitter();
  @Output() cancelEvent = new EventEmitter();
  @Output() changeAccount = new EventEmitter();
  personFormControl: FormGroup;
  entityFormControl: FormGroup;
  entityForm = new EntityForm();
  tEntity: tEntity = new tEntity();
  tEntityMyClient: t_entity_my_client = new t_entity_my_client();
  t_account: t_account = new t_account();
  displayedColumns: string[] = ['select', 'isPrimary', 'personReference', 'role', 'delete'];
  t_signatory: t_signatory = new t_signatory();

  dataSource = new MatTableDataSource([]);
  selection = new SelectionModel(true, []);
  accountNumber: string = null;
  cascadeAccountAction: boolean = false;
  viewSignatoryAction: boolean = false;
  deleteSignatoryAction: boolean = false;
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  
  masterToggle() {

    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }
  constructor(public dialog: MatDialog, public accountService: AccountService,
    private toaster: ToastrService, private dataService: SharingDataServieService, private util: UtilService, private auth: AuthService) { }

  ngOnInit() {
    this.setControls();
    this.setActions()
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      const chng = changes[propName];
      const cur = chng.currentValue;
      if (propName == 'accountForm') {


        this.accountForm = cur;
        this.setControls();
      }
    }

  }
  setControls() {
    if (this.accountForm.get('tEntity') && this.accountForm.get('tEntity').value) {
      this.addEntityDetail = true;
    } else {
      this.addEntityDetail = false;
    }
    if (this.addEntityDetail) {
      this.entityFormControl = this.accountForm.get('tEntity') as FormGroup;
    } else if (this.IsMyAccount()) {
      this.entityFormControl = this.tEntityMyClient.createEntityControl()

    }
    else {
      this.entityFormControl = this.tEntity.createEntityControl();
    }

    this.signatoriesControl = this.accountForm.get('signatory') as FormArray;


    this.dataSource.data = this.signatoriesControl.value;
    this.accountForm.get('account').valueChanges.subscribe(data => {
      this.accountNumber = data;
    });
    this.changeAccount.emit(this.accountForm);
  }
  openSignatoryDialog() {
    const accountNum = this.accountForm.get('account').value;
    let SignatoryControl: FormGroup
    if (this.IsMyAccount()) {
      SignatoryControl = this.t_signatory.create_MysignatoryControl()
    }
    else {
      SignatoryControl = this.t_signatory.create_signatoryControl()

    }
    const dialogRef = this.dialog.open(SignatoryComponent, {
      width: '1200',
      height: '800px',

      data: { 'accountNumber': accountNum, 'signatoryControl': SignatoryControl, 'IsMyAccount': this.IsMyAccount() }
    });

    dialogRef.afterClosed().subscribe(result => {
      result !== undefined? this.signatoriesControl.push(result):null;

    });
  }
  save() {
    this.saveEvent.emit(this.accountForm.value);
  }
  cancel() {
    this.cancelEvent.emit();
  }
  get f() {
    return this.accountForm.controls;
  }
  editSignatory(index) {
    const signatoryToEdit = this.signatoriesControl.at(index) as FormGroup;
    const accountNumber = this.accountForm.get('account').value;
    const dialogRef = this.dialog.open(SignatoryComponent, {
      width: '1200px',
      height: '800px',

      data: { 'signatoryControl': signatoryToEdit, 'accountNumber': accountNumber, 'IsMyAccount': this.IsMyAccount() }
    });

    dialogRef.afterClosed().subscribe(result => {




      if (result != undefined && result instanceof FormGroup) {

        this.signatoriesControl.removeAt(index);

        this.signatoriesControl.insert(index, result);



      }


    });


  }
  changeEntityDetail(checkbox: MatCheckbox, checked: boolean) {
    this.addEntityDetail=checked
    this.accountForm.get('isEntity').setValue(this.addEntityDetail);
    if (this.addEntityDetail) {

      this.accountForm.setControl('tEntity', this.entityFormControl);

    } else {

      this.accountForm.setControl('tEntity', new FormControl());

    }
  }
  getAccountById() {
    const id = this.accountForm.get('account').value;
    this.subscriptionAccountById = this.accountService.findAccountById(id).subscribe(data => {
      if (data) {
        this.toaster.success(`Account with Id ${id} is found`);

        this.accountForm = this.t_account.setAccountControl(data);
        this.setControls();


      } else {
        this.toaster.error(`Account with Id ${id} is not found`);
      }
    },
      error => {
        this.toaster.error(error.message);
      });
  }
  saveAccount() {
    const accountToBeSaved = Object.assign({}, this.accountForm.value);

    this.util.changeDateFormat(accountToBeSaved)
    if(accountToBeSaved){
   
      // this.saveEvent.emit(accountToBeSaved.account);
      this.saveEvent.emit(accountToBeSaved);
      console.log("accountTobeSaved ===" ,accountToBeSaved );
      
      // accountToBeSaved.signatory = null;
      if (this.accountForm.valid) {
        this.subscriptionSaveAccountToSource = this.accountService.saveAccountToSource(accountToBeSaved).subscribe(data => {
          if (data) {



            this.toaster.success('Account saved successfully');
          } else {
            this.toaster.error('Error in Account save');
          }
        },
          error => {
            this.toaster.error('An error occured');


          });
      }
      else {
        this.util.validateFormGroup2(this.accountForm, [], [])
        this.toaster.error("Account not Valid!!")
      }
    }


  }
  getAccountByAccountNumber() {
    const accountNumber = this.accountForm.get('account').value;
    this.subscriptionAccountByNumber = this.accountService.getAccountByAccountNumber(accountNumber).subscribe(data => {
      if (data) {
        console.log("account coming from source === ", data);
        
        this.toaster.success(`Account with Id ${accountNumber} is found`);

        this.accountForm = this.t_account.setAccountControl(data);
        this.setControls();
      } else {
        this.toaster.error(`Account with Id ${accountNumber} is not found`);
      }

    },
      () => {
        this.toaster.error(`Account with Id ${accountNumber} is not found`);
      });
  }
  deleteSignatory(index) {
    if (confirm('Are you sure you want to delete signatory')) {

      this.signatoriesControl.removeAt(index);
    }
  }
  changeAccountEntity(event) {
    this.accountForm.removeControl('tEntity');
    this.accountForm.addControl('tEntity', event);

  }
  setActions() {
    this.cascadeAccountAction = this.auth.has_Capabilities(Actions.cascadeAccount)
    this.viewSignatoryAction = this.auth.has_Capabilities(Actions.viewSignatory)
    this.deleteSignatoryAction = this.auth.has_Capabilities(Actions.deleteSignatory)
  }
  IsMyAccount(): boolean {

    return this.accountType ? true : false
  }
  get _getEntityAccountType() {
    return this.accountType ? 'myEntity' : null
  }

  ngOnDestroy(){
    if(this.subscriptionAccountById != undefined)
      this.subscriptionAccountById.unsubscribe();
    if(this.subscriptionSaveAccountToSource != undefined)
      this.subscriptionSaveAccountToSource.unsubscribe();
    if(this.subscriptionAccountByNumber != undefined)
      this.subscriptionAccountByNumber.unsubscribe();
  }
}
