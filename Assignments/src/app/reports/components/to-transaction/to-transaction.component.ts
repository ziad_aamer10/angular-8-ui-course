import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { EntityForm } from '../../models/entityFormGroup';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AccountForm } from '../../models/accountFormGroup';
import { LookupClass } from '../../lockups/Lookups';
import { t_person_my_client } from '../../validationControls/t_person_my_client';
import { t_account } from '../../validationControls/t_account';
import { t_person } from '../../validationControls/t_person';
import { t_entity_my_client } from '../../validationControls/t_entity_my_client';
import { t_account_my_client } from '../../validationControls/t_account_my_client';
import { tEntity } from '../../validationControls/t_entity';
import { t_to } from '../../validationControls/t_to';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { ToastrService } from 'ngx-toastr';

export interface Codes {
  key: string;
  value: string;
}
@Component({
  selector: 'app-to-transaction',
  templateUrl: './to-transaction.component.html',
  styleUrls: ['./to-transaction.component.css']
})


export class ToTransactionComponent implements OnInit, OnChanges {

  
  oldcontrol: FormGroup;
  To_type: string = ''
  t_toControl: FormGroup;
  @Input() cascade:boolean;
  @Input() cascadeMe;
  @Input() transactionControl: FormGroup
  @Output() emitToToCascade = new EventEmitter();
  @Output() emitPartyChange = new EventEmitter();
  types: string[] = []
  t_to: t_to = new t_to(this.fb);
  t_person: t_person = new t_person()
  t_entity: tEntity = new tEntity()
  t_account: t_account = new t_account()
  t_person_my_client: t_person_my_client = new t_person_my_client()
  t_entity_my_client: t_entity_my_client = new t_entity_my_client()
  t_account_my_client: t_account_my_client = new t_account_my_client()
  personFormControl_other: FormGroup
  personFormControl: FormGroup
  entityFormControl_other: FormGroup
  entityFormControl: FormGroup
  accountFormControl: FormGroup
  accountFormControl_other: FormGroup
  toForeignCurrency: FormGroup;
  isEgyptianPound: boolean = false

  // added by abdelrahman to assign values from lookups
  currencyCodes = LookupClass.Currency;
  fundCodes = LookupClass.funds_type;
  country_values = LookupClass.country

  constructor(private fb: FormBuilder, private dataService: SharingDataServieService, private toastr: ToastrService) { }
  setToControl() {

    if (this.transactionControl.get('tTo').value) {
      this.t_toControl = this.transactionControl.get('tTo') as FormGroup
      if (this.t_toControl.get('toAccount').value) {
        this.To_type = 'OtherClientAccount'
        this.accountFormControl_other = this.t_toControl.get('toAccount') as FormGroup
      }
      else if (this.t_toControl.get('toEntity').value) {
        this.To_type = 'OtherClientEntity'
        this.entityFormControl_other = this.t_toControl.get('toEntity') as FormGroup
      }
      else if (this.t_toControl.get('toPerson').value) {
        this.To_type = 'OtherClientPerson'
        this.personFormControl_other = this.t_toControl.get('toPerson') as FormGroup
      }
    }
    else if (this.transactionControl.get('tToMyClient').value) {
      this.t_toControl = this.transactionControl.get('tToMyClient') as FormGroup
      if (this.t_toControl.get('toAccount').value) {
        this.To_type = 'MyClientAccount'
        this.accountFormControl = this.t_toControl.get('toAccount') as FormGroup

      }
      else if (this.t_toControl.get('toEntity').value) {
        this.To_type = 'MyClientEntity'
        this.entityFormControl = this.t_toControl.get('toEntity') as FormGroup
      }
      else if (this.t_toControl.get('toPerson').value) {
        this.To_type = 'MyClientPerson'
        this.personFormControl = this.t_toControl.get('toPerson') as FormGroup
      }
    }
    else {
      // here there is not any clicked transaction , this case == add new single transaction
      this.t_toControl = this.t_to.create_t_to_my_controller()
      this.setEmptyControls()
    }
    this.oldcontrol = this.t_to.set_t_to_my_controller(this.t_toControl.value)
    this.subscribeOnGeneralInfo();

  }
  ngOnInit() {

    // check if we want to edit any transaction by editing it :
    // 1- if the transaction we presed on comes  with tFtom(other from) value then check if its type is account or entity or person then open this type . 
    this.setToControl()
    this.isEgyptianPound = this.t_toControl.get('toForeignCurrency').get('foreignCurrencyCode').value == 'EGP' || false

  }
  save(element) {
    
    this.emitPartyChange.emit(element);


  }
  close() { }

  setOldFromGeneralInfo(xObject) {
    this.t_toControl.get('toFundsCode').setValue(xObject.toFundsCode);
    this.t_toControl.get('toCountry').setValue(xObject.toCountry);
    this.t_toControl.get('toFundsComment').setValue(xObject.toFundsComment);
    // this.t_toControl.get('toForeignCurrency').setValue(xObject.fromForeignCurrency);
    this.t_toControl.get('toForeignCurrency').get('foreignCurrencyCode').setValue(xObject.foreignCurrencyCode);
    this.t_toControl.get('toForeignCurrency').get('foreignAmount').setValue(xObject.foreignAmount);
    this.t_toControl.get('toForeignCurrency').get('foreignExchangeRate').setValue(xObject.foreignExchangeRate);
    this.subscribeOnGeneralInfo()
  }

  subscribeOnGeneralInfo() {
    let list = ['toFundsComment', 'toFundsCode', "toCountry"];
    let list2 = ['foreignCurrencyCode', 'foreignAmount', 'foreignExchangeRate'];
    list.forEach(element => {
      this.t_toControl.get(element).valueChanges.subscribe(data => {
        this.oldcontrol.get(element).setValue(data)
      })
    })
    list2.forEach(element => {
      this.t_toControl.get('toForeignCurrency').get(element).valueChanges.subscribe(data => {
        this.oldcontrol.get('toForeignCurrency').get(element).setValue(data)
      })
    })
  }


  // to select type and switch between my client and other client
  changeToType() {
    let toFundsComment = this.oldcontrol.get('toFundsComment').value
    let toFundsCode = this.oldcontrol.get('toFundsCode').value;
    let toCountry = this.oldcontrol.get('toCountry').value;
    let toForeignCurrency = this.oldcontrol.get('toForeignCurrency').value;
    let foreignCurrencyCode = this.oldcontrol.get('toForeignCurrency').get('foreignCurrencyCode').value;
    let foreignAmount = this.oldcontrol.get('toForeignCurrency').get('foreignAmount').value;
    let foreignExchangeRate = this.oldcontrol.get('toForeignCurrency').get('foreignExchangeRate').value;
    let xObject = { toFundsComment, toFundsCode, toCountry, toForeignCurrency, foreignCurrencyCode, foreignAmount, foreignExchangeRate }

    this.t_toControl = this.t_to.create_t_to_my_controller()
    this.transactionControl.removeControl("tTo")
    this.transactionControl.removeControl("tToMyClient")
    this.transactionControl.addControl("tTo", new FormControl());
    this.transactionControl.addControl("tToMyClient", new FormControl())
    this.setEmptyControls()
    if (this.To_type === 'MyClientEntity') {

      this.t_toControl.setControl("toEntity", this.entityFormControl)

      this.transactionControl.setControl("tToMyClient", this.t_toControl)
    }
    else if (this.To_type === 'MyClientPerson') {


      this.t_toControl.setControl("toPerson", this.personFormControl);

      this.transactionControl.setControl("tToMyClient", this.t_toControl)
    }
    else if (this.To_type === 'MyClientAccount') {


      this.t_toControl.setControl("toAccount", this.accountFormControl);

      this.transactionControl.setControl("tToMyClient", this.t_toControl)
    }
    else if (this.To_type === 'OtherClientEntity') {


      this.t_toControl.setControl("toEntity", this.entityFormControl_other);

      this.transactionControl.setControl("tTo", this.t_toControl);
    }
    else if (this.To_type === 'OtherClientPerson') {
      ;

      this.t_toControl.setControl("toPerson", this.personFormControl_other);

      this.transactionControl.setControl("tTo", this.t_toControl);
    }
    else if (this.To_type === 'OtherClientAccount') {
      ;

      this.t_toControl.setControl("toAccount", this.accountFormControl_other);

      this.transactionControl.setControl("tTo", this.t_toControl);
    }
    else if (this.To_type === 'None') {
      this.setNullValuesWhenSelectingNone();
    }

    this.setOldFromGeneralInfo(xObject);

  }

  setNullValuesWhenSelectingNone() {
    this.t_toControl.setControl('toEntity', new FormControl());
    this.t_toControl.setControl('toPerson', new FormControl());
    this.t_toControl.setControl('toAccount', new FormControl());
    this.transactionControl.setControl('tTo', new FormControl());
    this.transactionControl.setControl('tToMyClient', new FormControl());
  }
  changeToControlFromSource(event, toType) {
    let tToMyClient = this.transactionControl.get("tToMyClient") as FormGroup
    let tTo = this.transactionControl.get("tTo") as FormGroup



    switch (toType) {

      case "myPerson": {
        tToMyClient.removeControl("toPerson")
        tToMyClient.addControl("toPerson", event)
        break;
      }
      case "otherPerson": {
        tTo.removeControl("toPerson")
        tTo.addControl("toPerson", event)
        break;
      }
      case "myAccount": {
        tToMyClient.removeControl("toAccount")
        tToMyClient.addControl("toAccount", event)
        break;
      }
      case "otherAccount": {
        tTo.removeControl("toAccount")
        tTo.addControl("toAccount", event)
        break;
      }
      case "myEntity": {
        tToMyClient.removeControl("toEntity")
        tToMyClient.addControl("toEntity", event)
        break;
      }
      case "otherEntity": {
        tTo.removeControl("toEntity")
        tTo.addControl("toEntity", event)
        break;
      }
    }



  }


  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let change = changes[propName];
      let curVal = change.currentValue;
      let prevVal = change.previousValue;
      if (propName == 'transactionControl') {

        this.transactionControl = curVal
        this.setToControl()
      }
    }
  }
  setEmptyControls() {
    this.personFormControl = this.t_person_my_client.createPersonGroup()
    this.personFormControl_other = this.t_person.createPersonControl()
    this.entityFormControl = this.t_entity_my_client.createEntityControl()
    this.accountFormControl_other = this.t_account.createAccountControl()
    this.accountFormControl = this.t_account_my_client.createAccountControl()
    this.entityFormControl_other = this.t_entity.createEntityControl()
  }

  sendToTocascade() {
    this.emitToToCascade.emit(this.transactionControl);
    this.dataService.changeMessage2('close cascade tab');
    this.toastr.success('Cadcade is done successfully');
  }
  changeCurrencyCode() {
    let fromForeignCurrency = this.t_toControl.get('toForeignCurrency')
    let foreignCurrencyCode = fromForeignCurrency.get('foreignCurrencyCode')
    let foreignAmount = fromForeignCurrency.get('foreignAmount')
    let foreignExchangeRate = fromForeignCurrency.get('foreignExchangeRate')
    this.isEgyptianPound = foreignCurrencyCode.value == 'EGP'||foreignCurrencyCode.value==null
    if (this.isEgyptianPound) {

      foreignAmount.reset()
      foreignExchangeRate.reset()
      foreignCurrencyCode.reset()
      fromForeignCurrency.clearValidators()
      foreignCurrencyCode.clearValidators()
      foreignAmount.clearValidators()
      foreignExchangeRate.clearValidators()
    }
    else {
      foreignCurrencyCode.setValidators(Validators.required)
      foreignAmount.setValidators(Validators.required)
      foreignExchangeRate.setValidators(Validators.required)
    }
    foreignCurrencyCode.updateValueAndValidity()
    foreignAmount.updateValueAndValidity()
    foreignExchangeRate.updateValueAndValidity()
  }
 
}