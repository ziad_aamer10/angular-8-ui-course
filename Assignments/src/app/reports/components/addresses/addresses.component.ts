import { Component, OnInit, Input, OnChanges, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { t_address } from '../../validationControls/t_address';
import { LookupClass } from '../../lockups/Lookups';
import { UtilService } from 'src/app/util.service';
import { delay } from 'rxjs/operators';
@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.css']
})
export class AddressesComponent implements OnInit, AfterViewInit, OnChanges {
  inputAddress: FormGroup;

  
  country_values = LookupClass.country;
  addtype_values = LookupClass.contact_type;
  t_address: t_address = new t_address(this.fb);

  @Input() addressFormGroup: FormArray;
  constructor(public fb: FormBuilder, private utilsevice: UtilService) {


  }
  addFieldValue() {
    if (this.inputAddress.invalid) {
      Object.keys(this.inputAddress.controls).forEach(key => {
        this.inputAddress.get(key).markAsDirty();
        this.inputAddress.get(key).markAsTouched();
      });
    } else {
      this.addAddress();
      const value = this.inputAddress.value;

      this.addressFormGroup.at(this.addressFormGroup.length - 1).setValue(value);
      this.inputAddress.reset();
    }
  }
  deleteFieldValue(index) {
    if (confirm('You are going to delete Address')) {
      this.addressFormGroup.removeAt(index);
    }
  }
  ngOnInit() {


    this.inputAddress = this.t_address.createAddress();




  }

  ngOnChanges(changes: any) {


  }

  ngAfterViewInit() {
    delay(0);
  }
  addAddress() {

    const x = this.t_address.createAddress();
    x.setParent(this.addressFormGroup);
    this.addressFormGroup.push(x);

  }
}
