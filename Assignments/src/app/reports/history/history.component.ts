import { Component, OnInit, ViewChild, Input, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ReportService } from '../services/report.service';
import { FormGroup } from '@angular/forms';
import { map, startWith, switchMap, catchError } from 'rxjs/operators';
import { merge, Observable, of as observableOf } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

export class History {
  id;
  reportId;
  action: any;
  byUser: any;
  lastUpdatedDate: any;
}

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit, AfterViewInit {

  @Input() reportDetailControl: FormGroup;
  dataSource: any = [];
  displayedColumns: string[] = ['action', 'byUser', 'lastUpdatedDate'];
  // dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private reportService: ReportService, private toastr: ToastrService) {
    // this.dataSource = new MatTableDataSource(history[]);
  }

  ngOnInit() {
    // this.getHistory();
  }

  ngAfterViewInit(): void {
    this.getHistory();
  }


  getHistory() {
    merge()
      .pipe(
        startWith({}),
        switchMap(() => {
          const reportId = this.reportDetailControl.value.id;
          return this.reportService.getReportHistory(reportId);
        }),
        map(data => {
         
          // this.resultsLength = data.totalElements;
          return data;
        }),
        catchError(() => {
          this.toastr.error('error while getting history');
          return observableOf([]);
        })
      ).subscribe(data => {
        
        
        this.dataSource = new MatTableDataSource();
        if (data) {
          this.dataSource.data = data;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      });

  }

}
