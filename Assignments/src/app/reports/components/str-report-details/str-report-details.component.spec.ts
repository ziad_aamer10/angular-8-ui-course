import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrReportDetailsComponent } from './str-report-details.component';

describe('StrReportDetailsComponent', () => {
  let component: StrReportDetailsComponent;
  let fixture: ComponentFixture<StrReportDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrReportDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrReportDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
