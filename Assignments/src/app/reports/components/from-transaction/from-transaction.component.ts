import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { t_person } from '../../validationControls/t_person';
import { t_person_my_client } from '../../validationControls/t_person_my_client';
import { t_entity_my_client } from '../../validationControls/t_entity_my_client';
import { t_account } from '../../validationControls/t_account';
import { t_account_my_client } from '../../validationControls/t_account_my_client';
import { LookupClass } from '../../lockups/Lookups';
import { tEntity } from '../../validationControls/t_entity';
import { t_from } from '../../validationControls/t_from';
import { SharingDataServieService } from '../../services/sharing-data-servie.service';
import { ToastrService } from 'ngx-toastr';
import { UtilService } from 'src/app/util.service';


// export inet
export enum Direction {
  Up = "UP",
  Down = "DOWN",
  Left = "LEFT",
  Right = "RIGHT",
}

export interface Codes {
  key: string;
  value: string;
}

@Component({
  selector: 'app-from-transaction',
  templateUrl: './from-transaction.component.html',
  styleUrls: ['./from-transaction.component.css']
})
export class FromTransactionComponent implements OnInit, OnChanges {
  conductor:boolean=true;
  oldcontrol: FormGroup
  fromType = Direction
  Conductor_type: string = 'None'
  From_type: string = ''
  t_fromControl: FormGroup;
  @Input() cascade: boolean
  @Input() cascadeMe
  @Input() transactionControl: FormGroup
  @Output() emitFromToCascade = new EventEmitter();
  @Output() emitPartyChange = new EventEmitter();
  t_person: t_person = new t_person()
  t_account: t_account = new t_account()
  t_person_my_client: t_person_my_client = new t_person_my_client()
  t_entity_my_client: t_entity_my_client = new t_entity_my_client()
  t_account_my_client: t_account_my_client = new t_account_my_client()
  t_from: t_from = new t_from(this.fb)
  personFormControl_other: FormGroup
  personFormControl: FormGroup
  entityFormControl_other: FormGroup
  entityFormControl: FormGroup
  accountFormControl: FormGroup
  accountFormControl_other: FormGroup
  fromForeignCurrency: FormGroup
  tConductorControl: FormGroup
  tEntity: tEntity = new tEntity()

  currencyCodes = LookupClass.Currency;
  fundCodes = LookupClass.funds_type;
  country_values = LookupClass.country
  isEgyptianPound: boolean = false

  constructor(private fb: FormBuilder, private dataService: SharingDataServieService, private toastr: ToastrService) { }

  ngOnInit() {

    this.setFromControl()
    this.isEgyptianPound = this.t_fromControl.get('fromForeignCurrency').get('foreignCurrencyCode').value == 'EGP' || false
  }
  save(element) {

    this.emitPartyChange.emit(element);

  }

  close() { }

  checkForConductor() {
    
    if (this.Conductor_type == 'Yes') {
      this.t_fromControl.setControl("tConductor", this.tConductorControl)
    }
    else {
      this.t_fromControl.setControl("tConductor", new FormControl())
    }
  }

  // to select type and switch between my client and other client
  changeFromType() {
    let fromFundsComment = this.oldcontrol.get('fromFundsComment').value
    let fromFundsCode = this.oldcontrol.get('fromFundsCode').value;
    let fromCountry = this.oldcontrol.get('fromCountry').value;
    let fromForeignCurrency = this.oldcontrol.get('fromForeignCurrency').value;
    let foreignCurrencyCode = this.oldcontrol.get('fromForeignCurrency').get('foreignCurrencyCode').value;
    let foreignAmount = this.oldcontrol.get('fromForeignCurrency').get('foreignAmount').value;
    let foreignExchangeRate = this.oldcontrol.get('fromForeignCurrency').get('foreignExchangeRate').value;
    let xObject = { fromFundsComment, fromFundsCode, fromCountry, fromForeignCurrency, foreignCurrencyCode, foreignAmount, foreignExchangeRate }

    this.t_fromControl = this.t_from.create_t_from_my_controller()
    this.transactionControl.removeControl("tFrom")
    this.transactionControl.removeControl("tFromMyClient")
    this.transactionControl.addControl("tFrom", new FormControl());
    this.transactionControl.addControl("tFromMyClient", new FormControl())
    this.setEmptyControls()



    if (this.From_type === 'MyClientEntity') {

      this.t_fromControl.setControl("fromEntity", this.entityFormControl)

      this.transactionControl.setControl("tFromMyClient", this.t_fromControl)
      this.checkForConductor()
      // if (this.Conductor_type == 'Yes') {
      //   this.t_fromControl.setControl("tConductor", this.tConductorControl)
      // }
      // else {
      //   this.t_fromControl.setControl("tConductor", new FormControl())
      // }

    }
    else if (this.From_type === 'MyClientPerson') {


      this.t_fromControl.setControl("fromPerson", this.personFormControl);

      this.transactionControl.setControl("tFromMyClient", this.t_fromControl)
      this.checkForConductor();

      // this.t_fromControl.setControl("tConductor", this.tConductorControl)
    }
    else if (this.From_type === 'MyClientAccount') {

      this.t_fromControl.setControl("fromAccount", this.accountFormControl);

      this.transactionControl.setControl("tFromMyClient", this.t_fromControl)
      this.checkForConductor();

      // if (this.Conductor_type == 'Yes') {
      //   this.t_fromControl.setControl("tConductor", this.tConductorControl)
      // }
      // else {
      //   this.t_fromControl.setControl("tConductor", new FormControl())
      // }
    }
    else if (this.From_type === 'OtherClientEntity') {

      this.t_fromControl.setControl("fromEntity", this.entityFormControl_other);

      this.transactionControl.setControl("tFrom", this.t_fromControl);
      this.checkForConductor();

      // this.t_fromControl.setControl("tConductor", this.tConductorControl)
    }
    else if (this.From_type === 'OtherClientPerson') {

      this.t_fromControl.setControl("fromPerson", this.personFormControl_other);

      this.transactionControl.setControl("tFrom", this.t_fromControl);
      this.checkForConductor();

      // this.t_fromControl.setControl("tConductor", this.tConductorControl)
    }
    else if (this.From_type === 'OtherClientAccount') {

      this.t_fromControl.setControl("fromAccount", this.accountFormControl_other);

      this.transactionControl.setControl("tFrom", this.t_fromControl);
      this.checkForConductor();

      // this.t_fromControl.setControl("tConductor", this.tConductorControl)
    }
    else if (this.From_type === 'None') {
      // this.transactionControl.setControl("tFrom",new FormControl());
      // this.transactionControl.setControl("tFromMyClient",new FormControl());
      this.setNullValuesWhenSelectingNone();
    }
    this.setOldFromGeneralInfo(xObject);
  }

  setNullValuesWhenSelectingNone() {
    this.t_fromControl.setControl('fromEntity', new FormControl());
    this.t_fromControl.setControl('fromPerson', new FormControl());
    this.t_fromControl.setControl('fromAccount', new FormControl());
    this.transactionControl.setControl('tFrom', new FormControl());
    this.transactionControl.setControl('tFromMyClient', new FormControl());
  }

  subscribeOnGeneralInfo() {
    let list = ['fromFundsComment', 'fromFundsCode', "fromCountry"];
    let list2 = ['foreignCurrencyCode', 'foreignAmount', 'foreignExchangeRate'];
    list.forEach(element => {
      this.t_fromControl.get(element).valueChanges.subscribe(data => {
        this.oldcontrol.get(element).setValue(data)
      })
    })
    list2.forEach(element => {
      this.t_fromControl.get('fromForeignCurrency').get(element).valueChanges.subscribe(data => {
        this.oldcontrol.get('fromForeignCurrency').get(element).setValue(data)
      })
    })
  }

  setOldFromGeneralInfo(xObject) {
    this.t_fromControl.get('fromFundsCode').setValue(xObject.fromFundsCode);
    this.t_fromControl.get('fromCountry').setValue(xObject.fromCountry);
    this.t_fromControl.get('fromFundsComment').setValue(xObject.fromFundsComment);
    // this.t_fromControl.get('fromForeignCurrency').setValue(xObject.fromForeignCurrency);
    this.t_fromControl.get('fromForeignCurrency').get('foreignCurrencyCode').setValue(xObject.foreignCurrencyCode);
    this.t_fromControl.get('fromForeignCurrency').get('foreignAmount').setValue(xObject.foreignAmount);
    this.t_fromControl.get('fromForeignCurrency').get('foreignExchangeRate').setValue(xObject.foreignExchangeRate);
    this.subscribeOnGeneralInfo()
  }

  // saveTransactionFromOnClick(){
  //   this.saveTransactionFrom.emit(this.t_fromControl);
  // }
  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let change = changes[propName];
      let curVal = change.currentValue;
      let prevVal = change.previousValue;
      if (propName == 'transactionControl') {
        // 

        this.transactionControl = curVal
        this.setFromControl()
      }


    }

  }

  setFromControl() {
    if (this.transactionControl.get('tFrom').value) {
      this.t_fromControl = this.transactionControl.get('tFrom') as FormGroup
      let tConductor = this.t_fromControl.get('tConductor') as FormGroup
      if (tConductor.value) {
        this.tConductorControl = tConductor
        this.Conductor_type = 'Yes'
      }
      if (this.t_fromControl.get('fromAccount').value) {
        this.accountFormControl_other = this.t_fromControl.get('fromAccount') as FormGroup
        this.From_type = 'OtherClientAccount'
      }
      else if (this.t_fromControl.get('fromEntity').value) {
        this.entityFormControl_other = this.t_fromControl.get('fromEntity') as FormGroup
        this.From_type = 'OtherClientEntity'
      }
      else if (this.t_fromControl.get('fromPerson').value) {
        this.personFormControl_other = this.t_fromControl.get('fromPerson') as FormGroup
        this.From_type = 'OtherClientPerson'
      }
    }
    else if (this.transactionControl.get('tFromMyClient').value) {
      this.t_fromControl = this.transactionControl.get('tFromMyClient') as FormGroup
      let tConductor = this.t_fromControl.get('tConductor') as FormGroup
      if (tConductor.value) {
        this.tConductorControl = tConductor
        this.Conductor_type = 'Yes'
      }
      if (this.t_fromControl.get('fromAccount').value) {

        this.accountFormControl = this.t_fromControl.get('fromAccount') as FormGroup
        this.From_type = 'MyClientAccount'

      }
      else if (this.t_fromControl.get('fromEntity').value) {
        this.entityFormControl = this.t_fromControl.get('fromEntity') as FormGroup
        this.From_type = 'MyClientEntity'
      }
      else if (this.t_fromControl.get('fromPerson').value) {
        this.personFormControl = this.t_fromControl.get('fromPerson') as FormGroup
        this.From_type = 'MyClientPerson'
      }
    }
    else {
      // 

      this.t_fromControl = this.t_from.create_t_from_my_controller()
      this.setEmptyControls()
      this.tConductorControl = this.t_person_my_client.createPersonGroup()
    }
    if (this.transactionControl.touched) {
      UtilService.markFormControl(this.t_fromControl)
    }

    this.oldcontrol = this.t_from.set_t_from_my_controller(this.t_fromControl.value)
    this.subscribeOnGeneralInfo();

    // this.t_fromControl.get('fromFundsComment').valueChanges.subscribe(data=>{
    //   this.oldcontrol.get('fromFundsComment').setValue(data)
    // })
    // })
    // this.t_fromControl.valueChanges.subscribe(data=>{

    //   this.oldcontrol = this.t_from.set_t_from_my_controller(data)
    // })
  }
  changeFromControlFromSource(event, fromType) {
    // 
    let tFromMyClient = this.transactionControl.get("tFromMyClient") as FormGroup
    let tFrom = this.transactionControl.get("tFrom") as FormGroup



    switch (fromType) {

      case "myPerson": {
        tFromMyClient.removeControl("fromPerson")
        tFromMyClient.addControl("fromPerson", event)
        break;
      }
      case "otherPerson": {
        tFrom.removeControl("fromPerson")
        tFrom.addControl("fromPerson", event)
        break;
      }
      case "myAccount": {
        tFromMyClient.removeControl("fromAccount")
        tFromMyClient.addControl("fromAccount", event)
        break;
      }
      case "otherAccount": {
        tFrom.removeControl("fromAccount")
        tFrom.addControl("fromAccount", event)
        break;
      }
      case "myEntity": {
        tFromMyClient.removeControl("fromEntity")
        tFromMyClient.addControl("fromEntity", event)
        break;
      }
      case "otherEntity": {
        tFrom.removeControl("fromEntity")
        tFrom.addControl("fromEntity", event)
        break;
      }
    }



  }
  changeConductor() {
    
    this.setTconductor()
    let tFrom: FormGroup = this.transactionControl.get('tFrom') as FormGroup
    let tFromMyClient: FormGroup = this.transactionControl.get('tFromMyClient') as FormGroup
    switch (this.Conductor_type) {
      case "Yes": {
        // this.tConductorControl=this.t_person_my_client.createPersonGroup()
        if (tFrom.value) {
          

          tFrom.setControl("tConductor", this.tConductorControl)

        }
        else if (tFromMyClient.value) {
          

          tFromMyClient.setControl("tConductor", this.tConductorControl)
        }
        break;
      }
      default: {
        // this.tConductorControl=new FormControl()
        if (tFrom.value) {
          tFrom.setControl("tConductor", new FormControl())

        }
        else if (tFromMyClient.value) {
          tFromMyClient.setControl("tConductor", new FormControl())
        }
        break;
      }
    }
  }


  setEmptyControls() {
    this.personFormControl = this.t_person_my_client.createPersonGroup()
    this.personFormControl_other = this.t_person.createPersonControl()
    this.entityFormControl = this.t_entity_my_client.createEntityControl()
    this.accountFormControl_other = this.t_account.createAccountControl()
    this.accountFormControl = this.t_account_my_client.createAccountControl()
    this.entityFormControl_other = this.tEntity.createEntityControl()
  }

  sendFromTocascade() {
    this.emitFromToCascade.emit(this.transactionControl);
    this.dataService.changeMessage2('close cascade tab');
    this.toastr.success('Cadcade is done successfully');
  }
  changeCurrencyCode() {
    let fromForeignCurrency = this.t_fromControl.get('fromForeignCurrency')
    let foreignCurrencyCode = fromForeignCurrency.get('foreignCurrencyCode')
    let foreignAmount = fromForeignCurrency.get('foreignAmount')
    let foreignExchangeRate = fromForeignCurrency.get('foreignExchangeRate')
    this.isEgyptianPound = foreignCurrencyCode.value == 'EGP' || foreignCurrencyCode.value == null
    if (this.isEgyptianPound) {

      foreignAmount.reset()
      foreignExchangeRate.reset()
      foreignCurrencyCode.reset()
      fromForeignCurrency.clearValidators()
      foreignCurrencyCode.clearValidators()
      foreignAmount.clearValidators()
      foreignExchangeRate.clearValidators()
    }
    else {
      foreignCurrencyCode.setValidators(Validators.required)
      foreignAmount.setValidators(Validators.required)
      foreignExchangeRate.setValidators(Validators.required)
    }
    foreignCurrencyCode.updateValueAndValidity()
    foreignAmount.updateValueAndValidity()
    foreignExchangeRate.updateValueAndValidity()
  }
  setTconductor() {
    console.info(this.tConductorControl, 'tConductorControl');

    if (this.tConductorControl == undefined) {
      console.error(this.tConductorControl, 'tl');

      if (this.transactionControl.controls.tFrom.value) {
        

        this.tConductorControl = this.t_person.createPersonControl()
      }
      else {
        

        this.tConductorControl = this.t_person_my_client.createPersonGroup()

      }
    }
  }
}
