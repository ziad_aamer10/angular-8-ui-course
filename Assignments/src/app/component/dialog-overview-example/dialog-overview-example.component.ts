import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

 export interface DialogData {
  animal: string;
  name: string;
}

/**
 * @title Dialog Overview
 */
@Component({
  selector: 'dialog-overview-example',
  templateUrl: 'dialog-overview-example.component.html',
  styleUrls: ['dialog-overview-example.component.css'],
})
export class DialogOverviewExampleComponent {

  animal: string;
  name: string;
  
  constructor(public dialog: MatDialog ) { }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      // data: {  }
    });

    dialogRef.afterClosed().subscribe(result => {
      
   //   this.animal = result;
    });
  }

 }

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {
  

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private router :Router,private route: ActivatedRoute) { }

  onCancelClick(): void {
    localStorage.removeItem('Report')
    localStorage.removeItem('ReportStr')
    localStorage.removeItem('ReportST')
    localStorage.removeItem('ReportAct')
    localStorage.removeItem('ReportAif')
    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
             this.router.navigate([returnUrl || '/']);
   // this.router.navigate(['/reports']);
    this.dialogRef.close();
  }
  onContinueClick(): void {
    
    let report:any=JSON.parse(localStorage.getItem('Report'))
    let ReportStr=JSON.parse(localStorage.getItem('ReportStr'))
    let ReportAif=JSON.parse(localStorage.getItem('ReportAif'))
    
    
if(report!= null )
    this.router.navigate([environment.sarEditRoute,report.id])
else if (ReportStr != null)  
     this.router.navigate([environment.strEditRoute,ReportStr.id]) 
else if (ReportAif != null)  
     this.router.navigate([environment.aifEditRoute,ReportAif.id])
else
{const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
this.router.navigate([returnUrl || '/']);}
   // this.router.navigate(['/reports']);
     
     this.dialogRef.close();
    // localStorage.removeItem('Report')
    // localStorage.removeItem('ReportAct')
  //  this.dialogRef.close();
  }

}
