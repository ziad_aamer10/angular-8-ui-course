import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { t_transaction } from '../validationControls/t_transaction';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  url = environment.projectUrl + '/transactions' ;
  // transaction: t_transaction
  constructor(private fb: FormBuilder, private Http: HttpClient) { }


  saveTransaction (transaction) {
   return this.Http.put(this.url, transaction);
  }
  getTransactionFromSource(transactionReferenceNumber) {
    const url = `${environment.projectUrl}/transactionFromSource/${transactionReferenceNumber}`;
   return  this.Http.get(url);
  }
  }



