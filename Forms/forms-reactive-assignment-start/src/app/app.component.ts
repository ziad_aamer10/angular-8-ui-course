import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  projectStatuses = ['Stable', 'Critical', 'Finished'];

  projectForm: FormGroup;

  constructor() {
  }

  ngOnInit() {
    this.projectForm = new FormGroup({
      'projectName': new FormControl(null, [Validators.required], this.checkForbiddenProjectNamesAsync),
      'projectMail': new FormControl(null, [Validators.required, Validators.email]),
      'projectStatus': new FormControl(null)
    });
  }

  onSubmit() {
    console.log(this.projectForm);
  }

  checkForbiddenProjectNames(control: FormControl): { [key: string]: boolean } {
    if (control.value === 'Test') {
      return {'projectNameIsForbidden': true};
    }
    return null;
  }

  checkForbiddenProjectNamesAsync(control: FormControl): Promise<any> | Observable<any> {
    return new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'Test') {
          resolve({'projectNameIsForbidden': true});
        }
        resolve(null);
      }, 1500);

    });

  }


}
