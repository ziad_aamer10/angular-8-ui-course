import { Subscription } from 'rxjs';
import { Component, ElementRef, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { saveAs } from 'file-saver';
import * as $ from 'jquery';
import { Comment } from '../model/comment.model';
import { CommentService } from '../service/comment.service';
import { WebSocketService } from '../service/web-socket.service';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';
import { Actions } from 'src/app/reports/Actions/action';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit,OnDestroy {
  subscriptionGetComment:Subscription;
  subscriptionUploadFiles:Subscription;
  desc;
  deleteCommentAction: boolean;
  updateCommentAction: any;

  constructor(private route: ActivatedRoute, private commentService: CommentService,
    private webSocketService: WebSocketService,
    private auth: AuthService
    // , private notification: NotificationService
  ) { }

  f: FileList;
  fTry: FileList;
  attachments: any = null;
  suspectId: Number;
  comments_block: Comment[] = [];
  alarmed_Obj_Key: string;
  alarmed_Obj_level_Cd: string;
  report_id: Number;
  addedComment: Comment;
  loggedInuser = localStorage.getItem('id');
  loggedInUserName = localStorage.getItem('userName');
  returnedComment: Comment[] = [];
  @ViewChild('files2') el2: ElementRef;
  @ViewChild('filesUpdated') el3: ElementRef;
  @Input() type;

  onChangeFile(f: FileList) {
    this.fTry = f;
    
  }

  ngOnInit() {
    this.deleteCommentAction = this.auth.has_Capabilities(Actions.deleteComment);
    this.updateCommentAction = this.auth.has_Capabilities(Actions.updateComment);

    
    this.route.paramMap.subscribe(params => {
      this.alarmed_Obj_Key = params.get('obj_key');
      this.alarmed_Obj_level_Cd = params.get('obj_level_code');
      this.report_id = +params.get('id');
      console.log('obj_key', params.get('obj_key'));
      console.log('obj_level_code', params.get('obj_level_code'));
      console.log('id', params.get('id'));

      if (this.type === 'report') {
        
      } else if (this.type === 'alarm') {
        
      } else {
        if (this.report_id > 0) {
          this.type = 'report';
        } else if (this.alarmed_Obj_Key, this.alarmed_Obj_level_Cd) {
          this.type = 'alarm';
        }
      }

      

    });

    const stompClient = this.webSocketService.connect();
    stompClient.connect({}, _frame => {
      let url;
      if (this.type === 'report') {
        url = `/topic/comment/${this.report_id}`;
      } else if (this.type === 'alarm') {
        url = `/topic/comment/${this.alarmed_Obj_Key}/${this.alarmed_Obj_level_Cd}`;
      }

      stompClient.subscribe(url, comment => {
        this.returnedComment = JSON.parse(comment.body);

        
        

        this.comments_block = this.returnedComment;
      });
    });

    if (this.type === 'alarm') {
      this.commentService.getCommentsAlarm(this.alarmed_Obj_Key, this.alarmed_Obj_level_Cd).subscribe(data => {
        this.comments_block = data;
        
      });
    } else if (this.type === 'report') {
      this.subscriptionGetComment = this.commentService.getCommentsReport(this.report_id).subscribe(data => {
        this.comments_block = data;
        
      });
    }

    $(document).ready(() => {
      const loggedUser = this.loggedInuser;
      const all = this.comments_block;

      $('.allcomments').each(function (_index) {
        $(this).fadeIn(3000);
      });

      $('.comment_block').each(function (index) {
        const yy = all[index];
        
        
        if (yy.uplodedById === loggedUser) {
          $('#B' + index).css('background', '#f5f2f0');
        }
        
        $('#B' + index).fadeOut();
        $('#B' + index).fadeIn(3000 + index * 1000);
      });
    });
  }

  get _hasDeleteComment() {
    return this.deleteCommentAction;
  }

  get _hasUpdatedComment() {
    return this.updateCommentAction;
  }

  addComment(form_: NgForm) {
    
    

    if (!form_.value.desc && form_.value.desc.length === 0 && this.el2.nativeElement.files['length'] === 0) {
      
    } else {
      
      if (this.type === 'alarm') {
       this.commentService.uploadFilesAlarm(this.el2.nativeElement.files, this.alarmed_Obj_Key, this.alarmed_Obj_level_Cd,
          form_.value.desc, this.loggedInuser).subscribe(
            data => {
              // this.notification.suspectCommentNoti(this.alarmed_Obj_level_Cd, this.alarmed_Obj_Key,
              //   'add-comment-to-suspect', this.loggedInuser, form_.desc);
              this.attachments = data;
              
            }, error => { }
            , () => {
              
              form_.reset();
              this.el2.nativeElement.value = '';
            }
          );
      } else if (this.type === 'report') {
        this.subscriptionUploadFiles = this.commentService.uploadFilesReport(this.el2.nativeElement.files, this.report_id,
          form_.value.desc, this.loggedInUserName).subscribe(
            data => {
              this.attachments = data;
              
            }, error => { }
            , () => {
              
              form_.reset();
              this.el2.nativeElement.value = '';
            }
          );
      }
    }
  }

  downloadFile(fileName) {
    
    this.commentService.downloadFile(fileName).subscribe(data => {
      
      saveAs(data, fileName);
    }
    );
  }

  deleteComment(id_, _description) {
    
    
    if (confirm('Are you sure you want to delete?')) {
      if (this.type === 'alarm') {
        this.commentService.deleteCommentAlarm(id_, this.loggedInuser, this.alarmed_Obj_Key, this.alarmed_Obj_level_Cd).subscribe(_data => {
          // this.notification.suspectCommentNoti(this.alarmed_Obj_level_Cd, this.alarmed_Obj_Key,
          //   'delete-comment-of-suspect', this.loggedInuser, description);
        });
      } else if (this.type === 'report') {
        this.commentService.deleteCommentReport(id_, this.loggedInUserName, this.report_id).subscribe(_data => {
          // this.notification.suspectCommentNoti(this.alarmed_Obj_level_Cd, this.alarmed_Obj_Key,
          //   'delete-comment-of-suspect', this.loggedInuser, description);
        });
      }
    }
  }

  showUpdateComment(index) {
    
    $('#U' + index).css('display', 'block');
    $('#C' + index).css('display', 'none');
  }

  updateComment(form_, index, comObj: Comment) {
    $('#U' + index).css('display', 'none');
    $('#C' + index).css('display', 'block');

    
    
    

    const comm_ = {
      id: comObj.id,
      alarmed_Obj_Key: this.alarmed_Obj_Key,
      alarmed_Obj_level_Cd: this.alarmed_Obj_level_Cd,
      description: form_.comment_desc ? form_.comment_desc : comObj.description,
      uplodedById: this.loggedInuser,
      uplodedByName: this.loggedInUserName,
      report_id: this.report_id
    };

    
    if (form_.comment_desc.length === 0 && this.fTry.length > 0) {
      
      if (this.type === 'alarm') {
        this.commentService.addNewFilesToCommentAlarm(this.fTry, comObj['id'], this.loggedInuser,
          this.alarmed_Obj_level_Cd, this.alarmed_Obj_Key);
      } else if (this.type === 'report') {
        this.commentService.addNewFilesToCommentReport(this.fTry, comObj['id'], this.loggedInUserName, this.report_id);
      }
    } else if (form_.comment_desc.length === 0 && this.el3.nativeElement.files['length'] === 0) {
      //
    } else {
      
      if (this.type === 'alarm') {
        this.commentService.updateCommentAlarm(this.el3.nativeElement.files, comm_);
      } else if (this.type === 'report') {
        this.commentService.updateCommentReport(this.el3.nativeElement.files, comm_);
      }
    }

    

    
    
    console.log('this.el3.nativeElement.files[length])', this.el3.nativeElement.files['length']);

    form_.comment_desc = '';
    this.el3.nativeElement.value = '';
  }

  deleteSpecificFile(file_) {
    if (confirm('Are you sure you want to delete?')) {
      if (this.type === 'alarm') {
        this.commentService.deleteSpecificFileAlarm(file_.id, this.loggedInuser, this.alarmed_Obj_Key, this.alarmed_Obj_level_Cd);
      } else if (this.type === 'report') {
        this.commentService.deleteSpecificFileReport(file_.id, this.loggedInUserName, this.report_id);
      }
    }
  }

  closeUpdateComment(index) {
    $('#U' + index).css('display', 'none');
    $('#C' + index).css('display', 'block');
  }

  ngOnDestroy(){
    if(this.subscriptionGetComment != undefined)
      this.subscriptionGetComment.unsubscribe();
    if(this.subscriptionUploadFiles != undefined)
      this.subscriptionUploadFiles.unsubscribe();
  }
}

export enum CommentType {
  alarm,
  report
}
