import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LookupClass } from '../../lockups/Lookups';

@Component({
  selector: 'app-goods-item',
  templateUrl: './goods-item.component.html',
  styleUrls: ['./goods-item.component.css']
})
export class GoodsItemComponent implements OnInit {
@Input()GoodsForm:FormGroup
inputAddress:FormGroup

item_type_values=LookupClass.item_type;
trans_item_status_values=LookupClass.trans_item_status;
CurrencyValues=LookupClass.Currency;
country_values = LookupClass.country;
addtype_values = LookupClass.contact_type;
  constructor() { }

  ngOnInit() {
    this.inputAddress=this.GoodsForm.get('address') as FormGroup
  }

}
