import { FormBuilder, Validators, FormGroup, FormArray, FormControl } from "@angular/forms";
import { t_from } from "./t_from";
import { t_to } from "./t_to";
import { NullTemplateVisitor } from "@angular/compiler";
import { DatePipe } from "@angular/common";
import { UtilService } from "src/app/util.service";
import { goodAndServices } from "src/environments/goodsAndServices";

export class t_transaction {
    // fb:FormBuilder;
    tfrom: t_from = new t_from(this.fb);
    tto: t_to = new t_to(this.fb);
     util:UtilService=new UtilService()
     goodAndServices:goodAndServices=new goodAndServices(this.fb)
    constructor(private fb: FormBuilder) {
        // this.fb=new FormBuilder()
    }
    createTransactionControl(): FormGroup {
        
        return this.fb.group({
            id: new FormControl(),
            transactionnumber: ['', [Validators.required]],
            internalRefNumber: new FormControl(),
            transactionLocation: ['', [Validators.required]],
            transactionDescription: ['', [Validators.required]],
            dateTransaction: ['', [Validators.required]],
            teller: new FormControl(),
            authorized: new FormControl(),
            lateDeposit: this.fb.group({
                value: new FormControl()
            }),
            datePosting: new FormControl(),
            valueDate: new FormControl(),
            transmodeCode: ['', [Validators.required]],
            transmodeComment: new FormControl(),
            amountLocal: ['', [Validators.required]],
            tFromMyClient: new FormControl(),
            tFrom: new FormControl(),
            tToMyClient: new FormControl(),
            tTo: new FormControl(),
            Comments: new FormControl(),
            isGoodsServices: new FormControl(),
            goodsServices:this.goodAndServices.createControl()
        })

    }

    create(): FormArray {
        return this.fb.array([
        ])
    }

    // this function to edit the transaction 
    setTransaction(ttransaction): FormGroup {
        return this.fb.group({
            id: [ttransaction.id],
            transactionnumber: [ttransaction.transactionnumber,[Validators.required]],
            internalRefNumber: [ttransaction.internalRefNumber],
            transactionLocation: [ttransaction.transactionLocation,[Validators.required]],
            transactionDescription: [ttransaction.transactionDescription,[Validators.required]],
            dateTransaction: [this.util.convertDateFromServer(ttransaction.dateTransaction),[Validators.required]],
            teller: [ttransaction.teller],
            authorized: [ttransaction.authorized],
            lateDeposit: this.fb.group({
                value:ttransaction.lateDeposit? ttransaction.lateDeposit.value:false
            }),
            datePosting: [ttransaction.datePosting],
            valueDate: [this.util.convertDateFromServer(ttransaction.valueDate)],
            transmodeCode: this.util.checkLockUpValue('transmodeCode',ttransaction.transmodeCode)?[ttransaction.transmodeCode,[Validators.required]]:new FormControl(null,Validators.required),
            transmodeComment: [ttransaction.transmodeComment],
            amountLocal: [ttransaction.amountLocal,[Validators.required]],
            tFromMyClient: ttransaction.tFromMyClient ? this.setTFromMyClient(ttransaction.tFromMyClient) : new FormControl(),
            tFrom: ttransaction.tFrom ? this.setTFrom(ttransaction.tFrom) : new FormControl(),
            tToMyClient: ttransaction.tToMyClient ? this.setTToMyClient(ttransaction.tToMyClient) : new FormControl(),
            tTo: ttransaction.tTo ? this.setTTo(ttransaction.tTo) : new FormControl(),
            Comments: [ttransaction.Comments],
            isGoodsServices: [ttransaction.isGoodsServices],
            goodsServices:ttransaction.goodsServices?this.goodAndServices.setControl(ttransaction.goodsServices):this.goodAndServices.createControl()
            
        })
    }

    setTransactions(transactions) {
        let trasactionsControl = this.fb.array([],Validators.required);
        if (transactions) {
            for (let x of transactions) {
                let s = this.setTransaction(x);
                trasactionsControl.push(s);
            }
        }
        return trasactionsControl;
    }

    setTFrom(tfrom) {
        return this.tfrom.set_t_from_other_controller(tfrom);
    }
    setTFromMyClient(tfrom) {
        return this.tfrom.set_t_from_my_controller(tfrom)
    }
    setTTo(tto) {
        return this.tto.set_t_to_other_controller(tto)
    }
    setTToMyClient(tto) {
        return this.tto.set_t_to_my_controller(tto);
    }
}