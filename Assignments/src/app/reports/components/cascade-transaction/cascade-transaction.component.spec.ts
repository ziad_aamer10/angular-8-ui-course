import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CascadeTransactionComponent } from './cascade-transaction.component';

describe('CascadeTransactionComponent', () => {
  let component: CascadeTransactionComponent;
  let fixture: ComponentFixture<CascadeTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CascadeTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CascadeTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
