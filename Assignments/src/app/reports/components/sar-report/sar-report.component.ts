import { activity } from './../../validationControls/activity';
import { SharingDataServieService } from './../../services/sharing-data-servie.service';
import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { t_report } from '../../validationControls/t_report';
import { environment } from 'src/environments/environment';
import { ReportService } from '../../services/report.service';
import { Subscription } from 'rxjs';
import { t_report_party_type } from '../../validationControls/report_party_type';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UtilService } from 'src/app/util.service';
import { LookupClass } from '../../lockups/Lookups';
import { CategoryClass } from '../../lockups/report-indiators-lockup';
import { notValidElementWithMessageOnly } from '../../models/notValidElement';
import { Actions } from '../../Actions/action';
import { AuthService } from 'src/app/loginModule/services/auth-service.service';


@Component({
  selector: 'app-sar-report',
  templateUrl: './sar-report.component.html',
  styleUrls: ['./sar-report.component.css']
})
export class SarReportComponent implements OnInit {
  sub: Subscription
  confirmCloseReport = false;
  disableButton: boolean = false;
  validationIndicators = false;
  validationErrors;
  testForm: FormGroup = new FormGroup({});
  addIndicator = false;
  t_report: t_report = new t_report(this.fb, this.reportservice);
  dataSource: any = [];
  reportPartyType: t_report_party_type = new t_report_party_type(this.fb);
  activity: activity = new activity(this.fb);
  reportDetailControl: FormGroup;
  EditActivitycontrol: FormGroup;
  index = 0;
  timerid = null;
  CategoryObject = new CategoryClass();
  validationErrorsLabels: notValidElementWithMessageOnly[] = [];


  createReportAction: boolean = false;
  editReportAction: boolean = false;
  generateXMLAction: boolean = false;
  addActivityAction: boolean = false;
  SendToCheckerAction: boolean = false;
  SendToMakerAction: boolean = false;
  REOPENReportAction: boolean = false;
  MLCUAcknowledgeAction: boolean = false;
  ReturnToCheckerAction: boolean = false;
  ReturnToMakerAction: boolean = false;
  SendToAmlHeadAction: boolean = false;
  ReportStatus: string = '';
  closeReportAction: boolean;
  SubmitAction: boolean;
  ReturnToMakerFromManagerAction: boolean;
  routeReportAction: boolean;
  AIFRequestAction: boolean;
  ActionButton: boolean;
  sendToManagerFromMakerAction: boolean;


  constructor(public dialogActivity: MatDialog, private reportservice: ReportService
    , private fb: FormBuilder, private route: ActivatedRoute, private toaster: ToastrService,
    private router: Router, private dialog: MatDialog, private util: UtilService,
    private ref: ChangeDetectorRef, private auth: AuthService, private dataService: SharingDataServieService) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        if (localStorage.getItem("Report") == null) {
          this.reportservice.getReportById(id).subscribe(data => {
            this.reportDetailControl = this.t_report.setReport(data);




            this.ReportStatus = this.reportDetailControl.get('reportStatusCode').value

            this.timerid = setInterval(() => this.TimeInterval(), 10000);
            

            //Disabled Forms
            let reportUserLockId = this.reportDetailControl.get('reportUserLockId').value
            let userName = localStorage.getItem('name')
            if (userName == reportUserLockId || reportUserLockId == null) {
              this.setStaticValue();
            }
            else if (userName != reportUserLockId && reportUserLockId != null) {
              this.reportDetailControl.disable()
              this.disableButton = true
            }
          },
            error => {
              this.toaster.error(error)

            })
        }
        else {
          console.log("autosave")
          const report = JSON.parse(localStorage.getItem("Report"))
          this.reportservice.processIndicators(report)
          this.reportDetailControl = this.t_report.setReport(report)
          this.setStaticValue();
          console.log("12131:", localStorage.getItem("Report"));
          // populate Auto save data
          this.ReportStatus = this.reportDetailControl.get('reportStatusCode').value

          if (localStorage.getItem("ReportAct") != null) {
            console.log("12131:", localStorage.getItem("ReportAct"));
            this.addIndicator = true;
            this.EditActivitycontrol = this.reportPartyType.set_report_party_type(JSON.parse(localStorage.getItem('ReportAct')));
          }
        }

      }


    });

    this.setActions();

  }
  TimeInterval(): void {
    // 
    localStorage.setItem('Report', JSON.stringify(this.reportDetailControl.value));

  }
  async ngOnDestroy() {
    this.unLock();
    localStorage.removeItem('Report');
    localStorage.removeItem('ReportAct');
    clearInterval(this.timerid);
    await this.delay(500);

  }

  unLock() {
    const userName = localStorage.getItem('name');
    let reportUserLockId = this.reportDetailControl.get('reportUserLockId').value
    if(reportUserLockId==userName){
    this.route.params.subscribe(params => {
      const id = params.id;
      this.reportservice.unLock(id, userName).subscribe(data => {
            
      }, error => { });
    });

  }else
  console.log('unLock Not Allowed! ');
  }
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}
  setStaticValues() {
    this.reportDetailControl.controls.reportCode.setValue('SAR');
    this.reportDetailControl.controls.reportingPersonType.setValue('AML Head');
    this.reportDetailControl.controls.currencyCodeLocal.setValue('EGP');
    this.reportDetailControl.controls.rentityBranch.setValue(environment.entity_branch);
    this.reportDetailControl.controls.submissionCode.setValue('E');

    // //
    this.reportDetailControl.controls.reportCode.disable();
    this.reportDetailControl.controls.submissionCode.disable();
    this.reportDetailControl.controls.rentityId.disable({ onlySelf: true });
    this.reportDetailControl.controls.submissionDate.disable();
    this.reportDetailControl.controls.rentityBranch.disable();
    this.reportDetailControl.controls.currencyCodeLocal.disable();
    this.reportDetailControl.controls.rentityId.disable();

    // this.reportDetailControl.controls.reportIndicators
  }

  setStaticValue() {
    this.reportDetailControl.controls.reportCode.setValue('SAR');
    this.reportDetailControl.controls.reportingPersonType.setValue('AML Head');
    this.reportDetailControl.controls.currencyCodeLocal.setValue('EGP');
    this.reportDetailControl.controls.rentityBranch.setValue(environment.entity_branch);
    this.reportDetailControl.controls.submissionCode.setValue('E');
    



    // //
    this.reportDetailControl.controls.reportCode.disable();
    this.reportDetailControl.controls.submissionCode.disable();
    this.reportDetailControl.controls.submissionDate.disable();
    this.reportDetailControl.controls.rentityBranch.disable();
    this.reportDetailControl.controls.currencyCodeLocal.disable();
    this.reportDetailControl.controls.entityReference.disable();


  }
  

  disabledform(group: FormGroup): void {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.get(key);


      if (control instanceof FormGroup) {
        

        this.disabledform(control);
        control.disable()
      }
      if (control instanceof FormControl) {
        
        control.disable()
      }
      if (control instanceof FormArray) {
        
        for (let i = 0; i < control.length; i++) {
          console.log('arr', control[i])
        }


      }
    });
  }




  addActivity() {
    this.addIndicator = false;
    this.EditActivitycontrol = null;
    this.addIndicator = true;
    this.index = 4;

  }
  saveActivity(result) {
    localStorage.removeItem('ReportAct');
    this.t_report.activity.createActivity();
    const parties = this.reportDetailControl.controls.activity.get('reportParties').get('reportParty') as FormArray;
    parties.push(result);

    this.index = 2;
    this.addIndicator = false;

  }
  saveReport() {
    this.mapToFinalReportFormat();
  }
  mapToFinalReportFormat() {
    let indicators = [];
    let value = this.reportDetailControl.getRawValue();

    Object.keys(value.reportIndicators).forEach(a => {
      if (Array.isArray(value.reportIndicators[a])) {
        indicators = indicators.concat(value.reportIndicators[a]);

      } else if (typeof value.reportIndicators[a] == 'string') {
        indicators.push(value.reportIndicators[a]);
      }



    });

    value = indicators;
    const x = this.reportDetailControl.getRawValue();
    x.reportIndicators = indicators;


  }
 
  saveSarReport() {
    clearInterval(this.timerid);
    localStorage.removeItem('Report');
    localStorage.removeItem('ReportAct');
    this.reportservice.saveSarReport(this.reportDetailControl.getRawValue()).subscribe((data: any) => {
      if (data) {
        this.reportDetailControl.get('id').setValue(data.id);
        this.toaster.success('Save sar report');
        this.confirmCloseReport = true;


      }

    },
      error => {
        this.toaster.error(error.message);

      });
  }
  updateSarReport() {
    clearInterval(this.timerid);
    localStorage.removeItem('Report');
    localStorage.removeItem('ReportAct');
    this.reportservice.updateSarReport(this.reportDetailControl.getRawValue()).subscribe(data => {
      this.toaster.success('Save sar report');




    },
      error => {
        this.toaster.error(error.message);

      });
  }

  editActivity($event: FormGroup) {
    
    
    this.addIndicator = false;
    this.EditActivitycontrol = $event;
    
    this.addIndicator = true;
    this.index = 4;



  }
  changeActivity(event) {

    this.addIndicator = false;
    this.index = 2
  }


  closeReportPage() {
    clearInterval(this.timerid);
    localStorage.removeItem('Report');
    localStorage.removeItem('ReportAct');
    this.router.navigate(['reports']);
  }
  closeActivity() {
    if (confirm('You will lose information')) {
      localStorage.removeItem('ReportAct');
      // clearInterval(this.timerid)
      this.addIndicator = false;
      this.index = 2

    }

  }
  ReportPreview() {
    this.updateSarReport()
    this.reportservice.PreviewReport(this.reportDetailControl.get('id').value).subscribe((data: any) => {

   
      let file = new Blob([data], { type: "application/pdf;charset=utf-8" });
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL, '_blank');
 
    });


  }

  GenerateXML() {

    console.log('this.reportDetailControl.get(\'id\').value', this.reportDetailControl.get('id').value);

    if (this.reportDetailControl.valid) {
      if (!confirm("GenerateXML ")) return

      this.reportservice.GenerateXMLAndSaveReport(this.reportDetailControl.getRawValue()).subscribe((data: any) => {
        if (data) {
          this.toaster.success('Generate xml successfully sar report');
          this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode)
          this.ReportStatus = data.reportStatusCode
          this.closeReportPageAndConfirmClose()

        }
        else {
          this.toaster.warning('An error has occured');
        }

      },
        error => {
          this.toaster.error(error.error.message);

        });
    } else {
      this.toaster.warning('Please validate report before XML Generation');
    }
  }
  validateReport() {

    const resultc = [];
    this.validationErrorsLabels = []
    let NestedSectionsErrors: notValidElementWithMessageOnly[] = []
    this.util.validateFormGroup2(this.reportDetailControl, resultc, NestedSectionsErrors);
    

    for (const x of resultc) {
      this.util.getOptionsFromLockup(x);
    }
    const reportParties = this.reportDetailControl.get('activity.reportParties.reportParty') as FormArray;

    if (reportParties && reportParties.length === 0) {
      const noReportParties: notValidElementWithMessageOnly = { message: 'Report must have at least one party', name: 'Report Activities' };
      this.validationErrorsLabels.push(noReportParties);
    }
    this.validationErrors = resultc;
    if (resultc.length > 0 || this.validationErrorsLabels.length > 0) {
      this.validationIndicators = true;
      this.index = 4;
    } else {
      this.toaster.warning('No Validation Errors Found!!');
    }





  }


  closeValidationTab(event?) {
    this.validationErrorsLabels = [];
    this.validationIndicators = false;
    this.index = 2;
  }
  setActions() {
    this.createReportAction = this.auth.has_Capabilities(Actions.createReport)
    this.editReportAction = this.auth.has_Capabilities(Actions.editReport)
    this.generateXMLAction = this.auth.has_Capabilities(Actions.generateXML)
    this.addActivityAction = this.auth.has_Capabilities(Actions.addActivity)
    this.SendToMakerAction = this.auth.has_Capabilities(Actions.sendToMaker)
    this.SendToCheckerAction = this.auth.has_Capabilities(Actions.sendTochecker)
    this.REOPENReportAction = this.auth.has_Capabilities(Actions.reOpenReport)
    this.MLCUAcknowledgeAction = this.auth.has_Capabilities(Actions.MLCUAcknowledgeAction)
    this.ReturnToCheckerAction = this.auth.has_Capabilities(Actions.returnToChecker)
    this.ReturnToMakerAction = this.auth.has_Capabilities(Actions.returnToMaker)
    this.ReturnToMakerFromManagerAction = this.auth.has_Capabilities(Actions.returnToMakerFromManager)
    this.SendToAmlHeadAction = this.auth.has_Capabilities(Actions.sendToAMLHead)
    this.closeReportAction = this.auth.has_Capabilities(Actions.closeReport)
    this.SubmitAction = this.auth.has_Capabilities(Actions.submitReport)
    this.routeReportAction = this.auth.has_Capabilities(Actions.routeReport)
    this.AIFRequestAction = this.auth.has_Capabilities(Actions.AIFRequest)
this.sendToManagerFromMakerAction=this.auth.has_Capabilities(Actions.sendToManagerFromMaker)
  }

  SendToChecker() {
    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid')
      return;
    }
    if (!confirm("Send To Checker")) return
    const report = Object.assign({}, this.reportDetailControl.getRawValue());
    this.reportservice.SendToChecker(report).subscribe((data: any) => {
      if (data) {

        this.toaster.success('SendToChecker Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }
    },
      error => {
        

        this.toaster.error(error.error.message);
      });
  }
  ReturnToMaker() {

    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid');
      return;
    }
    if (!confirm("Return To Maker")) return
    let report = Object.assign({}, this.reportDetailControl.getRawValue());
    this.reportservice.ReturnToMaker(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Send To Maker Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();
      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  ReturnToChecker() {

    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid');
      return;
    }
    if (!confirm("Send To Checker")) return
    let report = Object.assign({}, this.reportDetailControl.getRawValue());
    this.reportservice.ReturnToChecker(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Return To Checker Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);

        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();
      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  reOpenReport() {
    if (!confirm("Reopen Report")) return

    let reportid = this.reportDetailControl.get('id').value;
    this.reportservice.reOpenReport(reportid).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Report re-opened Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  MLCUAcknowledge() {
    if (!confirm("MLCUAcknowledge Report")) return

    let reportid = this.reportDetailControl.get('id').value;
    this.reportservice.MLCUAcknowledge(reportid).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Report MLCU acknowledge Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  SendToAMLHead() {

    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid');
      return;
    }
    if (!confirm("Send To AML Head")) return
    let report = Object.assign({}, this.reportDetailControl.getRawValue());
    this.reportservice.sendToAMLHead(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Send To AML HEAD Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });
  }
  get _ReturnToCheckerAction() {
    return this.ReturnToCheckerAction && (['WaitingHeadDecision'].includes(this.ReportStatus))
  }
  get _ReturnToMakerAction() {
    return this.ReturnToMakerAction && (['CHECKING', 'ReturnToChecker', 'MLCUModification', 'REOPEN'].includes(this.ReportStatus))

  }
  get _SendToCheckerAction() {
    return this.SendToCheckerAction && (['MAKING', 'ReturnToMaker', 'MLCUModification', 'REOPEN'].includes(this.ReportStatus))
  }
  get _SendToAMLHEADAction() {
    return this.SendToAmlHeadAction && (['CHECKING', 'ReturnToChecker'].includes(this.ReportStatus))
  }
  get _reOpenReportAction() {
    return this.REOPENReportAction && (['CLOSED', 'SUBMITTED'].includes(this.ReportStatus))
  }

  Submit() {
    if (!confirm("Submit Report")) return

    let reportid = this.reportDetailControl.get('id').value

    this.reportservice.submitReport(reportid).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Report is Submitted Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });

  }
  get _MLCUAcknowledgeAction() {
    return this.MLCUAcknowledgeAction && (['XMLGenerated']).includes(this.ReportStatus)
  }
  get _SubmitAction() {
    return this.SubmitAction && (['XMLGenerated']).includes(this.ReportStatus)

  }

  isReportValid(): boolean {
    let parties = this.reportDetailControl.get('activity.reportParties.reportParty') as FormArray
    return parties && parties.length > 0 && this.reportDetailControl.valid

  }
  closeReport() {

    if (!this.isReportValid()) {
      this.toaster.warning('The report is not Valid');
      return;
    }
    if (!confirm("Close Report")) return
    let report = Object.assign({}, this.reportDetailControl.getRawValue());
    this.reportservice.closeReport(report).subscribe((data: any) => {

      if (data) {
        this.toaster.success('Report is Closed Successfully');
        this.reportDetailControl.get('reportStatusCode').setValue(data.reportStatusCode);
        this.ReportStatus = data.reportStatusCode;
        this.closeReportPageAndConfirmClose();

      }
      else {
        this.toaster.warning('An error has occured');
      }

    },
      error => {
        

        this.toaster.error(error.error.message);


      });

  }
  get _closeReportAction() {
    return this.closeReportAction && (['WaitingHeadDecision']).includes(this.ReportStatus)
  }
  get _ReturnToMakerActionManager() {
    return this.ReturnToMakerFromManagerAction && (['WaitingHeadDecision','REOPEN'].includes(this.ReportStatus))

  }
  get _generateXMLAction() {
    return this.generateXMLAction && (['WaitingHeadDecision'].includes(this.ReportStatus))

  }
  setConfirmCloseReport(confirm: boolean) {
    this.confirmCloseReport = confirm

  }
  closeReportPageAndConfirmClose() {
    this.setConfirmCloseReport(true)
    this.closeReportPage()
  }
  makeAIfRequest() {
    let StrReport = Object.assign({}, this.reportDetailControl.getRawValue())
    this.reportservice.makeAIFRequest(StrReport).subscribe((report: any) => {
      if (report) {
        let AifReportId = report.id
        this.setConfirmCloseReport(true)
        this.router.navigate([environment.aifEditRoute, AifReportId])
      }
      else {
        this.toaster.error("No Returned Data")
      }
    }, error => this.errorFunction(error))
  }
  get _AIFRequestAction() {
    return this.AIFRequestAction && (['XMLGenerated']).includes(this.ReportStatus)
  }
  errorFunction(error) {

    this.toaster.error(error.error.message)
  }
   get _ButtonAction(){
    return this._AIFRequestAction||this._MLCUAcknowledgeAction||
     this._ReturnToCheckerAction||this._ReturnToMakerAction
     ||this._ReturnToMakerActionManager||this._SendToAMLHEADAction||this._SendToCheckerAction
     ||this._SubmitAction||this._closeReportAction||this._generateXMLAction||this._reOpenReportAction
       
   }
   get _SendToAMLHEADActionFromMaker(){
     return this.sendToManagerFromMakerAction&& (['MAKING', 'ReturnToMaker'].includes(this.ReportStatus))
   }
}


